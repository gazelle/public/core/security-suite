package net.ihe.gazelle.atna.action;

import java.security.Provider;
import java.security.Security;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;

import net.ihe.gazelle.pki.bouncycastle.CertificateBC;
import net.ihe.gazelle.pki.model.PKiProvider;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Startup
@Scope(ScopeType.APPLICATION)
@Name("registerBouncyCastle")
@GenerateInterface("RegisterBouncyCastleLocal")
public class RegisterBouncyCastle implements RegisterBouncyCastleLocal {
    PKiProvider pkiProvider = new CertificateBC();

    private String providerName;
    private static Logger log = LoggerFactory.getLogger(RegisterBouncyCastle.class);

    @Create
    public void registerBC() {
        try {
            Provider provider = pkiProvider.createProvider();
            providerName = provider.getName();
            Security.addProvider(provider);
            log.info("Bouncycastle provider added : '" + providerName + "'");
        } catch(Exception e){
            log.error("Unable to load Bouncycastle provider", e);
        }
    }

    @Destroy
    public void removeBC() {
        Security.removeProvider(providerName);
        log.info("Bouncycastle provider removed : '" + providerName + "'");
    }
}
