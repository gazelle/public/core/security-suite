package net.ihe.gazelle.atna;

import java.util.Map;

import net.ihe.gazelle.common.servletfilter.CSPPoliciesPreferences;

import org.kohsuke.MetaInfServices;

@MetaInfServices(CSPPoliciesPreferences.class)
public class GazelleAtnaCSPPoliciesPreferences implements CSPPoliciesPreferences {

    @Override
    public Map<String, String> getHttpSecurityPolicies() {
        return null;
    }

    @Override
    public boolean getSqlInjectionFilterSwitch() {
        return false;
    }

    @Override
    public boolean isContentPolicyActivated() {
        return false;
    }

}
