package net.ihe.gazelle.atna.pki;

import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.atna.pki.request.RequestWithSubjectCreator;
import net.ihe.gazelle.atna.questionnaire.preferences.UserAttributes;
import net.ihe.gazelle.pki.CertificateManager;
import net.ihe.gazelle.pki.CertificateUtil;
import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.enums.CertificateVersion;
import net.ihe.gazelle.pki.model.CertificateRequestAuthority;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.security.cert.CertificateException;

/**
 * Created by Cedric EOCHE-DUVAL, KEREVAL on 12/07/16.
 */
@Name("certificateAuthorityCreator")
@Scope(ScopeType.PAGE)
@Synchronized(timeout = CertificateAuthorityCreator.SYNCHRONIZED_TIMEOUT)
public class CertificateAuthorityCreator extends RequestWithSubjectCreator implements Serializable {

    static final long SYNCHRONIZED_TIMEOUT = 2000;

    private static final long serialVersionUID = 1196057708561190358L;
    private static Logger log = LoggerFactory.getLogger(CertificateAuthorityCreator.class);

    @In
    EntityManager entityManager;

    public String createCA() {

        if (StringUtils.trimToNull(getOrganization()) == null) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Organization MUST be provided");
            return null;
        }
        if (StringUtils.trimToNull(getCommonName()) == null) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Common name MUST be provided");
            return null;
        }

        CertificateRequestAuthority cra;
        try {
            cra = new CertificateRequestAuthority(DEFAULT_KEY_ALGORITHM, getKeySize().getSize());
        } catch (CertificateException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to generate keys: " + e.getMessage());
            return null;
        }
        cra.setCertificateExtension(CertificateType.CA_KEY_USAGE_ALL);
        cra.setCertificateVersion(CertificateVersion.V3);
        cra.setNotAfter(CertificateUtil.getNotAfter());
        cra.setNotBefore(CertificateUtil.getNotBefore());
        cra.setRequester(Identity.instance().getCredentials().getUsername(), UserAttributes.instance().getCasKeyword());

        cra.setSubjectUsingAttributes(getCountry(), getOrganization(), getCommonName(), getTitle(), getGivenName(),
                getSurname(), getOrganizationalUnit(), geteMail());
        cra.setIssuer(cra.getSubject());
        try {
            cra.setCertificate(CertificateManager.createCertificateAuthority(cra, entityManager));
        } catch (CertificateException e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    "Failed to create CA (" + e.getMessage() + ")");
            log.error("Failed to create CA", e);
            return null;
        }
        entityManager.merge(cra);

        setCertificateRequest(cra);
        FacesMessages.instance().add(StatusMessage.Severity.INFO,
                "Certificate authority request added and signed (subject :" + getCertificateRequest().getSubject() + ")");

        return Pages.PKI_LIST_REQUESTS.getMenuLink();
    }

}
