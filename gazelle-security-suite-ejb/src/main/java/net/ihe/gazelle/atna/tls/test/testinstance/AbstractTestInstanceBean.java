package net.ihe.gazelle.atna.tls.test.testinstance;

import net.ihe.gazelle.atna.action.ApplicationManagerBean;
import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.proxy.admin.gui.ApplicationConfigurationManager;
import net.ihe.gazelle.simulators.tls.enums.TestVerdictType;
import net.ihe.gazelle.simulators.tls.test.model.TlsTestInstance;

public abstract class AbstractTestInstanceBean {

    public String getPermanentLink(TlsTestInstance testInstance) {
        String link = "";
        if (testInstance != null) {
            link = ApplicationConfigurationManager.instance().getApplicationUrl()
                    + Pages.TLS_VIEW_TEST_INSTANCE.getMenuLink() + "?id=" + testInstance.getId();
        }
        return link;
    }

    public String getConnectionPermanentLink(TlsTestInstance testInstance) {
        String link = "";
        if (testInstance != null) {
            if (testInstance.getConnection() != null) {
                link = ApplicationManagerBean.getUrl() + Pages.TLS_VIEW_CONNECTION.getMenuLink() +
                        "?id=" + testInstance.getConnection().getId();
            }
        }
        return link;
    }

    public boolean isConnectionRecorded(TlsTestInstance testInstance) {
        if (testInstance != null && testInstance.getConnection() != null) {
            return true;
        } else {
            return false;
        }
    }

    public String getHandshakeResult(TlsTestInstance testInstance) {
        if (isConnectionRecorded(testInstance)) {
            if (testInstance.getConnection().isSuccess()) {
                return "Success";
            } else if (testInstance.getConnection().getExceptionMessage() == null) {
                String[] lines = testInstance.getConnection().getExceptionStackTrace().split(System.getProperty("line.separator"));
                return lines[0];
            } else if (testInstance.getConnection().getExceptionMessage().contains("Received") ||
                    testInstance.getConnection().getExceptionMessage().contains("No TLS authentication")) {
                return "Failure";
            } else {
                return "Failure without close_notify";
            }
        } else {
            return "No result";
        }
    }

    public String getAlertResult(TlsTestInstance testInstance) {
        if (isConnectionRecorded(testInstance)) {
            if (testInstance.getConnection().getExceptionMessage() != null) {
                return testInstance.getConnection().getExceptionMessage();
            } else {
                return "No alert";
            }
        } else {
            return "No result";
        }
    }

    public String getVerdictIcon(TestVerdictType verdict) {
        if (verdict != null) {
            switch (verdict) {
                case FAILED:
                    return "fa fa-times text-danger";
                case NOT_RUN:
                    return "fa fa-times";
                case PASSED:
                    return "fa fa-check text-success";
                default:
                    return "";
            }
        } else {
            return "";
        }
    }

}
