package net.ihe.gazelle.atna.action;

import net.ihe.gazelle.atna.menu.AtnaMenu;
import net.ihe.gazelle.atna.questionnaire.preferences.ApplicationAttributes;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.common.pages.menu.Menu;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.io.Serializable;

@Name("applicationManagerBean")
@AutoCreate
@Scope(ScopeType.APPLICATION)
@GenerateInterface("ApplicationManagerLocal")
public class ApplicationManagerBean implements Serializable, ApplicationManagerLocal {

    private static final long serialVersionUID = -2588986432369205223L;
    private static String urlBaseResource = null ;

    public static String getTitle() {
        return "Gazelle Security Suite";
    }

    public Menu getMainMenu() {
        return AtnaMenu.getMainMenu();
    }

    public Menu getSideMenu() {
        return AtnaMenu.getSideMenu();
    }

    public static String getUrl() {
        return ApplicationAttributes.instance().getApplicationUrl();
    }

    public static String getUrlBaseResource() {
        if(urlBaseResource == null) {
            String[] split = StringUtils.split(getUrl(), '/');
            if (split.length > 0) {
                urlBaseResource = split[split.length - 1];
            } else {
                urlBaseResource = "";
            }
        }
        return urlBaseResource;
    }

}
