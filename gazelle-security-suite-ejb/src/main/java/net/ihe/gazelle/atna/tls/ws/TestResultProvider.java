package net.ihe.gazelle.atna.tls.ws;

import net.ihe.gazelle.simulators.tls.model.TlsConnection;
import net.ihe.gazelle.simulators.tls.model.TlsConnectionQuery;
import net.ihe.gazelle.simulators.tls.test.model.TlsTestInstance;
import net.ihe.gazelle.simulators.tls.test.model.TlsTestInstanceQuery;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Stateless
@Name("testResultProvider")
public class TestResultProvider implements TestResultProviderApi {

    private static Logger log = LoggerFactory.getLogger(TestResultProvider.class);
    private static final String PASSED = "PASSED";
    private static final String FAILED = "FAILED";

    @Override
    public Response getConnectionTestResult(String id) {
        if (id != null && StringUtils.isNumeric(id)) {
            Integer idInteger = Integer.decode(id);
            try {
                TlsConnectionQuery query = new TlsConnectionQuery();
                query.id().eq(idInteger);
                TlsConnection connection = query.getUniqueResult();
                if (connection != null) {
                    String status = (connection.isSuccess()) ? PASSED : FAILED;
                    return Response.ok(status).build();
                } else {
                    return Response.status(Status.NOT_FOUND).header("Warning", "No TlsConnection found with id " + idInteger)
                            .build();
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                return Response.status(Status.INTERNAL_SERVER_ERROR).header("Warning", e.getMessage()).build();
            }
        } else {
            return Response.status(Status.BAD_REQUEST).header("Warning", "id shall be a valid positive integer")
                    .build();
        }
    }

    @Override
    public Response getTestInstanceResult(String id) {
        if (id != null && StringUtils.isNumeric(id)) {
            Integer idInteger = Integer.decode(id);
            try {
                TlsTestInstanceQuery query = new TlsTestInstanceQuery();
                query.id().eq(idInteger);
                TlsTestInstance instance = query.getUniqueResult();
                if (instance != null) {
                    String status = instance.getTestVerdict().name();
                    return Response.ok(status).build();
                } else {
                    return Response.status(Status.NOT_FOUND).header("Warning", "No TLS test instance found with id " + idInteger)
                            .build();
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                return Response.status(Status.INTERNAL_SERVER_ERROR).header("Warning", e.getMessage()).build();
            }
        } else {
            return Response.status(Status.BAD_REQUEST).header("Warning", "id shall be a valid positive integer")
                    .build();
        }
    }
}
