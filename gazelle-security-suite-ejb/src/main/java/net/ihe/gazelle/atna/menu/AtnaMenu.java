package net.ihe.gazelle.atna.menu;

import net.ihe.gazelle.atna.questionnaire.menu.ATNAQuestionnairePages;
import net.ihe.gazelle.audit.message.menu.AuditMessagePages;
import net.ihe.gazelle.common.pages.menu.Menu;
import net.ihe.gazelle.common.pages.menu.MenuEntry;
import net.ihe.gazelle.common.pages.menu.MenuGroup;

import java.util.ArrayList;
import java.util.List;

public class AtnaMenu {

    private static MenuGroup mainMenu = null;
    private static MenuGroup sideMenu = null;

    private AtnaMenu() {
        /* Avoid public constructor for utility class : Sonar issue "Hide Utility Class Constructor" */
    }

    public static Menu getMainMenu() {
        if (mainMenu == null) {
            synchronized (AtnaMenu.class) {
                if (mainMenu == null) {
                    mainMenu = computeMainMenu();
                }
            }
        }
        return mainMenu;
    }

    public static Menu getSideMenu() {
        if (sideMenu == null) {
            synchronized (AtnaMenu.class) {
                if (sideMenu == null) {
                    sideMenu = computeSideMenu();
                }
            }
        }
        return sideMenu;
    }

    private static MenuGroup computeMainMenu() {
        ArrayList<Menu> menus = new ArrayList<Menu>();

        List<Menu> pkiMenu = new ArrayList<Menu>();
        pkiMenu.add(new MenuEntry(Pages.PKI_REQUEST_CERTIFICATE));
        pkiMenu.add(new MenuEntry(Pages.PKI_LIST_REQUESTS));
        pkiMenu.add(new MenuEntry(Pages.PKI_LIST_CERTIFICATES));
        pkiMenu.add(new MenuEntry(Pages.PKI_INSTALL_AUTOLOGIN));
        pkiMenu.add(new MenuEntry(Pages.PKI_VALIDATE));
        menus.add(new MenuGroup(Pages.PKI_TOP, pkiMenu));

        List<Menu> tlsMenu = new ArrayList<Menu>();

        List<Menu> tlsSimulatorMenu = new ArrayList<Menu>();
        tlsSimulatorMenu.add(new MenuEntry(Pages.TLS_LIST_CLIENTS));
        tlsSimulatorMenu.add(new MenuEntry(Pages.TLS_LIST_SERVER));
        tlsSimulatorMenu.add(new MenuEntry(Pages.TLS_LIST_CONNECTIONS));
        tlsMenu.add(new MenuGroup(Pages.TLS_SIMULATOR_TOP, tlsSimulatorMenu));

        List<Menu> tlsTestingMenu = new ArrayList<Menu>();
        tlsTestingMenu.add(new MenuEntry(Pages.TLS_LIST_TEST_CASE));
        tlsTestingMenu.add(new MenuEntry(Pages.TLS_LIST_DATA_SET));
        tlsMenu.add(new MenuGroup(Pages.TLS_TESTING_TOP, tlsTestingMenu));
        tlsMenu.add(new MenuEntry(Pages.TLS_LIST_CERTIFICATES));
        menus.add(new MenuGroup(Pages.TLS_TOP, tlsMenu));

        List<Menu> atnaMenu = new ArrayList<Menu>();
        atnaMenu.add(new MenuEntry(AuditMessagePages.AM_LIST));
        atnaMenu.add(new MenuEntry(ATNAQuestionnairePages.QUESTIONNAIRES));
        atnaMenu.add(new MenuEntry(Pages.SYSLOG_LIST));
        menus.add(new MenuGroup(AuditMessagePages.AM_HOME, atnaMenu));

        List<Menu> xuaMenu = new ArrayList<Menu>();
        xuaMenu.add(new MenuEntry(Pages.XUA_CLASS_DOC));
        xuaMenu.add(new MenuEntry(Pages.XUA_CONSTRAINT_DOC));
        xuaMenu.add(new MenuEntry(Pages.XUA_CONSTRAINT_ADMIN));

        List<Menu> xuaTests = new ArrayList<Menu>();
        xuaTests.add(new MenuEntry(Pages.XUA_TEST_CASES));
        xuaTests.add(new MenuEntry(Pages.XUA_TEST_INSTANCES));
        xuaTests.add(new MenuEntry(Pages.XUA_TRANSACTIONS));
        xuaMenu.add(new MenuGroup(Pages.XUA_SERVICE_PROVIDER_TESTS, xuaTests));

        menus.add(new MenuGroup(Pages.XUA_TOP, xuaMenu));

        return new MenuGroup(null, menus);
    }

    private static MenuGroup computeSideMenu() {

        ArrayList<Menu> menus = new ArrayList<Menu>();

        List<Menu> adminMenu = new ArrayList<Menu>();
        adminMenu.add(new MenuEntry(Pages.ADMIN_PREFERENCES));
        adminMenu.add(new MenuEntry(Pages.PKI_CREATE_FROM_CSV));
        adminMenu.add(new MenuEntry(Pages.PKI_CREATE_CA));
        adminMenu.add(new MenuEntry(Pages.PKI_IMPORT_P12));
        adminMenu.add(new MenuEntry(Pages.PKI_IMPORT_PEM));
        adminMenu.add(new MenuEntry(Pages.TLS_CREATE_CERTIFICATE));
        adminMenu.add(new MenuEntry(Pages.TLS_CREATE_CA));
        adminMenu.add(new MenuEntry(Pages.TLS_CREATE_CERTIFICATE_SET));
        adminMenu.add(new MenuEntry(Pages.SYSLOG_ADMIN));
        menus.add(new MenuGroup(Pages.ADMIN_TOP, adminMenu));

        return new MenuGroup(null, menus);
    }
}
