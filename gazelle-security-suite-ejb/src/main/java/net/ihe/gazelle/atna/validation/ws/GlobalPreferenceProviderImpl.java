package net.ihe.gazelle.atna.validation.ws;

import net.ihe.gazelle.audit.message.validator.GlobalPreferenceProvider;
import net.ihe.gazelle.proxy.admin.model.ApplicationConfiguration;

public class GlobalPreferenceProviderImpl implements GlobalPreferenceProvider {

    @Override
    public String provideValueOfVariable(String variable) {
        return ApplicationConfiguration.getValueOfVariable(variable);
    }

}
