package net.ihe.gazelle.atna.syslog;

import net.ihe.gazelle.syslog.App;

/**
 * Created by Cedric EOCHE-DUVAL, KEREVAL on 09/06/16.
 */
class SyslogCollectorThread extends Thread {

    private static final int SLEEPING_TIME = 10000;
    private String args[];

    SyslogCollectorThread(String iniFilePath) {
        args = new String[4];
        args[0] = "-a";
        args[1] = "collector";
        args[2] = "-c";
        args[3] = iniFilePath;
    }

    @Override
    public void run() {

        App.main(args);

        try {
            while (true) {
                Thread.sleep(SLEEPING_TIME);
            }
        } catch (InterruptedException ie) {
            App.shutdown();
        }

    }

}
