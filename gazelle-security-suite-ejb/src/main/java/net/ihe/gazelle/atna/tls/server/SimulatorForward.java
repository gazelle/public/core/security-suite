package net.ihe.gazelle.atna.tls.server;

import net.ihe.gazelle.atna.questionnaire.preferences.ApplicationAttributes;
import net.ihe.gazelle.simulators.tls.model.TlsServer;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Test if forward ports are reachable by TLS Server simulator.
 * Keep forward status in cache while is not invalidated.
 * Created by cel on 24/02/17.
 */
public class SimulatorForward {

    private static final Logger LOG = LoggerFactory.getLogger(SimulatorForward.class);
    private static final int SOCKET_TEST_FORWARD_TIMEOUT = 1000;
    private static final int DEFAULT_CACHE_DURATION = 60;

    private static ConcurrentNavigableMap<Integer, Boolean> treeMapForwardReachable;
    private static DB db;

    private static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private static final Runnable cleaner = new Runnable() {
        public void run() {
            SimulatorForward.reset();
        }
    };

    private SimulatorForward() {
        db = DBMaker.newMemoryDB().closeOnJvmShutdown().make();
        treeMapForwardReachable = db.getTreeMap("forwardReachableMap");
        scheduler.scheduleWithFixedDelay(cleaner, getCacheValidityPeriod(), getCacheValidityPeriod(), TimeUnit.SECONDS);
    }

    synchronized public static boolean isReachable(TlsServer server) {
        if (getTreeMapForwardReachable().get(server.getId()) == null) {
            getTreeMapForwardReachable().put(server.getId(),
                    isForwardReachable(server.getRemoteHost(), server.getRemotePort(), server.getKeyword()));
        }
        return getTreeMapForwardReachable().get(server.getId());
    }


    synchronized public static void reset() {
        getTreeMapForwardReachable().clear();
        LOG.debug("TLS-Forward cache reset");
    }

    synchronized public static void close() {
        if (db != null) {
            db.close();
            db = null;
        }
        scheduler.shutdownNow();
    }

    private static ConcurrentNavigableMap<Integer, Boolean> getTreeMapForwardReachable() {
        if (treeMapForwardReachable == null) {
            new SimulatorForward();
        }
        return treeMapForwardReachable;
    }

    private static boolean isForwardReachable(String host, int port, String serverName) {
        if (host != null && port > 0) {
            try (Socket socket = new Socket()) {
                socket.connect(new InetSocketAddress(host, port), SOCKET_TEST_FORWARD_TIMEOUT);
                LOG.debug("Forward {}:{} is reachable for simulator {}", host, port, serverName);
                return true;
            } catch (IOException e) {
                LOG.warn("Unable to reach forward {}:{} for simulator {}", host, port, serverName);
            }
        }
        return false;
    }

    private int getCacheValidityPeriod() {
        try {
            return ApplicationAttributes.getTlsForwardCacheDuration();
        } catch (NumberFormatException nfe) {
            LOG.error("Property {} has not an integer value", ApplicationAttributes.TLS_FORWARD_CACHE_SECONDS_DURATION);
            return DEFAULT_CACHE_DURATION;
        }
    }

}
