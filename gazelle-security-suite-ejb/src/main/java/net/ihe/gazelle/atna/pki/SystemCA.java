package net.ihe.gazelle.atna.pki;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.pki.CertificateDAO;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.preferences.PreferenceService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManager;
import java.io.Serializable;

/**
 * Created by cel on 20/07/15.
 */
@Name("systemCA")
@Scope(ScopeType.PAGE)
public class SystemCA implements Serializable {

    private String subject="" ;

    public SystemCA() {
        super();
    }

    public String getSubject() {

        if(subject.isEmpty()) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();

            Integer certificateId = PreferenceService.getInteger("certificate_authority_Id");
            Certificate certificateAuthority = CertificateDAO.getByID(certificateId, entityManager);

            if (certificateAuthority != null) {
                subject = certificateAuthority.getSubject();
            } else {
                subject = "none";
            }
        }
        return subject ;
    }

}
