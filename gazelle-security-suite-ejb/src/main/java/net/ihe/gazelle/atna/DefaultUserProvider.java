package net.ihe.gazelle.atna;

import java.util.Locale;

import org.jboss.seam.security.Identity;
import org.kohsuke.MetaInfServices;

import net.ihe.gazelle.users.UserProvider;

@MetaInfServices(UserProvider.class)
public class DefaultUserProvider implements UserProvider {

    @Override
    public String getUsername() {
        if (Identity.instance().isLoggedIn()) {
            return Identity.instance().getCredentials().getUsername();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasRole(String roleName) {
        return Identity.instance().isLoggedIn() && Identity.instance().hasRole(roleName);
    }

    @Override
    public Locale getLocale() {
        return org.jboss.seam.web.Locale.instance();
    }

}
