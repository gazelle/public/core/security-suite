package net.ihe.gazelle.atna.syslog;

import net.ihe.gazelle.atna.questionnaire.preferences.ApplicationAttributes;
import net.ihe.gazelle.atna.questionnaire.preferences.MapDbCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Cedric EOCHE-DUVAL, KEREVAL on 29/06/16.
 */
final class SyslogConfiguration {

    private static final String CONFIGURATION_FILE_NAME = "syslog.ini";
    private static final String RUN_DIR_CACHE_KEY = "run_directory";

    private static Logger log = LoggerFactory.getLogger(SyslogConfiguration.class);

    private SyslogConfiguration() {
        // It is an utility class, constructor is hidden.
    }

    static String getRunDirectory() throws SyslogConfigurationException {

        ConcurrentNavigableMap<String, String> cache = MapDbCache.getSyslogCache();
        if (!cache.containsKey(RUN_DIR_CACHE_KEY)) {
            String runDir = ApplicationAttributes.getSyslogDirectory();

            if (runDir != null && !runDir.isEmpty()) {
                if (Files.exists(Paths.get(runDir))) {

                    verifyFileExistence(Paths.get(runDir + File.separator + CONFIGURATION_FILE_NAME));
                    cache.put(RUN_DIR_CACHE_KEY, runDir);

                } else {
                    ApplicationAttributes.instance().resetApplicationConfiguration();
                    throw new SyslogConfigurationException(
                            "The directory defined in application preference 'syslog_directory' does not exist");
                }
            } else {
                throw new SyslogConfigurationException("The preference 'syslog_directory' is not defined");
            }
        }
        return cache.get(RUN_DIR_CACHE_KEY);
    }

    static Path getConfigFilePath() throws SyslogConfigurationException {
        return Paths.get(getRunDirectory() + File.separator + CONFIGURATION_FILE_NAME);
    }

    private static void verifyFileExistence(Path filePath) throws SyslogConfigurationException {
        if (!Files.exists(filePath) || !Files.isReadable(filePath)) {
            throw new SyslogConfigurationException(
                    "Cannot access Syslog file '" + filePath.toString() + "'");
        }
    }

    enum Properties {

        UDPPorts("udp_ports"),
        TCPPorts("tcp_ports"),
        TLSPorts("tls_ports");

        private static final int COMMENTED_PATTERN_GROUP = 1;
        private static final int NAME_PATTERN_GROUP = 2;
        private static final int VALUE_PATTERN_GROUP = 3;

        private String linePattern;
        private String name;

        Properties(String name) {
            this.name = name;
            this.linePattern = "^([#;]?)(" + Pattern.quote(name) + "\\s?=)(.*)$";
        }

        String getName() {
            return name;
        }

        /**
         * get the value of the property.
         *
         * @return the String value fo the property as it is written in the SyslogCollector.ini file or null if
         * the property is not defined or commented out.
         * @throws SyslogConfigurationException if the Syslog Run Directory is wrongly set up
         * @throws IOException                  if there is a reading error of SyslogCollector.ini file.
         */
        String getValue() throws SyslogConfigurationException, IOException {

            ConcurrentNavigableMap<String, String> cache = MapDbCache.getSyslogCache();
            if (!cache.containsKey(getName())) {
                String value = getValueFromConfigFile();
                if (value != null) {
                    cache.put(getName(), value);
                }
            }
            return cache.get(getName());

        }

        /**
         * Set the value of a property
         *
         * @param value new value for this property
         * @throws IOException                  in case of reading or writing error with SyslogCollector.ini
         * @throws SyslogConfigurationException if the Syslog Run Directory is wrongly set up
         */
        void setValue(String value) throws IOException, SyslogConfigurationException {
            String configString = getConfigFileAsString();

            Pattern propertyPattern = Pattern.compile(this.linePattern, Pattern.MULTILINE);
            Matcher propertyMatcher = propertyPattern.matcher(configString);

            if (propertyMatcher.find()) {
                configString = propertyMatcher.replaceFirst("$" + NAME_PATTERN_GROUP + value);
            } else {
                configString = configString + "\n" + getName() + "=" + value + "\n";
            }

            try {
                Files.write(getConfigFilePath(), configString.getBytes(StandardCharsets.UTF_8));
            } catch (IOException ioe) {
                throw new IOException(
                        "Unable to write to " + getConfigFilePath().toString() + " : " + ioe.getMessage(), ioe);
            }
            MapDbCache.getSyslogCache().clear();

        }

        private String getValueFromConfigFile() throws SyslogConfigurationException, IOException {

            String configString = getConfigFileAsString();

            Pattern propertyPattern = Pattern.compile(this.linePattern, Pattern.MULTILINE);
            Matcher propertyMatcher = propertyPattern.matcher(configString);
            if (propertyMatcher.find()) {
                String commentGroup = propertyMatcher.group(COMMENTED_PATTERN_GROUP);
                if (commentGroup.isEmpty()) {
                    log.debug("property '{}' value: {}", this.getName(), propertyMatcher.group());
                    return propertyMatcher.group(VALUE_PATTERN_GROUP);
                } else {
                    //it means commented out property
                    log.debug("property '{}' commented out", this.getName());
                    return null;
                }
            } else {
                //it means unexisting property
                log.debug("property '{}' undefined", this.getName());
                return null;
            }
        }

        private String getConfigFileAsString() throws SyslogConfigurationException, IOException {
            Path configFilePath = getConfigFilePath();
            String configString = null;

            try {
                configString = new String(Files.readAllBytes(configFilePath), StandardCharsets.UTF_8);
            } catch (IOException ioe) {
                throw new IOException(
                        "Unable to read Syslog configuration file '" + configFilePath.toString() + "' : " + ioe
                                .getMessage(), ioe);
            }
            return configString;
        }

    }

}
