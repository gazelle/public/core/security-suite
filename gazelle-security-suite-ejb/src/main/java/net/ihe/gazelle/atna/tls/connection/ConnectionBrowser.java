package net.ihe.gazelle.atna.tls.connection;

import javax.faces.context.FacesContext;

import net.ihe.gazelle.atna.action.ApplicationManagerBean;
import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.simulators.tls.model.TlsConnection;

public abstract class ConnectionBrowser {

    private Filter<TlsConnection> filter;
    private FilterDataModel<TlsConnection> connections;

    public ConnectionBrowser() {
        super();
    }

    public Filter<TlsConnection> getFilter() {
        if (filter == null) {
            filter = new Filter<TlsConnection>(getHQLCriterionsForFilter(),
                    FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap());
        }
        return filter;
    }

    public abstract HQLCriterionsForFilter<TlsConnection> getHQLCriterionsForFilter();

    public FilterDataModel<TlsConnection> getConnections() {
        if (connections == null) {
            connections = new FilterDataModel<TlsConnection>(getFilter()) {
                @Override
                protected Object getId(TlsConnection t) {
                    return t.getId();
                }
            };
        }
        return connections;
    }

    public void reset() {
        getFilter().clear();
        getConnections().resetCache();
    }

    public void reload() {
        getConnections().resetCache();
    }

    public String getPermanentLink(int connectionId) {
        return ApplicationManagerBean.getUrl() + Pages.TLS_VIEW_CONNECTION.getMenuLink() + "?id=" + connectionId;
    }

}