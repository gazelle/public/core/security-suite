package net.ihe.gazelle.atna.action.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import net.ihe.gazelle.pki.enums.KeySize;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;

@Name("keySizeConverter")
@BypassInterceptors
@Converter(forClass = KeySize.class)
public class KeySizeConverter implements javax.faces.convert.Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        KeySize[] values = KeySize.values();
        for (KeySize keySize : values) {
            if (keySize.toString().equals(value)) {
                return keySize;
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return value.toString();
    }

}
