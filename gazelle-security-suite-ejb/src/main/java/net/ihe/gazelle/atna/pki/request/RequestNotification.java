package net.ihe.gazelle.atna.pki.request;

import net.ihe.gazelle.atna.communication.Notification;
import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.atna.questionnaire.preferences.UserAttributes;
import net.ihe.gazelle.pki.model.CertificateRequest;
import net.ihe.gazelle.preferences.PreferenceService;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

/**
 * Created by Cedric EOCHE-DUVAL, KEREVAL on 12/07/16.
 */
public final class RequestNotification {

    private RequestNotification() {
        // Used to hide constructor of utility class
    }

    public static void requested(CertificateRequest request) {
        String message = "This user has requested a certificate with the PKI";
        String recipients = UserAttributes.instance().getTMPkiAdmins();
        Notification.sendMessage(message, recipients, PreferenceService.getString("application_url")
                + Pages.PKI_LIST_REQUESTS.getMenuLink() + "?requester=" + request.getRequesterUsername());

        FacesMessages.instance().add(StatusMessage.Severity.INFO,
                "You will be notified as soon as an admin validates your request");
    }

    public static void signed(CertificateRequest request) {
        String message = "An admin signed your certificate request.";
        String recipients = request.getRequesterUsername();
        Notification.sendMessage(message, recipients, PreferenceService.getString("application_url")
                + Pages.PKI_VIEW_CERTIFICATE.getMenuLink() + "?id=" + request.getCertificate().getId());
    }

    public static void regenerated(CertificateRequest request) {
        String message = "An admin updated your certificate request.";
        String recipients = request.getRequesterUsername();
        Notification.sendMessage(message, recipients, PreferenceService.getString("application_url")
                + Pages.PKI_VIEW_CERTIFICATE.getMenuLink() + "?id=" + request.getCertificate().getId());
    }

}
