package net.ihe.gazelle.atna.action.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import net.ihe.gazelle.pki.enums.CertificateType;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;

@Name("certificateTypeConverter")
@BypassInterceptors
@Converter(forClass = CertificateType.class)
public class CertificateTypeConverter implements javax.faces.convert.Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        CertificateType[] values = CertificateType.values();
        for (CertificateType certificateType : values) {
            if (certificateType.toString().equals(value)) {
                return certificateType;
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return value.toString();
    }

}
