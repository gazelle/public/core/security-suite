package net.ihe.gazelle.atna.syslog;

import net.ihe.gazelle.preferences.PreferenceService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 * Created by Cedric EOCHE-DUVAL, KEREVAL on 07/06/16.
 */
@Name("syslogCollectorServerLauncher")
@Startup(depends = {"entityManager"})
@Scope(ScopeType.APPLICATION)
public class SyslogCollectorServerLauncher {

    private static final int TERMINATED_LOOP_WAITING_TIME = 20;

    private static Logger log = LoggerFactory.getLogger(SyslogCollectorServerLauncher.class);
    private SyslogCollectorThread syslogCollectorThread;

    @Create
    @Transactional
    public void automaticStartUp() {
        if (PreferenceService.getBoolean("syslog_collector_enabled") &&
                PreferenceService.getBoolean("syslog_automatic_start_enabled")) {
            start();
        }
    }

    @Restrict("#{s:hasRole('admin_role')}")
    public void start() {

        if (PreferenceService.getBoolean("syslog_collector_enabled")) {
            if (syslogCollectorThread == null) {

                try {

                    syslogCollectorThread = new SyslogCollectorThread(
                            SyslogConfiguration.getConfigFilePath().toString());
                    syslogCollectorThread.start();

                    String message = "Syslog Collector started";
                    FacesMessages.instance().add(StatusMessage.Severity.INFO, message);
                    log.info(message);
                } catch (SyslogConfigurationException sce) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, sce.getMessage());
                    log.error(sce.getMessage());
                }
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.INFO, "Syslog Collector already started");
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.WARN,
                    "Syslog Collector is disabled, it cannot be started");
        }

    }

    @Restrict("#{s:hasRole('admin_role')}")
    public void stop() { effectiveStop();
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Syslog Collector stopped");
    }

    @Restrict("#{s:hasRole('admin_role')}")
    public void restart() {
        effectiveStop();
        start();
    }

    @Destroy
    public void effectiveStop() {
        if (syslogCollectorThread != null) {
            syslogCollectorThread.interrupt();

            //wait for the Syslog thread to stop itself (time to close sockets, etc.).
            while (!syslogCollectorThread.getState().equals(Thread.State.TERMINATED)) {
                try {
                    TimeUnit.MILLISECONDS.sleep(TERMINATED_LOOP_WAITING_TIME);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }

            syslogCollectorThread = null;
            log.info("Syslog Collector stopped");
        }
    }

    public String getThreadState() {
        if (syslogCollectorThread != null) {
            return syslogCollectorThread.getState().toString();
        } else {
            return "Thread does not exist";
        }
    }

    public boolean isRunning() {
        return syslogCollectorThread != null;
    }
}
