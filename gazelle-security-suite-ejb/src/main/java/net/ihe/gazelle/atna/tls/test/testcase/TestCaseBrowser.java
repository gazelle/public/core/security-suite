package net.ihe.gazelle.atna.tls.test.testcase;

import javax.faces.context.FacesContext;

import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.simulators.tls.test.model.TlsTestCase;

public abstract class TestCaseBrowser {

    private Filter<TlsTestCase> filter;
    private FilterDataModel<TlsTestCase> testCases;
    private TlsTestCase pendingTestCase;

    public TestCaseBrowser() {
        super();
    }

    public Filter<TlsTestCase> getFilter() {
        if (filter == null) {
            filter = new Filter<TlsTestCase>(getHQLCriterionsForFilter(),
                    FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap());
        }
        return filter;
    }

    public abstract HQLCriterionsForFilter<TlsTestCase> getHQLCriterionsForFilter();

    public FilterDataModel<TlsTestCase> getTestCases() {
        if (testCases == null) {
            testCases = new FilterDataModel<TlsTestCase>((Filter<TlsTestCase>) getFilter()) {
                @Override
                protected Object getId(TlsTestCase t) {
                    // TODO Auto-generated method stub
                    return t.getId();
                }
            };
        }
        return testCases;
    }

    public TlsTestCase getPendingTestCase() {
        return pendingTestCase;
    }

    public void setPendingTestCase(TlsTestCase pendingInstance) {
        this.pendingTestCase = pendingInstance;
    }

    public void reset() {
        getFilter().clear();
        getTestCases().resetCache();
    }

    public String getPermanentLink(TlsTestCase testCase) {
        return Pages.TLS_VIEW_TEST_CASE.getMenuLink() + "?id=" + testCase.getId();
    }

}