package net.ihe.gazelle.atna.pki.request;

import net.ihe.gazelle.atna.exceptions.PreferenceException;
import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.pki.CertificateDAO;
import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.enums.KeyAlgorithm;
import net.ihe.gazelle.pki.enums.KeySize;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.pki.model.CertificateRequest;
import net.ihe.gazelle.preferences.PreferenceService;

import java.io.Serializable;

/**
 * Created by Cedric EOCHE-DUVAL, KEREVAL on 12/07/16.
 */
public abstract class AbstractRequestCreator implements Serializable {

    protected static final KeyAlgorithm DEFAULT_KEY_ALGORITHM = KeyAlgorithm.RSA;
    protected static final KeySize DEFAULT_KEY_SIZE = KeySize._1024;

    private CertificateRequest certificateRequest;

    private Integer appCertificateAuthorityId;
    private Certificate appCertificateAuthority;

    protected CertificateRequest getCertificateRequest() {
        return certificateRequest;
    }

    protected void setCertificateRequest(CertificateRequest certificateRequest) {
        this.certificateRequest = certificateRequest;
    }

    public CertificateType getCertificateExtension() {
        return certificateRequest.getCertificateExtension();
    }

    public void setCertificateExtension(CertificateType certificateExtension) {
        certificateRequest.setCertificateExtension(certificateExtension);
    }

    public String getRequestWithoutCSRLink() {
        return PreferenceService.getString("application_url") + Pages.PKI_REQUEST_CERTIFICATE.getMenuLink();
    }

    public String getSubmitCSRLink() {
        return PreferenceService.getString("application_url") + Pages.PKI_SUBMIT_CSR.getMenuLink();
    }

    public boolean isAutomaticSigningEnabled() {
        return PreferenceService.getBoolean("pki_automatic_request_signing");
    }

    protected Integer getAppCertificateAuthorityId() throws PreferenceException {
        if (appCertificateAuthorityId == null) {
            appCertificateAuthorityId = PreferenceService.getInteger("certificate_authority_Id");
            if (appCertificateAuthorityId == null) {
                throw new PreferenceException(
                        "Preference 'certificate_authority_Id' is not defined. Please contact an administrator");
            }
        }
        return appCertificateAuthorityId;
    }

    protected Certificate getAppCertificateAuthority() throws PreferenceException {
        if (appCertificateAuthority == null && getAppCertificateAuthorityId() != null) {
            appCertificateAuthority = CertificateDAO
                    .getByID(getAppCertificateAuthorityId(), EntityManagerService.provideEntityManager());
            if (appCertificateAuthority == null) {
                throw new PreferenceException(
                        "Unable to get application certificate authority for id '" + getAppCertificateAuthorityId() +
                                "'. Please contact an administrator.");
            }
        }
        return appCertificateAuthority;
    }
}
