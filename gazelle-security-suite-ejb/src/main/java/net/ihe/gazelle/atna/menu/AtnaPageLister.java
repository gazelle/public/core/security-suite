package net.ihe.gazelle.atna.menu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import net.ihe.gazelle.atna.questionnaire.menu.ATNAQuestionnairePages;
import net.ihe.gazelle.audit.message.menu.AuditMessagePages;
import net.ihe.gazelle.common.pages.Page;
import net.ihe.gazelle.common.pages.PageLister;

import org.kohsuke.MetaInfServices;

@MetaInfServices(PageLister.class)
public class AtnaPageLister implements PageLister {

    @Override
    public Collection<Page> getPages() {
        Collection<Page> pages = new ArrayList<Page>();
        pages.addAll(Arrays.asList(AuditMessagePages.values()));
        pages.addAll(Arrays.asList(ATNAQuestionnairePages.values()));
        pages.addAll(Arrays.asList(Pages.values()));
        return pages;
    }
}
