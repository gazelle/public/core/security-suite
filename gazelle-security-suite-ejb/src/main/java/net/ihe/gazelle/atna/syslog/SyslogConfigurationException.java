package net.ihe.gazelle.atna.syslog;

/**
 * Created by Cedric EOCHE-DUVAL, KEREVAL on 29/06/16.
 */
public class SyslogConfigurationException extends Exception {

    private static final long serialVersionUID = -8944439801485004728L;

    SyslogConfigurationException(String message) {
        super(message);
    }
}
