package net.ihe.gazelle.atna;

import net.ihe.gazelle.hql.providers.detached.AbstractEntityManagerProvider;

import org.jboss.seam.contexts.Contexts;

public class GazelleAtnaEntityManagerProvider extends AbstractEntityManagerProvider {

    @Override
    public Integer getWeight() {
        if (Contexts.isApplicationContextActive()) {
            return 100;
        } else {
            return -100;
        }
    }

    @Override
    public String getHibernateConfigPath() {
        return "META-INF/hibernate.cfg.xml";
    }

}
