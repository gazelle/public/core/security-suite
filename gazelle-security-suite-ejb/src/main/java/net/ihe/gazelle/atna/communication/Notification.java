package net.ihe.gazelle.atna.communication;

import net.ihe.gazelle.atna.questionnaire.preferences.UserAttributes;
import net.ihe.gazelle.tm.messages.GazelleMessageWSServiceStub;
import net.ihe.gazelle.tm.messages.SendMessage;
import net.ihe.gazelle.tm.messages.SendMessageE;
import org.apache.axis2.Constants;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jboss.seam.security.Identity;

public class Notification {

    private static final Logger log = Logger.getLogger(Notification.class);


    /**
     * Send message to gazelle test management
     */
    public static void sendMessage(String message, String recipients, String link) {
        try {
            String sender = Identity.instance().getPrincipal().getName();

            SendMessageE messageToSend = forgeMessage(message, recipients, link, sender);

            GazelleMessageWSServiceStub gazelleMessageService = new GazelleMessageWSServiceStub(
                    UserAttributes.instance().getTMMessageWs());
            gazelleMessageService._getServiceClient().getOptions().setProperty(
                    Constants.Configuration.DISABLE_SOAP_ACTION, true);
            gazelleMessageService.sendMessage(messageToSend);
        } catch (Exception e) {
            log.error("Failed to send message", e);
        }
    }

    private static SendMessageE forgeMessage(String message, String recipients, String link, String sender) {
        SendMessageE sendMessage0 = new SendMessageE();
        SendMessage param = new SendMessage();
        param.setMessage(message);
        param.setLink(link);
        param.setSendUsername(sender);
        param.setUsernames(StringUtils.split(recipients, ","));
        sendMessage0.setSendMessage(param);
        return sendMessage0;
    }

}
