package net.ihe.gazelle.atna.tls.test.testinstance;

import javax.faces.context.FacesContext;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.simulators.tls.test.model.TlsTestInstance;

public abstract class TestInstanceBrowser extends AbstractTestInstanceBean {

    private Filter<TlsTestInstance> filter;
    private FilterDataModel<TlsTestInstance> instances;
    private TlsTestInstance pendingInstance;

    public TestInstanceBrowser() {
        super();
    }

    public Filter<TlsTestInstance> getFilter() {
        if (filter == null) {
            filter = new Filter<TlsTestInstance>(getHQLCriterionsForFilter(),
                    FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap());
        }
        return filter;
    }

    public abstract HQLCriterionsForFilter<TlsTestInstance> getHQLCriterionsForFilter();

    public FilterDataModel<TlsTestInstance> getInstances() {
        if (instances == null) {
            instances = new FilterDataModel<TlsTestInstance>((Filter<TlsTestInstance>) getFilter()) {
                @Override
                protected Object getId(TlsTestInstance t) {
                    return t.getId();
                }
            };
        }
        return instances;
    }

    public TlsTestInstance getPendingInstance() {
        return pendingInstance;
    }

    public void setPendingInstance(TlsTestInstance pendingInstance) {
        this.pendingInstance = pendingInstance;
    }

    public void reset() {
        getFilter().clear();
        getInstances().resetCache();
    }

    public void refresh() {
        getInstances().resetCache();
    }

}