package net.ihe.gazelle.atna.pki;

import javax.faces.context.FacesContext;

import net.ihe.gazelle.common.filter.HibernateDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.pki.CertificateDAO;
import net.ihe.gazelle.pki.model.Certificate;

public class CertificateDataModel extends HibernateDataModel<Certificate> {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private boolean pki;

    public CertificateDataModel(boolean pki) {
        super(Certificate.class);
        this.pki = pki;
    }

    protected void appendFilters(FacesContext context, HQLQueryBuilder<Certificate> queryBuilder) {
        super.appendFilters(context, queryBuilder);
        if (pki) {
            CertificateDAO.criteriaNotForCustomsQueryBuilder(queryBuilder);
        } else {
            CertificateDAO.criteriaForCustomsQueryBuilder(queryBuilder);
        }
    }

    public boolean getPKI() {
        return pki;
    }

    @Override
    protected Object getId(Certificate t) {
        // TODO Auto-generated method stub
        return t.getId();
    }
}
