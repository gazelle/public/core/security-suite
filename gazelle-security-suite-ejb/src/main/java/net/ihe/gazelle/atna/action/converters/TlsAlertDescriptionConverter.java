package net.ihe.gazelle.atna.action.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import net.ihe.gazelle.simulators.tls.enums.TlsAlertDescription;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;

@Name("alertDescriptionConverter")
@BypassInterceptors
@Converter(forClass = TlsAlertDescription.class)
public class TlsAlertDescriptionConverter implements javax.faces.convert.Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        TlsAlertDescription[] values = TlsAlertDescription.values();
        for (TlsAlertDescription alertDescription : values) {
            if (alertDescription.toString().equals(value)) {
                return alertDescription;
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value != null) {
            return value.toString();
        } else {
            return null;
        }
    }
}
