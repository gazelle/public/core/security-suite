package net.ihe.gazelle.atna.action.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulators.tls.test.model.TlsTestCase;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;

@Name("tlsTestCaseConverter")
@BypassInterceptors
@Converter(forClass = TlsTestCase.class)
public class TlsTestCaseConverter implements javax.faces.convert.Converter {


    @Override
    @Transactional
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        EntityManager em = EntityManagerService.provideEntityManager();
        String[] parts = value.split(", ");
        return em.find(TlsTestCase.class, Integer.parseInt(parts[0]));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return String.valueOf(((TlsTestCase) value).getId()) + ", " + ((TlsTestCase) value).getName();
    }

}
