package net.ihe.gazelle.atna.syslog;

import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.syslog.model.SyslogCollectorMessage;
import net.ihe.gazelle.syslog.model.SyslogCollectorMessageQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.faces.context.FacesContext;
import java.io.Serializable;

/**
 * Created by Cedric EOCHE-DUVAL, KEREVAL on 26/07/16.
 */
@Name("syslogCollectorBrowser")
@Scope(ScopeType.PAGE)
public class SyslogCollectorBrowser extends SyslogCommonBean implements Serializable {

    private static final long serialVersionUID = 1073279201384244004L;
    private static String syslogMessageViewbaseLink = PreferenceService.getString("application_url") + Pages.SYSLOG_VIEW
            .getMenuLink();

    private Filter<SyslogCollectorMessage> filter;
    private FilterDataModel<SyslogCollectorMessage> syslogMessages;

    public Filter<SyslogCollectorMessage> getFilter() {
        if (filter == null) {
            filter = new Filter<SyslogCollectorMessage>(getHQLCriterionsForFilter(),
                    FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap());
        }
        return filter;
    }

    public HQLCriterionsForFilter<SyslogCollectorMessage> getHQLCriterionsForFilter() {
        SyslogCollectorMessageQuery query = new SyslogCollectorMessageQuery();
        HQLCriterionsForFilter<SyslogCollectorMessage> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("senderIP", query.senderIP());
        criteria.addPath("transportType", query.transportType());
        criteria.addPath("timestamp", query.arrivalTime());
        return criteria;
    }

    public FilterDataModel<SyslogCollectorMessage> getSyslogMessages() {
        if (syslogMessages == null) {
            syslogMessages = new FilterDataModel<SyslogCollectorMessage>(getFilter()) {

                @Override
                protected Object getId(SyslogCollectorMessage t) {
                    return t.getId();
                }
            };
        }
        return syslogMessages;
    }

    public void reset() {
        getFilter().clear();
        getSyslogMessages().resetCache();
    }

    public void refresh() {
        getSyslogMessages().resetCache();
    }

    public String getMessageViewLink(Integer id) {
        return syslogMessageViewbaseLink + "?id=" + id;
    }

}
