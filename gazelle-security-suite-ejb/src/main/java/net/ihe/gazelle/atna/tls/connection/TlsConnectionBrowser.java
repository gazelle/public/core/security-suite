package net.ihe.gazelle.atna.tls.connection;

import java.io.Serializable;

import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.simulators.tls.model.TlsConnection;
import net.ihe.gazelle.simulators.tls.model.TlsConnectionQuery;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("tlsConnectionBrowser")
@Scope(ScopeType.PAGE)
public class TlsConnectionBrowser extends ConnectionBrowser implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8601561104656234695L;

    @Override
    public HQLCriterionsForFilter<TlsConnection> getHQLCriterionsForFilter() {
        TlsConnectionQuery query = new TlsConnectionQuery();
        HQLCriterionsForFilter<TlsConnection> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("simulator", query.simulator());
        criteria.addPath("timestamp", query.date());
        criteria.addPath("remoteHost", query.remoteHost());
        criteria.addPath("success", query.success());
        return criteria;
    }

}
