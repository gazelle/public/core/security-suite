package net.ihe.gazelle.atna.syslog;

import net.ihe.gazelle.atna.action.ApplicationManagerBean;
import net.ihe.gazelle.atna.menu.Pages;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by Cedric EOCHE-DUVAL, KEREVAL on 15/06/16.
 */
@Name("syslogCollectorServer")
@Scope(ScopeType.PAGE)
public class SyslogCollectorServer implements Serializable {

    private static Logger log = LoggerFactory.getLogger(SyslogCollectorServer.class);

    private String tcpPorts;
    private String udpPorts;
    private String tlsPorts;

    @Create
    public void initialize() {
        tcpPorts = getPropertyValue(SyslogConfiguration.Properties.TCPPorts);
        tlsPorts = getPropertyValue(SyslogConfiguration.Properties.TLSPorts);
        udpPorts = getPropertyValue(SyslogConfiguration.Properties.UDPPorts);
    }

    public String getLinkToApplicationPreferences() {
        return "/" + ApplicationManagerBean.getUrlBaseResource() + Pages.ADMIN_PREFERENCES.getMenuLink();
    }

    public String getTCPPorts() {
        return tcpPorts;
    }

    public String getTLSPorts() {
        return tlsPorts;
    }

    public String getUDPPorts() {
        return udpPorts;
    }

    @Restrict("#{s:hasRole('admin_role')}")
    public void setTCPPorts(String value) {
        setPropertyValue(SyslogConfiguration.Properties.TCPPorts, value);
        tcpPorts = value;
    }

    @Restrict("#{s:hasRole('admin_role')}")
    public void setTLSPorts(String value) {
        setPropertyValue(SyslogConfiguration.Properties.TLSPorts, value);
        tlsPorts = value;
    }

    @Restrict("#{s:hasRole('admin_role')}")
    public void setUDPPorts(String value) {
        setPropertyValue(SyslogConfiguration.Properties.UDPPorts, value);
        udpPorts = value;
    }

    private void setPropertyValue(SyslogConfiguration.Properties property, String value) {
        try {
            property.setValue(value);
        } catch (SyslogConfigurationException sce) {
            String msg = "Configuration error in Syslog-Collector directory : " + sce.getMessage();
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg);
            log.error(msg);
        } catch (IOException ioe) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, ioe.getMessage());
            log.error(ioe.getMessage());
        }
        mayRestartSyslog();
    }

    private String getPropertyValue(SyslogConfiguration.Properties property) {
        try {
            return property.getValue();
        } catch (SyslogConfigurationException sce) {
            String msg = "Configuration error in Syslog-Collector directory : " + sce.getMessage();
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg);
            log.error(msg);
        } catch (IOException ioe) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, ioe.getMessage());
            log.error(ioe.getMessage());
        }
        return null;
    }

    private void mayRestartSyslog() {
        SyslogCollectorServerLauncher launcher = (SyslogCollectorServerLauncher) Component.getInstance("syslogCollectorServerLauncher");
        if (launcher.isRunning()) {
            launcher.restart();
        }
    }

}
