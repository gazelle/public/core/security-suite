package net.ihe.gazelle.atna.model;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.validation.model.ValidatorUsageProvider;
import org.jboss.seam.annotations.Name;
import org.kohsuke.MetaInfServices;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * This class is used to be able to report statistics on the usage of the validator embedded in the validator
 *
 * @author Anne-Gaëlle Bergé / IHE Europe
 * @version 1.0 - August 3rd 2012
 */

@Entity
@Name("validatorUsage")
@Table(name = "atna_validator_usage", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "cmn_validator_usage_sequence", sequenceName = "cmn_validator_usage_id_seq", allocationSize = 1)
@MetaInfServices(ValidatorUsageProvider.class)
public class ValidatorUsage implements Serializable, ValidatorUsageProvider {

    private static final long serialVersionUID = -6685024671308605414L;

    @Id
    @GeneratedValue(generator = "cmn_validator_usage_sequence", strategy = GenerationType.SEQUENCE)
    @NotNull
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;

    @Column(name = "status")
    private String status;

    @Column(name = "type")
    private String type;

    @Column(name = "caller")
    private String caller;

    public ValidatorUsage() {

    }

    public ValidatorUsage(Date validationDate, String validationStatus, String validationType, String caller) {
        this.date = validationDate;
        this.status = validationStatus;
        this.caller = caller;
        this.type = validationType;
    }

    public void save() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        entityManager.merge(this);
        entityManager.flush();
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setCaller(String caller) {
        this.caller = caller;
    }

    @Override
    public void newEntry(Date date, String status, String type, String caller) {
        ValidatorUsage usage = new ValidatorUsage(date, status, type, caller);
        usage.save();
    }

}
