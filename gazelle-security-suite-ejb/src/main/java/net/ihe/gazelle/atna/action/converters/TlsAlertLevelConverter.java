package net.ihe.gazelle.atna.action.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import net.ihe.gazelle.simulators.tls.enums.TlsAlertLevel;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;

@Name("alertLevelConverter")
@BypassInterceptors
@Converter(forClass = TlsAlertLevel.class)
public class TlsAlertLevelConverter implements javax.faces.convert.Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        TlsAlertLevel[] values = TlsAlertLevel.values();
        for (TlsAlertLevel alertLevel : values) {
            if (alertLevel.toString().equals(value)) {
                return alertLevel;
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value != null) {
            return value.toString();
        } else {
            return null;
        }
    }
}
