package net.ihe.gazelle.atna.exceptions;

/**
 * Created by Cedric EOCHE-DUVAL, KEREVAL on 12/07/16.
 */
public class PreferenceException extends Exception {

    private static final long serialVersionUID = 6017730990291247267L;

    public PreferenceException() {
        super();
    }

    public PreferenceException(String message) {
        super(message);
    }

    public PreferenceException(String message, Throwable cause) {
        super(message, cause);
    }

    public PreferenceException(Throwable cause) {
        super(cause);
    }

}
