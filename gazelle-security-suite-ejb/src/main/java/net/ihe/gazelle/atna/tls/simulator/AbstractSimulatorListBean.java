package net.ihe.gazelle.atna.tls.simulator;

import net.ihe.gazelle.atna.action.ApplicationManagerBean;
import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulators.tls.model.TlsConnectionQuery;
import net.ihe.gazelle.simulators.tls.model.TlsSimulator;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import java.io.Serializable;
import java.util.Map;

/**
 * Created by cel on 16/09/15.
 */
public abstract class AbstractSimulatorListBean<T extends TlsSimulator> implements Serializable, QueryModifier<T> {

    private static Logger log = LoggerFactory.getLogger(AbstractSimulatorListBean.class);

    @In
    private EntityManager entityManager;

    private int selectedSimulatorId = 0;
    private boolean showOnlyEnabled = true;

    private transient FilterDataModel<T> simulators;
    private transient Filter<T> filter;

    public int getSelectedSimulatorId() {
        return selectedSimulatorId;
    }

    public void setSelectedSimulatorId(int selectedSimulatorId) {
        this.selectedSimulatorId = selectedSimulatorId;
    }

    public boolean isShowOnlyEnabled() {
        return showOnlyEnabled;
    }

    public void setShowOnlyEnabled(boolean showOnlyEnabled) {
        this.showOnlyEnabled = showOnlyEnabled;
        this.getFilter().modified();
    }

    public FilterDataModel<T> getSimulators() {
        if (simulators == null) {
            simulators = new FilterDataModel<T>(getFilter()) {
                @Override
                protected Object getId(T t) {
                    return t.getId();
                }
            };
        }
        return this.simulators;
    }

    public Filter<T> getFilter() {
        if (filter == null) {
            filter = new Filter<T>(getHQLCriterionsForFilter(), FacesContext.getCurrentInstance().getExternalContext()
                    .getRequestParameterMap());
        }
        return filter;
    }

    @Override
    public void modifyQuery(HQLQueryBuilder<T> hqlQueryBuilder, Map<String, Object> map) {
        if (isShowOnlyEnabled()) {
            hqlQueryBuilder.addEq("enabled", true);
        }
    }

    public String getSelectedSimulatorKeyword() {
        if (selectedSimulatorId != 0) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            T simulator = entityManager.find(getSimulatorClass(), selectedSimulatorId);
            return simulator.getKeyword();
        } else {
            return null;
        }
    }

    public abstract void resetFilter();

    @Restrict("#{s:hasRole('admin_role')}")
    public abstract String linkToNewSimulator();

    public abstract String linkToViewSimulator(int simulatorId);

    public abstract String linkToEditSimulator(int simulatorId);

    public String accessLogs(int simulatorId) {
        return ApplicationManagerBean.getUrl() + Pages.TLS_LIST_CONNECTIONS.getMenuLink() + "?simulator=" + simulatorId;
    }

    protected abstract HQLCriterionsForFilter<T> getHQLCriterionsForFilter();

    protected abstract Class<T> getSimulatorClass();

    @Restrict("#{s:hasRole('admin_role')}")
    public void duplicateSimulator(int simulatorId) {
        if (simulatorId != 0) {
            TlsSimulator simulator = entityManager.find(getSimulatorClass(), simulatorId);
            TlsSimulator copy = (TlsSimulator) simulator.clone();
            entityManager.persist(copy);
        }
    }

    @Restrict("#{s:hasRole('admin_role')}")
    public void enableSimulator(int simulatorId) {
        T simulator = entityManager.find(getSimulatorClass(), simulatorId);
        simulator.setEnabled(true);
        entityManager.merge(simulator);
        entityManager.flush();
        this.getFilter().modified();
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Simulator " + simulator.getKeyword() + " enabled");
    }

    @Restrict("#{s:hasRole('admin_role')}")
    public void disableSimulator(int simulatorId) {
        T simulator = entityManager.find(getSimulatorClass(), simulatorId);
        simulator.setEnabled(false);
        entityManager.merge(simulator);
        entityManager.flush();
        this.getFilter().modified();
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Simulator " + simulator.getKeyword() + " disabled");
    }

    @Restrict("#{s:hasRole('admin_role')}")
    public void deleteSelectedSimulator() {
        String msg;
        if (selectedSimulatorId != 0) {
            if (!hasConnections(selectedSimulatorId)) {
                T simulatorToDelete = entityManager.find(getSimulatorClass(), selectedSimulatorId);
                try {
                    entityManager.remove(simulatorToDelete);
                    entityManager.flush();
                    FacesMessages.instance().add(StatusMessage.Severity.INFO,
                            "Simulator #" + selectedSimulatorId + " deleted.");
                } catch (PersistenceException e) {
                    msg = "Unable to delete simulator #" + selectedSimulatorId + " (referenced by another entity).";
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg);
                }
                setSelectedSimulatorId(0);
                getFilter().modified();
                getSimulators().resetCache();
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        "Unable to delete simulator #" + selectedSimulatorId + ", it has connections attempts.");
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Error, no simulator selected for deletion.");
        }
    }

    public boolean hasConnections(int simulatorId) {
        TlsConnectionQuery query = new TlsConnectionQuery();
        query.simulator().id().eq(simulatorId);
        int count = query.getCount();
        return count > 0;
    }
}
