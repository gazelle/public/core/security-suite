package net.ihe.gazelle.xua.test.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.xua.model.ServiceProviderTestInstance;
import net.ihe.gazelle.xua.model.ServiceProviderTestInstanceQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.security.Identity;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by aberge on 02/06/17.
 */
@Name("xuaTestInstanceManager")
@Scope(ScopeType.PAGE)
public class XUATestInstanceManager implements Serializable, QueryModifier<ServiceProviderTestInstance>{

    private Filter<ServiceProviderTestInstance> filter;

    public Filter<ServiceProviderTestInstance> getFilter() {
        if (filter == null){
            filter = new Filter<ServiceProviderTestInstance>(getHqlCriteria());
        }
        return filter;
    }

    private HQLCriterionsForFilter<ServiceProviderTestInstance> getHqlCriteria() {
        ServiceProviderTestInstanceQuery query = new ServiceProviderTestInstanceQuery();
        HQLCriterionsForFilter<ServiceProviderTestInstance> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("testcase", query.testCase());
        criteria.addPath("timestamp", query.timestamp());
        criteria.addPath("transaction", query.ancillaryTransaction().keyword());
        criteria.addPath("endpoint", query.testedEndpoint());
        criteria.addPath("username", query.username());
        criteria.addPath("status", query.testStatus());
        criteria.addQueryModifier(this);
        return criteria;
    }

    public FilterDataModel<ServiceProviderTestInstance> getTestInstances(){
        return new FilterDataModel<ServiceProviderTestInstance>(getFilter()) {
            @Override
            protected Object getId(ServiceProviderTestInstance serviceProviderTestInstance) {
                return serviceProviderTestInstance.getId();
            }
        };
    }

    @Override
    public void modifyQuery(HQLQueryBuilder<ServiceProviderTestInstance> hqlQueryBuilder, Map<String, Object> map) {
        ServiceProviderTestInstanceQuery query = new ServiceProviderTestInstanceQuery();
        if (Identity.instance().isLoggedIn()){
            Identity user = Identity.instance();
            if (!(user.hasRole("monitor_role") || user.hasRole("admin_role"))){
                hqlQueryBuilder.addRestriction(query.username().eqRestriction(user.getCredentials().getUsername()));
            }
        } else {
            hqlQueryBuilder.addRestriction(query.username().eqRestriction(null));
        }
    }

    public void clearFilter(){
        getFilter().clear();
    }
    
}
