package net.ihe.gazelle.xua.test.action;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.xua.model.AncillaryTransaction;
import net.ihe.gazelle.xua.model.AncillaryTransactionQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.io.Serializable;

/**
 * Created by aberge on 02/06/17.
 */

@Name("ancillaryTransactionManager")
@Scope(ScopeType.PAGE)
public class AncillaryTransactionManager implements Serializable{

    private Filter<AncillaryTransaction> filter;

    public Filter<AncillaryTransaction> getFilter() {
        if (filter == null){
            filter = new Filter<AncillaryTransaction>(getHQLCriteria());
        }
        return filter;
    }

    private HQLCriterionsForFilter<AncillaryTransaction> getHQLCriteria() {
        AncillaryTransactionQuery query = new AncillaryTransactionQuery();
        HQLCriterionsForFilter<AncillaryTransaction> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("keyword", query.keyword());
        criteria.addPath("name", query.name());
        criteria.addPath("net/ihe/gazelle/xua/test/action", query.soapAction());
        return criteria;
    }

    public FilterDataModel<AncillaryTransaction> getAncillaryTransactions(){
        return new FilterDataModel<AncillaryTransaction>(getFilter()) {
            @Override
            protected Object getId(AncillaryTransaction ancillaryTransaction) {
                return ancillaryTransaction.getId();
            }
        };
    }

    public void clearFilter(){
        getFilter().clear();
    }

    public void deleteTransaction(AncillaryTransaction transaction){
        //TODO
    }

    public String editTransaction(AncillaryTransaction transaction){
        // TODO
        return null;
    }
}
