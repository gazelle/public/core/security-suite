package net.ihe.gazelle.xua.test.action;

import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.xua.dao.ServiceProviderTestInstanceDAO;
import net.ihe.gazelle.xua.model.ServiceProviderTestInstance;
import net.ihe.gazelle.xua.model.TestStatus;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by aberge on 07/06/17.
 */
@Name("xuaTestInstanceDisplay")
@Scope(ScopeType.PAGE)
public class XUATestInstanceDisplay implements Serializable {

    public static final String NO_MESSAGE_RECORDED = "No message has been recorded";

    public ServiceProviderTestInstance getSelectedTestInstance() {
        return selectedTestInstance;
    }

    private ServiceProviderTestInstance selectedTestInstance;

    @Create
    public void initialize() {
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String idAsString = urlParams.get("id");
        if (idAsString == null) {
            selectedTestInstance = null;
        } else {
            try {
                Integer id = Integer.parseInt(idAsString);
                selectedTestInstance = ServiceProviderTestInstanceDAO.getTestInstanceById(id);
            } catch (NumberFormatException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, idAsString + " is not a valid test instance id");
            }
        }
    }


    public String getSentMessage() {
        if (selectedTestInstance != null) {
            return selectedTestInstance.getRequestAsFormattedXML();
        } else {
            return NO_MESSAGE_RECORDED;
        }
    }


    public String getReceivedMessage() {
        if (selectedTestInstance != null) {
            return selectedTestInstance.getResponseAsFormattedXML();
        } else {
            return NO_MESSAGE_RECORDED;
        }
    }

    public String getReceivedHeaders() {
        if (selectedTestInstance != null && selectedTestInstance.getResponseHeaders() != null) {
            return new String(selectedTestInstance.getResponseHeaders(), StandardCharsets.UTF_8);
        } else {
            return NO_MESSAGE_RECORDED;
        }
    }

    public String getSentMessageRaw() {
        if (selectedTestInstance != null && selectedTestInstance.getRequest() != null) {
            return new String(selectedTestInstance.getRequest(), StandardCharsets.UTF_8);
        } else {
            return NO_MESSAGE_RECORDED;
        }
    }


    public String getReceivedMessageRaw() {
        if (selectedTestInstance != null && selectedTestInstance.getResponse() != null) {
            return new String(selectedTestInstance.getResponse(), StandardCharsets.UTF_8);
        } else {
            return NO_MESSAGE_RECORDED;
        }
    }

    public String backToListOfTestInstances() {
        return Pages.XUA_TEST_INSTANCES.getMenuLink();
    }

    public List<SelectItem> getAvailableStatuses() {
        List<SelectItem> items = new ArrayList<SelectItem>();
        for (TestStatus status : TestStatus.values()) {
            items.add(new SelectItem(status, status.name()));
        }
        return items;
    }

    public void saveTestInstance() {
        ServiceProviderTestInstanceDAO.saveTestInstance(selectedTestInstance);
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Test instance successfully updated");
    }

}
