package net.ihe.gazelle.signature.validator.test;

import net.ihe.gazelle.signature.exception.SignatureValidatorException;
import net.ihe.gazelle.signature.validator.utils.XMLValidation;
import net.ihe.gazelle.validation.DocumentValidXSD;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * <p>XMLValidationTest class.</p>
 *
 * @author abe
 * @version 1.0: 19/03/18
 */

public class XMLValidationTest {

    private static final String ZERO_COUNT = "0";

    @Test
    public void kelaSignatureOk(){
        try {
            String contents = new String(Files.readAllBytes(Paths.get("src/test/resources/EsimerkkiAllekirjoitus1_yksiresepti.xml")));
            XMLValidation xmlValidation = new XMLValidation();
            DocumentValidXSD result = xmlValidation.performSchemaValidation(contents);
            Assert.assertEquals(ZERO_COUNT, result.getNbOfErrors());
            Assert.assertEquals(ZERO_COUNT, result.getNbOfWarnings());
        } catch (SignatureValidatorException | IOException e) {
            Assert.fail(e.getMessage());
        }
    }

     @Test
     @Ignore
     // FIXME GSS-525 This test does not work, the schema error is not detected
    public void kelaSignatureKO(){
        try {
            String contents = new String(Files.readAllBytes(Paths.get("src/test/resources/malFormedSignature.xml")));
            XMLValidation xmlValidation = new XMLValidation();
            DocumentValidXSD result = xmlValidation.performSchemaValidation(contents);
            Assert.assertEquals("1", result.getNbOfErrors());
        } catch (SignatureValidatorException | IOException e) {
            Assert.fail(e.getMessage());
        }
    }

}
