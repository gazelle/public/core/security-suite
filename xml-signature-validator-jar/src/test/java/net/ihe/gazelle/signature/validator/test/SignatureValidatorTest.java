package net.ihe.gazelle.signature.validator.test;

import net.ihe.gazelle.signature.exception.SignatureValidatorException;
import net.ihe.gazelle.signature.validator.core.SignatureValidator;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.Error;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * <p>SignatureValidatorTest class.</p>
 *
 * @author abe
 * @version 1.0: 29/03/18
 */

public class SignatureValidatorTest {

    @Test
    public void testValidationOK() {
        DetailedResult result = executeValidation("src/test/resources/EsimerkkiAllekirjoitus2_yksiPDF.xml");
        Assert.assertEquals("FAILED", result.getValidationResultsOverview().getValidationTestResult());
    }

    @Test
    public void testValidationOK2(){
        DetailedResult result = executeValidation("src/test/resources/EsimerkkiAllekirjoitus1_yksiresepti.xml");
        Assert.assertEquals("FAILED", result.getValidationResultsOverview().getValidationTestResult());
    }

    @Test
    public void testWithMalformedSignature(){
        DetailedResult result = executeValidation("src/test/resources/malFormedSignature.xml");
        Assert.assertEquals("FAILED", result.getValidationResultsOverview().getValidationTestResult());
    }

    @Test
    public void testWithInvalidSignatureValue(){
        DetailedResult result = executeValidation("src/test/resources/invalidSignatureValue.xml");
        Assert.assertEquals("FAILED", result.getValidationResultsOverview().getValidationTestResult());
    }

    @Test
    public void testWithTooShortDigestValue(){
        DetailedResult result = executeValidation("src/test/resources/invalidDigestValue_wrongLength.xml");
        Assert.assertEquals("FAILED", result.getValidationResultsOverview().getValidationTestResult());
    }

    @Test
    public void testWithInvalidDigestValue(){
        DetailedResult result = executeValidation("src/test/resources/invalidDigestValue_wrongValue.xml");
        Assert.assertEquals("FAILED", result.getValidationResultsOverview().getValidationTestResult());
    }

    @Test
    public void testWithMissingDigestValue(){
        DetailedResult result = executeValidation("src/test/resources/missingDigestValue.xml");
        Assert.assertEquals("FAILED", result.getValidationResultsOverview().getValidationTestResult());
    }

    @Test
    public void testNonCDAInputFile(){
        DetailedResult result = executeValidation("src/test/resources/saml.xml");
        Assert.assertEquals("FAILED", result.getValidationResultsOverview().getValidationTestResult());
    }

    @Test
    //OK but certificate is expired
    public void testHL7v3WithSaml(){
        DetailedResult result = executeValidation("src/test/resources/hl7v3withsaml.xml");
        Assert.assertEquals("PASSED", result.getDocumentValidXSD().getResult());
        Assert.assertEquals(1, result.getMDAValidation().getValidationCounters().getNrOfValidationErrors().intValue());
        for(Object object : result.getMDAValidation().getWarningOrErrorOrNote()){
            if(object instanceof Error){
                Assert.assertEquals("The certificate is no more valid", ((Error) object).getDescription());
            }
        }
    }

    @Test
    public void testPORTALDRREQRECEIVED(){
        DetailedResult result = executeValidation("src/test/resources/PORTAL_DR_REQ_RECEIVED.xml");
        Assert.assertEquals("FAILED", result.getValidationResultsOverview().getValidationTestResult());
    }

    @Test
    public void testRequestOfDataRetrieve(){
        DetailedResult result = executeValidation("src/test/resources/Request_of_Data_Retrieve_CY_Signed.xml");
        Assert.assertEquals("PASSED", result.getValidationResultsOverview().getValidationTestResult());
    }


    @Test
    public void invalidReferences(){
        DetailedResult result = executeValidation("src/test/resources/invalidReference.xml");
        Assert.assertEquals("FAILED", result.getValidationResultsOverview().getValidationTestResult());
    }


    @Test
    public void tesNoSignature(){
        String samplePath = "src/test/resources/noSignature.xml";
        String contents = null;
        try {
            contents = new String(Files.readAllBytes(Paths.get(samplePath)));
        } catch (IOException e) {
            Assert.fail("Cannot access the sample file");
        }
        SignatureValidator validator = new SignatureValidator();
        try {
            validator.validate(contents);
        }catch (SignatureValidatorException e){
            Assert.assertEquals("No signature found in the given document", e.getMessage());
        }
    }

    @Test
    public void testMissingSignatureTimestamp(){
        DetailedResult result = executeValidation("src/test/resources/missingSignatureTimestamp.xml");
        Assert.assertEquals("FAILED", result.getValidationResultsOverview().getValidationTestResult());
    }

    @Test
    public void testWithTwoSignatures(){
        DetailedResult result = executeValidation("src/test/resources/sequoiaSoapHeader.xml");
        Assert.assertEquals("FAILED", result.getValidationResultsOverview().getValidationTestResult());
    }



    private DetailedResult executeValidation(String samplePath) {
        String contents = null;
        try {
            contents = new String(Files.readAllBytes(Paths.get(samplePath)));
        } catch (IOException e) {
            Assert.fail("Cannot access the sample file");
        }
        SignatureValidator validator = new SignatureValidator();
        try {
            DetailedResult result = validator.validate(contents);
            System.out.println(validator.getDetailedResultAsString(result));
            return result;
        } catch (SignatureValidatorException e) {
            Assert.fail(e.getMessage());
        }
        return null;
    }
}
