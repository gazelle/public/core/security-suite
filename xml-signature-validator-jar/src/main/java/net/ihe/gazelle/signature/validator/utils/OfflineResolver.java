/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.ihe.gazelle.signature.validator.utils;

import org.apache.commons.io.IOUtils;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.utils.resolver.ResourceResolverContext;
import org.apache.xml.security.utils.resolver.ResourceResolverException;
import org.apache.xml.security.utils.resolver.ResourceResolverSpi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

/**
 * This class helps us home users to resolve http URIs without a network
 * connection
 *
 * @author $Author$
 */
public class OfflineResolver extends ResourceResolverSpi {

    private static final Logger LOG = LoggerFactory.getLogger(OfflineResolver.class);
    
    /** Field _uriMap */
    private static Map<String, String> _uriMap = null;

    /** Field _mimeMap */
    private static Map<String, String> _mimeMap = null;

    static {
        org.apache.xml.security.Init.init();

        _uriMap = new HashMap<String, String>();
        _mimeMap = new HashMap<String, String>();

        _uriMap.put("http://www.w3.org/TR/xml-stylesheet", "samples/data/org/w3c/www/TR/xml-stylesheet.html");
        _mimeMap.put("http://www.w3.org/TR/xml-stylesheet", "text/html");


        _uriMap.put("http://www.w3.org/TR/2000/REC-xml-20001006", "samples/data/org/w3c/www/TR/2000/REC-xml-20001006");
        _mimeMap.put("http://www.w3.org/TR/2000/REC-xml-20001006",  "text/xml");

        _uriMap.put("http://www.nue.et-inf.uni-siegen.de/index.html", "samples/data/org/apache/xml/security/temp/nuehomepage");
        _mimeMap.put("http://www.nue.et-inf.uni-siegen.de/index.html", "text/html");

        _uriMap.put("http://www.nue.et-inf.uni-siegen.de/~geuer-pollmann/id2.xml","samples/data/org/apache/xml/security/temp/id2.xml");
        _mimeMap.put("http://www.nue.et-inf.uni-siegen.de/~geuer-pollmann/id2.xml","text/xml");

        _uriMap.put("http://xmldsig.pothole.com/xml-stylesheet.txt", "samples/data/com/pothole/xmldsig/xml-stylesheet.txt");
        _mimeMap.put("http://xmldsig.pothole.com/xml-stylesheet.txt", "text/xml");

        _uriMap.put("http://www.w3.org/Signature/2002/04/xml-stylesheet.b64","samples/data/ie/baltimore/merlin-examples/merlin-xmldsig-twenty-three/xml-stylesheet.b64");
        _mimeMap.put("http://www.w3.org/Signature/2002/04/xml-stylesheet.b64", "text/plain");
    }

    /**
     * Method engineResolve
     *
     * @param uri
     * @param BaseURI
     *
     * @throws ResourceResolverException
     */
    public XMLSignatureInput engineResolve(Attr uri, String BaseURI)
        throws ResourceResolverException {
        InputStream is = null;
        try {
            String URI = uri.getNodeValue();

            if (OfflineResolver._uriMap.containsKey(URI)) {
                String newURI = OfflineResolver._uriMap.get(URI);

                LOG.debug("Mapped " + URI + " to " + newURI);

                 is = new FileInputStream(newURI);

                LOG.debug("Available bytes = " + is.available());

                XMLSignatureInput result = new XMLSignatureInput(is);

                result.setSourceURI(URI);
                result.setMIMEType((String) OfflineResolver._mimeMap.get(URI));

                return result;
            } else {
                Object exArgs[] = {"The URI " + URI + " is not configured for offline work"};

                throw new ResourceResolverException("generic.EmptyMessage", exArgs, uri.getTextContent(), BaseURI);
            }
        } catch (IOException ex) {
            throw new ResourceResolverException("generic.EmptyMessage", ex, uri.getTextContent(), BaseURI);
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    /**
     * We resolve http URIs <I>without</I> fragment...
     *
     * @param uri
     * @param BaseURI
     *
     */
    public boolean engineCanResolve(Attr uri, String BaseURI) {

        String uriNodeValue = uri.getNodeValue();
        if (uriNodeValue.equals("") || uriNodeValue.startsWith("#")) {
            return false;
        }

        try {
            URI uriNew = getNewURI(uri.getNodeValue(), BaseURI);
            if (uriNew.getScheme().equals("http")) {
                LOG.debug("I state that I can resolve " + uriNew);
                return true;
            }

            LOG.debug("I state that I can't resolve " + uriNew);
        } catch (URISyntaxException ex) {
            //
        }

        return false;
    }


    @Override
    public XMLSignatureInput engineResolveURI(ResourceResolverContext resourceResolverContext) throws ResourceResolverException {
        return null;
    }

    public boolean engineCanResolveURI(ResourceResolverContext context) {
    	Attr uri = context.attr;
    	String BaseURI = context.baseUri;
    	return engineCanResolve(uri, BaseURI);
    }

    
    private static URI getNewURI(String uri, String baseURI) throws URISyntaxException {
        URI newUri;
        if (baseURI == null || "".equals(baseURI)) {
            newUri = new URI(uri);
        } else {
            newUri = new URI(baseURI).resolve(uri);
        }
        
        // if the URI contains a fragment, ignore it
        if (newUri.getFragment() != null) {
            URI uriNewNoFrag = 
                new URI(newUri.getScheme(), newUri.getSchemeSpecificPart(), null);
            return uriNewNoFrag;
        }
        return newUri;
    }
}
