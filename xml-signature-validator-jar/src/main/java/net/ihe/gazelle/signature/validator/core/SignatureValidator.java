package net.ihe.gazelle.signature.validator.core;

import net.ihe.gazelle.signature.exception.SignatureValidatorException;
import net.ihe.gazelle.signature.validator.utils.DSNamespaceContext;
import net.ihe.gazelle.signature.validator.utils.HL7NamespaceContext;
import net.ihe.gazelle.signature.validator.utils.OfflineResolver;
import net.ihe.gazelle.signature.validator.utils.POMVersion;
import net.ihe.gazelle.signature.validator.utils.XMLValidation;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.DocumentValidXSD;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.MDAValidation;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.ValidationCounters;
import net.ihe.gazelle.validation.ValidationResultsOverview;
import net.ihe.gazelle.validation.Warning;
import org.apache.xml.security.Init;
import org.apache.xml.security.c14n.CanonicalizationException;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.keys.KeyInfo;
import org.apache.xml.security.keys.keyresolver.KeyResolverException;
import org.apache.xml.security.signature.Reference;
import org.apache.xml.security.signature.SignedInfo;
import org.apache.xml.security.signature.XMLSignature;
import org.apache.xml.security.signature.XMLSignatureException;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.utils.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.PublicKey;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <p>SignatureValidator class.</p>
 *
 * @author abe
 * @version 1.0: 19/03/18
 */

public class SignatureValidator {

    private static final Logger LOG = LoggerFactory.getLogger(SignatureValidator.class);

    private XPath xpath;
    private int countError = 0;
    private int countWarning = 0;
    private int countInfo = 0;
    private int countNote = 0;
    private int countChecks = 0;

    public SignatureValidator() {

    }

    public DetailedResult validate(String fileToValidate) throws SignatureValidatorException {

        // XSD validation
        XMLValidation xmlValidation = new XMLValidation();
        DetailedResult detailedResult = new DetailedResult();
        Document documentToValidate;
        try {
            DocumentValidXSD xsdReport = xmlValidation.performSchemaValidation(fileToValidate);
            detailedResult.setDocumentValidXSD(xsdReport);
        } catch (SignatureValidatorException e) {
            throw e;
        }

        // get the file to validate as DOM document
        documentToValidate = xmlValidation.getDocumentToValidate();

        // detect ds:Signature elements
        XPathFactory factory = XPathFactory.newInstance();
        xpath = factory.newXPath();
        xpath.setNamespaceContext(new DSNamespaceContext());


        String expression = "//ds:Signature";
        NodeList signatureNodes;
        try {
            signatureNodes = (NodeList) xpath.evaluate(expression, documentToValidate, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            throw new SignatureValidatorException("Cannot get list of ds:Signature elements", e);
        }
        setIdsIfPresent(documentToValidate);

        if (signatureNodes.getLength() > 0) {
            MDAValidation validationReport = new MDAValidation();
            for (int index = 0; index < signatureNodes.getLength(); index++) {
                Node node = signatureNodes.item(index);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    String location = "//ds:Signature[" + index + "]";
                    Element signatureElement = (Element) node;
                    XMLSignature xmlSignature = buildXMLSignature(signatureElement, validationReport, location);
                    if (xmlSignature != null) {
                        if (checkKeyInfo(xmlSignature, validationReport, location)) {
                            checkReferences(xmlSignature, validationReport, location);
                            checkSignatureValue(xmlSignature, validationReport, location);
                            processReferences(xmlSignature, validationReport, location);
                            if (isKelaSignature(node)) {
                                String fiSignatureLocation = "//fi:signature[" + index + "]";
                                validateFiSignatureTimestamp(node.getParentNode(), validationReport, fiSignatureLocation);
                                validateMultipleSignature(node.getParentNode(), validationReport, fiSignatureLocation);
                                validateStructuredBody(documentToValidate, validationReport);
                            }
                            // structure body
                        }
                    }
                }
            }
            setMDAValidationResult(validationReport);
            detailedResult.setMDAValidation(validationReport);
        } else {
            throw new SignatureValidatorException("No signature found in the given document");
        }
        addResultOverview(detailedResult);
        return detailedResult;
    }

    private void setIdsIfPresent(Document documentToValidate) {
        setSamlAssertionID(documentToValidate);
        setWsuTimestampID(documentToValidate);
        setHl7FiSignatureTimestampID(documentToValidate);
        setSectionID(documentToValidate);
        setStructuredBodyID(documentToValidate);
        setNonXmlBodyID(documentToValidate);
    }

    private void setWsuTimestampID(Document documentToValidate) {
        String wsuTimestampRoot = "//wsu:Timestamp";
        setIdAttribute(documentToValidate, wsuTimestampRoot, "wsu:Id");
    }

    private void setSamlAssertionID(Document documentToValidate) {
        // needed in case the signature is embedded in SAML assertion
        String samlAssertionRoot = "//saml:Assertion";
        setIdAttribute(documentToValidate, samlAssertionRoot, "ID");
    }

    private void setHl7FiSignatureTimestampID(Document documentToValidate) {
        // needed when the hl7fi:signatureTimestamp is signed
        String signatureTimestampRoot = "//hl7fi:signatureTimestamp";
        setIdAttribute(documentToValidate, signatureTimestampRoot, "ID");
    }

    private void setStructuredBodyID(Document documentToValidate) {
        // needed when the structuredBody of the CDA is signed
        String structureBodyRoot = "//hl7:structuredBody";
        setIdAttribute(documentToValidate, structureBodyRoot, "ID");
    }

    private void setSectionID(Document documentToValidate) {
        // needed when the section of a CDA is signed
        String section = "//hl7:section";
        setIdAttribute(documentToValidate, section, "ID");
    }

    private void setNonXmlBodyID(Document documentToValidate) {
        // needed when the section of a CDA is signed
        String nonXMLBodyRoot = "//hl7:nonXMLBody";
        setIdAttribute(documentToValidate, nonXMLBodyRoot, "ID");
    }

    private void setIdAttribute(Document documentToValidate, String pathToElement, String idAttributeName) {
        NodeList nodeList = null;
        try {
            nodeList = (NodeList) xpath.evaluate(pathToElement, documentToValidate, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            // no assertion here
        }
        if (nodeList != null && nodeList.getLength() > 0) {
            for (int index = 0; index < nodeList.getLength(); index++) {
                Element item = (Element) nodeList.item(index);
                if (item.hasAttribute(idAttributeName)) {
                    item.setIdAttribute(idAttributeName, true);
                }
            }
        }
    }

    private void setMDAValidationResult(MDAValidation validationReport) {
        validationReport.setResult(countError == 0 ? "PASSED" : "FAILED");
        ValidationCounters counters = new ValidationCounters();
        counters.setNrOfChecks(BigInteger.valueOf(countChecks));
        counters.setNrOfValidationErrors(BigInteger.valueOf(countError));
        counters.setNrOfValidationInfos(BigInteger.valueOf(countInfo));
        counters.setNrOfValidationReports(BigInteger.valueOf(countNote));
        counters.setNrOfValidationWarnings(BigInteger.valueOf(countWarning));
        validationReport.setValidationCounters(counters);
    }

    private void addResultOverview(DetailedResult detailedResult) {
        ValidationResultsOverview overview = new ValidationResultsOverview();
        SimpleDateFormat sdfDate = new SimpleDateFormat("YYYY-MM-dd");
        SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm:ss (Z)");
        Date now = new Date();
        overview.setValidationDate(sdfDate.format(now));
        overview.setValidationTime(sdfTime.format(now));
        overview.setValidationEngine("xml-signature-validator-jar");
        overview.setValidationServiceName("Gazelle XML Signature Validator");
        overview.setValidationServiceVersion(getModuleVersion());
        overview.setValidationTestResult(detailedResult.getMDAValidation().getResult());
        detailedResult.setValidationResultsOverview(overview);
    }

    private String getModuleVersion(){
        return POMVersion.getVersion();
    }

    private void checkSignatureValue(XMLSignature xmlSignature, MDAValidation validationReport, String location) {
        try {
            X509Certificate cert = xmlSignature.getKeyInfo().getX509Certificate();
            Notification notification = null;
            Boolean isSignatureValid = null;
            String signatureLocation = location.concat("/ds:SignatureValue");
            if (cert != null) {
                try {
                    isSignatureValid = xmlSignature.checkSignatureValue(cert);
                } catch (XMLSignatureException e) {
                    notification = createNotificationOnXMLSignatureException(signatureLocation, e);
                }
                checkCertificateValidity(cert, validationReport, location);
                appendCertificateInfos(cert, validationReport, location);
            } else {
                PublicKey pk = xmlSignature.getKeyInfo().getPublicKey();
                if (pk != null) {
                    try {
                        isSignatureValid = xmlSignature.checkSignatureValue(pk);
                    } catch (XMLSignatureException e) {
                        notification = createNotificationOnXMLSignatureException(signatureLocation, e);
                    }
                } else {
                    notification = newWarning();
                    notification.setIdentifiant(SignatureValidatorConstants.CHECK_SIGNATURE_VALUE_NO_CERT);
                    notification.setDescription(SignatureValidatorConstants.CHECK_SIGNATURE_VALUE_NO_CERT_MESSAGE);
                    notification.setLocation(location);
                }
            }
            if (isSignatureValid != null) {
                if (isSignatureValid) {
                    notification = newNote();
                    notification.setIdentifiant(SignatureValidatorConstants.SIGNATURE_VALUE_VALID);
                    notification.setLocation(signatureLocation);
                    notification.setDescription(SignatureValidatorConstants.SIGNATURE_VALUE_VALID_MESSAGE);
                } else {
                    notification = newError();
                    notification.setIdentifiant(SignatureValidatorConstants.SIGNATURE_VALUE_INVALID);
                    notification.setLocation(signatureLocation);
                    notification.setDescription(SignatureValidatorConstants.SIGNATURE_VALUE_INVALID_MESSAGE);
                }
            }
            if (notification != null) {
                validationReport.getWarningOrErrorOrNote().add(notification);
            }
        } catch (KeyResolverException e) {
            // FIXME do something here: when does it occur ?
        }
    }

    private void appendCertificateInfos(X509Certificate cert, MDAValidation validationReport, String location) {
        String certlocation = location.concat("/ds:KeyInfo/ds:X509Data/ds:X509Certificate");

        reportValue(validationReport, certlocation, cert.getIssuerDN().getName(), SignatureValidatorConstants.CERT_ISSUER_DN);
        reportValue(validationReport, certlocation, cert.getSubjectDN().getName(), SignatureValidatorConstants.CERT_SUBJECT_DN);
    }

    private void checkCertificateValidity(X509Certificate cert, MDAValidation validationReport, String location) {
        Notification notification;
        try{
            cert.checkValidity();
            notification = newNote();
            notification.setIdentifiant(SignatureValidatorConstants.CERT_VALID);
            notification.setDescription(SignatureValidatorConstants.CERT_VALID_MESSAGE);
        }catch (CertificateExpiredException e){
            notification = newError();
            notification.setIdentifiant(SignatureValidatorConstants.CERT_NO_MORE_VALID);
            notification.setDescription(SignatureValidatorConstants.CERT_NO_MORE_VALID_MESSAGE);
        } catch (CertificateNotYetValidException e){
            notification = new Error();
            notification.setIdentifiant(SignatureValidatorConstants.CERT_NOT_YET_VALID);
            notification.setDescription(SignatureValidatorConstants.CERT_NOT_YET_VALID_MESSAGE);
        }
        location = location + "/ds:KeyInfo/ds:X509Data/ds:X509Certificate";
        notification.setLocation(location);
        validationReport.getWarningOrErrorOrNote().add(notification);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss Z");
        Notification notBefore = newInfo();
        notBefore.setLocation(location);
        notBefore.setTest(SignatureValidatorConstants.CERT_NOT_BEFORE);
        notBefore.setDescription(simpleDateFormat.format(cert.getNotBefore()));
        validationReport.getWarningOrErrorOrNote().add(notBefore);
        Notification notAfter = newInfo();
        notAfter.setLocation(location);
        notAfter.setTest(SignatureValidatorConstants.CERT_NOT_AFTER);
        notAfter.setDescription(simpleDateFormat.format(cert.getNotAfter()));
        validationReport.getWarningOrErrorOrNote().add(notAfter);
    }

    private Notification createNotificationOnXMLSignatureException(String location, XMLSignatureException e) {
        Notification notification;
        notification = newWarning();
        notification.setIdentifiant(SignatureValidatorConstants.PRINT_DESCRIPTION);
        notification.setLocation(location);
        notification.setDescription(e.getMessage());
        return notification;
    }

    private Notification newWarning() {
        countWarning++;
        countChecks++;
        return new Warning();
    }

    private Notification newNote() {
        countNote++;
        countChecks++;
        return new Note();
    }

    private Notification newError() {
        countError++;
        countChecks++;
        return new Error();
    }

    private Notification newInfo(){
        countInfo++;
        countChecks++;
        return new Info();
    }

    private void processReferences(XMLSignature xmlSignature, MDAValidation validationReport, String location) {
        SignedInfo signedInfo = xmlSignature.getSignedInfo();
        int signedContentLength = signedInfo.getLength();
        location = location.concat("/ds:SignedInfo");
        for (int index = 0; index < signedContentLength; index++) {
            String currentLocation = location.concat("/ds:Reference[" + index + "]");
            try {
                processSingleReference(signedInfo.item(index), validationReport, currentLocation);
            } catch (XMLSecurityException e) {
                LOG.warn("Cannot access reference with index " + index + ": " + e.getMessage());
            }
        }
    }

    private void processSingleReference(Reference reference, MDAValidation validationReport, String currentLocation) {
        try {
            Notification notification = checkDigestValue(reference, currentLocation, validationReport);
            validationReport.getWarningOrErrorOrNote().add(notification);
            reportCanonicalizedReferencedContent(reference, validationReport, currentLocation);
        } catch (XMLSecurityException e) {
            LOG.error(e.getMessage());
        }
    }

    private void reportCanonicalizedReferencedContent(Reference reference, MDAValidation validationReport, String currentLocation) throws XMLSignatureException, CanonicalizationException {
        try {
            XMLSignatureInput input = reference.getContentsAfterTransformation();
            byte[] canonicalizedContent = input.getBytes();
            String canonicalizedContentAsString = new String(canonicalizedContent, StandardCharsets.UTF_8);
            reportValue(validationReport, currentLocation, canonicalizedContentAsString, SignatureValidatorConstants.CANONICALIZED_REFERENCED_CONTENT);
        }catch (IOException e){
            LOG.debug(e.getMessage());
        }catch (NullPointerException e){
            LOG.debug(e.getMessage());
        }
    }

    private Notification checkDigestValue(Reference reference, String currentLocation, MDAValidation mdaValidation) throws XMLSecurityException {
        Notification notification;
        String digestValueLocation = currentLocation.concat("/ds:DigestValue");
        byte[] actualDigestValue = reference.getDigestValue();
        String actualDigestString = Base64.encode(actualDigestValue);
        reportValue(mdaValidation, digestValueLocation, actualDigestString, SignatureValidatorConstants.DECLARED_DIGEST_VALUE);
        try {
            reference.generateDigestValue();
            byte[] computedDigestValue = reference.getDigestValue();
            String computedDigestString = Base64.encode(computedDigestValue);

            if (actualDigestString.equals(computedDigestString)){
                notification = newNote();
                notification.setIdentifiant(SignatureValidatorConstants.DIGEST_VALUE_CORRECT);
                notification.setDescription(SignatureValidatorConstants.DIGEST_VALUE_CORRECT_MESSAGE);
            } else {
                notification = newError();
                notification.setIdentifiant(SignatureValidatorConstants.DIGEST_VALUE_MISTMATCH);
                notification.setDescription(SignatureValidatorConstants.DIGEST_VALUE_MISTMATCH_MESSAGE);
                reportValue(mdaValidation, digestValueLocation, computedDigestString, SignatureValidatorConstants.ACTUAL_CALCULATED_DIGEST);
            }
        } catch (NullPointerException e){
            notification = newWarning();
            notification.setDescription("Unable to compute the digest value");
        }
        notification.setLocation(digestValueLocation);
        return notification;
    }

    private void reportValue(MDAValidation mdaValidation, String location, String valueToReport, String identifiant) {
        Notification actualDigest = newInfo();
        actualDigest.setTest(identifiant);
        actualDigest.setDescription(valueToReport);
        actualDigest.setLocation(location);
        mdaValidation.getWarningOrErrorOrNote().add(actualDigest);
    }

    private void checkReferences(XMLSignature xmlSignature, MDAValidation validationReport, String location) {
        Notification notification;
        SignedInfo signedInfo = xmlSignature.getSignedInfo();
        location = location.concat("/ds:SignedInfo");
        try {
            signedInfo.verifyReferences(true);
            int signedInfoLength = signedInfo.getLength();
            for (int index = 0; index < signedInfoLength; index++) {
                String currentLocation = location.concat("/ds:Reference[" + index + "]");
                try {
                    boolean result = signedInfo.getVerificationResult(index);
                    if(result) {
                        notification = newNote();
                        notification.setIdentifiant(SignatureValidatorConstants.REFERENCE_CHECK_OK);
                        notification.setDescription(SignatureValidatorConstants.REFERENCE_CHECK_OK_MESSAGE);
                    } else {
                        notification = newError();
                        notification.setIdentifiant(SignatureValidatorConstants.REFERENCE_CHECK_KO);
                        notification.setDescription(SignatureValidatorConstants.REFERENCE_CHECK_KO_MESSAGE);
                    }
                } catch (XMLSecurityException e) {
                    notification = newError();
                    notification.setIdentifiant(SignatureValidatorConstants.PRINT_DESCRIPTION);
                    notification.setDescription(e.getMessage());
                }
                notification.setLocation(currentLocation);
                validationReport.getWarningOrErrorOrNote().add(notification);
            }
        } catch (XMLSecurityException e) {
            notification = newError();
            notification.setIdentifiant(SignatureValidatorConstants.PRINT_DESCRIPTION);
            notification.setDescription(e.getMessage());
            notification.setLocation(location);
            validationReport.getWarningOrErrorOrNote().add(notification);
        } catch (Exception e){
            notification = newWarning();
            notification.setIdentifiant(SignatureValidatorConstants.REFERENCE_CHECK_KO);
            notification.setDescription(SignatureValidatorConstants.REFERENCE_CHECK_KO_MESSAGE);
            notification.setLocation(location);
            validationReport.getWarningOrErrorOrNote().add(notification);
        }
    }

    private void validateStructuredBody(Document documentToValidate, MDAValidation validationReport) {
        xpath.setNamespaceContext(new HL7NamespaceContext());
        String expression = "//hl7:structuredBody";
        try {
            Element strucBodyElement = (Element) xpath.evaluate(expression, documentToValidate, XPathConstants.NODE);
            if (strucBodyElement != null) {
                Notification notification;
                if (strucBodyElement.hasAttribute("ID")) {
                    notification = newNote();
                    strucBodyElement.setIdAttribute("ID", true);
                } else {
                    notification = newWarning();
                }
                notification.setLocation(getXpath(strucBodyElement));
                notification.setIdentifiant(SignatureValidatorConstants.CHECK_STRUCTUREDBODY_ID);
                notification.setDescription(SignatureValidatorConstants.CHECK_STRUCTUREDBODY_ID_MESSAGE);
                validationReport.getWarningOrErrorOrNote().add(notification);
            }
        } catch (XPathExpressionException e) {
            // FIXME: how to log this exception ?
        }
    }

    private void validateMultipleSignature(Node fiSignatureNode, MDAValidation validationReport, String fiSignatureLocation) {
        NodeList children = fiSignatureNode.getChildNodes();
        Node multipleSignature = null;
        for (int index = 0; index < children.getLength(); index++) {
            Node currentNode = children.item(index);
            if (currentNode.getNodeType() == Node.ELEMENT_NODE
                    && currentNode.getLocalName().equals("multipleDocumentSignature") && currentNode.getNamespaceURI().equals("urn:hl7finland")) {
                multipleSignature = children.item(index);
                break;
            }
        }
        if (multipleSignature != null) {
            Notification notification = newNote();
            notification.setLocation(fiSignatureLocation);
            notification.setIdentifiant(SignatureValidatorConstants.CHECK_MULTIPLEDOCSIGNATURE_PRESENT);
            notification.setDescription(SignatureValidatorConstants.CHECK_MULTIPLEDOCSIGNATURE_PRESENT_MESSAGE);
            validationReport.getWarningOrErrorOrNote().add(validationReport);
            Element multipleSignatureElement = (Element) multipleSignature;
            checkMultipleDocumentSignatureId(multipleSignatureElement, validationReport, fiSignatureLocation);
        }
    }

    private void checkMultipleDocumentSignatureId(Element multipleSignatureElement, MDAValidation validationReport, String fiSignatureLocation) {
        Notification notification;
        if (multipleSignatureElement.hasAttribute("ID")) {
            notification = newNote();
            multipleSignatureElement.setIdAttribute("ID", true);
        } else {
            notification = newWarning();
        }
        notification.setLocation(fiSignatureLocation + "/multipleDocumentSignature");
        notification.setIdentifiant(SignatureValidatorConstants.CHECK_MULTIPLEDOCSIG_ID);
        notification.setDescription(SignatureValidatorConstants.CHECK_MULTIPLEDOCSIG_ID_MESSAGE);
        validationReport.getWarningOrErrorOrNote().add(notification);
    }

    private void validateFiSignatureTimestamp(Node fiSignatureNode, MDAValidation validationReport, String location) {
        NodeList children = fiSignatureNode.getChildNodes();
        Node timestamp = null;
        for (int index = 0; index < children.getLength(); index++) {
            Node currentNode = children.item(index);
            if (currentNode.getNodeType() == Node.ELEMENT_NODE
                    && currentNode.getLocalName().equals("signatureTimestamp") && currentNode.getNamespaceURI().equals("urn:hl7finland")) {
                timestamp = children.item(index);
                break;
            }
        }
        Notification notification;
        if (timestamp == null) {
            notification = newError();
        } else {
            Element timestampElement = (Element) timestamp;
            checkTimestampElementId(timestampElement, validationReport, location);
            notification = newNote();
        }
        notification.setIdentifiant(SignatureValidatorConstants.CHECK_SIGNATURETIMESTAMP_PRESENT);
        notification.setDescription(SignatureValidatorConstants.CHECK_SIGNATURETIMESTAMP_PRESENT_MESSAGE);
        notification.setLocation(location);
        validationReport.getWarningOrErrorOrNote().add(notification);
    }

    private void checkTimestampElementId(Element timestampElement, MDAValidation validationReport, String location) {
        Notification notification;
        if (timestampElement.hasAttribute("ID")) {
            notification = newNote();
            timestampElement.setIdAttribute("ID", true);
        } else {
            notification = newError();
        }
        notification.setLocation(location + "/signatureTimestamp");
        notification.setIdentifiant(SignatureValidatorConstants.CHECK_SIGNATURETIMESTAMP_ID);
        notification.setDescription(SignatureValidatorConstants.CHECK_SIGNATURETIMESTAMP_ID_MESSAGE);
        validationReport.getWarningOrErrorOrNote().add(notification);
    }

    private boolean isKelaSignature(Node node) {
        Node parent = node.getParentNode();
        if (parent.getNamespaceURI().equals("urn:hl7finland") && parent.getLocalName().equals("signature")) {
            return true;
        } else {
            return false;
        }
    }

    private XMLSignature buildXMLSignature(Element signatureElement, MDAValidation validationReport, String location) {
        Notification error;
        try {
            Init.init();
            return new XMLSignature(signatureElement, "");
        } catch (XMLSecurityException e) {
            error = buildErrorOnXMLSignatureBuildingException(location, e.getMessage());
        } catch (NullPointerException e) {
            error = buildErrorOnXMLSignatureBuildingException(location, "Cannot process ds:Signature element");
        }
        validationReport.getWarningOrErrorOrNote().add(error);
        return null;
    }

    private Notification buildErrorOnXMLSignatureBuildingException(String location, String message) {
        Notification error;
        error = newError();
        error.setIdentifiant(SignatureValidatorConstants.PRINT_DESCRIPTION);
        error.setLocation(location);
        error.setDescription(message);
        return error;
    }


    /**
     * @param xmlSignature
     * @param validationReport
     * @param location
     *
     * @return true if validation can keep on, false if we stop there
     */
    private boolean checkKeyInfo(XMLSignature xmlSignature, MDAValidation validationReport, String location) {
        xmlSignature.addResourceResolver(new OfflineResolver());
        KeyInfo ki = xmlSignature.getKeyInfo();
        if (ki == null) {
            Notification warning = newWarning();
            warning.setLocation(location);
            warning.setIdentifiant(SignatureValidatorConstants.CHECK_KEYINFO_KO);
            warning.setDescription(SignatureValidatorConstants.CHECK_KEYINFO_KO_MESSAGE);
            validationReport.getWarningOrErrorOrNote().add(warning);
            return false;
        } else {
            // X509Data element is required
            Notification notification = isX509DataPresent(ki, location);
            validationReport.getWarningOrErrorOrNote().add(notification);
            return true;
        }
    }

    private Notification isX509DataPresent(KeyInfo ki, String location) {
        Notification notification;
        if (ki.containsX509Data()) {
            notification = newNote();
        } else {
            notification = newWarning();
        }
        notification.setDescription(SignatureValidatorConstants.CHECK_X_509_DATA_PRESENT);
        notification.setDescription(SignatureValidatorConstants.CHECK_X_509_DATA_PRESENT_MESSAGE);
        notification.setLocation(location + "/ds:KeyInfo");
        return notification;
    }

    private static String getXpath(Node node) {
        Node parent = node.getParentNode();
        if (parent == null) {
            return "/" + node.getNodeName();
        }
        return getXpath(parent) + "/";
    }

    public static String getDetailedResultAsString(DetailedResult dr) {
        if (dr != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                JAXBContext jc = JAXBContext.newInstance("net.ihe.gazelle.validation");
                Marshaller m = jc.createMarshaller();
                m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
                m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                m.marshal(dr, baos);
            } catch (JAXBException e) {
                LOG.error(e.getMessage(), e);
            }
            return new String(baos.toByteArray(), StandardCharsets.UTF_8);
        }
        return null;
    }

}
