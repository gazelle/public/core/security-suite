package net.ihe.gazelle.signature.exception;

/**
 * <p>SignatureValidatorException class.</p>
 *
 * @author abe
 * @version 1.0: 19/03/18
 */

public class SignatureValidatorException extends Exception {

    public SignatureValidatorException(String message){
        super(message);
    }

    public SignatureValidatorException(String message, Exception cause){
        super(message, cause);
    }
}
