package net.ihe.gazelle.signature.validator.utils;


/**
 * <p>GazelleErrorHandler class.</p>
 *
 * @author abe
 * @version 1.0: 19/03/18
 */

import net.ihe.gazelle.validation.DocumentValidXSD;
import net.ihe.gazelle.validation.XSDMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class GazelleXSDErrorHandler implements ErrorHandler {

    private static final Logger LOG = LoggerFactory.getLogger(GazelleXSDErrorHandler.class);

    private static final String FATAL = "fatal";
    private static final String ERROR = "error";
    private static final String WARNING = "warning";
    private static final String XMLDSIG_SYSTEM_ID = "xmldsig-core-schema.xsd";

    private String systemId;
    private DocumentValidXSD xsdReport;

    private int countError = 0;
    private int countWarning = 0;

    public GazelleXSDErrorHandler() {
        xsdReport = new DocumentValidXSD();
    }

    @Override
    public void warning(SAXParseException e) throws SAXException {
        addXSDMessage(e, WARNING);
    }

    private void addXSDMessage(SAXParseException e, String level) {
        if (systemId != null && systemId.contains(XMLDSIG_SYSTEM_ID)) {
            XSDMessage warningMessage = new XSDMessage();
            warningMessage.setSeverity(level);
            warningMessage.setMessage(e.getMessage());
            warningMessage.setLineNumber(e.getLineNumber());
            warningMessage.setColumnNumber(e.getColumnNumber());
            xsdReport.getXSDMessage().add(warningMessage);
            if (WARNING.equals(level)){
                countWarning++;
            } else {
                countError++;
            }
        }
    }

    @Override
    public void error(SAXParseException e) throws SAXException {
        addXSDMessage(e, ERROR);
    }

    @Override
    public void fatalError(SAXParseException e) throws SAXException {
        addXSDMessage(e, FATAL);
    }

    public DocumentValidXSD getXsdReport() {
        setValidationSummary();
        return xsdReport;
    }

    private void setValidationSummary() {
        if (countError > 0){
            xsdReport.setResult("FAILED");
        } else {
            xsdReport.setResult("PASSED");
        }
        xsdReport.setNbOfErrors(Integer.toString(countError));
        xsdReport.setNbOfWarnings(Integer.toString(countWarning));
    }

    public void setCurrentSystemId(String inSystemId){
        if (inSystemId == null){
            this.systemId = "";
        } else {
            this.systemId = inSystemId;
        }
    }
}
