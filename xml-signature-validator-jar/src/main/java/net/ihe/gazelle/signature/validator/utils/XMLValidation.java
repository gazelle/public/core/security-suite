package net.ihe.gazelle.signature.validator.utils;

import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.signature.exception.SignatureValidatorException;
import net.ihe.gazelle.validation.DocumentValidXSD;
import org.apache.xml.security.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * <p>XMLValidation class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class XMLValidation {

    private Document documentToValidate;


    public DocumentValidXSD performSchemaValidation(String fileToValidate) throws SignatureValidatorException {

        DocumentBuilderFactory factory = createFactory();
        try {
            GazelleXSDErrorHandler xsdErrorHandler = new GazelleXSDErrorHandler();
            DocumentBuilder documentBuilder = createDocumentBuilder(factory, xsdErrorHandler);
            documentToValidate = documentBuilder.parse(new InputSource( new StringReader( fileToValidate ) ));
            return xsdErrorHandler.getXsdReport();
        } catch (IOException e) {
            throw new SignatureValidatorException(e.getMessage(), e);
        } catch (ParserConfigurationException|SAXException e) {
            throw new SignatureValidatorException("Error when building the DocumentBuilder object", e);
        }
    }

    private DocumentBuilder createDocumentBuilder(DocumentBuilderFactory factory, final GazelleXSDErrorHandler xsdErrorHandler) throws ParserConfigurationException, SAXException {

        final String signatureSchemaFile = getPathToXmldsigCoreSchema();
        DocumentBuilder documentBuilder = factory.newDocumentBuilder();
        documentBuilder.setErrorHandler(xsdErrorHandler);
        documentBuilder.setEntityResolver(new EntityResolver() {
            public InputSource resolveEntity(
                    String publicId, String systemId
            ) throws SAXException {
               xsdErrorHandler.setCurrentSystemId(systemId);
                if (systemId.endsWith("xmldsig-core-schema.xsd")) {
                    try {
                        URL url = new URL(signatureSchemaFile);
                        InputStream is = url.openStream();
                        return new InputSource(is);
                    } catch (IOException ex){
                        throw new SAXException(ex);
                    }
                } else {
                    return null;
                }
            }
        });
        return documentBuilder;
    }

    private static String getPathToXmldsigCoreSchema() {
        try {
            return PreferenceService.getString("xmldsig_core_schema_location");
        }catch (NullPointerException e){
            String location = System.getProperty("user.dir");
            location = location.concat("/src/test/resources/xmldsig-core-schema.xsd");
            return "file:///" + location;
        }
    }

    private static DocumentBuilderFactory createFactory() {
        DocumentBuilderFactory dbf =
                DocumentBuilderFactory.newInstance();

        dbf.setAttribute("http://apache.org/xml/features/validation/schema", Boolean.TRUE);
        dbf.setAttribute("http://apache.org/xml/features/dom/defer-node-expansion", Boolean.TRUE);
        dbf.setValidating(true);
        dbf.setAttribute("http://xml.org/sax/features/validation", Boolean.TRUE);
        dbf.setNamespaceAware(true);
        dbf.setAttribute("http://xml.org/sax/features/namespaces", Boolean.TRUE);
        dbf.setAttribute("http://apache.org/xml/properties/schema/external-schemaLocation",
                    Constants.SignatureSpecNS + " " + getPathToXmldsigCoreSchema());
        return dbf;
    }

    public Document getDocumentToValidate() {
        return documentToValidate;
    }
}
