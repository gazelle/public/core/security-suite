package net.ihe.gazelle.signature.validator.utils;

import net.ihe.gazelle.validation.model.ValidatorDescription;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>XMLSignatureValidatorDescription class.</p>
 *
 * @author abe
 * @version 1.0: 03/04/18
 */

public enum XMLSignatureValidatorDescription implements ValidatorDescription {

    DEFAULT("oid", "Digital Signature Validator", "IHE");


    String oid;
    String name;
    String descriminator;

    XMLSignatureValidatorDescription(String inOid, String inName, String inDescriminator) {
        this.oid = inOid;
        this.name = inName;
        this.descriminator = inDescriminator;
    }

    @Override
    public String getOid() {
        return this.oid;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getDescriminator() {
        return this.descriminator;
    }

    //FIXME


    @Override
    public String getRootElement() {
        return null;
    }

    @Override
    public String getNamespaceURI() {
        return null;
    }

    @Override
    public boolean extractPartToValidate() {
        return false;
    }

    public static XMLSignatureValidatorDescription getEntryByOidOrName(String value){
        for (XMLSignatureValidatorDescription description: values()){
            if (description.getOid().equals(value) || description.getName().equals(value)){
                return description;
            }
        }
        return null;
    }

    public static List<ValidatorDescription> getValidatorsForDescriminator(String descriminator){
        List<ValidatorDescription> validators = new ArrayList<ValidatorDescription>();
        for (XMLSignatureValidatorDescription validator: values()){
            if (descriminator == null || descriminator.isEmpty() || validator.getDescriminator().equals(descriminator)){
                validators.add(validator);
            }
        }
        return validators;
    }
}
