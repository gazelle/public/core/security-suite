
--
-- Rename am_active_participant_description foreign keys
--
ALTER TABLE am_active_participant_description 
 DROP CONSTRAINT IF EXISTS fk954fad2a13d699b1,
 ADD CONSTRAINT am_active_participant_description_networkaccesspointtypecode_id_fkey FOREIGN KEY (networkaccesspointtypecode_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_active_participant_description 
 DROP CONSTRAINT IF EXISTS fk954fad2a84e54d20,
 ADD CONSTRAINT am_active_participant_description_username_id_fkey FOREIGN KEY (username_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_active_participant_description 
 DROP CONSTRAINT IF EXISTS fk954fad2aa40ad1bd,
 ADD CONSTRAINT am_active_participant_description_networkaccesspointid_id_fkey FOREIGN KEY (networkaccesspointid_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_active_participant_description 
 DROP CONSTRAINT IF EXISTS fk954fad2aa83b61d0,
 ADD CONSTRAINT am_active_participant_description_userid_id_fkey FOREIGN KEY (userid_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_active_participant_description 
 DROP CONSTRAINT IF EXISTS fk954fad2ac0186a79,
 ADD CONSTRAINT am_active_participant_description_userisrequestor_id_fkey FOREIGN KEY (userisrequestor_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_active_participant_description 
 DROP CONSTRAINT IF EXISTS fk954fad2ac793b709,
 ADD CONSTRAINT am_active_participant_description_mediaidentifier_id_fkey FOREIGN KEY (mediaidentifier_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_active_participant_description 
 DROP CONSTRAINT IF EXISTS fk954fad2aed782f78,
 ADD CONSTRAINT am_active_participant_description_roleidcode_id_fkey FOREIGN KEY (roleidcode_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_active_participant_description 
 DROP CONSTRAINT IF EXISTS fk954fad2af7738cc3,
 ADD CONSTRAINT am_active_participant_description_alternativeuserid_id_fkey FOREIGN KEY (alternativeuserid_id) REFERENCES am_element_description(id) ;

--
-- Rename am_audit_message_specification foreign keys
--

ALTER TABLE am_audit_message_specification 
 DROP CONSTRAINT IF EXISTS fka58bbfd46c80db26,
 ADD CONSTRAINT am_audit_message_specification_auditsource_id_fkey FOREIGN KEY (auditsource_id) REFERENCES am_audit_source_description(id) ;
ALTER TABLE am_audit_message_specification 
 DROP CONSTRAINT IF EXISTS fka58bbfd47538881d,
 ADD CONSTRAINT am_audit_message_specification_event_id_fkey FOREIGN KEY (event_id) REFERENCES am_event_identification_desc(id) ;

--
-- Rename am_audit_message_specification_active_participants foreign keys
--

ALTER TABLE am_audit_message_specification_active_participants
  DROP CONSTRAINT IF EXISTS fk385b49f18997ce01,
  ADD CONSTRAINT am_audit_message_specification_active_participants_am_audit_message_specification_id_fkey FOREIGN KEY (am_audit_message_specification_id) REFERENCES am_audit_message_specification(id);
ALTER TABLE am_audit_message_specification_active_participants
  DROP CONSTRAINT IF EXISTS fk385b49f1f0d7ed5f,
  ADD CONSTRAINT am_audit_message_specification_active_participants_activeparticipants_id_fkey FOREIGN KEY (activeparticipants_id) REFERENCES am_active_participant_description(id) ;

--
-- Rename am_audit_message_specification_constraint_specifications foreig keys
--

ALTER TABLE am_audit_message_specification_constraint_specifications 
 DROP CONSTRAINT IF EXISTS fke275d9c78997ce01,
 ADD CONSTRAINT am_audit_message_specification_constraint_specifications_am_audit_message_specification_id_fkey FOREIGN KEY (am_audit_message_specification_id) REFERENCES am_audit_message_specification(id) ;
ALTER TABLE am_audit_message_specification_constraint_specifications 
 DROP CONSTRAINT IF EXISTS fke275d9c792c0e8a7,
 ADD CONSTRAINT am_audit_message_specification_constraint_specifications_extraconstraintspecifications_id_fkey FOREIGN KEY (extraconstraintspecifications_id) REFERENCES am_constraint_specification(id) ;

--
-- Rename am_audit_message_specification_participant_object_ident foreign keys
--

ALTER TABLE am_audit_message_specification_participant_object_ident 
 DROP CONSTRAINT IF EXISTS fkfda18c876da6787c,
 ADD CONSTRAINT am_audit_message_specification_participant_object_ident_participantobjectidentifications_id_fkey FOREIGN KEY (participantobjectidentifications_id) REFERENCES am_participant_object_identification_desc(id) ;
ALTER TABLE am_audit_message_specification_participant_object_ident 
 DROP CONSTRAINT IF EXISTS fkfda18c878997ce01,
 ADD CONSTRAINT am_audit_message_specification_participant_object_ident_am_audit_message_specification_id_fkey FOREIGN KEY (am_audit_message_specification_id) REFERENCES am_audit_message_specification(id) ;

--
-- Rename am_audit_source_description foreign keys
--

ALTER TABLE am_audit_source_description
  DROP CONSTRAINT IF EXISTS fk3c1f796f22dc2dd8,
  ADD CONSTRAINT am_audit_source_description_auditenterprisesiteid_id_fkey FOREIGN KEY (auditenterprisesiteid_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_audit_source_description
  DROP CONSTRAINT IF EXISTS fk3c1f796f2cde76b9,
  ADD CONSTRAINT am_audit_source_description_auditsourcetypecode_id_fkey FOREIGN KEY (auditsourcetypecode_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_audit_source_description
  DROP CONSTRAINT IF EXISTS fk3c1f796f3f3cbe48,
  ADD CONSTRAINT am_audit_source_description_auditsourcecodevalue_id_fkey FOREIGN KEY (auditsourcecodevalue_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_audit_source_description
  DROP CONSTRAINT IF EXISTS fk3c1f796fa35f14c5,
  ADD CONSTRAINT am_audit_source_description_auditsourceid_id_fkey FOREIGN KEY (auditsourceid_id) REFERENCES am_element_description(id) ;

--
-- Rename am_event_identification_desc foreign keys
--

ALTER TABLE am_event_identification_desc 
 DROP CONSTRAINT IF EXISTS fk72243e4a12b70155,
 ADD CONSTRAINT am_event_identification_desc_eventtypecode_id_fkey FOREIGN KEY (eventtypecode_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_event_identification_desc 
 DROP CONSTRAINT IF EXISTS fk72243e4a69961684,
 ADD CONSTRAINT am_event_identification_desc_purposeofuse_id_fkey FOREIGN KEY (purposeofuse_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_event_identification_desc 
 DROP CONSTRAINT IF EXISTS fk72243e4a6a50cd59,
 ADD CONSTRAINT am_event_identification_desc_eventactioncode_id_fkey FOREIGN KEY (eventactioncode_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_event_identification_desc 
 DROP CONSTRAINT IF EXISTS fk72243e4a9ae799b2,
 ADD CONSTRAINT am_event_identification_desc_eventoutcomedescription_id_fkey FOREIGN KEY (eventoutcomedescription_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_event_identification_desc 
 DROP CONSTRAINT IF EXISTS fk72243e4aa826a581,
 ADD CONSTRAINT am_event_identification_desc_eventdatetime_id_fkey FOREIGN KEY (eventdatetime_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_event_identification_desc 
 DROP CONSTRAINT IF EXISTS fk72243e4aceaac461,
 ADD CONSTRAINT am_event_identification_desc_eventid_id_fkey FOREIGN KEY (eventid_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_event_identification_desc 
 DROP CONSTRAINT IF EXISTS fk72243e4ae6dc0bf,
 ADD CONSTRAINT am_event_identification_desc_eventoutcomeindicator_id_fkey FOREIGN KEY (eventoutcomeindicator_id) REFERENCES am_element_description(id) ;

--
-- Rename am_participant_object_identification_desc foreign keys
--

ALTER TABLE am_participant_object_identification_desc 
 DROP CONSTRAINT IF EXISTS fk4528e0c1162f3501,
 ADD CONSTRAINT am_participant_object_identification_desc_instance_id_fkey FOREIGN KEY (instance_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_participant_object_identification_desc 
 DROP CONSTRAINT IF EXISTS fk4528e0c11f8ccd72,
 ADD CONSTRAINT am_participant_object_identification_desc_encrypted_id_fkey FOREIGN KEY (encrypted_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_participant_object_identification_desc 
 DROP CONSTRAINT IF EXISTS fk4528e0c130b0b312,
 ADD CONSTRAINT am_participant_object_identification_desc_accession_id_fkey FOREIGN KEY (accession_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_participant_object_identification_desc 
 DROP CONSTRAINT IF EXISTS fk4528e0c13fb498ec,
 ADD CONSTRAINT am_participant_object_identification_desc_participantobjectdescription_id_fkey FOREIGN KEY (participantobjectdescription_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_participant_object_identification_desc 
 DROP CONSTRAINT IF EXISTS fk4528e0c1510f229e,
 ADD CONSTRAINT am_participant_object_identification_desc_participantobjectcontainsstudy_id_fkey FOREIGN KEY (participantobjectcontainsstudy_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_participant_object_identification_desc 
 DROP CONSTRAINT IF EXISTS fk4528e0c16943bad2,
 ADD CONSTRAINT am_participant_object_identification_desc_sopclass_id_fkey FOREIGN KEY (sopclass_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_participant_object_identification_desc 
 DROP CONSTRAINT IF EXISTS fk4528e0c173c8d5c9,
 ADD CONSTRAINT am_participant_object_identification_desc_participantobjectid_id_fkey FOREIGN KEY (participantobjectid_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_participant_object_identification_desc 
 DROP CONSTRAINT IF EXISTS fk4528e0c17abd0158,
 ADD CONSTRAINT am_participant_object_identification_desc_numberofinstances_id_fkey FOREIGN KEY (numberofinstances_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_participant_object_identification_desc 
 DROP CONSTRAINT IF EXISTS fk4528e0c18655bd42,
 ADD CONSTRAINT am_participant_object_identification_desc_participantobjectidtypecode_id_fkey FOREIGN KEY (participantobjectidtypecode_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_participant_object_identification_desc 
 DROP CONSTRAINT IF EXISTS fk4528e0c19fc00746,
 ADD CONSTRAINT am_participant_object_identification_desc_anonymized_id_fkey FOREIGN KEY (anonymized_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_participant_object_identification_desc 
 DROP CONSTRAINT IF EXISTS fk4528e0c1a2e5a6d9,
 ADD CONSTRAINT am_participant_object_identification_desc_participantobjectname_id_fkey FOREIGN KEY (participantobjectname_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_participant_object_identification_desc 
 DROP CONSTRAINT IF EXISTS fk4528e0c1b0cc64bd,
 ADD CONSTRAINT am_participant_object_identification_desc_participantobjecttypecode_id_fkey FOREIGN KEY (participantobjecttypecode_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_participant_object_identification_desc 
 DROP CONSTRAINT IF EXISTS fk4528e0c1bacd3fd3,
 ADD CONSTRAINT am_participant_object_identification_desc_participantobjectdetail_id_fkey FOREIGN KEY (participantobjectdetail_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_participant_object_identification_desc 
 DROP CONSTRAINT IF EXISTS fk4528e0c1be5a2e20,
 ADD CONSTRAINT am_participant_object_identification_desc_participantobjectquery_id_fkey FOREIGN KEY (participantobjectquery_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_participant_object_identification_desc 
 DROP CONSTRAINT IF EXISTS fk4528e0c1c87bad07,
 ADD CONSTRAINT am_participant_object_identification_desc_participantobjecttypecoderole_id_fkey FOREIGN KEY (participantobjecttypecoderole_id) REFERENCES am_element_description(id) ;
ALTER TABLE am_participant_object_identification_desc 
 DROP CONSTRAINT IF EXISTS fk4528e0c1deff1d10,
 ADD CONSTRAINT am_participant_object_identification_desc_mpps_id_fkey FOREIGN KEY (mpps_id) REFERENCES am_element_description(id) ;

--
-- Rename atna_audited_event foreign keys
--

ALTER TABLE atna_audited_event 
 DROP CONSTRAINT IF EXISTS fk8e53f07c4331afb6,
 ADD CONSTRAINT atna_audited_event_atna_questionnaire_id_fkey FOREIGN KEY (atna_questionnaire_id) REFERENCES atna_questionnaire(id) ;

--
-- Rename atna_network_communication foreign keys
--

ALTER TABLE atna_network_communication 
 DROP CONSTRAINT IF EXISTS fkd6d3ad2c4331afb6,
 ADD CONSTRAINT atna_network_communication_atna_questionnaire_id_fkey FOREIGN KEY (atna_questionnaire_id) REFERENCES atna_questionnaire(id) ;

--
-- Rename atna_questionnaire foreign keys
--

ALTER TABLE atna_questionnaire 
 DROP CONSTRAINT IF EXISTS fk95bebe4a73b1fcca,
 ADD CONSTRAINT atna_questionnaire_testing_session_id_fkey FOREIGN KEY (testing_session_id) REFERENCES atna_testing_session(id) ;

--
-- Rename atna_tls_test foreign keys
--

ALTER TABLE atna_tls_test 
 DROP CONSTRAINT IF EXISTS fk9879e44f4331afb6,
 ADD CONSTRAINT atna_tls_test_atna_questionnaire_id_fkey FOREIGN KEY (atna_questionnaire_id) REFERENCES atna_questionnaire(id) ;

--
-- Rename mbv_assertion foreign keys
--

ALTER TABLE mbv_assertion 
 DROP CONSTRAINT IF EXISTS fk16ce89c47778d25e,
 ADD CONSTRAINT mbv_assertion_constraint_id_fkey FOREIGN KEY (constraint_id) REFERENCES mbv_constraint(id) ;

--
-- Rename mbv_class_type foreign keys
--

ALTER TABLE mbv_class_type 
 DROP CONSTRAINT IF EXISTS fk350aaf1f8e9bb0b6,
 ADD CONSTRAINT mbv_class_type_package_id_fkey FOREIGN KEY (package_id) REFERENCES mbv_package(id) ;
ALTER TABLE mbv_class_type 
 DROP CONSTRAINT IF EXISTS fk350aaf1f9ba1deeb,
 ADD CONSTRAINT mbv_class_type_documentation_spec_id_fkey FOREIGN KEY (documentation_spec_id) REFERENCES mbv_documentation_spec(id) ;

--
-- Rename mbv_constraint foreign keys
--

ALTER TABLE mbv_constraint 
 DROP CONSTRAINT IF EXISTS fk3afefb5bc668fe56,
 ADD CONSTRAINT mbv_constraint_classtype_id_fkey FOREIGN KEY (classtype_id) REFERENCES mbv_class_type(id) ;

--
-- Rename mbv_standards foreign keys
--

ALTER TABLE mbv_standards 
 DROP CONSTRAINT IF EXISTS fk41dd2c188e9bb0b6,
 ADD CONSTRAINT mbv_standards_package_id_fkey FOREIGN KEY (package_id) REFERENCES mbv_package(id) ;

--
-- Rename pxy_abstract_message foreign keys
--

ALTER TABLE pxy_abstract_message 
 DROP CONSTRAINT IF EXISTS fk439433f8caf208ed,
 ADD CONSTRAINT pxy_abstract_message_connection_id_fkey FOREIGN KEY (connection_id) REFERENCES pxy_connection(id) ;
ALTER TABLE pxy_abstract_message 
 DROP CONSTRAINT IF EXISTS fk439433f8de980cfc,
 ADD CONSTRAINT pxy_abstract_message_matchingmessage_id_fkey FOREIGN KEY (matchingmessage_id) REFERENCES pxy_abstract_message(id) ;

--
-- Rename pxy_abstract_message_attributesforcommandset foreign keys
--

ALTER TABLE pxy_abstract_message_attributesforcommandset 
 DROP CONSTRAINT IF EXISTS fkf75b62202ed380,
 ADD CONSTRAINT pxy_abstract_message_attributesforcommandset_pxy_abstract_message_id_fkey FOREIGN KEY (pxy_abstract_message_id) REFERENCES pxy_abstract_message(id) ;

--
-- Rename tls_certificate foreign keys
--

ALTER TABLE tls_certificate 
 DROP CONSTRAINT IF EXISTS fk7524c05314bff85,
 ADD CONSTRAINT tls_certificate_privatekey_id_fkey FOREIGN KEY (privatekey_id) REFERENCES tls_certificate_key(id) ;
ALTER TABLE tls_certificate 
 DROP CONSTRAINT IF EXISTS fk7524c0532bc6010f,
 ADD CONSTRAINT tls_certificate_publickey_id_fkey FOREIGN KEY (publickey_id) REFERENCES tls_certificate_key(id) ;
ALTER TABLE tls_certificate 
 DROP CONSTRAINT IF EXISTS fk7524c0535694cbcf,
 ADD CONSTRAINT tls_certificate_request_id_fkey FOREIGN KEY (request_id) REFERENCES tls_certificate_request(id) ;
ALTER TABLE tls_certificate 
 DROP CONSTRAINT IF EXISTS fk7524c053851e162e,
 ADD CONSTRAINT tls_certificate_certificatex509_id_fkey FOREIGN KEY (certificatex509_id) REFERENCES tls_certificate_x509(id) ;
ALTER TABLE tls_certificate 
 DROP CONSTRAINT IF EXISTS fk7524c053e45e63b9,
 ADD CONSTRAINT tls_certificate_certificateauthority_id_fkey FOREIGN KEY (certificateauthority_id) REFERENCES tls_certificate(id) ;

--
-- Rename tls_certificate_request foreign keys
--

ALTER TABLE tls_certificate_request 
 DROP CONSTRAINT IF EXISTS fk447356a314bff85,
 ADD CONSTRAINT tls_certificate_request_privatekey_id_fkey FOREIGN KEY (privatekey_id) REFERENCES tls_certificate_key(id) ;
ALTER TABLE tls_certificate_request 
 DROP CONSTRAINT IF EXISTS fk447356a32bc6010f,
 ADD CONSTRAINT tls_certificate_request_publickey_id_fkey FOREIGN KEY (publickey_id) REFERENCES tls_certificate_key(id) ;
ALTER TABLE tls_certificate_request 
 DROP CONSTRAINT IF EXISTS fk447356a3e45e63b9,
 ADD CONSTRAINT tls_certificate_request_certificateauthority_id_fkey FOREIGN KEY (certificateauthority_id) REFERENCES tls_certificate(id) ;

--
-- Rename tls_test_actor foreign keys
--

ALTER TABLE tls_test_actor 
 DROP CONSTRAINT IF EXISTS fk27d4d5cc34c881ce,
 ADD CONSTRAINT tls_test_actor_certificate_id_fkey FOREIGN KEY (certificate_id) REFERENCES tls_certificate(id) ;

--
-- Rename tls_test_actor_tls_certificate foreign keys
--

ALTER TABLE tls_test_actor_trusted_issuers 
 DROP CONSTRAINT IF EXISTS fkc72dd3e032c873ab,
 ADD CONSTRAINT tls_test_actor_trusted_issuers_certificate_id_fkey FOREIGN KEY (certificate_id) REFERENCES tls_certificate(id) ;
ALTER TABLE tls_test_actor_trusted_issuers 
 DROP CONSTRAINT IF EXISTS fkc72dd3e0d4deaa18,
 ADD CONSTRAINT tls_test_actor_trusted_issuers_tls_test_actor_id_fkey FOREIGN KEY (tls_test_actor_id) REFERENCES tls_test_actor(id) ;

--
-- Rename tls_test_actor_tls_test_ciphersuite foreign keys
--

ALTER TABLE tls_test_actor_tls_test_ciphersuite 
 DROP CONSTRAINT IF EXISTS fk6c894af7d4deaa18,
 ADD CONSTRAINT tls_test_actor_tls_test_ciphersuite_tls_test_actor_id_fkey FOREIGN KEY (tls_test_actor_id) REFERENCES tls_test_actor(id) ;

--
-- Rename tls_test_actor_tls_test_protocol foreign keys
--

ALTER TABLE tls_test_actor_tls_test_protocol 
 DROP CONSTRAINT IF EXISTS fk545ba3aed4deaa18,
 ADD CONSTRAINT tls_test_actor_tls_test_protocol_tls_test_actor_id_fkey FOREIGN KEY (tls_test_actor_id) REFERENCES tls_test_actor(id) ;

--
-- Rename tls_test_case foreign keys
--

ALTER TABLE tls_test_case 
 DROP CONSTRAINT IF EXISTS fk95eef879399ff94f,
 ADD CONSTRAINT tls_test_case_actor_id_fkey FOREIGN KEY (actor_id) REFERENCES tls_test_actor(id) ;
ALTER TABLE tls_test_case 
 DROP CONSTRAINT IF EXISTS fk95eef87993ae5383,
 ADD CONSTRAINT tls_test_case_dataset_id_fkey FOREIGN KEY (dataset_id) REFERENCES tls_test_data_set(id) ;

--
-- Rename tls_test_case_expectedalertdescriptions foreign keys
--

ALTER TABLE tls_test_case_expectedalertdescriptions 
 DROP CONSTRAINT IF EXISTS fk2134ab55a341f988,
 ADD CONSTRAINT tls_test_case_expectedalertdescriptions_tls_test_case_id_fkey FOREIGN KEY (tls_test_case_id) REFERENCES tls_test_case(id) ;

--
-- Rename tls_test_connection foreign keys
--

ALTER TABLE tls_test_connection 
 DROP CONSTRAINT IF EXISTS fk981a216748fd5413,
 ADD CONSTRAINT tls_test_connection_tls_actor_id_fkey FOREIGN KEY (tls_actor_id) REFERENCES tls_test_actor(id) ;

--
-- Rename tls_test_connection_data_item_aliases_item foreign keys
--

ALTER TABLE tls_test_connection_data_item_aliases_item 
 DROP CONSTRAINT IF EXISTS fkd3475e372619767,
 ADD CONSTRAINT tls_test_connection_data_item_aliases_item_join_key_type_fkey FOREIGN KEY (join_key_type) REFERENCES tls_test_connection_data_item(id) ;
ALTER TABLE tls_test_connection_data_item_aliases_item 
 DROP CONSTRAINT IF EXISTS fkd3475e38dfe9646,
 ADD CONSTRAINT tls_test_connection_data_item_aliases_item_join_issuer_fkey FOREIGN KEY (join_issuer) REFERENCES tls_test_connection_data_item(id) ;

--
-- Rename tls_test_connection_tls_test_connection_data_item foreign keys
--

ALTER TABLE tls_test_connection_tls_test_connection_data_item 
 DROP CONSTRAINT IF EXISTS fk4e62e8d82240c33c,
 ADD CONSTRAINT tls_test_connection_tls_test_connection_data_item_tls_test_connection_id_fkey FOREIGN KEY (tls_test_connection_id) REFERENCES tls_test_connection(id) ;
ALTER TABLE tls_test_connection_tls_test_connection_data_item 
 DROP CONSTRAINT IF EXISTS fk4e62e8d8e763b740,
 ADD CONSTRAINT tls_test_connection_tls_test_connection_data_item_items_id_fkey FOREIGN KEY (items_id) REFERENCES tls_test_connection_data_item(id) ;

--
-- Rename tls_test_data_set foreign keys
--

ALTER TABLE tls_test_data_set 
 DROP CONSTRAINT IF EXISTS fk5e6a71634c881ce,
 ADD CONSTRAINT tls_test_data_set_certificate_id_fkey FOREIGN KEY (certificate_id) REFERENCES tls_certificate(id) ;

--
-- Rename tls_test_data_set_tls_test_ciphersuite foreign keys
--

ALTER TABLE tls_test_data_set_tls_test_ciphersuite 
 DROP CONSTRAINT IF EXISTS fkc31c0bedc617f425,
 ADD CONSTRAINT tls_test_data_set_tls_test_ciphersuite_tls_test_data_set_id_fkey FOREIGN KEY (tls_test_data_set_id) REFERENCES tls_test_data_set(id) ;

--
-- Rename tls_test_data_set_tls_test_protocol foreign keys
--

ALTER TABLE tls_test_data_set_tls_test_protocol 
 DROP CONSTRAINT IF EXISTS fkbd3b7678c617f425,
 ADD CONSTRAINT tls_test_data_set_tls_test_protocol_tls_test_data_set_id_fkey FOREIGN KEY (tls_test_data_set_id) REFERENCES tls_test_data_set(id) ;

--
-- Rename tls_test_data_set_trusted_issuers foreign keys
--

ALTER TABLE tls_test_data_set_trusted_issuers 
 DROP CONSTRAINT IF EXISTS fk6297d12a69ffe2,
 ADD CONSTRAINT tls_test_data_set_trusted_issuers_certificate_id_fkey FOREIGN KEY (certificate_id) REFERENCES tls_certificate(id) ;
ALTER TABLE tls_test_data_set_trusted_issuers 
 DROP CONSTRAINT IF EXISTS fk6297d12ac617f425,
 ADD CONSTRAINT tls_test_data_set_trusted_issuers_tls_test_data_set_id_fkey FOREIGN KEY (tls_test_data_set_id) REFERENCES tls_test_data_set(id) ;

--
-- Rename tls_test_instance foreign keys
--

ALTER TABLE tls_test_instance 
 DROP CONSTRAINT IF EXISTS fkbc55455e8b57a0c4,
 ADD CONSTRAINT tls_test_instance_test_case_id_fkey FOREIGN KEY (test_case_id) REFERENCES tls_test_case(id) ;
ALTER TABLE tls_test_instance 
 DROP CONSTRAINT IF EXISTS fkbc55455ebad35e65,
 ADD CONSTRAINT tls_test_instance_connection_id_fkey FOREIGN KEY (connection_id) REFERENCES tls_test_connection(id) ;

--
-- Rename tls_test_suite_instance foreign keys
--

ALTER TABLE tls_test_suite_instance 
 DROP CONSTRAINT IF EXISTS fk25b5fc05705bd2f0,
 ADD CONSTRAINT tls_test_suite_instance_test_suite_id_fkey FOREIGN KEY (test_suite_id) REFERENCES tls_test_suite(id) ;

--
-- Rename tls_test_suite_instance_tls_test_instance foreign keys
--

ALTER TABLE tls_test_suite_instance_tls_test_instance 
 DROP CONSTRAINT IF EXISTS fk6d7b13a4a1744d6b,
 ADD CONSTRAINT tls_test_suite_instance_tls_test_instance_tls_test_suite_instance_id_fkey FOREIGN KEY (tls_test_suite_instance_id) REFERENCES tls_test_suite_instance(id) ;
ALTER TABLE tls_test_suite_instance_tls_test_instance 
 DROP CONSTRAINT IF EXISTS fk6d7b13a4e57cd8da,
 ADD CONSTRAINT tls_test_suite_instance_tls_test_instance_testinstances_id_fkey FOREIGN KEY (testinstances_id) REFERENCES tls_test_instance(id) ;

--
-- Rename tls_test_suite_tls_test_case foreign keys
--

ALTER TABLE tls_test_suite_tls_test_case 
 DROP CONSTRAINT IF EXISTS fk1ef6b70955bc92ac,
 ADD CONSTRAINT tls_test_suite_tls_test_case_tls_test_suite_id_fkey FOREIGN KEY (tls_test_suite_id) REFERENCES tls_test_suite(id) ;
ALTER TABLE tls_test_suite_tls_test_case 
 DROP CONSTRAINT IF EXISTS fk1ef6b709df082d10,
 ADD CONSTRAINT tls_test_suite_tls_test_case_testcases_id_fkey FOREIGN KEY (testcases_id) REFERENCES tls_test_case(id) ;

--
-- CONSOLE OUTPUT, long fkey names must be re-written :
--
-- NOTICE:  identifier "am_active_participant_description_networkaccesspointtypecode_id_fkey" will be truncated to "am_active_participant_description_networkaccesspointtypecode_id"
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- NOTICE:  identifier "am_audit_message_specification_active_participants_am_audit_message_specification_id_fkey" will be truncated to "am_audit_message_specification_active_participants_am_audit_mes"
-- ALTER TABLE
-- NOTICE:  identifier "am_audit_message_specification_active_participants_activeparticipants_id_fkey" will be truncated to "am_audit_message_specification_active_participants_activepartic"
-- ALTER TABLE
-- NOTICE:  identifier "am_audit_message_specification_constraint_specifications_am_audit_message_specification_id_fkey" will be truncated to "am_audit_message_specification_constraint_specifications_am_aud"
-- ALTER TABLE
-- NOTICE:  identifier "am_audit_message_specification_constraint_specifications_extraconstraintspecifications_id_fkey" will be truncated to "am_audit_message_specification_constraint_specifications_extrac"
-- ALTER TABLE
-- NOTICE:  identifier "am_audit_message_specification_participant_object_ident_participantobjectidentifications_id_fkey" will be truncated to "am_audit_message_specification_participant_object_ident_partici"
-- ALTER TABLE
-- NOTICE:  identifier "am_audit_message_specification_participant_object_ident_am_audit_message_specification_id_fkey" will be truncated to "am_audit_message_specification_participant_object_ident_am_audi"
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- NOTICE:  identifier "am_participant_object_identification_desc_participantobjectdescription_id_fkey" will be truncated to "am_participant_object_identification_desc_participantobjectdesc"
-- ALTER TABLE
-- NOTICE:  identifier "am_participant_object_identification_desc_participantobjectcontainsstudy_id_fkey" will be truncated to "am_participant_object_identification_desc_participantobjectcont"
-- ALTER TABLE
-- ALTER TABLE
-- NOTICE:  identifier "am_participant_object_identification_desc_participantobjectid_id_fkey" will be truncated to "am_participant_object_identification_desc_participantobjectid_i"
-- ALTER TABLE
-- NOTICE:  identifier "am_participant_object_identification_desc_numberofinstances_id_fkey" will be truncated to "am_participant_object_identification_desc_numberofinstances_id_"
-- ALTER TABLE
-- NOTICE:  identifier "am_participant_object_identification_desc_participantobjectidtypecode_id_fkey" will be truncated to "am_participant_object_identification_desc_participantobjectidty"
-- ALTER TABLE
-- ALTER TABLE
-- NOTICE:  identifier "am_participant_object_identification_desc_participantobjectname_id_fkey" will be truncated to "am_participant_object_identification_desc_participantobjectname"
-- ALTER TABLE
-- NOTICE:  identifier "am_participant_object_identification_desc_participantobjecttypecode_id_fkey" will be truncated to "am_participant_object_identification_desc_participantobjecttype"
-- ALTER TABLE
-- NOTICE:  identifier "am_participant_object_identification_desc_participantobjectdetail_id_fkey" will be truncated to "am_participant_object_identification_desc_participantobjectdeta"
-- ALTER TABLE
-- NOTICE:  identifier "am_participant_object_identification_desc_participantobjectquery_id_fkey" will be truncated to "am_participant_object_identification_desc_participantobjectquer"
-- ALTER TABLE
-- NOTICE:  identifier "am_participant_object_identification_desc_participantobjecttypecoderole_id_fkey" will be truncated to "am_participant_object_identification_desc_participantobjecttype"
-- ERROR:  constraint "am_participant_object_identification_desc_participantobjecttype" for relation "am_participant_object_identification_desc" already exists
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- NOTICE:  identifier "pxy_abstract_message_attributesforcommandset_pxy_abstract_message_id_fkey" will be truncated to "pxy_abstract_message_attributesforcommandset_pxy_abstract_messa"
-- ALTER TABLE
-- ALTER TABLE
-- NOTICE:  constraint "fk7524c0532bc6010f" of relation "tls_certificate" does not exist, skipping
-- ERROR:  constraint "tls_certificate_publickey_id_fkey" for relation "tls_certificate" already exists
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- NOTICE:  identifier "tls_test_connection_tls_test_connection_data_item_tls_test_connection_id_fkey" will be truncated to "tls_test_connection_tls_test_connection_data_item_tls_test_conn"
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- NOTICE:  identifier "tls_test_data_set_tls_test_ciphersuite_tls_test_data_set_id_fkey" will be truncated to "tls_test_data_set_tls_test_ciphersuite_tls_test_data_set_id_fke"
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- NOTICE:  identifier "tls_test_suite_instance_tls_test_instance_tls_test_suite_instance_id_fkey" will be truncated to "tls_test_suite_instance_tls_test_instance_tls_test_suite_instan"
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
-- ALTER TABLE
