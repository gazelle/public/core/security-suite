INSERT INTO xua_ancillary_transaction (id, keyword, name, description, path_to_soap_body, soap_action)
VALUES (nextval('xua_ancillary_transaction_id_seq'), 'ITI-55', 'Cross Community Patient Discovery [ITI-55]', 'Cross Community Patient Discovery request for patient Xavier XUA', 'ca_prpain201305uv02.xml', 'urn:hl7-org:v3:PRPA_IN201305UV02:CrossGatewayPatientDiscovery');
INSERT INTO xua_ancillary_transaction (id, keyword, name, description, path_to_soap_body, soap_action)
VALUES (nextval('xua_ancillary_transaction_id_seq'), 'ITI-38', 'Cross Gateway Query [ITI-38]', 'Query stable documents for patient Xavier XUA', 'ca_iti38.xml', 'urn:ihe:iti:2007:CrossGatewayQuery');
INSERT INTO xua_ancillary_transaction (id, keyword, name, description, path_to_soap_body, soap_action)
VALUES (nextval('xua_ancillary_transaction_id_seq'), 'ITI-18', 'Registry Stored Query [ITI-18]', 'Query stable documents for patient Xavier XUA', 'ca_iti18.xml', 'urn:ihe:iti:2007:RegistryStoredQuery');
INSERT INTO xua_ancillary_transaction (id, keyword, name, description, path_to_soap_body, soap_action)
VALUES (nextval('xua_ancillary_transaction_id_seq'), 'ITI-43', 'Retrieve Document Set [ITI-43]', 'Retrieve document with uniqueId for patient Xavier XUA', 'ca_iti43.xml', 'urn:ihe:iti:2007:RetrieveDocumentSet');
INSERT INTO xua_ancillary_transaction (id, keyword, name, description, path_to_soap_body, soap_action)
VALUES (nextval('xua_ancillary_transaction_id_seq'), 'ITI-47', 'Patient Demographic Query HL7V3 [ITI-47]', 'PDQV3 request for patient Xavier XUA', 'ca_prpain201305uv02.xml', 'urn:hl7-org:v3:PRPA_IN201305UV02');
INSERT INTO xua_ancillary_transaction (id, keyword, name, description, path_to_soap_body, soap_action)
VALUES (nextval('xua_ancillary_transaction_id_seq'), 'ITI-45', 'PIXV3 Query [ITI-45]', 'PIXV3 Query for patient Richard Rodman in CATT-SILVER Domain', 'ca_prpain201309uv02.xml', 'urn:hl7-org:v3:PRPA_IN201309UV02');



-- valid
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'valid', 'Send a valid XUA assertion', 'NO_SOAP_FAULT', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'valid'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'valid'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'valid'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'valid'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'valid', 'connectathon', 'https://conformity-assessment-testing.ihe-europe.net/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'valid'));

-- notyetvalid
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'notyetvalid', 'Send an assertion which will be valid in one day', 'NO_SOAP_FAULT', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'notyetvalid'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'notyetvalid'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'notyetvalid'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'notyetvalid'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'notyetvalid', 'connectathon', 'https://conformity-assessment-testing.ihe-europe.net/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'notyetvalid'));

-- expired
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'expired', 'Send an assertion which is no more valid since one day', 'NO_SOAP_FAULT', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'expired'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'expired'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'expired'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'expired'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'expired', 'connectathon', 'https://conformity-assessment-testing.ihe-europe.net/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'expired'));

-- unsigned
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'unsigned', 'Send an assertion which is not signed (missing ds:Signature)', 'NO_SOAP_FAULT', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'unsigned'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'unsigned'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'unsigned'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'unsigned'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'unsigned', 'connectathon', 'https://conformity-assessment-testing.ihe-europe.net/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'unsigned'));

-- unknownaudience
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'unknownaudience', 'Send an assertion where saml2:Conditions/saml2:AudienceRestriction/saml2:Audience does not match the AppliesTo element of the request', 'NO_SOAP_FAULT', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'invalidsignature'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'invalidsignature'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'invalidsignature'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'invalidsignature'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'unknownaudience', 'connectathon', 'https://conformity-assessment-testing.ihe-europe.net/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'invalidsignature'));

-- withauthzconsent
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'withauthzconsent', 'Send an assertion  where attributes statement “urn:ihe:iti:bppc:2007:docid” and “urn:oasis:names:tc:xacml:2.0:resource:resource-id” are present', 'NO_SOAP_FAULT', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'withauthzconsent'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'withauthzconsent'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'withauthzconsent'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'withauthzconsent'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'withauthzconsent', 'connectathon', 'https://conformity-assessment-testing.ihe-europe.net/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'withauthzconsent'));


UPDATE xua_service_provider_test_case set expected_result_details = 'Your system is expected to send back a valid answer' where keyword = 'valid';
UPDATE xua_service_provider_test_case set expected_result_details = 'Your system shall not grand access to the resource. The response from your system shall not give indication that the request failed because of the assertion validation failure.' where keyword != 'valid';
UPDATE xua_service_provider_test_case set available = true;
