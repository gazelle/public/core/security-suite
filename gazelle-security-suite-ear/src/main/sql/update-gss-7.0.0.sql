INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'cas_enabled', 'true');
DELETE FROM app_configuration WHERE variable = 'cas_enable';
DELETE FROM app_configuration WHERE variable = 'second_cas_enabled';
DELETE FROM app_configuration WHERE variable = 'second_cas_url';
DELETE FROM app_configuration WHERE variable = 'second_cas_name';
DELETE FROM app_configuration WHERE variable = 'second_cas_keyword';
DELETE FROM app_configuration WHERE variable = 'second_tm_application_url';
DELETE FROM app_configuration WHERE variable = 'second_tm_message_ws';
DELETE FROM app_configuration WHERE variable = 'second_tm_pki_admins';

-- [PROXY-229] Filter secured or not secured messages
ALTER TABLE public.pxy_connection ADD COLUMN secured BOOLEAN NOT NULL DEFAULT FALSE;
UPDATE public.pxy_connection set secured = false;