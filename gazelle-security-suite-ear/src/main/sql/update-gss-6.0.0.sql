DELETE FROM app_configuration WHERE variable='main_cas_url';
DELETE FROM app_configuration WHERE variable='second_cas_url';


ALTER TABLE atna_questionnaire ALTER COLUMN last_modified_date TYPE timestamp with time zone ;

ALTER TABLE pki_certificate_request ADD COLUMN IF NOT EXISTS subject_alternative_name VARCHAR(511);

-- from mbval-documentation --
ALTER TABLE mbv_package ADD COLUMN IF NOT EXISTS last_updated timestamp with time zone;
UPDATE mbv_package SET last_updated=current_timestamp;