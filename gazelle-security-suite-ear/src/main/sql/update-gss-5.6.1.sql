ALTER TABLE xua_service_provider_test_case add available boolean;
UPDATE xua_service_provider_test_case set available = true;
ALTER TABLE "xua_service_provider_test_case" RENAME COLUMN "expected_result" TO "expected_result_details";
ALTER TABLE "xua_service_provider_test_case" ADD COLUMN "active" BOOLEAN;
UPDATE xua_service_provider_test_case set active = true;
ALTER TABLE "xua_service_provider_test_case" ADD COLUMN "expected_result" VARCHAR(255);
ALTER TABLE "xua_service_provider_test_instance" ALTER COLUMN "test_status" TYPE VARCHAR(255);
ALTER TABLE "xua_service_provider_test_instance" ADD COLUMN "reason_for_failure" text;
UPDATE xua_service_provider_test_case SET expected_result = 'UNSPECIFIED'; -- unspecified by default
