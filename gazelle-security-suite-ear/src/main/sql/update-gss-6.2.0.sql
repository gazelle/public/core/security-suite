-- Update CertificateValidatorType from ORDINAL to STRING

-- TLS Simulators
-- Create a new column for migrating enum string values
ALTER TABLE tls_simulator ADD COLUMN new_validator VARCHAR(64);
-- Migrate ordinal values to String in the new column
UPDATE tls_simulator SET new_validator='EPSOS_VPN_CLIENT' WHERE validator=0;
UPDATE tls_simulator SET new_validator='EPSOS_VPN_SERVER' WHERE validator=1;
UPDATE tls_simulator SET new_validator='EPSOS_SERVICE_CONSUMER' WHERE validator=2;
UPDATE tls_simulator SET new_validator='EPSOS_SERVICE_PROVIDER' WHERE validator=3;
UPDATE tls_simulator SET new_validator='EPSOS_NCP_SIGNATURE' WHERE validator=4;
UPDATE tls_simulator SET new_validator='EPSOS_OCSP_RESPONDER' WHERE validator=5;
UPDATE tls_simulator SET new_validator='TLS_SERVER' WHERE validator=6;
UPDATE tls_simulator SET new_validator='TLS_CLIENT' WHERE validator=7;
UPDATE tls_simulator SET new_validator='CA' WHERE validator=8;
UPDATE tls_simulator SET new_validator='EPSOS_VPN_V3' WHERE validator=9;
UPDATE tls_simulator SET new_validator='EPSOS_TLS_V3' WHERE validator=10;
UPDATE tls_simulator SET new_validator='EPSOS_NCP_V3' WHERE validator=11;
UPDATE tls_simulator SET new_validator='EPSOS_SEAL_V4' WHERE validator=12;
UPDATE tls_simulator SET new_validator='EPSOS_TLS_V4' WHERE validator=13;
-- Drop the old column
ALTER TABLE tls_simulator DROP COLUMN validator ;
-- Rename the new column to match the entity declaration
ALTER TABLE tls_simulator RENAME COLUMN new_validator TO validator ;

