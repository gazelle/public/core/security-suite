DROP SEQUENCE tls_keyword_generator_id_seq ;

-- TlsTestDataSet update

ALTER TABLE tls_test_data_set_tls_test_ciphersuite ADD element integer ;
UPDATE tls_test_data_set_tls_test_ciphersuite SET element = (SELECT ciphersuite FROM tls_test_ciphersuite WHERE tls_test_ciphersuite.id = tls_test_data_set_tls_test_ciphersuite.ciphersuites_id) ;
ALTER TABLE tls_test_data_set_tls_test_ciphersuite DROP CONSTRAINT fkc31c0bed623f8556 ; -- foreign key to tls_test_ciphersuite(id)
ALTER TABLE tls_test_data_set_tls_test_ciphersuite DROP COLUMN ciphersuites_id ;
ALTER TABLE tls_test_data_set_tls_test_ciphersuite RENAME COLUMN element TO ciphersuite ;

ALTER TABLE tls_test_data_set_tls_test_protocol ADD element integer ;
UPDATE tls_test_data_set_tls_test_protocol SET element = (SELECT protocol FROM tls_test_protocol WHERE tls_test_protocol.id = tls_test_data_set_tls_test_protocol.protocols_id) ;
ALTER TABLE tls_test_data_set_tls_test_protocol DROP CONSTRAINT fkbd3b7678bd1ee9c2; -- foreign key to tls_test_protocol(id)
ALTER TABLE tls_test_data_set_tls_test_protocol DROP COLUMN protocols_id ;
ALTER TABLE tls_test_data_set_tls_test_protocol RENAME COLUMN element TO protocol ;

-- TlsActor update

ALTER TABLE tls_test_actor_tls_test_ciphersuite ADD element integer ;
UPDATE tls_test_actor_tls_test_ciphersuite SET element = (SELECT ciphersuite FROM tls_test_ciphersuite WHERE tls_test_ciphersuite.id = tls_test_actor_tls_test_ciphersuite.ciphersuites_id) ;
ALTER TABLE tls_test_actor_tls_test_ciphersuite DROP CONSTRAINT fk6c894af74856fb3c ; -- foreign key to tls_test_ciphersuite(id)
ALTER TABLE tls_test_actor_tls_test_ciphersuite DROP CONSTRAINT fk6c894af7623f8556 ; -- duplicated foreign key to tls_test_ciphersuite(id)
ALTER TABLE tls_test_actor_tls_test_ciphersuite DROP CONSTRAINT fk6c894af727a6047e ; -- duplicated foreign key to tls_test_actor(id)
ALTER TABLE tls_test_actor_tls_test_ciphersuite DROP COLUMN ciphersuites_id ;
ALTER TABLE tls_test_actor_tls_test_ciphersuite RENAME COLUMN element TO ciphersuite ;

ALTER TABLE tls_test_actor_tls_test_protocol ADD element integer ;
UPDATE tls_test_actor_tls_test_protocol SET element = (SELECT protocol FROM tls_test_protocol WHERE tls_test_protocol.id = tls_test_actor_tls_test_protocol.protocols_id) ;
ALTER TABLE tls_test_actor_tls_test_protocol DROP CONSTRAINT fk545ba3aebd1ee9c2 ; -- foreign key to tls_test_protocol(id)
ALTER TABLE tls_test_actor_tls_test_protocol DROP CONSTRAINT fk545ba3ae27a6047e ; -- duplicated foreign key to tls_test_actor(id)
ALTER TABLE tls_test_actor_tls_test_protocol DROP COLUMN protocols_id ;
ALTER TABLE tls_test_actor_tls_test_protocol RENAME COLUMN element TO protocol ;

DROP TABLE tls_test_ciphersuite ;
DROP TABLE tls_test_protocol ;