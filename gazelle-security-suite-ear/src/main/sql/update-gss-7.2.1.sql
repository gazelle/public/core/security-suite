ALTER TABLE public.xua_service_provider_test_instance ADD COLUMN response_headers bytea;

INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'),'application_admin_email','gazellesupport@kereval.com');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'),'application_admin_name','Gazelle Support Team');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'),'application_admin_title','IHE Gazelle Support Team');
