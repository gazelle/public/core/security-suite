-- New application preferences
INSERT INTO app_configuration(id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'xua_soap_body_directory', '/opt/atna/xuatesting/');

-- Ancillary transactions
INSERT INTO xua_ancillary_transaction (id, keyword, name, description, path_to_soap_body, soap_action)
  VALUES (nextval('xua_ancillary_transaction_id_seq'), 'PD', 'Patient Discovery', 'Basic patient discovery query', 'sequoia_pd_basic.xml', 'urn:hl7-org:v3:PRPA_IN201305UV02:CrossGatewayPatientDiscovery');
INSERT INTO xua_ancillary_transaction (id, keyword, name, description, path_to_soap_body, soap_action)
VALUES (nextval('xua_ancillary_transaction_id_seq'), 'RD', 'Retrieve Document', 'Basic retrieve document request', 'sequoia_rd_basic.xml', 'urn:ihe:iti:2007:CrossGatewayRetrieve');
INSERT INTO xua_ancillary_transaction (id, keyword, name, description, path_to_soap_body, soap_action)
VALUES (nextval('xua_ancillary_transaction_id_seq'), 'QD', 'Query Document', 'Basic query document request', 'sequoia_qd_basic.xml', 'urn:ihe:iti:2007:CrossGatewayQuery');

-- Sequoia test cases
-- MAQD-R-0003.000
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
    VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.000', 'Handle missing wsse:Security element', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
    VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.000'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.000'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.000'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.000'));

-- MAQD-R-0003.101
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.101', 'Handle missing Security/Timestamp element', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.101'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.101'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.101'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.101'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
    VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'valid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.101'));

-- MAQD-R-0003.102
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.102', 'Handle an expired Security/Timestamp', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.102'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.102'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.102'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.102'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'valid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.102'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
    VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', -3600, 1800, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.102'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
    VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.102'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.103
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.103', 'Handle a Security/Timestamp in the future', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.103'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.103'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.103'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.103'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'valid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.103'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 120, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.103'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.103'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.201
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.201', 'Handle missing MessageID element', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.201'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.201'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.201'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'valid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.201'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.201'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.201'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.301
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.301', 'Handle missing Assertion signature element', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.301'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.301'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.301'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.301'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'unsigned', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.301'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.301'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.301'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.303
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.303', 'Handle missing timestamp signature element', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.303'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.303'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.303'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.303'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'valid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.303'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.303'));

-- MAQD-R-0003.401
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.401', 'Handle missing Assertion element', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.401'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.401'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.401'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.401'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.401'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.401'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.302
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.302', 'Handle invalid assertion signature', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.302'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.302'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.302'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.302'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'invalidsignature', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.302'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.302'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.302'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.326
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.326', 'Handle Missing KeyInfo in Assertion signature', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.326'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.326'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.326'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.326'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'missingkeyinfo', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.326'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.326'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_passwordv)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.326'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.327
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.327', 'Handle missing KeyValue in assertion signature', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.327'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.327'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.327'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.327'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'missingkeyvalue', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.327'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.327'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.327'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.328
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.328', 'Handle missing RSAKeyValue in assertion signature', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.328'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.328'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.328'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.328'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'missingrsakeyvalue', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.328'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.328'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.328'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.329
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.329', 'Handle missing RSAKeyValue/Modulus in assertion signature', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.329'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.329'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.329'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.329'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'missingrsakeymodulus', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.329'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.329'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.329'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.330
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.330', 'Handle missing RSAKeyValue/Exponent in assertion signature', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.330'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.330'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.330'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.330'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'missingrsakeyexponent', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.330'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.330'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.330'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.402
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.402', 'Handle an invalid Version in Assertion', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.402'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.402'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.402'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.402'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'invalidversion', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.402'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.402'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.402'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.403
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.403', 'Handle a missing Version in Assertion', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.403'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.403'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.403'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.403'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'missingversion', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.403'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.403'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.403'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.404
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.404', 'Handle a missing ID in Assertion', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.404'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.404'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.404'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.404'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'missingid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.404'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.404'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.404'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.405
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.405', 'Handle an invalid ID in Assertion', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.405'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.405'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.405'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.405'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'invalidid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.405'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.405'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.405'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.406
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.406', 'Handle a missing IssueInstant in Assertion', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.406'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.406'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.406'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.406'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'missingissueinstant', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.406'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.406'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.406'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.407
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.407', 'Handle an invalid IssueInstant in Assertion', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.407'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.407'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.407'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.407'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'invalidissueinstant', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.407'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.407'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.407'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.408
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.408', 'Handle an IssueInstant much later than the message Timestamp', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.408'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.408'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.408'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.408'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'lateissueinstant', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.408'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.408'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.408'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.409
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.409', 'Handle a missing Issuer in Assertion', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.409'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.409'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.409'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.409'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'missingissuer', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.409'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.409'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.409'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.410
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.410', 'Handle a missing Issuer Format in Assertion', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.410'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.410'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.410'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.410'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'missingissuerformat', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.410'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.410'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.410'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.411
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.411', 'Handle a missing Issuer Email Name ID in Assertion', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.411'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.411'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.411'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.411'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'invalidissueremailformat', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.411'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.411'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.411'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.412
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.412', 'Handle an invalid Issuer X.509 Name ID in Assertion', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.412'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.412'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.412'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.412'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'invalidissuerx509format', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.412'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.412'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.412'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.413
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.413', 'Handle an invalid Issuer Windows Name ID in Assertion', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.413'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.413'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.413'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.413'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'invalidissuerwindowsdomainformat', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.413'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.413'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.413'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.420
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.420', 'Handle a missing Subject element in Assertion', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.420'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.420'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.420'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.420'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'missingsubject', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.420'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.420'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.420'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.421
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.421', 'Handle a missing Subject Name ID in Assertion', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.421'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.421'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.421'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.421'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'missingsubjectnameid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.421'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.421'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.421'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.422
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.422', 'Handle an invalid Subject Name ID in Assertion', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.422'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.422'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.422'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.422'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'invalidsubjectnameidformat', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.422'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.422'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.422'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.423
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.423', 'Handle a missing Subject Confirmation in Assertion', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.423'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.423'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.423'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.423'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'missingsubjectconfirmation', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.423'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.423'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.423'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.424
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.424', 'Handle a missing Subject Confirmation Method in Assertion', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.424'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.424'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.424'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.424'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'missingsubjectconfirmationmethod', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.424'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.424'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.424'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.426
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.426', 'Handle a missing Subject Confirmation Data in Assertion', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.426'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.426'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.426'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.426'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'missingsubjectconf', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.426'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.426'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.426'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.427
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.427', 'Handle a missing Subject Confirmation Key Info in Assertion', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.427'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.427'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.427'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.427'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'missingsubjectconfirmationkeyinfo', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.427'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.427'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.427'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.429
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.429', 'Handle an invalid RSA Public Key Modulus in Assertion', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.429'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.429'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.429'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.429'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'invalidsubjectconfrsapublickeymodulus', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.429'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.429'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.429'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');


-- MAQD-R-0003.430
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.430', 'Handle a missing RSA Public Key Exponent in Assertion', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.430'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.430'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.430'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.430'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'missingsubjectconfrsapublickeyexponent', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.430'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.430'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.430'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.431
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.431', 'Handle an invalid RSA Public Key Exponent in Assertion', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.431'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.431'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.431'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.431'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'invalidsubjectconfrsapublickeyexponent', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.431'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.431'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.431'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.432
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.432', '[TODO]Handle an invalid X.509 Certificate Public Key in Assertion', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.432'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.432'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.432'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.432'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'TBD', 'TBD', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.432'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.432'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.432'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');


-- MAQD-R-0003.433
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.433', 'Handle an invalid X.509 Certificate in Assertion', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.433'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.433'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.433'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.433'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'invalidx509certificate', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.433'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.433'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.433'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

-- MAQD-R-0003.305
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.305', 'Handle a missing SignedInfo element in Timestamp signature', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.305'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.305'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.305'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.305'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'valid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.305'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.305'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.305'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

INSERT INTO xua_exception(id, exception_type, new_value, xpath, part_id) VALUES (nextval('xua_exception_id_seq'), 0, NULL , './/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''SignedInfo'']', (select id from xua_soap_header_part where test_case_id = (select id from xua_service_provider_test_case WHERE keyword = 'MAQD-R-0003.305') and dtype = 'signature'));

-- MAQD-R-0003.317
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.317', 'Handle a missing SignatureValue element in Timestamp signature', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.317'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.317'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.317'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.317'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'valid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.317'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.317'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.317'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

INSERT INTO xua_exception(id, exception_type, new_value, xpath, part_id) VALUES (nextval('xua_exception_id_seq'), 0, NULL , './/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''SignatureValue'']', (select id from xua_soap_header_part where test_case_id = (select id from xua_service_provider_test_case WHERE keyword = 'MAQD-R-0003.317') and dtype = 'signature'));

-- MAQD-R-0003.318
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.318', 'Handle a missing KeyInfo element in timestamp signature', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.318'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.318'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.318'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.318'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'valid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.318'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.318'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.318'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

INSERT INTO xua_exception(id, exception_type, new_value, xpath, part_id) VALUES (nextval('xua_exception_id_seq'), 0, NULL , './/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''KeyInfo'']', (select id from xua_soap_header_part where test_case_id = (select id from xua_service_provider_test_case WHERE keyword = 'MAQD-R-0003.318') and dtype = 'signature'));

-- MAQD-R-0003.319
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.319', 'Handle a missing KeyInfo/SecurityTokenReference element in timestamp signature', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.319'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.319'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.319'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.319'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'valid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.319'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.319'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.319'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

INSERT INTO xua_exception(id, exception_type, new_value, xpath, part_id) VALUES (nextval('xua_exception_id_seq'), 0, NULL , './/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''KeyInfo''][1]/*[namespace-uri()=''http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'' and local-name()=''SecurityTokenReference'']', (select id from xua_soap_header_part where test_case_id = (select id from xua_service_provider_test_case WHERE keyword = 'MAQD-R-0003.319') and dtype = 'signature'));

-- MAQD-R-0003.320
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.320', 'Handle a missing /KeyInfo/SecurityTokenReference/@TokenType attribute in timestamp signature', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.320'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.320'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.320'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.320'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'valid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.320'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.320'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.320'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

INSERT INTO xua_exception(id, exception_type, new_value, xpath, part_id) VALUES (nextval('xua_exception_id_seq'), 0, NULL , './/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''KeyInfo''][1]/*[namespace-uri()=''http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'' and local-name()=''SecurityTokenReference''][1]/@*[namespace-uri()=''http://docs.oasis-open.org/wss/oasis-wss-wssecurity-secext-1.1.xsd'' and local-name()=''TokenType'']', (select id from xua_soap_header_part where test_case_id = (select id from xua_service_provider_test_case WHERE keyword = 'MAQD-R-0003.320') and dtype = 'signature'));

-- MAQD-R-0003.322
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.322', 'Handle a missing a missing KeyIdentifier element in Timestamp signature', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.322'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.322'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.322'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.322'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'valid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.322'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.322'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.322'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

INSERT INTO xua_exception(id, exception_type, new_value, xpath, part_id) VALUES (nextval('xua_exception_id_seq'), 0, NULL , './/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''KeyInfo''][1]/*[namespace-uri()=''http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'' and local-name()=''SecurityTokenReference''][1]/*[namespace-uri()=''http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'' and local-name()=''KeyIdentifier''][1]', (select id from xua_soap_header_part where test_case_id = (select id from xua_service_provider_test_case WHERE keyword = 'MAQD-R-0003.322') and dtype = 'signature'));

-- MAQD-R-0003.323
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.323', 'Handle a missing /SecurityTokenReference/KeyIdentifier/@ValueType attribute in timestamp signature', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.323'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.323'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.323'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.323'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'valid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.323'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.323'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.323'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

INSERT INTO xua_exception(id, exception_type, new_value, xpath, part_id) VALUES (nextval('xua_exception_id_seq'), 0, NULL , './/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''KeyInfo''][1]/*[namespace-uri()=''http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'' and local-name()=''SecurityTokenReference''][1]/*[namespace-uri()=''http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'' and local-name()=''KeyIdentifier''][1]/@ValueType', (select id from xua_soap_header_part where test_case_id = (select id from xua_service_provider_test_case WHERE keyword = 'MAQD-R-0003.323') and dtype = 'signature'));

-- MAQD-R-0003.324
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.324', 'Handle an invalid ValueType version in timestamp signature', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.324'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.324'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.324'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.324'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'valid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.324'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.324'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.324'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

INSERT INTO xua_exception(id, exception_type, new_value, xpath, part_id) VALUES (nextval('xua_exception_id_seq'), 1, 'http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.0#SAMLID' , './/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''KeyInfo''][1]/*[namespace-uri()=''http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'' and local-name()=''SecurityTokenReference''][1]/*[namespace-uri()=''http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'' and local-name()=''KeyIdentifier''][1]/@ValueType', (select id from xua_soap_header_part where test_case_id = (select id from xua_service_provider_test_case WHERE keyword = 'MAQD-R-0003.324') and dtype = 'signature'));

-- MAQD-R-0003.304
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.304', 'Handle an invalid timestamp signature', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.304'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.304'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.304'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.304'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'valid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.304'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.304'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.304'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

INSERT INTO xua_exception(id, exception_type, new_value, xpath, part_id) VALUES (nextval('xua_exception_id_seq'), 1, 'ciTS2Wnd34yZMrDQyPs3F+ZcuUSuG/ngq42sAeNzjE3PqW' , './/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''SignatureValue'']', (select id from xua_soap_header_part where test_case_id = (select id from xua_service_provider_test_case WHERE keyword = 'MAQD-R-0003.304') and dtype = 'signature'));


-- MAQD-R-0003.321
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.321', 'Handle an invalid TokenType version in Timestamp signature', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.321'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.321'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.321'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.321'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'valid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.321'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.321'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.321'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

INSERT INTO xua_exception(id, exception_type, new_value, xpath, part_id) VALUES (nextval('xua_exception_id_seq'), 1, 'http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.1#SAMLV1.0' , './/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''KeyInfo''][1]/*[namespace-uri()=''http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'' and local-name()=''SecurityTokenReference''][1]/@*[namespace-uri()=''http://docs.oasis-open.org/wss/oasis-wss-wssecurity-secext-1.1.xsd'' and local-name()=''TokenType'']', (select id from xua_soap_header_part where test_case_id = (select id from xua_service_provider_test_case WHERE keyword = 'MAQD-R-0003.321') and dtype = 'signature'));

-- MAQD-R-0003.325
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.325', 'Handle an invalid KeyIdentifier (assertionID) in timestamp signature', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.325'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.325'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.325'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.325'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'valid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.325'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.325'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.325'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

INSERT INTO xua_exception(id, exception_type, new_value, xpath, part_id) VALUES (nextval('xua_exception_id_seq'), 1, 'ID_XXXXXXXXX' , './/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''KeyInfo''][1]/*[namespace-uri()=''http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'' and local-name()=''SecurityTokenReference''][1]/*[namespace-uri()=''http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'' and local-name()=''KeyIdentifier''][1]', (select id from xua_soap_header_part where test_case_id = (select id from xua_service_provider_test_case WHERE keyword = 'MAQD-R-0003.325') and dtype = 'signature'));


-- MAQD-R-0003.306
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.306', 'Handle missing CanonicalizationMethod element in Timestamp signature', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.306'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.306'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.306'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.306'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'valid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.306'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.306'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.306'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

INSERT INTO xua_exception(id, exception_type, new_value, xpath, part_id) VALUES (nextval('xua_exception_id_seq'), 0, null , './/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''SignedInfo'']/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''CanonicalizationMethod'']', (select id from xua_soap_header_part where test_case_id = (select id from xua_service_provider_test_case WHERE keyword = 'MAQD-R-0003.306') and dtype = 'signature'));

-- MAQD-R-0003.307
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.307', 'Handle missing CanonicalizationMethod/algorithm element in Timestamp signature', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.307'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.307'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.307'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.307'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'valid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.307'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.307'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.307'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

INSERT INTO xua_exception(id, exception_type, new_value, xpath, part_id) VALUES (nextval('xua_exception_id_seq'), 0, null , './/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''SignedInfo'']/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''CanonicalizationMethod'']/@Algorithm', (select id from xua_soap_header_part where test_case_id = (select id from xua_service_provider_test_case WHERE keyword = 'MAQD-R-0003.307') and dtype = 'signature'));


-- MAQD-R-0003.308
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.308', 'Handle missing SignatureMethod element in Timestamp signature', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.308'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.308'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.308'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.308'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'valid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.308'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.308'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.308'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

INSERT INTO xua_exception(id, exception_type, new_value, xpath, part_id) VALUES (nextval('xua_exception_id_seq'), 0, null , './/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''SignedInfo'']/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''SignatureMethod'']', (select id from xua_soap_header_part where test_case_id = (select id from xua_service_provider_test_case WHERE keyword = 'MAQD-R-0003.308') and dtype = 'signature'));


-- MAQD-R-0003.309
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.309', 'Handle missing SignatureMethod/algorithm element in Timestamp signature', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.309'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.309'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.309'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.309'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'valid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.309'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.309'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.309'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

INSERT INTO xua_exception(id, exception_type, new_value, xpath, part_id) VALUES (nextval('xua_exception_id_seq'), 0, null , './/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''SignedInfo'']/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''SignatureMethod'']/@Algorithm', (select id from xua_soap_header_part where test_case_id = (select id from xua_service_provider_test_case WHERE keyword = 'MAQD-R-0003.309') and dtype = 'signature'));

-- MAQD-R-0003.310
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.310', 'Handle missing Reference element in Timestamp signature', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.310'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.310'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.310'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.310'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'valid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.310'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.310'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.310'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

INSERT INTO xua_exception(id, exception_type, new_value, xpath, part_id) VALUES (nextval('xua_exception_id_seq'), 0, null , './/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''SignedInfo'']/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''Reference'']', (select id from xua_soap_header_part where test_case_id = (select id from xua_service_provider_test_case WHERE keyword = 'MAQD-R-0003.310') and dtype = 'signature'));

-- MAQD-R-0003.313
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.313', 'Handle missing Transform algorithm element in Timestamp signature', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.313'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.313'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.313'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.313'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'valid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.313'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.313'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.313'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

INSERT INTO xua_exception(id, exception_type, new_value, xpath, part_id) VALUES (nextval('xua_exception_id_seq'), 0, null , './/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''SignedInfo''][1]/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''Reference''][1]/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''Transforms'']/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''Transform'']/@Algorithm', (select id from xua_soap_header_part where test_case_id = (select id from xua_service_provider_test_case WHERE keyword = 'MAQD-R-0003.313') and dtype = 'signature'));

-- MAQD-R-0003.312
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.312', 'Handle missing Transform element in Timestamp signature', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.312'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.312'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.312'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.312'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'valid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.312'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.312'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.312'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

INSERT INTO xua_exception(id, exception_type, new_value, xpath, part_id) VALUES (nextval('xua_exception_id_seq'), 0, null , './/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''SignedInfo''][1]/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''Reference''][1]/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''Transforms'']/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''Transform'']', (select id from xua_soap_header_part where test_case_id = (select id from xua_service_provider_test_case WHERE keyword = 'MAQD-R-0003.312') and dtype = 'signature'));

-- MAQD-R-0003.314
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.314', 'Handle missing DigestMethod in Timestamp signature', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.314'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.314'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.314'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.314'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'valid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.314'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.314'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.314'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

INSERT INTO xua_exception(id, exception_type, new_value, xpath, part_id) VALUES (nextval('xua_exception_id_seq'), 0, null , './/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''SignedInfo''][1]/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''Reference''][1]/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''DigestMethod'']', (select id from xua_soap_header_part where test_case_id = (select id from xua_service_provider_test_case WHERE keyword = 'MAQD-R-0003.314') and dtype = 'signature'));

-- MAQD-R-0003.311
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.311', 'Handle invalid URI in Timestamp signature', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.311'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.311'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.311'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.311'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'valid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.311'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.311'));

INSERT INTO xua_exception(id, exception_type, new_value, xpath, part_id) VALUES (nextval('xua_exception_id_seq'), 1, 'new_ref' , './/@*[namespace-uri()=''http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd'' and local-name()=''Id'']', (select id from xua_soap_header_part where test_case_id = (select id from xua_service_provider_test_case WHERE keyword = 'MAQD-R-0003.311') and dtype = 'timestamp'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.311'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');


-- MAQD-R-0003.315
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.315', 'Handle missing DigestValue in Timestamp signature', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.315'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.315'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.315'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.315'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'valid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.315'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.315'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.315'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');

INSERT INTO xua_exception(id, exception_type, new_value, xpath, part_id) VALUES (nextval('xua_exception_id_seq'), 0, null , './/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''SignedInfo''][1]/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''Reference''][1]/*[namespace-uri()=''http://www.w3.org/2000/09/xmldsig#'' and local-name()=''DigestValue'']', (select id from xua_soap_header_part where test_case_id = (select id from xua_service_provider_test_case WHERE keyword = 'MAQD-R-0003.315') and dtype = 'signature'));

-- MAQD-R-0003.316
INSERT INTO xua_service_provider_test_case (id, keyword, description, expected_result, last_changed, last_modifier)
VALUES (nextval('xua_service_provider_test_case_id_seq'), 'MAQD-R-0003.316', 'Handle invalid DigestValue in Timestamp signature', 'SOAPFault', 'now', 'aberge');

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'MessageID', 'urn:uuid:$$RANDOM$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.316'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'To', '$$ENDPOINT$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.316'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'Action', '$$ACTION$$', true, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.316'));

INSERT INTO xua_soap_header_part(id, dtype, namespace_uri, local_name, value, must_understand, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'soapHeaderElement', 'http://www.w3.org/2005/08/addressing', 'ReplyTo', '$$ANONYMOUS$$', false, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.316'));

INSERT INTO xua_soap_header_part(id, dtype, username, password, sts_endpoint, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'assertion', 'valid', 'connectathon', 'https://validation.sequoiaproject.org/gazelle-sts?wsdl', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.316'));

INSERT INTO xua_soap_header_part(id, dtype, created_offset, duration, test_case_id)
VALUES (nextval('xua_soap_header_part_id_seq'), 'timestamp', 0, 3600, (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.316'));

INSERT INTO xua_exception(id, exception_type, new_value, xpath, part_id) VALUES (nextval('xua_exception_id_seq'), 1, '$$NOW$$' , './/*[namespace-uri()=''http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd'' and local-name()=''Created'']', (select id from xua_soap_header_part where test_case_id = (select id from xua_service_provider_test_case WHERE keyword = 'MAQD-R-0003.316') and dtype = 'timestamp'));

INSERT INTO xua_soap_header_part(id, dtype, signed_element_local_name, signed_element_namespace_uri, test_case_id, keystore, alias, keystore_password, private_key_password)
VALUES (nextval('xua_soap_header_part_id_seq'), 'signature', 'Timestamp', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd', (select id from xua_service_provider_test_case where keyword = 'MAQD-R-0003.316'), '/opt/gazelle/cert/keystore.jks', 'validation.sequoiaproject.org', 'password', 'password');