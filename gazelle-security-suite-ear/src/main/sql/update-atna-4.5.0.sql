-- Uptade to Gazelle-ATNA 4.5.0
-- Change all column type 'timestamp without timezone' to 'timestamp with timezone'
CREATE FUNCTION exec(text) RETURNS text AS $$ BEGIN EXECUTE $1; RETURN $1; END $$ LANGUAGE plpgsql;
SELECT exec('ALTER TABLE ' || table_name || ' ALTER ' || column_name || ' TYPE TIMESTAMP WITH TIME ZONE;') AS command FROM information_schema.columns WHERE table_schema ='public' AND data_type ='timestamp without time zone';
DROP function exec(text);
insert into app_configuration(id, variable, value) values (nextval('app_configuration_id_seq'), 'audit_message_root_oid', '1.3.6.1.4.1.12559.11.1.2.1.12.');
insert into app_configuration(id, variable, value) values (nextval('app_configuration_id_seq'), 'audit_message_index', '68');
update am_audit_message_specification set oid = '1.3.6.1.4.1.12559.11.1.2.1.12.' ||id where oid is null;
