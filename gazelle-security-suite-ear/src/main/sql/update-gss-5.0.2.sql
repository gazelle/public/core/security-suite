INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'ser_resp_xsd', '/opt/tls/xacml-os2/XACML-2.0-OS-NORMATIVE/access_control-xacml-2.0-saml-assertion-schema-os.xsd');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'ser_req_xsd', '/opt/tls/xacml-os2/XACML-2.0-OS-NORMATIVE/access_control-xacml-2.0-saml-protocol-schema-os.xsd');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'svs_simulator_restful_url', 'https://gazelle.ihe.net/SVSSimulator/rest/RetrieveValueSetForSimulator');

 -- First copy your datas from tlstestcase_expectedalertdescriptions into tls_test_case_expectedalertdescriptions without duplications.
 -- then remove the table tlstestcase_expectedalertdescriptions :
 -- DROP TABLE tlstestcase_expectedalertdescriptions ;
 -- See GSS-300 for more issue explanations