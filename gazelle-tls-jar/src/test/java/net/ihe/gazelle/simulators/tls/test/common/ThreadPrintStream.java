package net.ihe.gazelle.simulators.tls.test.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.Locale;

public class ThreadPrintStream extends PrintStream {

    private static int counter = 0;

    private ThreadLocal<PrintStream> streams = new ThreadLocal<PrintStream>();

    public ThreadPrintStream(File file, String csn) throws FileNotFoundException, UnsupportedEncodingException {
        super(file, csn);
    }

    public ThreadPrintStream(File file) throws FileNotFoundException {
        super(file);
    }

    public ThreadPrintStream(OutputStream out, boolean autoFlush, String encoding) throws UnsupportedEncodingException {
        super(out, autoFlush, encoding);
    }

    public ThreadPrintStream(OutputStream out, boolean autoFlush) {
        super(out, autoFlush);
    }

    public ThreadPrintStream(OutputStream out) {
        super(out);
    }

    public ThreadPrintStream(String fileName, String csn) throws FileNotFoundException, UnsupportedEncodingException {
        super(fileName, csn);
    }

    public ThreadPrintStream(String fileName) throws FileNotFoundException {
        super(fileName);
    }

    private PrintStream getStream() {
        PrintStream stream = streams.get();
        if (stream == null) {
            synchronized (this) {
                stream = streams.get();
                if (stream == null) {
                    String fileName = "stream-" + counter + ".txt";
                    counter++;
                    FileOutputStream fos;
                    try {
                        fos = new FileOutputStream(fileName);
                    } catch (FileNotFoundException e) {
                        throw new IllegalStateException(e);
                    }
                    stream = new PrintStream(fos, true);
                    streams.set(stream);
                }
            }
        }
        return stream;
    }

    public int hashCode() {
        return getStream().hashCode();
    }

    public void write(byte[] b) throws IOException {
        getStream().write(b);
    }

    public boolean equals(Object obj) {
        return getStream().equals(obj);
    }

    public String toString() {
        return getStream().toString();
    }

    public void flush() {
        getStream().flush();
    }

    public void close() {
        getStream().close();
    }

    public boolean checkError() {
        return getStream().checkError();
    }

    public void write(int b) {
        getStream().write(b);
    }

    public void write(byte[] buf, int off, int len) {
        getStream().write(buf, off, len);
    }

    public void print(boolean b) {
        getStream().print(b);
    }

    public void print(char c) {
        getStream().print(c);
    }

    public void print(int i) {
        getStream().print(i);
    }

    public void print(long l) {
        getStream().print(l);
    }

    public void print(float f) {
        getStream().print(f);
    }

    public void print(double d) {
        getStream().print(d);
    }

    public void print(char[] s) {
        getStream().print(s);
    }

    public void print(String s) {
        getStream().print(s);
    }

    public void print(Object obj) {
        getStream().print(obj);
    }

    public void println() {
        getStream().println();
    }

    public void println(boolean x) {
        getStream().println(x);
    }

    public void println(char x) {
        getStream().println(x);
    }

    public void println(int x) {
        getStream().println(x);
    }

    public void println(long x) {
        getStream().println(x);
    }

    public void println(float x) {
        getStream().println(x);
    }

    public void println(double x) {
        getStream().println(x);
    }

    public void println(char[] x) {
        getStream().println(x);
    }

    public void println(String x) {
        getStream().println(x);
    }

    public void println(Object x) {
        getStream().println(x);
    }

    public PrintStream printf(String format, Object... args) {
        return getStream().printf(format, args);
    }

    public PrintStream printf(Locale l, String format, Object... args) {
        return getStream().printf(l, format, args);
    }

    public PrintStream format(String format, Object... args) {
        return getStream().format(format, args);
    }

    public PrintStream format(Locale l, String format, Object... args) {
        return getStream().format(l, format, args);
    }

    public PrintStream append(CharSequence csq) {
        return getStream().append(csq);
    }

    public PrintStream append(CharSequence csq, int start, int end) {
        return getStream().append(csq, start, end);
    }

    public PrintStream append(char c) {
        return getStream().append(c);
    }

}
