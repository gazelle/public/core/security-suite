package net.ihe.gazelle.simulators.tls.test.common;

import net.ihe.gazelle.pki.bouncycastle.CertificateBC;
import net.ihe.gazelle.pki.env.CrlServer;
import net.ihe.gazelle.pki.env.TestParam;
import net.ihe.gazelle.pki.model.PKiProvider;
import net.ihe.gazelle.preferences.PreferenceService;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.security.Security;

/**
 * Created by cel on 22/07/15.
 */
@PowerMockIgnore({"javax.*", "net.ihe.gazelle.hql.providers.*"})
@PrepareForTest({PreferenceService.class })
@RunWith(PowerMockRunner.class)
public abstract class TLSTestEnvironment {

    private static CrlServer crlServer;
    private static String BCproviderName;

    public void mockPreferences(){
        PowerMockito.mockStatic(PreferenceService.class);
        Mockito.when(PreferenceService.getString("crl_url")).thenReturn(TestParam.CRL_URL);
        Mockito.when(PreferenceService.getString("java_cacerts_truststore_pwd")).thenReturn(TestParam.CACERTS_PASSWORD);
    }

    /*******
     * SETUP / TEARDOWN
     ******/

    @Before
    public void classSetUp() throws Exception {
        System.out.println("setUp TLS Test dependencies");
        mockPreferences();
        setUpBouncyCastle();
        setUpCrlServer();
    }

    @After
    public void classTearDown() throws Exception {
        System.out.println("tearDown TLS Test dependencies");
        tearDownCrlServer();
        tearDownBouncyCastle();
    }

    /******
     * BOUNCYCASTLE
     ******/

    protected static void setUpBouncyCastle() {
        // PKiProvider pKiProvider =  (PKiProvider) org.jboss.seam.Component.getInstance("CertificateBC", true);
        PKiProvider pKiProvider = new CertificateBC();
        BCproviderName = pKiProvider.getProviderName();
        Security.addProvider(pKiProvider.createProvider());
    }

    protected static void tearDownBouncyCastle() {
        Security.removeProvider(BCproviderName);
    }

    /******
     * CRL SERVER
     *****/

    protected static void setUpCrlServer() {
        crlServer = new CrlServer(TestParam.CRL_PORT, false);
        crlServer.start();
    }

    protected static void tearDownCrlServer() {
        crlServer.stop();
    }

}
