package net.ihe.gazelle.simulators.tls.model;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.pki.StringWrapper;
import net.ihe.gazelle.proxy.netty.ConnectionConfig;
import net.ihe.gazelle.proxy.netty.Proxy;
import net.ihe.gazelle.simulators.tls.TlsConnectionListener;
import net.ihe.gazelle.simulators.tls.TlsConnectionRecorder;
import net.ihe.gazelle.simulators.tls.client.Client;
import net.ihe.gazelle.simulators.tls.common.TlsConnectionListenerCheckClientAuth;
import net.ihe.gazelle.simulators.tls.common.TlsProxyConnectionConfigAsClient;
import net.ihe.gazelle.simulators.tls.common.TlsSimulatorException;
import net.ihe.gazelle.simulators.tls.enums.SimulatorType;
import net.ihe.gazelle.simulators.tls.test.TlsTestDataSetLoader;
import net.ihe.gazelle.simulators.tls.test.model.TlsTestCase;

import javax.net.ssl.SSLParameters;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;

@Entity
@DiscriminatorValue("Client")
public class TlsClient extends TlsSimulator {

    private static final long serialVersionUID = 4686502510994105129L;

    public TlsClient() {
        super();
    }

    public void ping(TlsConnectionListener connectionListener, String host, int port, Client client)
            throws TlsSimulatorException {
        int localPort = getNextAvailable(1024);
        List<TlsConnectionListener> connectionListeners = new ArrayList<TlsConnectionListener>();
        if (this.getCertificate() != null) {
            connectionListeners.add(new TlsConnectionListenerCheckClientAuth(this));
        }
        connectionListeners.add(connectionListener);
        ConnectionConfig connectionConfig = new TlsProxyConnectionConfigAsClient(this, connectionListeners, localPort,
                host, port, client.getChannelType());
        Proxy<?, ?> proxy = client.getChannelType().getProxy(connectionConfig);
        proxy.start();
        if (client.start(proxy)) {
            proxy.stop();
        }
    }

    @Override
    public String toString() {
        return StringWrapper.wrap("Client [simulator=" + super.toString() + "]");
    }

    @Override
    public TlsClient clone() {
        TlsClient copy = (TlsClient) super.clone();
        return copy;
    }

    @Override
    protected TlsClient newInstance() {
        return new TlsClient();
    }

    @Override
    public SimulatorType getType() {
        return SimulatorType.CLIENT;
    }

    @Override
    protected void updateParameters(SSLParameters sslParameters) {
        sslParameters.setNeedClientAuth(false);
    }

    public static String getSimulatorKeywordPrefix() {
        return "client_";
    }

    @Override
    public void executeTest(TlsConnectionListener tlsTestInstanceCallbacker, int testCaseId, String sutHost,
                            int sutPort, Client applicationMsg) throws TlsSimulatorException {

        TlsTestDataSetLoader.loadDataSet(this, EntityManagerService.provideEntityManager().find(TlsTestCase.class, testCaseId)
                .getTlsDataSet());
        TlsConnectionRecorder connectionListener = new TlsConnectionRecorder(tlsTestInstanceCallbacker);
        this.ping(connectionListener, sutHost, sutPort, applicationMsg);

    }

}
