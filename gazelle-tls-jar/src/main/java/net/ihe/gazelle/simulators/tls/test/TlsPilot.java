package net.ihe.gazelle.simulators.tls.test;

import net.ihe.gazelle.simulators.tls.TlsConnectionListener;
import net.ihe.gazelle.simulators.tls.client.Client;
import net.ihe.gazelle.simulators.tls.common.TlsSimulatorException;

public interface TlsPilot {

    /**
     * Execute a test instance.
     *
     * @param tlsTestInstanceCallbacker the test instance that will be callbacked at the end of the test execution.
     * @param testCaseId                id of the instanced test case
     * @param sutHost                   the targeted host
     * @param sutPort                   the targeted port
     * @param applicationMessage        the client message to sent. Set it to null if the pilot is a server.
     * @throws TlsSimulatorException if unable to set TLS context
     */
    void executeTest(TlsConnectionListener tlsTestInstanceCallbacker, int testCaseId, String sutHost, int sutPort, Client applicationMessage) throws TlsSimulatorException;

}

