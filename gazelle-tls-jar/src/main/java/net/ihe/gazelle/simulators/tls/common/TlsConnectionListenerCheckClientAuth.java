package net.ihe.gazelle.simulators.tls.common;

import net.ihe.gazelle.pki.bouncycastle.CertificateXName;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.simulators.tls.TlsConnectionListener;
import net.ihe.gazelle.simulators.tls.enums.TlsConnectionDataItemType;
import net.ihe.gazelle.simulators.tls.model.TlsClient;
import net.ihe.gazelle.simulators.tls.model.TlsConnection;
import net.ihe.gazelle.simulators.tls.model.TlsConnectionDataItem;
import net.ihe.gazelle.simulators.tls.model.TlsConnectionDataItemAliases;
import net.ihe.gazelle.simulators.tls.model.TlsConnectionDataItemAliasesItem;

import java.util.ArrayList;
import java.util.List;

public class TlsConnectionListenerCheckClientAuth implements TlsConnectionListener {

    private TlsClient tlsClient;

    public TlsConnectionListenerCheckClientAuth(TlsClient tlsClient) {
        super();
        this.tlsClient = tlsClient;
    }

    @Override
    public void logConnection(TlsConnection connection, int channelId) {
        if (connection != null && connection.isSuccess()) {
            CheckClientAuthResult result = CheckClientAuthResult.NO_AUTHENTICATION;
            if (connection.getData() != null && connection.getData().getItems() != null) {
                List<TlsConnectionDataItem> items = connection.getData().getItems();
                for (TlsConnectionDataItem tlsConnectionDataItem : items) {
                    if (tlsConnectionDataItem.getType().equals(TlsConnectionDataItemType.CHOOSE_ALIAS_CLIENT)) {
                        result = checkValidAuthentication(connection,
                                (TlsConnectionDataItemAliases) tlsConnectionDataItem);
                    }
                }
            }
            if (result != CheckClientAuthResult.OK) {
                String exceptionMessage = connection.getExceptionMessage();
                if (exceptionMessage == null) {
                    exceptionMessage = result.getMessage();
                } else {
                    exceptionMessage = result.getMessage() + " AND " + exceptionMessage;
                }
                connection.setExceptionMessage(exceptionMessage);
            }
            connection.setSuccess(result.isValidRegardingATNA());
        }
    }

    private CheckClientAuthResult checkValidAuthentication(TlsConnection connection,
                                                           TlsConnectionDataItemAliases tlsConnectionDataItem) {
        List<CertificateXName> validIssuers = new ArrayList<CertificateXName>();
        addIssuers(tlsClient.getCertificate(), validIssuers);

        List<TlsConnectionDataItemAliasesItem> issuers = tlsConnectionDataItem.getIssuers();
        if (issuers.isEmpty()){
            return CheckClientAuthResult.CA_LIST_EMPTY;
        } else {
            for (TlsConnectionDataItemAliasesItem issuer : issuers) {
                CertificateXName X509Name = new CertificateXName(issuer.getValue());
                if (validIssuers.contains(X509Name)) {
                    return CheckClientAuthResult.OK;
                }
            }
            return CheckClientAuthResult.CA_NOT_FOUND;
        }
    }

    private void addIssuers(Certificate certificate, List<CertificateXName> validIssuers) {

        validIssuers.add(new CertificateXName(certificate.getSubject()));
        Certificate certificateAuthority = certificate.getCertificateAuthority();
        if (certificateAuthority != null) {
            addIssuers(certificateAuthority, validIssuers);
        }
    }
}
