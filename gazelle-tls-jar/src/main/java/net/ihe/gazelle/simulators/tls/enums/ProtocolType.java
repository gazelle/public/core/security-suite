package net.ihe.gazelle.simulators.tls.enums;

import net.ihe.gazelle.common.jsf.Labelable;

public enum ProtocolType implements Labelable {

    SSLv2Hello("SSLv2Hello"),
    SSLv3("SSLv3"),
    TLSv1("TLSv1"),
    TLSv11("TLSv1.1"),
    TLSv12("TLSv1.2");

    private String name;

    ProtocolType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return getName();
    }

    public static ProtocolType getProtocolTypeFromName(String name) {
        if (name != null && !name.isEmpty()) {
            for (ProtocolType value : ProtocolType.values()) {
                if (value.getName().equals(name)) {
                    return value;
                }
            }
            throw new IllegalArgumentException("No enum constant " + ProtocolType.class.getCanonicalName() + "." + name);
        } else {
            throw new IllegalArgumentException("Name cannot be null or empty to find " + ProtocolType.class.getCanonicalName() + " enum value");
        }
    }

    @Override
    public String getDisplayLabel() {
        return this.getName();
    }

}
