package net.ihe.gazelle.simulators.tls.client.parameters;

public class ClientParameterText extends ClientParameterString {

    public ClientParameterText(String label, String initialValue) {
        super(label, initialValue);
    }

    @Override
    public int getType() {
        return 1;
    }

}
