package net.ihe.gazelle.simulators.tls.test;

import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.simulators.tls.enums.CipherSuiteType;
import net.ihe.gazelle.simulators.tls.enums.ProtocolType;
import net.ihe.gazelle.simulators.tls.model.TlsClient;
import net.ihe.gazelle.simulators.tls.model.TlsServer;
import net.ihe.gazelle.simulators.tls.test.model.TlsTestDataSet;

import java.util.HashSet;

public class TlsTestDataSetLoader {

    /**
     * Load {@link TlsTestDataSet} into a TLS client simulator
     * @param client is the targeted TLS client simulator
     * @param dataSet are the data to load in.
     */
    public static void loadDataSet(TlsClient client, TlsTestDataSet dataSet) {
        if (dataSet.isNeedClientAuth()) {
            client.setCertificate(dataSet.getCertificate());
        } else {
            client.setCertificate(null);
        }
        client.setCheckRevocation(dataSet.isCheckRevocation());

        client.setIssuers(new HashSet<Certificate>(dataSet.getTrustedIssuers()));
        client.setCipherSuites(new HashSet<CipherSuiteType>(dataSet.getCipherSuites()));
        client.setProtocols(new HashSet<ProtocolType>(dataSet.getProtocols()));
        client.setUseCACerts(dataSet.isUseSystemCACerts());
        client.setEnabled(false);
    }

    public static void loadDataSet(TlsServer server, TlsTestDataSet dataSet) {
        // TODO automated server simulator
    }

}
