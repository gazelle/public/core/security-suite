package net.ihe.gazelle.simulators.tls.client.parameters;

public class ClientParameterInteger implements ClientParameter {

    private String label;
    private int value;

    public ClientParameterInteger(String label, int value) {
        super();
        this.label = label;
        this.value = value;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public String getValueAsString() {
        return Integer.toString(value);
    }

    @Override
    public void setValueAsString(String value) {
        this.value = Integer.parseInt(value);
    }

    public int getValue() {
        return value;
    }

    @Override
    public int getType() {
        return 3;
    }
}
