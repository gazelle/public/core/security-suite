package net.ihe.gazelle.simulators.tls;

import net.ihe.gazelle.pki.CertificateUtil;
import net.ihe.gazelle.simulators.tls.model.TlsSimulator;
import net.ihe.gazelle.simulators.tls.model.TlsConnection;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import javax.persistence.EntityManager;
import java.util.List;

public class SimulatorDAO {

    private SimulatorDAO() {
        super();
    }

    public static TlsSimulator getByID(Integer simulatorId, EntityManager entityManager) {
        Session session = CertificateUtil.getSession(entityManager);
        Criteria criteria = session.createCriteria(TlsSimulator.class);
        criteria.add(Restrictions.eq("id", Integer.valueOf(simulatorId)));
        criteria.setFetchMode("certificate.privateKey", FetchMode.JOIN);
        criteria.setFetchMode("certificate.publicKey", FetchMode.JOIN);
        criteria.setFetchMode("certificate.certificateX509", FetchMode.JOIN);
        List<TlsSimulator> list = criteria.list();
        if (list != null && list.size() > 0) {
            TlsSimulator simulator = list.get(0);
            return simulator;
        }
        return null;
    }

    public static List<TlsConnection> getSimulatorConnections(TlsSimulator simulator, EntityManager entityManager) {
        Session session = CertificateUtil.getSession(entityManager);
        Criteria criteria = session.createCriteria(TlsConnection.class);
        criteria.add(Restrictions.eq("simulator", simulator));
        return criteria.list();
    }

    public static TlsConnection getConnectionByID(Integer connectionId, EntityManager entityManager) {
        Session session = CertificateUtil.getSession(entityManager);
        Criteria criteria = session.createCriteria(TlsConnection.class);
        criteria.add(Restrictions.eq("id", Integer.valueOf(connectionId)));
        List<TlsConnection> list = criteria.list();
        if (list != null && list.size() > 0) {
            TlsConnection connection = list.get(0);
            return connection;
        }
        return null;
    }

}
