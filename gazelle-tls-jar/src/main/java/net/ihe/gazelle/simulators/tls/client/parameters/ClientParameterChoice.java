package net.ihe.gazelle.simulators.tls.client.parameters;

import java.util.List;

public class ClientParameterChoice extends ClientParameterString {

    private List<String> possibleValues;

    public ClientParameterChoice(String label, List<String> possibleValues) {
        super(label, possibleValues.get(0));
        this.possibleValues = possibleValues;
    }

    public List<String> getValues() {
        return possibleValues;
    }

    @Override
    public int getType() {
        return 2;
    }

}
