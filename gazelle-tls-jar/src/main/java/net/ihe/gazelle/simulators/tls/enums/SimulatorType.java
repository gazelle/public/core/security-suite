package net.ihe.gazelle.simulators.tls.enums;

import net.ihe.gazelle.common.jsf.Labelable;
import net.ihe.gazelle.simulators.tls.model.TlsClient;
import net.ihe.gazelle.simulators.tls.model.TlsServer;

import java.io.Serializable;

public enum SimulatorType implements Labelable, Serializable {

    CLIENT(TlsClient.class, "Client"),
    SERVER(TlsServer.class, "Server");

    private String friendlyName;
    private Class simulatorClass;

    SimulatorType(Class simulatorClass, String friendlyName) {
        this.simulatorClass = simulatorClass;
        this.friendlyName = friendlyName;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public Class getSimulatorClass() {
        return simulatorClass;
    }

    public String toString() {
        return getFriendlyName();
    }

    @Override
    public String getDisplayLabel() {
        return toString();
    }
}
