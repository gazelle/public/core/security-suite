package net.ihe.gazelle.simulators.tls.model;

import java.security.Principal;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import net.ihe.gazelle.simulators.tls.enums.TlsConnectionDataItemType;

@Embeddable
public class TlsConnectionData {

    @OneToMany(cascade = {CascadeType.ALL})
    @JoinTable(name = "tls_connection_tls_connection_data_item",
            joinColumns = @JoinColumn(name = "connection_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "data_item_id", referencedColumnName = "id"))
    private List<TlsConnectionDataItem> items = new ArrayList<TlsConnectionDataItem>();

    public TlsConnectionData() {
        super();
    }

    public List<TlsConnectionDataItem> getItems() {
        return items;
    }

    public void setItems(List<TlsConnectionDataItem> items) {
        this.items = items;
    }

    public void chooseAliasServer(String keyType, Principal[] issuers) {
        TlsConnectionDataItem chooseAlias = new TlsConnectionDataItemAliases(keyType, issuers,
                TlsConnectionDataItemType.CHOOSE_ALIAS_SERVER);
        items.add(chooseAlias);
    }

    public void chooseAliasClient(String[] keyType, Principal[] issuers) {
        TlsConnectionDataItem chooseAlias = new TlsConnectionDataItemAliases(keyType, issuers,
                TlsConnectionDataItemType.CHOOSE_ALIAS_CLIENT);
        items.add(chooseAlias);
    }

    public void getAliasesServer(String keyType, Principal[] issuers) {
        TlsConnectionDataItem getAliases = new TlsConnectionDataItemAliases(keyType, issuers,
                TlsConnectionDataItemType.GET_ALIASES_SERVER);
        items.add(getAliases);
    }

    public void getAliasesClient(String keyType, Principal[] issuers) {
        TlsConnectionDataItem getAliases = new TlsConnectionDataItemAliases(keyType, issuers,
                TlsConnectionDataItemType.GET_ALIASES_CLIENT);
        items.add(getAliases);
    }

    public void checkTrustedServer(X509Certificate[] chain, String authType) {
        TlsConnectionDataItem checkTrusted = new TlsConnectionDataItemCheckTrusted(chain, authType,
                TlsConnectionDataItemType.CHECK_TRUSTED_SERVER);
        items.add(checkTrusted);
    }

    public void checkTrustedClient(X509Certificate[] chain, String authType) {
        TlsConnectionDataItem checkTrusted = new TlsConnectionDataItemCheckTrusted(chain, authType,
                TlsConnectionDataItemType.CHECK_TRUSTED_CLIENT);
        items.add(checkTrusted);
    }
}
