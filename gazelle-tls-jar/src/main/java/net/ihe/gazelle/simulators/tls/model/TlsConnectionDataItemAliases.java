package net.ihe.gazelle.simulators.tls.model;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import net.ihe.gazelle.simulators.tls.enums.TlsConnectionDataItemType;

@Entity
@DiscriminatorValue("ALIASES")
public class TlsConnectionDataItemAliases extends TlsConnectionDataItem {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @OneToMany(cascade = {CascadeType.ALL})
    @JoinColumn(name = "join_key_type")
    private List<TlsConnectionDataItemAliasesItem> keyTypes = new ArrayList<TlsConnectionDataItemAliasesItem>();

    @OneToMany(cascade = {CascadeType.ALL})
    @JoinColumn(name = "join_issuer")
    private List<TlsConnectionDataItemAliasesItem> issuers = new ArrayList<TlsConnectionDataItemAliasesItem>();

    public TlsConnectionDataItemAliases() {
        super();
    }

    public TlsConnectionDataItemAliases(String[] keyTypes, Principal[] issuers,
                                        TlsConnectionDataItemType tlsConnectionDataItemType) {
        super();
        init(tlsConnectionDataItemType);
        initKeyTypeIssuers(keyTypes, issuers);
    }

    public TlsConnectionDataItemAliases(String keyType, Principal[] issuers,
                                        TlsConnectionDataItemType tlsConnectionDataItemType) {
        super();
        init(tlsConnectionDataItemType);
        initKeyTypeIssuers(new String[]{keyType}, issuers);
    }

    private void initKeyTypeIssuers(String[] keyTypesArray, Principal[] issuersArray) {
        if (issuersArray != null) {
            for (Principal principal : issuersArray) {
                issuers.add(new TlsConnectionDataItemAliasesItem(principal.toString()));
            }
        }
        if (keyTypesArray != null) {
            for (String keyType : keyTypesArray) {
                keyTypes.add(new TlsConnectionDataItemAliasesItem(keyType));
            }
        }
    }

    public List<TlsConnectionDataItemAliasesItem> getKeyTypes() {
        return keyTypes;
    }

    public void setKeyTypes(List<TlsConnectionDataItemAliasesItem> keyTypes) {
        this.keyTypes = keyTypes;
    }

    public List<TlsConnectionDataItemAliasesItem> getIssuers() {
        return issuers;
    }

    public void setIssuers(List<TlsConnectionDataItemAliasesItem> issuers) {
        this.issuers = issuers;
    }

    @Override
    public boolean isAliases() {
        return true;
    }

}
