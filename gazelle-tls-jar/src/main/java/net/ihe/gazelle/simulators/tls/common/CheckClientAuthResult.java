package net.ihe.gazelle.simulators.tls.common;

public enum CheckClientAuthResult {

    OK(true, ""),
    NO_AUTHENTICATION(false, "Server did not ask for a certificate! No Mutual TLS authentication!"),
    CA_LIST_EMPTY(true,
            "Server asked for a certificate, but list of certificate_authorities in the server response is empty. Some client may refuse such TLS handshake (see RFC2246 section 7.4.4)"),
    CA_NOT_FOUND(true,
            "Server asked for a certificate, but list of certificate_authorities in the server response does not contain the CA of the simulator's certificate. Some client may refuse such TLS handshake (see RFC2246 section 7.4.4)");

    private boolean validRegardingATNA;
    private String message;

    CheckClientAuthResult(boolean validRegardingATNA, String message) {
        this.validRegardingATNA = validRegardingATNA;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public boolean isValidRegardingATNA() {
        return validRegardingATNA;
    }

}

