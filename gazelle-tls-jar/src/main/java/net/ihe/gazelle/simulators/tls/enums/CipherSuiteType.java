package net.ihe.gazelle.simulators.tls.enums;

import net.ihe.gazelle.common.jsf.Labelable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum CipherSuiteType implements Labelable {

    SSL_RSA_WITH_RC4_128_MD5(true),
    SSL_RSA_WITH_RC4_128_SHA(true),
    TLS_RSA_WITH_AES_128_CBC_SHA(true),
    TLS_RSA_WITH_AES_256_CBC_SHA(false),
    TLS_DHE_RSA_WITH_AES_128_CBC_SHA(true),
    TLS_DHE_RSA_WITH_AES_256_CBC_SHA(false),
    TLS_DHE_DSS_WITH_AES_128_CBC_SHA(true),
    TLS_DHE_DSS_WITH_AES_256_CBC_SHA(false),
    SSL_RSA_WITH_3DES_EDE_CBC_SHA(true),
    SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA(true),
    SSL_DHE_DSS_WITH_3DES_EDE_CBC_SHA(true),
    SSL_RSA_WITH_DES_CBC_SHA(true),
    SSL_DHE_RSA_WITH_DES_CBC_SHA(true),
    SSL_DHE_DSS_WITH_DES_CBC_SHA(true),
    SSL_RSA_EXPORT_WITH_RC4_40_MD5(true),
    SSL_RSA_EXPORT_WITH_DES40_CBC_SHA(true),
    SSL_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA(true),
    SSL_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA(true),
    TLS_EMPTY_RENEGOTIATION_INFO_SCSV(true),
    SSL_RSA_WITH_NULL_MD5(true),
    SSL_RSA_WITH_NULL_SHA(true),
    SSL_DH_anon_WITH_RC4_128_MD5(false),
    TLS_DH_anon_WITH_AES_128_CBC_SHA(false),
    TLS_DH_anon_WITH_AES_256_CBC_SHA(false),
    SSL_DH_anon_WITH_3DES_EDE_CBC_SHA(false),
    SSL_DH_anon_WITH_DES_CBC_SHA(false),
    SSL_DH_anon_EXPORT_WITH_RC4_40_MD5(false),
    SSL_DH_anon_EXPORT_WITH_DES40_CBC_SHA(false),
    TLS_KRB5_WITH_RC4_128_SHA(true),
    TLS_KRB5_WITH_RC4_128_MD5(true),
    TLS_KRB5_WITH_3DES_EDE_CBC_SHA(true),
    TLS_KRB5_WITH_3DES_EDE_CBC_MD5(true),
    TLS_KRB5_WITH_DES_CBC_SHA(true),
    TLS_KRB5_WITH_DES_CBC_MD5(true),
    TLS_KRB5_EXPORT_WITH_RC4_40_SHA(true),
    TLS_KRB5_EXPORT_WITH_RC4_40_MD5(true),
    TLS_KRB5_EXPORT_WITH_DES_CBC_40_SHA(true),
    TLS_KRB5_EXPORT_WITH_DES_CBC_40_MD5(true),

    /* Added on 2018-04-03, do not know if they are safe */
    TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384(true),
    TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384(true),
    TLS_RSA_WITH_AES_256_CBC_SHA256(true),
    TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA384(true),
    TLS_ECDH_RSA_WITH_AES_256_CBC_SHA384(true),
    TLS_DHE_RSA_WITH_AES_256_CBC_SHA256(true),
    TLS_DHE_DSS_WITH_AES_256_CBC_SHA256(true),
    TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA(true),
    TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA(true),
    TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA(true),
    TLS_ECDH_RSA_WITH_AES_256_CBC_SHA(true),
    TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256(true),
    TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256(true),
    TLS_RSA_WITH_AES_128_CBC_SHA256(true),
    TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA256(true),
    TLS_ECDH_RSA_WITH_AES_128_CBC_SHA256(true),
    TLS_DHE_RSA_WITH_AES_128_CBC_SHA256(true),
    TLS_DHE_DSS_WITH_AES_128_CBC_SHA256(true),
    TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA(true),
    TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA(true),
    TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA(true),
    TLS_ECDH_RSA_WITH_AES_128_CBC_SHA(true),
    TLS_ECDHE_ECDSA_WITH_RC4_128_SHA(true),
    TLS_ECDHE_RSA_WITH_RC4_128_SHA(true),
    TLS_ECDH_ECDSA_WITH_RC4_128_SHA(true),
    TLS_ECDH_RSA_WITH_RC4_128_SHA(true),
    TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA(true),
    TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA(true),
    TLS_ECDH_ECDSA_WITH_3DES_EDE_CBC_SHA(true),
    TLS_ECDH_RSA_WITH_3DES_EDE_CBC_SHA(true),
    TLS_EMPTY_RENEGOTIATION_INFO_SCSV2(true),

    /* Added on 2019-01-13 */
    TLS_DHE_RSA_WITH_AES_128_GCM_SHA256(true),
    TLS_DHE_RSA_WITH_AES_256_GCM_SHA384(true),
    TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256(true),
    TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384(true);

    boolean safe;

    CipherSuiteType(boolean safe) {
        this.safe = safe;
    }

    /**
     * Return cipher suites considered safe.
     *
     * @return a list of cipher suites
     * @deprecated This is not maintained.
     */
    @Deprecated
    public static List<CipherSuiteType> safeValues() {
        List<CipherSuiteType> result = new ArrayList<CipherSuiteType>();
        for (CipherSuiteType value : values()) {
            if (value.safe) {
                result.add(value);
            }
        }
        return result;
    }

    @Override
    public String getDisplayLabel() {
        return this.name();
    }

    /**
     * Returns an array containing the constants of this CipherSuite enum type, in the alphabetical order.
     *
     * @return an array containing the constants of this CipherSuite enum type, in the alphabetical order.
     */
    public static CipherSuiteType[] sortedValues() {
        CipherSuiteType[] cipherSuiteTypes = values();
        Arrays.sort(cipherSuiteTypes, new CipherSuiteComparator());
        return cipherSuiteTypes;
    }

}
