package net.ihe.gazelle.simulators.tls.common;

import javax.net.ssl.SSLEngine;

import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.handler.ssl.SslHandler;

public class TlsHandlerListenHandshake extends SslHandler {

    private TlsListener listener;
    int renegociationCounter = 0;

    public TlsHandlerListenHandshake(SSLEngine engine, TlsListener listener) {
        super(engine);
        this.listener = listener;
    }

    @Override
    public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        super.channelConnected(ctx, e);
        // First handshake
        handshake();
    }

    @Override
    public ChannelFuture handshake() {
        ChannelFuture handshakeFuture = super.handshake();
        handshakeFuture.addListener(new HandshakeListener(listener, renegociationCounter, this));
        renegociationCounter++;
        return handshakeFuture;
    }
}
