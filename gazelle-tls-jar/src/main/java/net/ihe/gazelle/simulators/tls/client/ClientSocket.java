package net.ihe.gazelle.simulators.tls.client;

import net.ihe.gazelle.proxy.netty.Proxy;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import java.net.Socket;
import java.net.UnknownHostException;

public abstract class ClientSocket implements Client {

    private static final int TIMEOUT = 30000;
    private static final int PAUSE_TIME = 2000;
    private static final Logger log = Logger.getLogger(ClientSocket.class);

    @Override
    public final boolean start(final Proxy<?, ?> proxyToUse) {

        Thread clientSocket = new ClientSocketThread(proxyToUse) ;
        clientSocket.start();

        try {
            Thread.sleep(PAUSE_TIME);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        return false;
    }

    protected abstract void writeInSocket(OutputStream outputStream, Proxy<?, ?> proxyToUse) throws IOException;

    protected void println(Writer os, String string) throws IOException {
        os.write(string);
        os.write("\r\n");
    }

    private class ClientSocketThread extends Thread {

        private final Proxy<?, ?> proxy;

        private ClientSocketThread(Proxy<?, ?> proxy) {
            this.proxy = proxy;
        }

        @Override
        public void run() {

            try (Socket socket = new Socket("127.0.0.1", proxy.getProxyProviderPort());
                 InputStream inputStream = socket.getInputStream();
                 OutputStream outputStream = socket.getOutputStream()) {

                writeInSocket(outputStream, proxy);
                outputStream.flush();

                Thread socketReader = new SocketReaderThread(socket, inputStream);
                socketReader.start();

                try {
                    sleep(TIMEOUT);
                } catch (InterruptedException ie) {
                    //Recieve order to terminate thread. stream and sockets will be closed anyway.
                    socketReader.interrupt();
                }
            } catch (UnknownHostException e) {
                log.error(e);
            } catch (IOException e) {
                log.error(e);
            } finally {
                proxy.stop();
            }

        }

        private class SocketReaderThread extends Thread {

            private Socket socket;
            private InputStream inputStream;

            private SocketReaderThread(Socket socket, InputStream inputStream) {
                this.socket = socket;
                this.inputStream = inputStream;
            }

            @Override
            public void run() {
                try {
                    while (!socket.isClosed()) {
                        if (inputStream.read() == -1) {
                            break;
                        }
                    }
                } catch (IOException ioe) {
                    if(!socket.isClosed()) {
                        log.error("Unable to read input stream from socket: " + ioe.getMessage(), ioe);
                    }
                }
            }
        }

    }

}
