package net.ihe.gazelle.simulators.tls;

import net.ihe.gazelle.hql.providers.detached.HibernateActionPerformer;
import net.ihe.gazelle.hql.providers.detached.HibernateFailure;
import net.ihe.gazelle.hql.providers.detached.PerformHibernateAction;
import net.ihe.gazelle.proxy.listeners.ConnectionManager;
import net.ihe.gazelle.simulators.tls.model.TlsConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;

public class TlsConnectionRecorder implements TlsConnectionListener {

    private int connectionCount = 0;
    static final Logger log = LoggerFactory.getLogger(TlsConnectionRecorder.class);

    private TlsConnectionListener callbacker = null;

    public TlsConnectionRecorder() {
        super();
    }

    public TlsConnectionRecorder(TlsConnectionListener callbacker) {
        super();
        this.callbacker = callbacker;
    }

    @Override
    public void logConnection(TlsConnection connection, int channelId) {
        connectionCount++;
        Integer integerChannelId = channelId;
        try {
            HibernateActionPerformer.performHibernateAction(new PerformHibernateAction() {
                @Override
                public Object performAction(EntityManager entityManager, Object... context) {
                    TlsConnection connection = (TlsConnection) context[0];
                    Integer integerChannelId = (Integer) context[1];
                    if (connection.getProxyConnectionUuid() == null) {
                        connection.setProxyConnectionUuid(ConnectionManager.retrieveUuid(integerChannelId));
                    }
                    entityManager.persist(connection);
                    entityManager.flush();
                    return null;
                }
            }, connection, integerChannelId);
        } catch (HibernateFailure e) {
            log.error("Tls log connection failure", e);
        }
        if (callbacker != null) {
            callbacker.logConnection(connection, channelId);
        }
    }

    public int getConnectionCount() {
        return connectionCount;
    }
}
