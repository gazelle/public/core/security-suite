package net.ihe.gazelle.simulators.tls.common;

import net.ihe.gazelle.pki.CertificateUtil;
import net.ihe.gazelle.pki.validator.CertificateValidatorResult;
import net.ihe.gazelle.pki.validator.CertificateValidatorResultEnum;
import net.ihe.gazelle.pki.validator.CertificateValidatorType;
import net.ihe.gazelle.simulators.tls.model.TlsSimulator;

import javax.net.ssl.X509TrustManager;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collection;

public class TestTrustManager implements X509TrustManager {

    private CertificateValidatorType validator;
    private Collection<X509Certificate> trusted;
    private TlsListener listener;
    private boolean checkRevocation;

    public TestTrustManager(TlsSimulator simulator, TlsListener listener) {
        super();
        this.validator = simulator.getValidator();
        this.listener = listener;
        this.checkRevocation = simulator.isCheckRevocation();

        try {
            if (!simulator.isUseCACerts()) {
                this.trusted = Arrays.asList(simulator.getIssuersArray());
            } else {
                this.trusted = CertificateUtil.getJavaCAs();
            }

        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to create pkixParameters", e);
        }
    }

    @Override
    public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        if (validator != null) {
            CertificateValidatorResult result = validator.validate(Arrays.asList(chain), trusted, checkRevocation);
            listener.checkClientTrusted(chain, result.toString());
            if (!result.getResult().equals(CertificateValidatorResultEnum.PASSED)) {
                throw new CertificateException(result.toString());
            }
        }

    }

    @Override
    public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        if (validator != null) {
            CertificateValidatorResult result = validator.validate(Arrays.asList(chain), trusted, checkRevocation);
            listener.checkServerTrusted(chain, result.toString());
            if (!result.getResult().equals(CertificateValidatorResultEnum.PASSED)) {
                throw new CertificateException(result.toString());
            }
        }
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return trusted.toArray(new X509Certificate[trusted.size()]);
    }

}
