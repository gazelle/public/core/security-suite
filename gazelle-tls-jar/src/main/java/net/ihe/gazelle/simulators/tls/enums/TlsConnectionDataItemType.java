package net.ihe.gazelle.simulators.tls.enums;

public enum TlsConnectionDataItemType {

    CHOOSE_ALIAS_CLIENT("Choose alias client"),

    GET_ALIASES_CLIENT("Get aliases client"),

    CHECK_TRUSTED_CLIENT("Check trusted client"),

    CHOOSE_ALIAS_SERVER("Choose alias server"),

    GET_ALIASES_SERVER("Get aliases server"),

    CHECK_TRUSTED_SERVER("Check trusted server");

    private String friendlyName;

    TlsConnectionDataItemType(String friendlyName) {
        this.friendlyName = friendlyName;
    }

    @Override
    public String toString() {
        return friendlyName;
    }

}
