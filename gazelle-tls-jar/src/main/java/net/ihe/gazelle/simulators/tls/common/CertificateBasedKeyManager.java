package net.ihe.gazelle.simulators.tls.common;

import net.ihe.gazelle.pki.model.Certificate;
import org.hibernate.LazyInitializationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLEngine;
import javax.net.ssl.X509ExtendedKeyManager;
import java.net.Socket;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public class CertificateBasedKeyManager extends X509ExtendedKeyManager {

    static final Logger log = LoggerFactory.getLogger(CertificateBasedKeyManager.class);

    private static final String ALIAS = "default";
    private X509Certificate[] certificateChain = new X509Certificate[0];
    private PrivateKey privateKey = null;
    private TlsListener listener;

    public CertificateBasedKeyManager(Certificate certificate, TlsListener listener) {
        super();
        this.listener = listener;
        setPrivateKey(certificate);
        setCertificateChain(certificate);
    }

    public String[] getClientAliases(String keyType, Principal[] issuers) {
        listener.getClientAliases(keyType, issuers);
        return new String[]{ALIAS};
    }

    public String chooseClientAlias(String[] keyTypes, Principal[] issuers, Socket socket) {
        listener.chooseClientAlias(keyTypes, issuers, socket);
        return ALIAS;
    }

    public String[] getServerAliases(String keyType, Principal[] issuers) {
        listener.getServerAliases(keyType, issuers);
        return new String[]{ALIAS};
    }

    public String chooseServerAlias(String keyType, Principal[] issuers, Socket socket) {
        listener.chooseServerAlias(keyType, issuers, socket);
        return ALIAS;
    }

    public X509Certificate[] getCertificateChain(String alias) {
        if (this.certificateChain == null) {
            this.certificateChain = new X509Certificate[0];
        }
        return this.certificateChain.clone();
    }

    public PrivateKey getPrivateKey(String alias) {
        return this.privateKey;
    }

    @Override
    public String chooseEngineClientAlias(String[] keyTypes, Principal[] issuers,
                                          SSLEngine engine) {
        listener.chooseEngineClientAlias(keyTypes, issuers, engine);
        return ALIAS;
    }

    @Override
    public String chooseEngineServerAlias(String keyType, Principal[] issuers,
                                          SSLEngine engine) {
        listener.chooseEngineServerAlias(keyType, issuers, engine);
        return ALIAS;
    }

    private void setPrivateKey(Certificate certificate) {
        if (certificate != null) {
            try {
                this.privateKey = certificate.getPrivateKey().getKey();
            } catch (CertificateException e) {
                log.error("Failed to get private key", e);
            } catch (LazyInitializationException e) {
                log.error("Failed to load private key", e);
            }
        }
    }

    private void setCertificateChain(Certificate certificate) {
        if (certificate != null) {
            try {
                this.certificateChain = certificate.getChainAsArray() != null ? certificate.getChainAsArray() : new X509Certificate[0];
            } catch (CertificateException e) {
                log.error("Failed to get certificate chain", e);
            } catch (LazyInitializationException e) {
                log.error("Failed to load certificate chain", e);
            }
        }
    }

}
