package net.ihe.gazelle.pki.core;

public class Requester {

    private String username;
    private String casKey;

    public Requester(String username, String casKey) {
        if (username != null && !username.isEmpty() && casKey != null && !casKey.isEmpty()) {
            setRequester(username, casKey);
        } else {
            throw new IllegalArgumentException("A requester must have a username and a CAS SSO identifier");
        }
    }

    public String getUsername() {
        return username;
    }

    public String getCasKey() {
        return casKey;
    }

    public String getUsernameWithCasKey() {
        if (casKey != null && !casKey.isEmpty()) {
            return username + " (" + casKey + ")";
        } else {
            return username;
        }
    }

    void setRequester(String username, String casKey) {
        this.username = username;
        this.casKey = casKey;
    }

}
