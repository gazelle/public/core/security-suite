package net.ihe.gazelle.pki.validator;

import java.io.PrintWriter;
import java.io.StringWriter;

public class CertificateValidatorErrorTrace extends CertificateValidatorError {

    private String exceptionStackTrace;

    public CertificateValidatorErrorTrace() {
        super();
    }

    public CertificateValidatorErrorTrace(Throwable cause) {
        super(cause);
        if (cause == null) {
            setExceptionStackTrace(null);
        } else {
            StringWriter writer = new StringWriter();
            PrintWriter printWriter = new PrintWriter(writer);
            cause.printStackTrace(printWriter);
            setExceptionStackTrace(writer.toString());
        }
    }

    public String getExceptionStackTrace() {
        return exceptionStackTrace;
    }

    public void setExceptionStackTrace(String exceptionStackTrace) {
        this.exceptionStackTrace = exceptionStackTrace;
    }

    @Override
    public String toString() {
        return "\r\nCertificateValidationError [getExceptionMessage=" + getExceptionMessage() + "]";
    }

    @Override
    public String toLongString(String lineReturn) {
        String longString = super.toLongString(lineReturn);
        longString = longString + lineReturn + "StackTrace :" + exceptionStackTrace;
        return longString;
    }
}
