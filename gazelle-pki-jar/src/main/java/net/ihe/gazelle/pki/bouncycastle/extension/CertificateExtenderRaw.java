package net.ihe.gazelle.pki.bouncycastle.extension;

import net.ihe.gazelle.pki.CertificateExtender;
import net.ihe.gazelle.pki.bouncycastle.X509CertificateParametersContainer;
import net.ihe.gazelle.pki.bouncycastle.CertificateBuilder;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public class CertificateExtenderRaw implements CertificateExtender {

    @Override
    public void addExtension(CertificateBuilder certGen, X509CertificateParametersContainer parameters)
            throws CertificateException {
        // nothing
    }

    @Override
    public void modifyCertificate(X509Certificate cert, X509CertificateParametersContainer parameters)
            throws CertificateException {
        // nothing
    }

}
