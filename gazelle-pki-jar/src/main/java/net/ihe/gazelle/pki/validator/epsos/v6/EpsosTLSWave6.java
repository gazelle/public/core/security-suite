package net.ihe.gazelle.pki.validator.epsos.v6;

import net.ihe.gazelle.pki.model.PKiProvider;
import net.ihe.gazelle.pki.validator.epsos.v4.EpsosTLS;

public class EpsosTLSWave6 extends EpsosTLS {
    /**
     * Constructor.
     *
     * @param po_pkiProvider Instance of the PKI Provider.
     */
    public EpsosTLSWave6(PKiProvider po_pkiProvider) {
        super(po_pkiProvider);
    }
}
