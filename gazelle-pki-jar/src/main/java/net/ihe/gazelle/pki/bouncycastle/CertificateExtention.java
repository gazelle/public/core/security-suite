package net.ihe.gazelle.pki.bouncycastle;

import org.bouncycastle.asn1.x509.*;

public class CertificateExtention {
    /**
     *
     * @return
     */
    public static String getAuthorityKeyIdentifierObjectId() {
        return Extension.authorityKeyIdentifier.getId();
    }

    /**
     *
     * @return
     */
    public static String getIssuerAlternativeNameObjectId() {
        return Extension.issuerAlternativeName.getId();
    }

    /**
     *
     * @return
     */
    public static String getSubjectAlternativeNameObjectId() {
        return Extension.subjectAlternativeName.getId();
    }

    /**
     *
     * @return
     */
    public static String getBasicConstraintsObjectId() {
        return Extension.basicConstraints.getId();
    }

    /**
     *
     * @return
     */
    public static String getCRLDistributionPointsObjectId() {
        return Extension.cRLDistributionPoints.getId();
    }


    /**
     *
     * @return
     */
    public static String getCertificatePoliciesObjectId() {
        return Extension.certificatePolicies.getId();
    }

    /**
     *
     * @return
     */
    public static String getAuthorityInfoAccessObjectId() {
        return Extension.authorityInfoAccess.getId();
    }

    /**
     *
     * @return
     */
    public static String getQCStatementsObjectId() {
        return Extension.qCStatements.getId();
    }

    /**
     *
     * @return
     */
    public static String getExtendedKeyUsageObjectId() {
        return Extension.extendedKeyUsage.getId();
    }


    /**
     *
     * @return
     */
    public static String getKeyUsageObjectId() { return Extension.keyUsage.getId(); }

    /**
     *
     * @return
     */
    public static String getSubjectKeyIdentifierObjectId() {
        return Extension.subjectKeyIdentifier.getId();
    }

    /**
     *
     * @return
     */
    public static String getCRLNumberObjectId() { return Extension.cRLNumber.getId(); }

}
