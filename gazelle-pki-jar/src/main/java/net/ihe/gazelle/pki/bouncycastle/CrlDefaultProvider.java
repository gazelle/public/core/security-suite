package net.ihe.gazelle.pki.bouncycastle;

import net.ihe.gazelle.pki.crl.CrlProvider;
import net.ihe.gazelle.pki.enums.SignatureAlgorithm;
import net.ihe.gazelle.pki.model.Certificate;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.AuthorityKeyIdentifier;
import org.bouncycastle.asn1.x509.CRLNumber;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.cert.CertIOException;
import org.bouncycastle.cert.X509CRLHolder;
import org.bouncycastle.cert.X509v2CRLBuilder;
import org.bouncycastle.cert.jcajce.JcaX509ExtensionUtils;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;

import java.io.IOException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.List;

public class CrlDefaultProvider implements CrlProvider {
    /**
     *
     * @param certificateAuthority
     * @return
     * @throws CertificateException
     */
    public byte[] generateCRL(Certificate certificateAuthority) throws CertificateException {
        X509v2CRLBuilder generator;
        X509Certificate  caCertificate = certificateAuthority.getCertificateX509().getX509Certificate(new CertificateBC());
        X500Name         issuer        = new X500Name(caCertificate.getSubjectDN().getName());
        Date             thisUpdate    = new Date();

        generator = new X509v2CRLBuilder(issuer, thisUpdate);
        generator.setNextUpdate(new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 24));
        addCertificateAuthority(certificateAuthority, generator);

        try {
            JcaX509ExtensionUtils x509ExtensionUtils = new JcaX509ExtensionUtils();
            AuthorityKeyIdentifier authorityKeyIdentifier = x509ExtensionUtils
                    .createAuthorityKeyIdentifier(certificateAuthority.getPublicKey().getKey());

            generator.addExtension(Extension.authorityKeyIdentifier, false, authorityKeyIdentifier);
        } catch (NoSuchAlgorithmException e) {
            throw new CertificateException(e);
        } catch (CertIOException e) {
            throw new CertificateException(e);
        }

        CRLNumber crlNumber = new CRLNumber(BigInteger.valueOf(certificateAuthority.getCrlNumber() + 1));
        try {
            generator.addExtension(Extension.cRLNumber, false, crlNumber);
        } catch (CertIOException e) {
            throw new CertificateException(e);
        }
        certificateAuthority.setCrlNumber(certificateAuthority.getCrlNumber() + 1);

        return generateCRLWithOneException(certificateAuthority, generator);
    }

    /**
     *
     * @param certificateAuthority
     * @param generator
     * @throws CertificateException
     */
    private void addCertificateAuthority(Certificate certificateAuthority, X509v2CRLBuilder generator)
            throws CertificateException {
        List<Certificate> certificates = certificateAuthority.getCertificates();
        for (Certificate certificate : certificates) {
            if (certificate.isRevoked()) {
                BigInteger serialNumber = certificate.getCertificateX509().getX509Certificate(new CertificateBC()).getSerialNumber();
                generator.addCRLEntry(serialNumber, certificate.getRevokedDate(), certificate.getRevokedReason()
                        .getCode());
            }
            addCertificateAuthority(certificate, generator);
        }
    }

    /**
     *
     * @param certificateAuthority
     * @param generator
     * @return
     * @throws CertificateException
     */
    public static byte[] generateCRLWithOneException(Certificate certificateAuthority, X509v2CRLBuilder generator)
            throws CertificateException {
        byte[] result;
        try {
            ContentSigner sigGen = new JcaContentSignerBuilder(SignatureAlgorithm.SHA512WITHRSAENCRYPTION.getAlgorithmName()).setProvider("BC").build(certificateAuthority.getPrivateKey().getKey());
            X509CRLHolder generated = generator.build(sigGen);
            System.out.println(generated.getIssuer());
            result = generated.getEncoded();
        } catch (OperatorCreationException e) {
            throw new CertificateException(e);
        } catch (IOException e) {
            throw new CertificateException(e);
        }

        return result;
    }

}
