package net.ihe.gazelle.pki.bouncycastle;

import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.x509.*;

public class CertificateGeneralName {
    /**
     *
     * @return
     */
    public static int getRFC822Name() {
        return GeneralName.rfc822Name;
    }

    /**
     *
     * @return
     */
    public static int getUniformResourceIdentifier() {
        return GeneralName.uniformResourceIdentifier;
    }

    /**
     *
     * @param ps_name
     * @return
     */
    public static GeneralName getDirectoryName(String ps_name) {
        return new GeneralName(GeneralName.directoryName, ps_name);
    }

    /**
     *
     * @param ps_resource
     * @return
     */
    public static GeneralName getUniformResourceIdentifier(String ps_resource) {
        return new GeneralName(GeneralName.uniformResourceIdentifier, new DERIA5String(ps_resource));
    }

    /**
     *
     * @param po_generalName
     * @return
     */
    public static GeneralNames getGeneralNames(GeneralName po_generalName) {
        return new GeneralNames(po_generalName);
    }

    /**
     *
     * @param ps_resource
     * @return
     */
    public static AuthorityInformationAccess getAuthorityInformationAccessMethod(String ps_resource) {
        return new AuthorityInformationAccess(
                X509ObjectIdentifiers.ocspAccessMethod, getUniformResourceIdentifier(ps_resource));
    }

    /**
     *
     * @param ps_resource
     * @return
     */
    public static AuthorityInformationAccess getAuthorityInformationAccess(String ps_resource) {
        return new AuthorityInformationAccess(
                X509ObjectIdentifiers.id_ad_ocsp, getUniformResourceIdentifier(ps_resource));
    }

    /**
     *
     * @param ps_crlUrl
     * @return
     */
    public static DistributionPoint getDistributionPoint(String ps_crlUrl) {
        GeneralNames gns = GeneralNames.getInstance(new DERSequence(getUniformResourceIdentifier(ps_crlUrl)));
        DistributionPointName dpn = new DistributionPointName(0, gns);
        return new DistributionPoint(dpn, null, null);
    }

}
