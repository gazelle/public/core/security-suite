package net.ihe.gazelle.pki;

import net.ihe.gazelle.pki.bouncycastle.CertificateBuilder;
import net.ihe.gazelle.pki.bouncycastle.X509CertificateParametersContainer;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public interface CertificateExtender {

    public void addExtension(CertificateBuilder certGen, X509CertificateParametersContainer parameters)
            throws CertificateException;

    public void modifyCertificate(X509Certificate cert, X509CertificateParametersContainer parameters)
            throws CertificateException;

}
