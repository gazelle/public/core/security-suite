package net.ihe.gazelle.pki;

import net.ihe.gazelle.pki.bouncycastle.CertificateBC;
import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.enums.CertificateVersion;
import net.ihe.gazelle.pki.enums.KeyAlgorithm;
import net.ihe.gazelle.pki.enums.SignatureAlgorithm;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.pki.model.CertificateRequest;
import net.ihe.gazelle.pki.model.CertificateRequestAuthority;
import net.ihe.gazelle.pki.model.PKiProvider;

import javax.persistence.EntityManager;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public class CertificateManager {

    protected static int testCertificateId = 0;

    protected static PKiProvider mo_pKiProvider = new CertificateBC();



    private CertificateManager() {
        super();
    }

    private static PKiProvider getPkiProvider() {
        return mo_pKiProvider;
    }

    public static Certificate createCertificate(CertificateRequest certificateRequest, EntityManager entityManager)
            throws CertificateException {
        PublicKey publicKey = certificateRequest.getPublicKey();
        PrivateKey privateKey = certificateRequest.getPrivateKey();
        Certificate certificateAuthority = null;

        if (entityManager != null) {
            certificateAuthority = entityManager.find(
                    Certificate.class, certificateRequest.getCertificateAuthority().getId());
        } else {
            certificateAuthority = certificateRequest.getCertificateAuthority();
        }

        if (certificateAuthority == null || certificateAuthority.getCertificateX509() == null
                || certificateAuthority.getCertificateX509().getX509Certificate(getPkiProvider()) == null) {
            throw new IllegalArgumentException("Invalid certificateAuthority");
        }

        Certificate certificate = new Certificate();
        certificate.setRequest(certificateRequest);
        certificateRequest.setCertificate(certificate);
        certificate.setSubject(certificateRequest.getSubject());

        certificate.getPublicKey().setKey(publicKey);
        certificate.getPrivateKey().setKey(privateKey);
        certificate.setCertificateAuthority(certificateAuthority);

        if (entityManager != null) {
            entityManager.persist(certificate);
        } else {
            certificate.setId(testCertificateId++);
        }

        int serialNumber = certificateAuthority.getNextSerialNumber();
        X509Certificate cert = null;

        cert = getPkiProvider().createX509Certificate(certificateRequest, publicKey, certificateAuthority,
                BigInteger.valueOf(serialNumber), certificate);

        certificate.getCertificateX509().setX509Certificate(cert);

        certificateAuthority.getCertificates().add(certificate);

        if (entityManager != null) {
            certificate = entityManager.merge(certificate);
            entityManager.merge(certificateAuthority);
            entityManager.merge(certificateRequest);
        }

        return certificate;
    }

    public static Certificate createSelfSignedCertificate(CertificateRequest certificateRequest,
                                                          EntityManager entityManager) throws CertificateException {
        PublicKey publicKey = certificateRequest.getPublicKey();
        PrivateKey privateKey = certificateRequest.getPrivateKey();

        Certificate certificate = new Certificate();
        certificate.setRequest(certificateRequest);
        certificate.setSubject(certificateRequest.getSubject());
        certificate.getPublicKey().setKey(publicKey);
        certificate.getPrivateKey().setKey(privateKey);
        if (entityManager != null) {
            entityManager.persist(certificate);
        } else {
            certificate.setId(testCertificateId++);
        }

        X509Certificate cert = null;

        cert = getPkiProvider().generateCertificate(privateKey,certificateRequest, certificateRequest.getSubject(), publicKey, BigInteger.valueOf(1),
                certificate.getId());

        certificate.getCertificateX509().setX509Certificate(cert);

        if (entityManager != null) {
            entityManager.merge(certificate);
        }

        return certificate;
    }

    public static Certificate createCertificateAuthority(CertificateRequestAuthority certificateRequest,
                                                         EntityManager entityManager) throws CertificateException {
        PublicKey publicKey = certificateRequest.getPublicKey();
        PrivateKey privateKey = certificateRequest.getPrivateKey();

        Certificate ca = new Certificate();
        ca.setRequest(certificateRequest);
        ca.setSubject(certificateRequest.getSubject());
        ca.getPublicKey().setKey(publicKey);
        ca.getPrivateKey().setKey(privateKey);
        if (entityManager != null) {
            entityManager.persist(ca);
        } else {
            ca.setId(testCertificateId++);
        }

        X509Certificate cert = null;
        switch (certificateRequest.getCertificateVersion()) {
            case V1:
                cert = getPkiProvider().generateCertificateV1(privateKey,certificateRequest, certificateRequest.getSubject(), publicKey, BigInteger.valueOf(1),
                        ca.getId());
                break;
            case V3:
            default:
                cert = getPkiProvider().generateCertificate(privateKey,certificateRequest, certificateRequest.getSubject(), publicKey, BigInteger.valueOf(1),
                        ca.getId());

                break;
        }

        ca.getCertificateX509().setX509Certificate(cert);

        if (entityManager != null) {
            entityManager.merge(ca);
        }

        return ca;
    }

    public static void updateCertificate(CertificateRequest certificateRequest, EntityManager entityManager)
            throws CertificateException {
        Certificate certificate = certificateRequest.getCertificate();
        if (certificate != null) {
            PublicKey publicKey = certificateRequest.getPublicKey();

            Certificate certificateAuthority = certificateRequest.getCertificateAuthority();
            if (certificateAuthority == null || certificateAuthority.getCertificateX509() == null
                    || certificateAuthority.getCertificateX509().getX509Certificate(getPkiProvider()) == null) {
                throw new IllegalArgumentException("Invalid certificateAuthority");
            }
            int serialNumber = certificate.getCertificateX509().getX509Certificate(getPkiProvider()).getSerialNumber().intValue();

            X509Certificate cert = getPkiProvider().createX509Certificate(certificateRequest, publicKey, certificateAuthority,
                    BigInteger.valueOf(serialNumber), certificate);

                    certificate.getCertificateX509().setX509Certificate(cert);
            if (entityManager != null) {
                entityManager.merge(certificate);
                entityManager.merge(certificateRequest);
            }
        }
    }

    public static void updateCertificateAuthority(Certificate certificate, EntityManager entityManager)
            throws CertificateException {
        CertificateRequestAuthority cra = new CertificateRequestAuthority(KeyAlgorithm.RSA, 1024);

        PublicKey publicKey = certificate.getPublicKey().getKey();
        PrivateKey privateKey = certificate.getPrivateKey().getKey();

        cra.setSubject(certificate.getSubject());
        cra.setIssuer(certificate.getSubject());

        cra.setPublicKey(publicKey);
        cra.setPrivateKey(privateKey);
        cra.setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION);
        cra.setCertificateExtension(CertificateType.CA_KEY_USAGE_ALL);
        cra.setCertificateVersion(CertificateVersion.V3);

        cra.setNotAfter(CertificateUtil.getNotAfter());
        cra.setNotBefore(CertificateUtil.getNotBefore());

        X509Certificate newCert = getPkiProvider().generateCertificate(privateKey, cra, cra.getIssuer(), publicKey, BigInteger.valueOf(1),
                certificate.getId());

        certificate.setRequest(cra);
        certificate.getCertificateX509().setX509Certificate(newCert);

        entityManager.merge(certificate);
    }

    public static Certificate getCertificateFromX509(EntityManager entityManager, X509Certificate x509Certificate,
                                                     KeyPair keys) throws CertificateException {
        Certificate certificate = new Certificate();

        Certificate certificateAuthority = null;
        Principal issuerDN = x509Certificate.getIssuerDN();
        Principal subjectDN = x509Certificate.getSubjectDN();

        PrivateKey privateKey = null;
        PublicKey publicKey = x509Certificate.getPublicKey();

        if (keys != null) {
            if (!keys.getPublic().equals(publicKey)) {
                throw new CertificateException("Public key from keypair not matching certificate public key");
            }
            privateKey = keys.getPrivate();
        }

        if (issuerDN != null) {
            String issuerDNName = issuerDN.getName();
            boolean shouldSetCA = true;
            if (subjectDN != null && subjectDN.getName() != null && subjectDN.getName().equals(issuerDNName)) {
                shouldSetCA = false;
            }
            if (shouldSetCA && issuerDNName != null && !"".equals(issuerDNName)) {
                certificateAuthority = CertificateDAO.getUniqueBySubject(issuerDNName, entityManager);
                if (certificateAuthority == null) {
                    throw new CertificateException(issuerDNName + " not found");
                }
                certificate.setCertificateAuthority(certificateAuthority);
            }
        }

        certificate.setCertificateAuthority(certificateAuthority);
        certificate.getCertificateX509().setX509Certificate(x509Certificate);
        certificate.setSubject(x509Certificate.getSubjectDN().getName());
        certificate.getPrivateKey().setKey(privateKey);
        certificate.getPublicKey().setKey(publicKey);
        certificate.setRequest(null);
        return certificate;
    }
}