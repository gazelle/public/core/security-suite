package net.ihe.gazelle.pki.bouncycastle;

import org.bouncycastle.asn1.x509.KeyPurposeId;

public enum CertificateKeyPurposeId {

    CLIENT_AUTHENTICATION(KeyPurposeId.id_kp_clientAuth),
    SERVER_AUTHENTICATION(KeyPurposeId.id_kp_serverAuth),
    EMAIL_PROTECTION(KeyPurposeId.id_kp_emailProtection),
    SMARTCARD_LOGON(KeyPurposeId.id_kp_smartcardlogon);

    private KeyPurposeId keyPurposeId;

    /**
     *
     * @param keyPurposeId
     */
    private CertificateKeyPurposeId(KeyPurposeId keyPurposeId) {
        this.keyPurposeId = keyPurposeId;
    }

    /**
     *
     * @return
     */
    public KeyPurposeId getKeyPurposeId() {
        return keyPurposeId;
    }

}
