package net.ihe.gazelle.pki.enums;

/**
 * Revoked Reason Code enumeration contains 2 properties : code and label.
 * Those properties are compliant with RFC5280 section 5.3.1
 */
public enum RevokedReason {

    UNSPECIFIED("unspecified", 0),

    KEY_COMPROMISE("keyCompromise", 1),

    CA_COMPROMISE("cACompromise", 2),

    AFFILIATION_CHANGED("affiliationChanged", 3),

    SUPERSEDED("superseded", 4),

    CESSATION_OF_OPERATION("cessationOfOperation", 5),

    CERTIFICATE_HOLD("certificateHold", 6),

    REMOVE_FROM_CRL("removeFromCRL", 8),

    PRIVILEGE_WITHDRAWN("privilegeWithdrawn", 9),

    AA_COMPROMISE("aAcompromise", 10);

    private String label;
    private int code;

    private RevokedReason(String label, int code) {
        this.label = label;
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public int getCode() {
        return code;
    }

    public String toString() {
        return label;
    }

}
