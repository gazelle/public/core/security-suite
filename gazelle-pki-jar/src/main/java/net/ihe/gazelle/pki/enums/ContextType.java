package net.ihe.gazelle.pki.enums;

import net.ihe.gazelle.common.jsf.Labelable;

import java.io.Serializable;

public enum ContextType implements Labelable, Serializable {

    OTHER(0),
    EPSOS(1),
    IHE(2);

    private int value;

    private ContextType(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

    @Override
    public String getDisplayLabel() {
        return this.name();
    }

}
