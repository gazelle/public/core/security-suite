package net.ihe.gazelle.pki.validator.epsos.v4;

import net.ihe.gazelle.pki.model.PKiProvider;
import net.ihe.gazelle.pki.validator.CertificateNormException;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Set;

public class EpsosSeal extends EpsosValidator {

    /**
     * Constructor.
     *
     * @param po_pkiProvider Instance of the PKI Provider.
     */
    public EpsosSeal(PKiProvider po_pkiProvider) {
        super(po_pkiProvider);
    }

    @Override
    protected void validateKeyUsage(int bits, List<CertificateException> exceptions) {
        super.validateKeyUsage(bits, exceptions);
        int keyUsageBits = getPKiProvider().getSealKeyUsageBits();
        validateKeyUsageValue(bits, keyUsageBits,
                "The digitalSignature and keyEncipherment bits MUST both be set to true to the exclusion of all other KeyUSage bits that MUST be " +
                        "set to false.", exceptions,
                EHEALTH_X509_3_1);
    }

    @Override
    protected void validateExtendedKeyUsage(X509Certificate x509Certificate, Set<String> criticalExtensionOIDs,
                                            Set<String> nonCriticalExtensionOIDs, List<CertificateException> exceptions,
                                            List<CertificateException> warnings) {

        if (x509Certificate.getExtensionValue(getPKiProvider().getExtendedKeyUsageObjectId()) != null) {
            exceptions.add(new CertificateNormException(
                    "ExtendedKeyUsage MUST NOT be included as an extension in the certificate.", EHEALTH_X509_3_1));
        }
    }
}
