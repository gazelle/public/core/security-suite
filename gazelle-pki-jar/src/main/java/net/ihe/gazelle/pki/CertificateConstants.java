package net.ihe.gazelle.pki;

import java.nio.charset.Charset;
import java.util.TimeZone;

public class CertificateConstants {

    public static char[] passwd = {'p', 'a', 's', 's', 'w', 'o', 'r', 'd'};
    public static String p12alias = "cert";
    public static byte[] beginCertificate = "-----BEGIN CERTIFICATE-----".getBytes();
    public static byte[] endCertificate = "-----END CERTIFICATE-----".getBytes();
    public static byte[] NL = "\n".getBytes();
    public static String NEW_LINE = "\r\n";
    public static Charset UTF_8 = Charset.forName("UTF8");
    public static String UTC = "UTC";
    public static TimeZone UTC_TIMEZONE = TimeZone.getTimeZone(UTC);

    private CertificateConstants() {
        super();
    }

}
