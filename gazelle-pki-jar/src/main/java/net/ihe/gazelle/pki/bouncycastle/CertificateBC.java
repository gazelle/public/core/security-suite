package net.ihe.gazelle.pki.bouncycastle;

import net.ihe.gazelle.pki.CertificateConstants;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.pki.model.CertificatePrivateKey;
import net.ihe.gazelle.pki.model.CertificateRequest;
import net.ihe.gazelle.pki.model.CertificateX509;
import net.ihe.gazelle.pki.model.PKiProvider;
import org.apache.commons.ssl.PEMItem;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.asn1.x509.qualified.QCStatement;
import org.jboss.seam.annotations.Name;

import java.io.IOException;
import java.io.Reader;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.*;
import java.util.Hashtable;
import java.util.List;

@Name("CertificateBC")
public class CertificateBC implements PKiProvider {
    /**
     *
     * @return
     */
    public int getRFC822Name() {
        return CertificateGeneralName.getRFC822Name();
    }

    /**
     *
     * @return
     */
    public int getUniformResourceIdentifier() {
        return CertificateGeneralName.getUniformResourceIdentifier();
    }

    /**
     *
     * @return
     */
    public KeyPurposeId[] getKeyPurposeIds() {
        return CertificateKeyPurposeIdTools.getInstance().getKeyPurposeIds();
    }

    /**
     *
     * @param ps_value
     * @param pi_index
     * @param po_keyPurposeString
     * @return
     */
    public String getPurposeIdValue(String ps_value, Integer pi_index, String[] po_keyPurposeString) {
        return CertificateKeyPurposeIdTools.getInstance().getPurposeIdValue(ps_value, pi_index, po_keyPurposeString);
    }


    /**
     *
     * @return
     */
    public String getIdKpEmailProtection() {
        return CertificateKeyPurposeIdTools.getInstance().getIdKpEmailProtection();
    }

    /**
     *
     * @return
     */
    public String getIdKpClientAuthId() {
        return CertificateKeyPurposeIdTools.getInstance().getIdKpClientAuthId();
    }

    /**
     *
     * @return
     */
    public String getIdKpServerAuthId() {
        return CertificateKeyPurposeIdTools.getInstance().getIdKpServerAuthId();
    }

    /**
     *
     * @return
     */
    public String getAuthorityKeyIdentifierObjectId() {
        return CertificateExtention.getAuthorityKeyIdentifierObjectId();
    }

    /**
     *
     * @return
     */
    public String getCRLNumberObjectId() {
        return CertificateExtention.getCRLNumberObjectId();
    }

    /**
     *
     * @return
     */
    public String getIssuerAlternativeNameObjectId() {
        return CertificateExtention.getIssuerAlternativeNameObjectId();
    }

    /**
     *
     * @return
     */
    public String getSubjectAlternativeNameObjectId() {
        return CertificateExtention.getSubjectAlternativeNameObjectId();
    }

    /**
     *
     * @return
     */
    public String getBasicConstraintsObjectId() {
        return CertificateExtention.getBasicConstraintsObjectId();
    }

    /**
     *
     * @return
     */
    public String getCRLDistributionPointsObjectId() {
        return CertificateExtention.getCRLDistributionPointsObjectId();
    }

    /**
     *
     * @return
     */
    public String getCertificatePoliciesObjectId() {
        return CertificateExtention.getCertificatePoliciesObjectId();
    }

    public String getAuthorityInfoAccessObjectId() {
        return CertificateExtention.getAuthorityInfoAccessObjectId();
    }

    /**
     *
     * @return
     */
    public String getExtendedKeyUsageObjectId() {
        return CertificateExtention.getExtendedKeyUsageObjectId();
    }

    /**
     *
     * @return
     */
    public String getSubjectKeyIdentifierObjectId() {
        return CertificateExtention.getSubjectKeyIdentifierObjectId();
    }

    /**
     *
     * @return
     */
    public String getKeyUsageObjectId() {
        return CertificateExtention.getKeyUsageObjectId();
    }

    /**
     *
     * @param po_authorityKeyIdentifierBytes
     * @param po_exceptions
     * @return
     */
    public byte[] getKeyIdentifier(byte[] po_authorityKeyIdentifierBytes, List<CertificateException> po_exceptions) {
        return CertificateKeyIdentifier.getInstance().getKeyIdentifier(po_authorityKeyIdentifierBytes, po_exceptions);
    }

    /**
     *
     * @param po_subjectKeyIdentifierBytes
     * @return
     * @throws Exception
     */
    public byte[] getKeyIdentifier(byte[] po_subjectKeyIdentifierBytes) throws Exception {
        return CertificateKeyIdentifier.getInstance().getKeyIdentifier(po_subjectKeyIdentifierBytes);
    }

    /**
     *
     * @param po_caCertificate
     * @param po_exceptions
     * @return
     */
    public byte[] getCAKeyIdentifier(X509Certificate po_caCertificate, List<CertificateException> po_exceptions) {
        return CertificateKeyIdentifier.getInstance().getCAKeyIdentifier(po_caCertificate, po_exceptions);
    }

    /**
     *
     * @param po_authorityKeyIdentifierBytes
     * @return
     * @throws IOException
     */
    public byte[] getAuthorityKeyIdentifierKeyIdentifier(byte[] po_authorityKeyIdentifierBytes) throws IOException {
        return CertificateKeyIdentifier.getInstance().getAuthorityKeyIdentifierKeyIdentifier(po_authorityKeyIdentifierBytes);
    }

    /**
     *
     * @param po_authorityKeyIdentifierBytes
     * @return
     * @throws IOException
     */
    public Object getAuthorityCertIssuer(byte[] po_authorityKeyIdentifierBytes) throws IOException {
        return CertificateKeyIdentifier.getInstance().getAuthorityCertIssuer(po_authorityKeyIdentifierBytes);
    }

    /**
     *
     * @param po_authorityKeyIdentifierBytes
     * @return
     * @throws IOException
     */
    public Object getAuthorityCertSerialNumber(byte[] po_authorityKeyIdentifierBytes) throws IOException {
        return CertificateKeyIdentifier.getInstance().getAuthorityCertSerialNumber(po_authorityKeyIdentifierBytes);
    }

    /**
     *
     * @param x509Certificate
     * @return
     * @throws NoSuchAlgorithmException
     */
    public byte[] getSKIKeyIdentifier(X509Certificate x509Certificate) throws NoSuchAlgorithmException {
        return CertificateKeyIdentifier.getInstance().getSKIKeyIdentifier(x509Certificate);
    }

    /**
     *
     * @param x509Certificate
     * @param exceptions
     * @return
     */
    public byte[] getKeyIdKeyIdentifier(X509Certificate x509Certificate, List<CertificateException> exceptions) {
        return CertificateKeyIdentifier.getInstance().getKeyIdKeyIdentifier(x509Certificate, exceptions);
    }

    /**
     *
     * @param po_keyUsageBytes
     * @param po_exceptions
     * @return
     */
    public int getkeyUsageBytes(byte[] po_keyUsageBytes, List<CertificateException> po_exceptions) {
        return CertificateKeyUsage.getInstance().getkeyUsageBytes(po_keyUsageBytes, po_exceptions);
    }

    /**
     *
     * @return
     */
    public int getKeyUsageDigitalSignature() {
        return CertificateKeyUsage.getKeyUsageDigitalSignature();
    }

    /**
     *
     * @return
     */
    public int getKeyUsageNonRepudiation() {
        return CertificateKeyUsage.getKeyUsageNonRepudiation();
    }

    /**
     *
     * @return
     */
    public int getKeyUsageKeyEncipherment() {
        return CertificateKeyUsage.getKeyUsageKeyEncipherment();
    }

    /**
     *
     * @return
     */
    public int getKeyUsageDataEncipherment() {
        return CertificateKeyUsage.getKeyUsageDataEncipherment();
    }

    /**
     *
     * @return
     */
    public int getKeyUsageKeyAgreement() {
        return CertificateKeyUsage.getKeyUsageKeyAgreement();
    }

    /**
     *
     * @return
     */
    public int getKeyUsageKeyCertSign() {
        return CertificateKeyUsage.getKeyUsageKeyCertSign();
    }

    /**
     *
     * @return
     */
    public int getKeyUsageCRLSign() {
        return CertificateKeyUsage.getKeyUsageCRLSign();
    }

    /**
     *
     * @return
     */
    public int getKeyUsageEncipherOnly() {
        return CertificateKeyUsage.getKeyUsageEncipherOnly();
    }

    /**
     *
     * @return
     */
    public int getKeyUsageDecipherOnly() {
        return CertificateKeyUsage.getKeyUsageDecipherOnly();
    }

    /**
     *
     * @return
     */
    public int getSealKeyUsageBits() {
        return CertificateKeyUsage.getSealKeyUsageBits();
    }

    /**
     *
     * @return
     */
    public int[] getTLSKeyUsageBits() {
        return CertificateKeyUsage.getTLSKeyUsageBits();
    }

    /**
     *
     * @return
     */
    public CrlDefaultProvider createCRLDefaultProvider() {
        return new CrlDefaultProvider();
    };

    /**
     *
     * @param endCert
     * @param crlDistributionPointsEndCert
     * @throws CertificateException
     */
    public void getCRLDistributionPoints(X509Certificate endCert, List<String> crlDistributionPointsEndCert) throws CertificateException {
        CertificateExtentionChecker.getInstance().getCRLDistributionPoints(endCert, crlDistributionPointsEndCert);
    }

    /**
     *
     * @param po_crlNumberBytes
     * @return
     * @throws Exception
     */
    public byte[] getCRLNumbertoByteArray(byte[] po_crlNumberBytes) throws Exception {
        return CertificateTools.getInstance().getCRLNumbertoByteArray(po_crlNumberBytes);
    }

    /**
     *
     * @param po_x509Certificate
     * @return
     * @throws CertificateException
     */
    public String getPolicyIdentifier(X509Certificate po_x509Certificate) throws CertificateException{
        return CertificateExtentionChecker.getInstance().getPolicyIdentifier(po_x509Certificate);
    }

    /**
     *
     * @param po_crl
     * @param po_warnings
     */
    public void checkX509CRLExtention(X509CRL po_crl, List<CertificateException> po_warnings) {
        CertificateExtentionChecker.getInstance().checkX509CRLExtention(po_crl, po_warnings);
    }

    /**
     *
     * @param po_chain
     * @return
     */
    public Boolean setIssuingCaSupportOcsp(List<X509Certificate> po_chain) {
        return CertificateExtentionChecker.getInstance().setIssuingCaSupportOcsp(po_chain);
    }

    /**
     *
     * @param po_x509Certificate
     * @return
     */
    public Boolean getOCSPPresent(X509Certificate po_x509Certificate) {
        return CertificateExtentionChecker.getInstance().getOCSPPresent(po_x509Certificate);
    }

    /**
     *
     * @param po_x509Certificate
     * @return
     */
    public Boolean getAccessLocationForOcspPresent(X509Certificate po_x509Certificate) {
        return CertificateExtentionChecker.getInstance().getAccessLocationForOcspPresent(po_x509Certificate);
    }

    /**
     *
     * @param qcStatement
     * @param exceptions
     */
    public void validateQCStatementUsages(QCStatement qcStatement, List<CertificateException> exceptions) {
        CertificateExtentionChecker.getInstance().validateQCStatementUsages(qcStatement, exceptions);
    }

    /**
     *
     * @param po_x509Certificate
     * @param po_exceptions
     */
    public void validateQCStatementUsages(X509Certificate po_x509Certificate, List<CertificateException> po_exceptions) {
        CertificateExtentionChecker.getInstance().validateQCStatementUsages(po_x509Certificate, po_exceptions);
    }

    /**
     *
     * @param cert
     * @return
     * @throws CertificateException
     */
    public String getOCSPFromAuthorityInfoAccess(X509Certificate cert) throws CertificateException {
        return CertificateExtentionChecker.getInstance().getOCSPFromAuthorityInfoAccess(cert);
    }

    /**
     *
     * @param name
     * @param exceptions
     * @param warnings
     */
    public void validateSubject(String name, List<CertificateException> exceptions, List<CertificateException> warnings) {
        CertificateTools.getInstance().validateSubject(name, exceptions, warnings);
    }
    /**
     *
     * @return
     */
    public String getProviderName() {
        CertificateGenerator certificateGenerator;

        certificateGenerator = new CertificateGenerator();

        return certificateGenerator.getBouncycastleProviderName();
    }

    /**
     *
     * @return
     */
    public Provider createProvider() {
        return CertificateBuilder.newBCProvider();
    }

    /**
     *
     * @return
     */
    public static Provider createStaticProvider() {
        return CertificateBuilder.newBCProvider();
    }

    /**
     *
     * @param country
     * @param organization
     * @param commonName
     * @param title
     * @param givenName
     * @param surname
     * @param organizationalUnit
     * @param eMail
     * @return
     */
    public String computeSubjectUsingAttributes(String country, String organization, String commonName, String title,
                                                String givenName, String surname, String organizationalUnit, String eMail) {
        return CertificateTools.getInstance().computeSubjectUsingAttributes(country, organization, commonName, title,
                givenName, surname, organizationalUnit, eMail);
    }

    /**
     *
     * @param po_csrPemFormat
     * @return
     * @throws IOException
     */
    public String getPKCS10CertificationSubjectToString(PEMItem po_csrPemFormat) throws IOException {
        return CertificatePKx.getInstance().getPKCS10CertificationSubjectToString(po_csrPemFormat);
    }

    /**
     *
     * @param po_csrPemFormat
     * @return
     * @throws CertificateException
     * @throws IOException
     */
    public PublicKey getPublicKeyFromPKCS10(PEMItem po_csrPemFormat) throws CertificateException, IOException {
        return CertificatePKx.getInstance().getPublicKeyFromPKCS10(po_csrPemFormat);
    }

    /**
     *
     * @param subject
     * @return
     */
    public String getAliasFromSubject(String subject) {
        CertificateGenerator certificateGenerator;

        certificateGenerator = new CertificateGenerator();

        return certificateGenerator.getAliasFromSubject(subject);

    }

    /**
     *
     * @param certificateX509
     * @return
     * @throws CertificateException
     */
    public byte[] getPEMCertificate(CertificateX509 certificateX509) throws CertificateException {
        return new String(CertificatePEMTools.getInstance().getPEMCertificate(certificateX509), CertificateConstants.UTF_8).getBytes();
    }

    /**
     *
     * @param certificatePrivateKey
     * @return
     * @throws CertificateException
     */
    public byte[] getPEMKey(CertificatePrivateKey certificatePrivateKey) throws CertificateException {
        return new String(CertificatePEMTools.getInstance().getPEMKey(certificatePrivateKey), CertificateConstants.UTF_8).getBytes();
    }

    /**
     *
     * @param certificateChain
     * @return
     * @throws CertificateException
     */
    public byte[] getPEMCertificateChain(X509Certificate[] certificateChain) throws CertificateException {
        return new String(CertificatePEMTools.getInstance().getPEMCertificateChain(certificateChain), CertificateConstants.UTF_8).getBytes();
    }

    /**
     *
     * @param po_reader
     * @return
     * @throws IOException
     * @throws CertificateException
     */
    public List<X509Certificate> getCertificatesFromPemObject(Reader po_reader) throws IOException, CertificateException {
        return CertificatePEMTools.getInstance().getCertificatesFromPemObject(po_reader);
    }

    /**
     *
     * @param po_reader
     * @param ps_password
     * @return
     * @throws IOException
     */
    public KeyPair getKeyPairFromPEM(Reader po_reader, String ps_password) throws IOException {
        return CertificatePEMTools.getInstance().getKeyPairFromPEM(po_reader, ps_password);
    }

    /**
     *
     * @param po_chain
     * @param po_certPath
     * @param po_pkixParameters
     * @param po_exceptions
     */
    public void validatePKIX(List<X509Certificate> po_chain, CertPath po_certPath, PKIXParameters po_pkixParameters, List<CertificateException> po_exceptions) {
        CertificatePKx.getInstance().validatePKIX(po_chain, po_certPath, po_pkixParameters, po_exceptions);
    }

    /**
     *
     * @param po_bts
     * @return
     * @throws IOException
     */
    public PublicKey getNetscapeCertRequest(byte[] po_bts) throws IOException {
        return CertificateNetscapeCertType.getNetscapeCertRequest(po_bts);
    }

    /**
     *
     * @param privateKey
     * @param certificateRequest
     * @param issuerSubject
     * @param publicKey
     * @param serialNumber
     * @param certificateId
     * @return
     * @throws CertificateException
     */
    public X509Certificate generateCertificate(PrivateKey privateKey, CertificateRequest certificateRequest, String issuerSubject,
                                               PublicKey publicKey, BigInteger serialNumber,
                                               int certificateId) throws CertificateException {
        CertificateGenerator certificateGenerator = new CertificateGenerator();

        X509CertificateParametersContainer parameters = certificateGenerator
                .createX509CertificateParametersContainer(certificateRequest, issuerSubject, publicKey, serialNumber, certificateId);


       return CertificateGenerator.generateCertificate(privateKey, parameters);
    }

    /**
     *
     * @param privateKey
     * @param certificateRequest
     * @param issuerSubject
     * @param publicKey
     * @param serialNumber
     * @param certificateId
     * @return
     * @throws CertificateException
     */
    public X509Certificate generateCertificateV1(PrivateKey privateKey, CertificateRequest certificateRequest, String issuerSubject,
                                               PublicKey publicKey, BigInteger serialNumber,
                                               int certificateId) throws CertificateException {
        CertificateGenerator certificateGenerator = new CertificateGenerator();

        X509CertificateParametersContainer parameters = certificateGenerator
                .createX509CertificateParametersContainer(certificateRequest, issuerSubject, publicKey, serialNumber, certificateId);


        return CertificateGenerator.generateCertificateV1(privateKey, parameters);
    }

    /**
     *
     * @param certificateRequest
     * @param publicKey
     * @param certificateAuthority
     * @param serialNumber
     * @param certificate
     * @return
     * @throws CertificateException
     */
    public X509Certificate createX509Certificate(CertificateRequest certificateRequest, PublicKey publicKey,
                                                 Certificate certificateAuthority, BigInteger serialNumber, Certificate certificate) throws CertificateException {
        return CertificateGenerator.createX509Certificate(certificateRequest, publicKey,
                certificateAuthority, serialNumber, certificate);
    }
}
