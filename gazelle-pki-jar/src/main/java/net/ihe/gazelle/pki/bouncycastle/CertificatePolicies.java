package net.ihe.gazelle.pki.bouncycastle;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.x509.PolicyInformation;
import org.bouncycastle.asn1.x509.PolicyQualifierInfo;


public class CertificatePolicies {

    /**
     *
     * @param ps_qualifierInfo
     * @param ps_aSN1Identifier
     * @return
     */
    public static PolicyInformation getPolicyInformation(String ps_qualifierInfo, String ps_aSN1Identifier) {
        PolicyQualifierInfo policyQualifierInfo = new PolicyQualifierInfo(ps_qualifierInfo);
        return new PolicyInformation(new ASN1ObjectIdentifier(ps_aSN1Identifier),
                new DERSequence(policyQualifierInfo));
    }

    /**
     *
     * @param ps_aSN1Identifier
     * @return
     */
    public static PolicyInformation getPolicyInformation(String ps_aSN1Identifier) {
        return new PolicyInformation(new ASN1ObjectIdentifier(ps_aSN1Identifier));
    }
}
