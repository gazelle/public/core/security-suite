package net.ihe.gazelle.pki.enums;

import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.pki.CertificateExtender;
import net.ihe.gazelle.pki.bouncycastle.X509CertificateParametersContainer;
import net.ihe.gazelle.pki.bouncycastle.CertificateBuilder;
import net.ihe.gazelle.pki.bouncycastle.extension.CertificateExtenderCA;
import net.ihe.gazelle.pki.bouncycastle.extension.CertificateExtenderClient;
import net.ihe.gazelle.pki.bouncycastle.extension.CertificateExtenderClientServer;
import net.ihe.gazelle.pki.bouncycastle.extension.CertificateExtenderHttpServer;
import net.ihe.gazelle.pki.bouncycastle.extension.CertificateExtenderRaw;
import net.ihe.gazelle.pki.validator.CertificateValidatorType;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public enum CertificateType implements CertificateExtender {

    CA_KEY_USAGE_ALL("CA", false, CertificateValidatorType.CA, CertificateExtenderCA.class, false),

    HTTPS_SERVER("TLS server", true, CertificateValidatorType.TLS_SERVER, CertificateExtenderHttpServer.class, false),

    HTTPS_CLIENT_AUTH("TLS client", true, CertificateValidatorType.TLS_CLIENT, CertificateExtenderClient.class, false),

    EPSOS_VPN_CLIENT("epSOS VPN client (Deprecated)", true, CertificateValidatorType.EPSOS_VPN_CLIENT, null, true),

    EPSOS_VPN_SERVER("epSOS VPN server (Deprecated)", true, CertificateValidatorType.EPSOS_VPN_SERVER, null, true),

    EPSOS_SERVICE_CONSUMER("epSOS service consumer (Deprecated)", true, CertificateValidatorType.EPSOS_SERVICE_CONSUMER, null, true),

    EPSOS_SERVICE_PROVIDER("epSOS service provider (Deprecated)", true, CertificateValidatorType.EPSOS_SERVICE_PROVIDER, null, true),

    EPSOS_NCP_SIGNATURE("epSOS NCP signature (Deprecated)", true, CertificateValidatorType.EPSOS_NCP_SIGNATURE, null, true),

    EPSOS_OCSP_RESPONDER("epSOS OCSP responder (Deprecated)", true, CertificateValidatorType.EPSOS_OCSP_RESPONDER, null, true),

    CLIENT_AND_SERVER("Client and Server", true, CertificateValidatorType.TLS_CLIENT,
            CertificateExtenderClientServer.class, false),

    EPSOS_SEAL("Epsos Seal", true, CertificateValidatorType.EPSOS_SEAL_V4, CertificateExtenderRaw.class, false),

    EPSOS_TLS("Epsos TLS", true, CertificateValidatorType.EPSOS_TLS_V4, CertificateExtenderRaw.class, false),

    EPSOS_SEAL_V6("Epsos Seal V6", true, CertificateValidatorType.EPSOS_SEAL_V6, CertificateExtenderRaw.class, false),

    EPSOS_TLS_V6("Epsos TLS V6", true, CertificateValidatorType.EPSOS_TLS_V6, CertificateExtenderRaw.class, false),

    BASIC("Basic", true, CertificateValidatorType.TLS_CLIENT, CertificateExtenderRaw.class, false);

    private CertificateExtender certificateExtender;
    private CertificateValidatorType validator;
    private String friendlyName;
    private boolean alwaysVisible;
    private boolean deprecated;

    private CertificateType(String friendlyName, boolean alwaysVisible, CertificateValidatorType validator,
                            Class<? extends CertificateExtender> certificateExtenderClass, boolean deprecated) {
        this.alwaysVisible = alwaysVisible;
        this.friendlyName = friendlyName;
        this.validator = validator;
        this.deprecated = deprecated;
        if (certificateExtenderClass != null) {
            try {
                this.certificateExtender = certificateExtenderClass.newInstance();
            } catch (InstantiationException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public void addExtension(CertificateBuilder certGen, X509CertificateParametersContainer parameters)
            throws CertificateException {
        if (certificateExtender != null) {
            certificateExtender.addExtension(certGen, parameters);
        }
    }

    public void modifyCertificate(X509Certificate cert, X509CertificateParametersContainer parameters)
            throws CertificateException {
        if (certificateExtender != null) {
            certificateExtender.modifyCertificate(cert, parameters);
        }
    }

    @FilterLabel
    public String getFriendlyName() {
        return friendlyName;
    }

    public CertificateValidatorType getValidator() {
        return validator;
    }

    public boolean isAlwaysVisible() {
        return alwaysVisible;
    }

    public boolean isDeprecated() {
        return deprecated;
    }
}
