package net.ihe.gazelle.pki.crl;

import net.ihe.gazelle.pki.CertificateDAO;
import net.ihe.gazelle.pki.bouncycastle.CertificateBC;
import net.ihe.gazelle.pki.bouncycastle.CrlDefaultProvider;
import net.ihe.gazelle.pki.model.Certificate;

import javax.persistence.EntityManager;
import java.security.cert.CertificateException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class CrlUtil {

    private static final CrlDefaultProvider CRL_DEFAULT_PROVIDER = new CertificateBC().createCRLDefaultProvider();

    private static Map<Certificate, CrlProvider> testCertificates = Collections
            .synchronizedMap(new HashMap<Certificate, CrlProvider>());

    private CrlUtil() {
        super();
    }

    public static void addTestCertificate(Certificate testCertificate) {
        testCertificates.put(testCertificate, null);
    }

    public static void addTestCertificate(Certificate testCertificate, CrlProvider crlProvider) {
        testCertificates.put(testCertificate, crlProvider);
    }

    public static byte[] generateCRL(int id, EntityManager entityManager) throws CertificateException {
        Certificate certificateAuthority = null;
        CrlProvider crlProvider = CRL_DEFAULT_PROVIDER;
        if (entityManager != null) {
            certificateAuthority = CertificateDAO.getByID(id, entityManager);
        } else {
            Map<Certificate, CrlProvider> certificates = new HashMap<Certificate, CrlProvider>(testCertificates);
            Set<Entry<Certificate, CrlProvider>> entrySet = certificates.entrySet();
            for (Entry<Certificate, CrlProvider> entry : entrySet) {
                if (entry.getKey().getId() == id) {
                    certificateAuthority = entry.getKey();
                    if (entry.getValue() != null) {
                        crlProvider = entry.getValue();
                    }
                }
            }
        }
        if (certificateAuthority != null) {
            return crlProvider.generateCRL(certificateAuthority);
        } else {
            return null;
        }
    }

}
