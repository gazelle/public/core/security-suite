package net.ihe.gazelle.pki.validator;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.pki.CertificateDAO;
import net.ihe.gazelle.pki.CertificateUtil;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.pki.model.PKiProvider;

import javax.persistence.EntityManager;
//import java.awt.*;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchProviderException;
import java.security.cert.CertPath;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.PKIXParameters;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class AbstractCertificateValidator implements CertificateValidator {

    private PKiProvider mo_pKiProvider;

    private static final String X_509 = "X.509";
    private static final String PKIX = "PKIX";

    public AbstractCertificateValidator(PKiProvider po_pKiProvider) {
        mo_pKiProvider = po_pKiProvider;
    }

    /**
     * Getter of attribute mo_pkiProvider.
     * @return the Instance of PKIProvider.
     */
    protected PKiProvider getPKiProvider() {
        return mo_pKiProvider;
    }

    public abstract void validate(List<X509Certificate> chain, List<CertificateException> pkiValidation,
                                  List<CertificateException> warnings, boolean revocation);

    @Override
    public CertificateValidatorResult validate(List<X509Certificate> chain, boolean revocation) {
        EntityManager em = null;
        return validate(chain, em, revocation);
    }

    @Override
    public final CertificateValidatorResult validate(List<X509Certificate> chain, Collection<X509Certificate> trusted,
                                                     boolean revocation) {
        List<CertificateException> exceptions = new ArrayList<CertificateException>();
        List<CertificateException> warnings = new ArrayList<CertificateException>();

        CertificateValidatorResult result = new CertificateValidatorResult();
        if (chain == null || chain.size() == 0) {
            exceptions.add(new CertificateException("No certificate to validate..."));
            result.setValidatedCertificates("None");
        } else {
            try {
                validatePKIX(chain, trusted, revocation, exceptions, warnings);
            } catch (CertificateException e) {
                exceptions.add(new CertificateNormException(e, "PKIX"));
            } catch (NoSuchProviderException e) {
                exceptions.add(new CertificateNormException(e, "PKIX"));
            } catch (InvalidAlgorithmParameterException e) {
                exceptions.add(new CertificateNormException(e, "PKIX"));
            }

            // Perform subvalidation
            validate(chain, exceptions, warnings, revocation);

            result.setValidatedCertificates(CertificateUtil.getChain(chain));
        }

        for (int i = 0; i < exceptions.size(); i++) {
            result.getErrors().add(new CertificateValidatorErrorTrace(exceptions.get(i)));
        }
        for (int i = 0; i < warnings.size(); i++) {
            result.getWarnings().add(new CertificateValidatorErrorTrace(warnings.get(i)));
        }
        if (exceptions.size() == 0) {
            result.setResult(CertificateValidatorResultEnum.PASSED);
        } else {
            result.setResult(CertificateValidatorResultEnum.FAILED);
        }
        return result;
    }

    public final CertificateValidatorResult validate(List<X509Certificate> chain, EntityManager em, boolean revocation) {
        List<X509Certificate> newChain = retrieveCompleteChainFromPKI(chain, em);

        List<X509Certificate> trusted = new ArrayList<X509Certificate>();
        X509Certificate lastCertificate = newChain.get(newChain.size() - 1);
        if (CertificateUtil.isSelfSigned(lastCertificate)) {
            if(em != null) {
                List<Certificate> certs = CertificateDAO.getAllCertificatesQuery(EntityManagerService.provideEntityManager());
                for (Certificate cert : certs) {
                    try {
                        if (cert.getCertificateX509().getX509Certificate(mo_pKiProvider) != null) {
                            if (CertificateUtil.isSelfSigned(cert.getCertificateX509().getX509Certificate(mo_pKiProvider)))
                                trusted.add(cert.getCertificateX509().getX509Certificate(mo_pKiProvider));
                        }
                    } catch (CertificateException e) {
                        // Certificate not add to the list.
                    }
                }
            } else
                trusted.add(lastCertificate);
        }

        return validate(newChain, trusted, revocation);
    }

    private List<X509Certificate> retrieveCompleteChainFromPKI(List<X509Certificate> chain, EntityManager em) {
        List<X509Certificate> newChain = new ArrayList<X509Certificate>(chain);

        // we can validate buggy chain by retrieve parent certificates in
        // database
        if (em != null) {
            X509Certificate lastCertificate = newChain.get(newChain.size() - 1);
            boolean otherFund = false;
            do {
                String issuer = lastCertificate.getIssuerDN().getName();
                if (issuer.equals(lastCertificate.getSubjectDN().getName())) {
                    otherFund = false;
                } else {
                    Certificate uniqueBySubject = CertificateDAO.getUniqueBySubjectNew(issuer);
                    if (uniqueBySubject == null) {
                        otherFund = false;
                    } else {
                        otherFund = true;
                        try {
                            lastCertificate = uniqueBySubject.getCertificateX509().getX509Certificate(getPKiProvider());
                            newChain.add(lastCertificate);
                        } catch (CertificateException e) {
                            otherFund = false;
                        }
                    }
                }
            } while (otherFund);
        }
        return newChain;
    }

    private void validatePKIX(List<X509Certificate> chain,
                              Collection<X509Certificate> trusted,
                              boolean revocation,
                              List<CertificateException> exceptions,
                              List<CertificateException> warnings)
            throws CertificateException,
            NoSuchProviderException,
            InvalidAlgorithmParameterException
             {

        CertPath certPath = getCertPath(chain);

        PKIXParameters pkixParameters = getPKIXParameters(trusted, revocation);
        getPKiProvider().validatePKIX(chain, certPath, pkixParameters, exceptions);
    }

    private PKIXParameters getPKIXParameters(Collection<X509Certificate> trusted, boolean revocation)
            throws CertificateException, InvalidAlgorithmParameterException {
        Set<TrustAnchor> trustAnchors = new HashSet<TrustAnchor>();

        if (trusted == null || trusted.size() == 0) {
            List<X509Certificate> javaCAs = CertificateUtil.getJavaCAs();
            for (X509Certificate issuer : javaCAs) {
                trustAnchors.add(new TrustAnchor(issuer, null));
            }
            List<Certificate> certs = CertificateDAO.getCertificateAuthorities(EntityManagerService.provideEntityManager(), false);
            for (Certificate cert : certs) {
                if (cert.getRequest() != null) {
                    if (cert.getRequest().getCertificateExtension().getFriendlyName().equals("CA"))
                        trustAnchors.add(new TrustAnchor(cert.getCertificateX509().getX509Certificate(mo_pKiProvider), null));
                }
            }
        } else {
            for (X509Certificate issuer : trusted) {
                trustAnchors.add(new TrustAnchor(issuer, null));
            }
        }
        PKIXParameters pkixParameters = new PKIXParameters(trustAnchors);
        pkixParameters.setRevocationEnabled(revocation);
        pkixParameters.setDate(new Date());
        return pkixParameters;
    }

    private CertPath getCertPath(List<X509Certificate> chain) throws CertificateException, NoSuchProviderException {
        CertificateFactory certificateFactory = CertificateFactory.getInstance(X_509, getPKiProvider().getProviderName());
        CertPath certPath = certificateFactory.generateCertPath(chain);
        return certPath;
    }

}
