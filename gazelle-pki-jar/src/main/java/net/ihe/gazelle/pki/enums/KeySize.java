package net.ihe.gazelle.pki.enums;

import net.ihe.gazelle.common.jsf.Labelable;

import java.util.ArrayList;
import java.util.List;

/**
 * @author glandais, ceoche
 */
public enum KeySize implements Labelable {

    _64(64, false),

    _128(128, false),

    _256(256, false),

    _512(512, false),

    _1024(1024, true),

    _2048(2048, true),

    _4096(4096, true),

    _8192(8192, false);

    private int size;
    private boolean iheAllowed;

    KeySize(int size, boolean iheAllowed) {
        this.size = size;
        this.iheAllowed = iheAllowed;
    }

    public int getSize() {
        return size;
    }

    public boolean isIheAllowed() {
        return iheAllowed;
    }

    public String toString() {
        return Integer.toString(size);
    }

    @Override
    public String getDisplayLabel() {
        return toString();
    }

    public static KeySize[] getIHEAllowedKeySizes() {
        List<KeySize> keySizes = new ArrayList<>();
        for (KeySize size : KeySize.values()) {
            if (size.isIheAllowed()) {
                keySizes.add(size);
            }
        }
        return keySizes.toArray(new KeySize[keySizes.size()]);
    }
}
