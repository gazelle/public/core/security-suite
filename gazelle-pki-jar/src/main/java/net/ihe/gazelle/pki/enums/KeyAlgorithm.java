package net.ihe.gazelle.pki.enums;

import net.ihe.gazelle.common.jsf.Labelable;

public enum KeyAlgorithm implements Labelable {
    RSA("RSA"),

    ECDSA("ECDSA"),

    DSA("DSA");

    private String keySpec;

    private KeyAlgorithm(String keySpec) {
        this.keySpec = keySpec;
    }

    public String getKeySpec() {
        return keySpec;
    }

    @Override
    public String getDisplayLabel() {
        return getKeySpec();
    }
}
