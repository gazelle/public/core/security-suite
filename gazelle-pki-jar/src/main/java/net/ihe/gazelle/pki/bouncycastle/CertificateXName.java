package net.ihe.gazelle.pki.bouncycastle;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.RFC4519Style;

public class CertificateXName extends X500Name {

    /**
     *
     * @param dirName
     */
    public CertificateXName(String dirName) {
        super(dirName);
    }

}
