package net.ihe.gazelle.pki;

import java.security.cert.CertificateException;

public class NotUniqueCertificateException extends CertificateException {

    public NotUniqueCertificateException() {
    }

    public NotUniqueCertificateException(String message) {
        super(message);
    }

    public NotUniqueCertificateException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotUniqueCertificateException(Throwable cause) {
        super(cause);
    }
}
