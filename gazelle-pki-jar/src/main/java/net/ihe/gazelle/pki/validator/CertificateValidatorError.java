package net.ihe.gazelle.pki.validator;

import org.apache.commons.lang.StringUtils;

public class CertificateValidatorError {

    private String exceptionMessage;
    private String normMessage;
    private CertificateValidatorError exceptionCause;

    public CertificateValidatorError() {
        super();
        init(null);
    }

    public CertificateValidatorError(Throwable cause) {
        super();
        init(cause);
    }

    private void init(Throwable cause) {
        if (cause == null) {
            setExceptionMessage(null);
            setExceptionCause(null);
        } else {
            setExceptionMessage(cause.getMessage());
            if (cause instanceof CertificateNormException) {
                CertificateNormException certificateNormException = (CertificateNormException) cause;
                setNormMessage(StringUtils.trimToEmpty(certificateNormException.getNorm()));
            }

            Throwable parentCause = cause.getCause();
            if (parentCause != null && parentCause != cause) {
                setExceptionCause(new CertificateValidatorError(parentCause));
            }
        }
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }

    public void setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }

    public CertificateValidatorError getExceptionCause() {
        return exceptionCause;
    }

    public void setExceptionCause(CertificateValidatorError exceptionCause) {
        this.exceptionCause = exceptionCause;
    }

    public String getNormMessage() {
        return normMessage;
    }

    public void setNormMessage(String normMessage) {
        this.normMessage = normMessage;
    }

    public String getCleanMessage() {
        if (StringUtils.equals(null, normMessage)) {
            return exceptionMessage;
        } else {
            return exceptionMessage + " (" + normMessage + ")";
        }
    }

    public String toLongString() {
        return toLongString("\r\n");
    }

    public String toLongString(String lineReturn) {
        String result = exceptionMessage + lineReturn + normMessage;
        if (exceptionCause != null) {
            result = result + exceptionCause.toLongString(lineReturn + "  ");
        }
        return result;
    }

}
