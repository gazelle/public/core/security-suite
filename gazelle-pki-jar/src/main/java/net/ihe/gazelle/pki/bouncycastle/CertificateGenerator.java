package net.ihe.gazelle.pki.bouncycastle;

import net.ihe.gazelle.pki.CertificateExtender;
import net.ihe.gazelle.pki.enums.CertificateVersion;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.pki.model.CertificateRequest;
import org.bouncycastle.asn1.x500.AttributeTypeAndValue;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.util.PrivateKeyFactory;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.DefaultDigestAlgorithmIdentifierFinder;
import org.bouncycastle.operator.DefaultSignatureAlgorithmIdentifierFinder;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.bc.BcRSAContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;

import java.math.BigInteger;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.Date;

public class CertificateGenerator {

    public static String getBouncycastleProviderName() {
        return BouncyCastleProvider.PROVIDER_NAME;
    }

    public X509CertificateParametersContainer createX509CertificateParametersContainer(CertificateRequest certificateRequest, String issuerSubject,
                                                                                       PublicKey publicKey, BigInteger serialNumber,
                                                                                       int certificateId) {
        return new X509CertificateParametersContainer(certificateRequest, new X500Name(issuerSubject), publicKey, serialNumber,
                new X500Name(certificateRequest.getSubject()), certificateId);
    }

    /**
     * @param privateKey
     * @param parameters
     * @return
     * @throws CertificateException
     */
    public static X509Certificate generateCertificate(PrivateKey privateKey,
                                                      X509CertificateParametersContainer parameters) throws CertificateException {
        CertificateBuilder certGen;
        X509CertificateHolder certHolder;
        X509Certificate cert;
        try {
            certGen = getCertificateBuilder(parameters);
            ContentSigner contentSigner = new JcaContentSignerBuilder(parameters.getCertificateRequest().getSignatureAlgorithm().getAlgorithmName())
                    .setProvider(BouncyCastleProvider.PROVIDER_NAME).build(privateKey);

            certHolder = certGen.getCertV3Builder().build(contentSigner);
            cert = new JcaX509CertificateConverter().setProvider(BouncyCastleProvider.PROVIDER_NAME)
                    .getCertificate(certHolder);

            CertificateExtender certificateExtension = parameters.getCertificateRequest().getCertificateExtender();
            if (certificateExtension != null) {
                certificateExtension.modifyCertificate(cert, parameters);
            }

        } catch (CertificateEncodingException e) {
            throw new CertificateException(e);
        } catch (IllegalStateException e) {
            throw new CertificateException(e);
        } catch (OperatorCreationException e) {
            throw new CertificateException(e);
        }
        return cert;
    }

    /**
     *
     * @param dn
     * @param keyPair
     * @return
     * @throws CertificateException
     */
    public static X509Certificate generate(String dn, KeyPair keyPair) throws CertificateException {
        try {
            Security.addProvider(new BouncyCastleProvider());
            AlgorithmIdentifier sigAlgId = new DefaultSignatureAlgorithmIdentifierFinder().find("SHA1withRSA");
            AlgorithmIdentifier digAlgId = new DefaultDigestAlgorithmIdentifierFinder().find(sigAlgId);
            AsymmetricKeyParameter privateKeyAsymKeyParam = PrivateKeyFactory.createKey(keyPair.getPrivate().getEncoded());
            SubjectPublicKeyInfo subPubKeyInfo = SubjectPublicKeyInfo.getInstance(keyPair.getPublic().getEncoded());
            ContentSigner sigGen = new BcRSAContentSignerBuilder(sigAlgId, digAlgId).build(privateKeyAsymKeyParam);
            X500Name name = new X500Name(dn);
            Date from = new Date();
            Date to = new Date(from.getTime() + 30 * 86400000L);
            BigInteger sn = BigInteger.valueOf(1); //(64, new SecureRandom());
            X509v3CertificateBuilder v3CertGen = new X509v3CertificateBuilder(name, sn, from, to, name, subPubKeyInfo);

          /*  if (subjectAltName != null)
                v3CertGen.addExtension(Extension.subjectAlternativeName, false, subjectAltName); */
            X509CertificateHolder certificateHolder = v3CertGen.build(sigGen);
            return new JcaX509CertificateConverter().setProvider("BC").getCertificate(certificateHolder);
        } catch (CertificateException ce) {
            throw ce;
        } catch (Exception e) {
            throw new CertificateException(e);
        }
    }

    /**
     *
     * @param private1
     * @param parameters
     * @return
     * @throws CertificateException
     */
    public static X509Certificate generateCertificateV1(PrivateKey private1,
                                                        X509CertificateParametersContainer parameters) throws CertificateException {
        CertificateBuilder certGen;
        X509CertificateHolder certHolder;
        X509Certificate cert;
        try {
            certGen = getCertificateBuilder(parameters);
            ContentSigner contentSigner = new JcaContentSignerBuilder(parameters.getCertificateRequest().getSignatureAlgorithm().getAlgorithmName())
                    .setProvider(BouncyCastleProvider.PROVIDER_NAME).build(private1);

            certHolder = certGen.getCertV1Builder().build(contentSigner);
            cert = new JcaX509CertificateConverter().setProvider(BouncyCastleProvider.PROVIDER_NAME)
                    .getCertificate(certHolder);

            CertificateExtender certificateExtension = parameters.getCertificateRequest().getCertificateExtender();
            if (certificateExtension != null) {
                certificateExtension.modifyCertificate(cert, parameters);
            }

        } catch (CertificateEncodingException e) {
            throw new CertificateException(e);
        } catch (OperatorCreationException e) {
            throw new CertificateException(e);
        }
        return cert;
    }

    /**
     * @param certificateRequest
     * @param publicKey
     * @param certificateAuthority
     * @param serialNumber
     * @param certificate
     * @return
     * @throws CertificateException
     */
    public static X509Certificate createX509Certificate(CertificateRequest certificateRequest, PublicKey publicKey,
                                                        Certificate certificateAuthority, BigInteger serialNumber, Certificate certificate)
            throws CertificateException {
        X509Certificate caCert = certificateAuthority.getCertificateX509().getX509Certificate(new CertificateBC());

        X500Name issuerName;

        X509CertSelector caCertSelector = new X509CertSelector();
        caCertSelector.setCertificate(caCert);
        issuerName = new JcaX509CertificateHolder(caCert).getSubject();

        X500Name subjectPrincipal = new X500Name(certificateRequest.getSubject());

        X509CertificateParametersContainer parameters = new X509CertificateParametersContainer(certificateRequest,
                issuerName, publicKey, serialNumber, subjectPrincipal, certificate.getId());

        KeyPair keypair = new KeyPair(certificateRequest.getPublicKey(), certificateRequest.getPrivateKey());

        X509Certificate cert = generateCertificate(certificateAuthority.getPrivateKey().getKey(), parameters);

        return cert;
    }

    /**
     *
     * @param parameters
     * @return
     * @throws CertificateException
     */
    public static CertificateBuilder getCertificateBuilder(X509CertificateParametersContainer parameters)
            throws CertificateException {

        CertificateBuilder certBuilder = new CertificateBuilder(CertificateVersion.V3, parameters);

        CertificateExtender certificateExtension = parameters.getCertificateRequest().getCertificateExtender();
        if (certificateExtension != null) {
            certificateExtension.addExtension(certBuilder, parameters);
        }
        return certBuilder;
    }

    /**
     *
     * @param subject
     * @return
     */
    public String getAliasFromSubject(String subject) {
        AttributeTypeAndValue[] atts;
        X500Name name      = new X500Name(subject);
        RDN[]    rdns      = name.getRDNs();
        boolean  lb_findIt = false;
        int      j, i      = 0;
        String   alias     = "imported";

        while((i < rdns.length) && (!lb_findIt)) {
            atts = rdns[i].getTypesAndValues();
            j =0;
            while((j < atts.length) && (!lb_findIt)) {
                if (atts[j].getType().equals(BCStyle.CN)) {
                    alias = atts[j].getValue().toString();
                    lb_findIt = true;
                }
                j++;
            }
            i++;
        }
        return alias;
    }
}