package net.ihe.gazelle.pki.model;

import net.ihe.gazelle.pki.bouncycastle.CrlDefaultProvider;
import org.apache.commons.ssl.PEMItem;
import org.bouncycastle.asn1.x509.KeyPurposeId;

import java.io.IOException;
import java.io.Reader;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.*;
import java.util.List;

public interface PKiProvider {
    int getRFC822Name();
    int getUniformResourceIdentifier();

    KeyPurposeId[] getKeyPurposeIds();
    String getPurposeIdValue(String ps_value, Integer pi_index, String[] po_keyPurposeString);
    String getIdKpClientAuthId();
    String getIdKpServerAuthId();
    String getIdKpEmailProtection();
    String getAuthorityKeyIdentifierObjectId();
    String getCRLNumberObjectId();
    String getIssuerAlternativeNameObjectId();
    String getSubjectAlternativeNameObjectId();
    String getBasicConstraintsObjectId();
    String getCRLDistributionPointsObjectId();
    String getCertificatePoliciesObjectId();
    String getAuthorityInfoAccessObjectId();
    String getExtendedKeyUsageObjectId();
    String getSubjectKeyIdentifierObjectId();
    String getKeyUsageObjectId();

    byte[] getKeyIdentifier(byte[] po_authorityKeyIdentifierBytes, List<CertificateException> po_exceptions);
    byte[] getKeyIdentifier(byte[] po_subjectKeyIdentifierBytes) throws Exception;
    byte[] getCAKeyIdentifier(X509Certificate po_caCertificate, List<CertificateException> po_exceptions);
    byte[] getAuthorityKeyIdentifierKeyIdentifier(byte[] po_authorityKeyIdentifierBytes) throws IOException;
    Object getAuthorityCertIssuer(byte[] po_authorityKeyIdentifierBytes) throws IOException;
    Object getAuthorityCertSerialNumber(byte[] po_authorityKeyIdentifierBytes) throws IOException;
    byte[] getSKIKeyIdentifier(X509Certificate x509Certificate) throws NoSuchAlgorithmException;
    byte[] getKeyIdKeyIdentifier(X509Certificate x509Certificate, List<CertificateException> exceptions);

    int getkeyUsageBytes(byte[] po_keyUsageBytes, List<CertificateException> po_exceptions);
    int getKeyUsageDigitalSignature();
    int getKeyUsageNonRepudiation();
    int getKeyUsageKeyEncipherment();
    int getKeyUsageDataEncipherment();
    int getKeyUsageKeyAgreement();
    int getKeyUsageKeyCertSign();
    int getKeyUsageCRLSign();
    int getKeyUsageEncipherOnly();
    int getKeyUsageDecipherOnly();
    int getSealKeyUsageBits();
    int[] getTLSKeyUsageBits();

    CrlDefaultProvider createCRLDefaultProvider();
    void getCRLDistributionPoints(X509Certificate endCert, List<String> crlDistributionPointsEndCert) throws CertificateException;
    byte[] getCRLNumbertoByteArray(byte[] po_crlNumberBytes) throws Exception;
    String getPolicyIdentifier(X509Certificate po_x509Certificate) throws CertificateException;

    void checkX509CRLExtention(X509CRL po_crl, List<CertificateException> po_warnings);
    Boolean setIssuingCaSupportOcsp(List<X509Certificate> po_chain);
    Boolean getOCSPPresent(X509Certificate po_x509Certificate);
    Boolean getAccessLocationForOcspPresent(X509Certificate po_x509Certificate);
    void validateQCStatementUsages(X509Certificate po_x509Certificate, List<CertificateException> po_exceptions);
    String getOCSPFromAuthorityInfoAccess(X509Certificate cert) throws CertificateException;
    void validateSubject(String name, List<CertificateException> exceptions, List<CertificateException> warnings);

    String getProviderName();
    Provider createProvider();

    String computeSubjectUsingAttributes(String country, String organization, String commonName, String title,
                                         String givenName, String surname, String organizationalUnit, String eMail);

    String getPKCS10CertificationSubjectToString(PEMItem po_csrPemFormat) throws IOException;
    PublicKey getPublicKeyFromPKCS10(PEMItem po_csrPemFormat) throws CertificateException, IOException;
    String getAliasFromSubject(String subject);

    byte[] getPEMCertificate(CertificateX509 certificateX509) throws CertificateException;
    byte[] getPEMKey(CertificatePrivateKey certificatePrivateKey) throws CertificateException;
    byte[] getPEMCertificateChain(X509Certificate[] certificateChain) throws CertificateException;
    List<X509Certificate> getCertificatesFromPemObject(Reader po_reader) throws IOException, CertificateException;
    KeyPair getKeyPairFromPEM(Reader po_reader, String ps_password) throws IOException;

    void validatePKIX(List<X509Certificate> po_chain, CertPath po_certPath, PKIXParameters po_pkixParameters, List<CertificateException> po_exceptions);

    PublicKey getNetscapeCertRequest(byte[] po_bts) throws IOException;

    X509Certificate generateCertificate(PrivateKey privateKey, CertificateRequest certificateRequest, String issuerSubject,
                                        PublicKey publicKey, BigInteger serialNumber,
                                        int certificateId) throws CertificateException;
    X509Certificate generateCertificateV1(PrivateKey privateKey, CertificateRequest certificateRequest, String issuerSubject,
                                           PublicKey publicKey, BigInteger serialNumber,
                                           int certificateId) throws CertificateException;
    X509Certificate createX509Certificate(CertificateRequest certificateRequest, PublicKey publicKey,
                                          Certificate certificateAuthority, BigInteger serialNumber, Certificate certificate) throws CertificateException;




}
