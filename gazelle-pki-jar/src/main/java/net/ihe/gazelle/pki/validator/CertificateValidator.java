package net.ihe.gazelle.pki.validator;

import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public interface CertificateValidator {

    static final Logger log = LoggerFactory.getLogger(CertificateValidator.class);

    public CertificateValidatorResult validate(List<X509Certificate> chain, Collection<X509Certificate> trusted,
                                               boolean revocation);

    public CertificateValidatorResult validate(List<X509Certificate> chain, EntityManager em, boolean revocation);

    public CertificateValidatorResult validate(List<X509Certificate> chain, boolean revocation);

}
