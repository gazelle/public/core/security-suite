package net.ihe.gazelle.pki.enums;

import net.ihe.gazelle.common.jsf.Labelable;

public enum CertificateVersion implements Labelable {
    V1, V3;

    @Override
    public String getDisplayLabel() {
        return name();
    }
}
