package net.ihe.gazelle.pki.bouncycastle.extension;

import net.ihe.gazelle.pki.bouncycastle.X509CertificateParametersContainer;
import net.ihe.gazelle.pki.bouncycastle.CertificateBuilder;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public class CertificateExtenderBasic extends CertificateExtenderCRL {

    @Override
    public void addExtension(CertificateBuilder certGen, X509CertificateParametersContainer parameters)
            throws CertificateException {
        super.addExtension(certGen, parameters);
        certGen.addBasicConstraintExtension(false);
        certGen.addSubjectKeyIdentifierExtension(parameters.getPublicKey());
        if (parameters.getCertificateRequest().getCertificateAuthority() != null) {
            certGen.addAuthorityKeyIdentifierExtension(parameters.getCertificateRequest().getCertificateAuthority().getPublicKey().getKey());
        }
    }

    @Override
    public void modifyCertificate(X509Certificate cert, X509CertificateParametersContainer parameters)
            throws CertificateException {
        super.modifyCertificate(cert, parameters);
        // PKCS12BagAttributeCarrier bagAttr = (PKCS12BagAttributeCarrier) cert;
        // bagAttr.setBagAttribute(PKCSObjectIdentifiers.pkcs_9_at_friendlyName,
        // new DERBMPString(parameters
        // .getCertificateRequest().getName() + " Certificate"));
    }

}
