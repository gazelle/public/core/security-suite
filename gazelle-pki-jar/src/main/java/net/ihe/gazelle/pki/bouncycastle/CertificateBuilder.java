package net.ihe.gazelle.pki.bouncycastle;

import net.ihe.gazelle.pki.enums.CertificateVersion;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.misc.MiscObjectIdentifiers;
import org.bouncycastle.asn1.misc.NetscapeCertType;
import org.bouncycastle.asn1.misc.NetscapeRevocationURL;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.cert.CertIOException;
import org.bouncycastle.cert.X509v1CertificateBuilder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509ExtensionUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.PublicKey;
import java.security.cert.CertificateException;

public class CertificateBuilder {
    X509v3CertificateBuilder certV3Builder;
    X509v1CertificateBuilder certV1Builder;

    /**
     *
     * @param po_certificatVersion
     * @param parameters
     */
    public CertificateBuilder(CertificateVersion po_certificatVersion, X509CertificateParametersContainer parameters) {
        switch (po_certificatVersion) {
            case V1:
                certV1Builder = new X509v1CertificateBuilder(
                        parameters.getIssuerPrincipal(),
                        parameters.getSerialNumber(),
                        parameters.getCertificateRequest().getNotBefore(),
                        parameters.getCertificateRequest().getNotAfter(),
                        parameters.getSubjectPrincipal(),
                        parameters.getSubjectPublicKeyInfo());
                break;
            case V3:
            default:
                certV3Builder = new X509v3CertificateBuilder(
                        parameters.getIssuerPrincipal(),
                        parameters.getSerialNumber(),
                        parameters.getCertificateRequest().getNotBefore(),
                        parameters.getCertificateRequest().getNotAfter(),
                        parameters.getSubjectPrincipal(),
                        parameters.getSubjectPublicKeyInfo());
                break;
        }
    }


    /**
     *
     * @return
     */
    public static Provider newBCProvider() {
        return new BouncyCastleProvider();
    }

    /**
     *
     * @return
     */
    public X509v3CertificateBuilder getCertV3Builder() {
        return certV3Builder;
    }

    /**
     *
     * @return
     */
    public X509v1CertificateBuilder getCertV1Builder() {
        return certV1Builder;
    }

    /**
     *
     * @param po_issuerUniqueID
     */
    public void setIssuerUniqueID(boolean[] po_issuerUniqueID) {
        certV3Builder = certV3Builder.setIssuerUniqueID(po_issuerUniqueID);
    }

    /**
     *
     * @param po_issuerUniqueID
     */
    public void setSubjectUniqueID(boolean[] po_issuerUniqueID) {
        certV3Builder = certV3Builder.setSubjectUniqueID(po_issuerUniqueID);
    }

    /**
     *
     * @param pi
     * @throws CertificateException
     */
    public void addCertificatePoliciesExtension(PolicyInformation pi) throws CertificateException {
        try {
            certV3Builder.addExtension(Extension.certificatePolicies,true,  new DERSequence(pi));
        } catch (CertIOException e) {
            throw new CertificateException("Unable to add IssuerAlternativeNam extension", e);
        }
    }

    /**
     * Add Subject Alternative name extension to the certificate to be generated with this generator
     *
     * @throws CertificateException
     */
    public void addSubjectAlternativeNameExtension(GeneralNames generalNames) throws CertificateException {
        try {
            certV3Builder.addExtension(Extension.subjectAlternativeName, true, generalNames);
        } catch (CertIOException e) {
            throw new CertificateException("Unable to add SubjectAlternativeName extension", e);
        }
    }

    /**
     *
     * @param generalNames
     * @throws CertificateException
     */
    public void addIssuerAlternativeNameExtension(GeneralNames generalNames) throws CertificateException {
        try {
            certV3Builder.addExtension(Extension.issuerAlternativeName,true, generalNames);
        } catch (CertIOException e) {
            throw new CertificateException("Unable to add IssuerAlternativeNam extension", e);
        }
    }

    /**
     *
     * @param authorityInformationAccess
     * @throws CertificateException
     */
    public void addAuthorityInfoAccessExtension(AuthorityInformationAccess authorityInformationAccess) throws CertificateException {
        try {
            certV3Builder.addExtension(Extension.authorityInfoAccess,false, new DERSequence(
                    authorityInformationAccess));
        } catch (CertIOException e) {
            throw new CertificateException("Unable to add AuthorityInfoAccess extension", e);
        }
    }

    /**
     *
     * @param isCA
     * @throws CertificateException
     */
    public void addBasicConstraintExtension(boolean isCA) throws CertificateException {
        try {
            certV3Builder.addExtension(Extension.basicConstraints, true, new BasicConstraints(isCA));
        } catch (CertIOException e) {
            throw new CertificateException("Unable to add BasicConstraints extension", e);
        }
    }

    /**
     *
     * @param subjectPublicKey
     * @throws CertificateException
     */
    public void addSubjectKeyIdentifierExtension(PublicKey subjectPublicKey) throws CertificateException {
        try {
            JcaX509ExtensionUtils x509ExtensionUtils = new JcaX509ExtensionUtils();
            SubjectKeyIdentifier subjectKeyIdentifier = x509ExtensionUtils
                    .createSubjectKeyIdentifier(subjectPublicKey);

            certV3Builder.addExtension(Extension.subjectKeyIdentifier, false, subjectKeyIdentifier);
        } catch (NoSuchAlgorithmException | CertIOException e) {
            throw new CertificateException("Unable to add SubjectKeyIdentifier extension", e);
        }
    }

    /**
     *
     * @param authorityPublicKey
     * @throws CertificateException
     */
    public void addAuthorityKeyIdentifierExtension(PublicKey authorityPublicKey) throws CertificateException {
        try {
            JcaX509ExtensionUtils x509ExtensionUtils = new JcaX509ExtensionUtils();
            AuthorityKeyIdentifier authorityKeyIdentifier = x509ExtensionUtils
                    .createAuthorityKeyIdentifier(authorityPublicKey);
            certV3Builder.addExtension(Extension.authorityKeyIdentifier, false, authorityKeyIdentifier);
        } catch (NoSuchAlgorithmException | CertIOException e) {
            throw new CertificateException("Unable to add AuthorityKeyIdentifier extension", e);
        }
    }

    /**
     * Add Subject Alternative name extension to the certificate to be generated with this generator
     *
     * @param subjectAlternativeName as a String, containing one or several FQDN separated by commas.
     * @throws CertificateException
     */
    public void addSubjectAlternativeNameExtension(String subjectAlternativeName) throws CertificateException {

        if (subjectAlternativeName != null && !subjectAlternativeName.isEmpty()) {

            ASN1EncodableVector sanASN1Vector = new ASN1EncodableVector();

            for (String retval : subjectAlternativeName.split(",")) {
                sanASN1Vector.add(new GeneralName(GeneralName.dNSName, retval.trim()).toASN1Primitive());
            }
            GeneralNames generalNamesSAN = GeneralNames.getInstance(new DERSequence(sanASN1Vector));

            try {
                certV3Builder.addExtension(Extension.subjectAlternativeName, false, generalNamesSAN);
            } catch (CertIOException e) {
                throw new CertificateException("Unable to add SubjectAlternativeName extension", e);
            }
        }

    }

    /**
     *
     * @param crlUrl
     * @param netscape
     * @throws CertificateException
     */
    public void addCRLExtension(String crlUrl, boolean netscape) throws CertificateException {
        DistributionPointName distPointOne = new DistributionPointName(new GeneralNames(
                new GeneralName(GeneralName.uniformResourceIdentifier, crlUrl)));
        DistributionPoint[] distPoints = new DistributionPoint[1];
        distPoints[0] = new DistributionPoint(distPointOne, null, null);

        try {
            certV3Builder.addExtension(Extension.cRLDistributionPoints, false, new CRLDistPoint(distPoints));

            if (netscape) {
                certV3Builder.addExtension(MiscObjectIdentifiers.netscapeCARevocationURL, false, new DERIA5String(crlUrl));
                certV3Builder.addExtension(MiscObjectIdentifiers.netscapeRevocationURL, false, new NetscapeRevocationURL(
                        new DERIA5String(crlUrl)));
            }

        } catch (IOException e) {
            throw new CertificateException("Unable to complete Revocation extension", e);
        }
    }

    /**
     * Uses bit OR operator on {@link CertificateKeyUsage} values to add required KeyUsages.<br>
     * example: addKeyUsageExtension({@link CertificateKeyUsage#CRL_SIGN} | {@link CertificateKeyUsage#KEY_CERT_SIGN})
     *
     * @param keyUsages asn1 bitString for key usages.
     * @throws CertificateException
     */
    public void addKeyUsageExtension(int keyUsages) throws CertificateException {
        try {
            certV3Builder.addExtension(Extension.keyUsage, true, new KeyUsage(keyUsages));
        } catch (CertIOException e) {
            throw new CertificateException("Unable to add KeyUsage extension", e);
        }
    }

    /**
     * Uses bit OR operator on {@link CertificateKeyUsage} values to add required KeyUsages.<br>
     * example: addKeyUsageExtension({@link CertificateKeyUsage#CRL_SIGN} | {@link CertificateKeyUsage#KEY_CERT_SIGN})
     *
     * @param vector
     * @throws CertificateException
     */
    public void addKeyUsageExtensionExtendedKeyUsage(KeyPurposeId[] vector) throws CertificateException {
        try {
            certV3Builder.addExtension(Extension.keyUsage, true, new ExtendedKeyUsage(vector));
        } catch (CertIOException e) {
            throw new CertificateException("Unable to add KeyUsage extension", e);
        }
    }

    /**
     * Uses bit OR operator on {@link CertificateKeyUsage} values to add required KeyUsages.<br>
     * example: addKeyUsageExtension({@link CertificateKeyUsage#CRL_SIGN} | {@link CertificateKeyUsage#KEY_CERT_SIGN})
     *
     * @param keyPurposeId
     * @throws CertificateException
     */
    public void addKeyUsageExtensionExtendedKeyUsage(KeyPurposeId keyPurposeId) throws CertificateException {
        try {
            certV3Builder.addExtension(Extension.keyUsage, true, new ExtendedKeyUsage(keyPurposeId));
        } catch (CertIOException e) {
            throw new CertificateException("Unable to add KeyUsage extension", e);
        }
    }

    /**
     * Uses bit OR operator on {@link CertificateNetscapeCertType} values to add required netscapeCertTypes.<br>
     * example: {@link #addNetscapeCertTypeExtension}({@link CertificateNetscapeCertType#sslClient} | {@link CertificateNetscapeCertType#smime})
     *
     * @throws CertificateException
     */
    public void addNetscapeCertTypeExtension(int netscapeCertTypes) throws CertificateException {
        try {
            certV3Builder.addExtension(MiscObjectIdentifiers.netscapeCertType, false, new NetscapeCertType(netscapeCertTypes));
        } catch (CertIOException e) {
            throw new CertificateException("Unable to add netscapeCertType extension", e);
        }
    }

    /**
     *
     * @param keyPurposeIds
     * @throws CertificateException
     */
    public void addExtendedKeyUsageExtension(CertificateKeyPurposeId... keyPurposeIds) throws CertificateException {
        int i = 0;
        //Vector<KeyPurposeId> purposes = new Vector<KeyPurposeId>();
        KeyPurposeId[] purposes = new KeyPurposeId[keyPurposeIds.length];

        for (CertificateKeyPurposeId cedricKeyPurposeId : keyPurposeIds) {
            purposes[i] = cedricKeyPurposeId.getKeyPurposeId();
            i++;
        }
        try {
            certV3Builder.addExtension(Extension.extendedKeyUsage, false, new ExtendedKeyUsage(purposes));
        } catch (CertIOException e) {
            throw new CertificateException("Unable to add ExtendedKeyUsage Extension", e);
        }
    }
}
