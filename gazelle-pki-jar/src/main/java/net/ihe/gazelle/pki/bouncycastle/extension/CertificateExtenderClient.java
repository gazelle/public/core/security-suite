package net.ihe.gazelle.pki.bouncycastle.extension;

import net.ihe.gazelle.pki.bouncycastle.X509CertificateParametersContainer;
import net.ihe.gazelle.pki.bouncycastle.*;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public class CertificateExtenderClient extends CertificateExtenderCRL {

    @Override
    public void addExtension(CertificateBuilder certGen, X509CertificateParametersContainer parameters)
            throws CertificateException {
        super.addExtension(certGen, parameters);

        certGen.addKeyUsageExtension(CertificateKeyUsage.DIGITAL_SIGNATURE | CertificateKeyUsage.NON_REPUDIATION | CertificateKeyUsage.KEY_ENCIPHERMENT);
        certGen.addNetscapeCertTypeExtension(CertificateNetscapeCertType.sslClient | CertificateNetscapeCertType.smime);
        certGen.addExtendedKeyUsageExtension(CertificateKeyPurposeId.CLIENT_AUTHENTICATION, CertificateKeyPurposeId.EMAIL_PROTECTION,
                CertificateKeyPurposeId.SMARTCARD_LOGON);

    }

    @Override
    public void modifyCertificate(X509Certificate cert, X509CertificateParametersContainer parameters)
            throws CertificateException {
        super.modifyCertificate(cert, parameters);
        // nothing
    }

}
