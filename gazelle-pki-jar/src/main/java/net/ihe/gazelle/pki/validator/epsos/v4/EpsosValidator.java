package net.ihe.gazelle.pki.validator.epsos.v4;

import net.ihe.gazelle.pki.model.PKiProvider;
import net.ihe.gazelle.pki.validator.AbstractCertificateValidator;
import net.ihe.gazelle.pki.validator.CertificateNormException;
import org.apache.commons.lang.StringUtils;

import javax.security.auth.x500.X500Principal;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.Vector;

public abstract class EpsosValidator extends AbstractCertificateValidator {
    // FIXME Tab and for in new API BouncyCastle ?
    private static final String[] KEY_PURPOSE_STRINGS = new String[]{"anyExtendedKeyUsage", "id_kp_capwapAC",
            "id_kp_clientAuth", "id_kp_capwapWTP", "id_kp_codeSigning", "id_kp_dvcs", "id_kp_eapOverLAN",
            "id_kp_eapOverPPP", "id_kp_emailProtection", "id_kp_ipsecEndSystem", "id_kp_ipsecIKE", "id_kp_ipsecTunnel",
            "id_kp_ipsecUser", "id_kp_OCSPSigning", "id_kp_sbgpCertAAServerAuth", "id_kp_scvp_responder",
            "id_kp_scvpClient", "id_kp_scvpServer", "id_kp_serverAuth", "id_kp_smartcardlogon", "id_kp_timeStamping"};

    public static final String CA_S_CRLS_VALIDATION = "CA's CRLs validation";
    public static final int MAX_BYTES_SIZE = 20;
    public static final String EHEALTH_X509_3_1 = "eHealth DSI - X.509 Certificates Profile - Chapter 3.1";
    public static final String EHEALTH_X509_3_2 = "eHealth DSI - X.509 Certificates Profile - Chapter 3.2";

    /**
     * Constructor.
     * @param po_pkiProvider Instance of the PKI Provider.
     */
    public EpsosValidator(PKiProvider po_pkiProvider) {
        super(po_pkiProvider);
    }

    protected String showFund(List<String> extendedKeyUsage) {
        StringBuilder sb = new StringBuilder(" Fund : {");
        for (int i = 0; i < extendedKeyUsage.size(); i++) {
            if (i != 0) {
                sb.append(", ");
            }
            String value = extendedKeyUsage.get(i);
            for (int j = 0; j < getPKiProvider().getKeyPurposeIds().length; j++) {
                value = getPKiProvider().getPurposeIdValue(value, j, KEY_PURPOSE_STRINGS);
            }
            sb.append(value);
        }
        sb.append("}");
        return sb.toString();
    }

    private void validateCA(List<X509Certificate> list, List<CertificateException> exceptions,
                            List<CertificateException> warnings, boolean revocation) {

        if (revocation) {
            List<String> crlDistributionPointsCA = new ArrayList<String>();

            try {
                getPKiProvider().getCRLDistributionPoints(list.get(0), crlDistributionPointsCA);
                for (String location : crlDistributionPointsCA) {
                    try {
                        X509CRL crl = getCRL(location);
                        validateCRL(crl, list, exceptions, warnings);
                    } catch (CertificateException e) {
                        warnings.add(new CertificateNormException("Failed to check CRL of CA - " + location,
                                CA_S_CRLS_VALIDATION, e));
                    }
                }
            } catch (Exception e) {
                exceptions.add(new CertificateNormException("Failed to check CRL of CA", CA_S_CRLS_VALIDATION, e));
            }
        }
    }

    private void validateCRL(X509CRL crl, List<X509Certificate> list, List<CertificateException> exceptions,
                             List<CertificateException> warnings) {

        if (crl.getVersion() != 2) {
            exceptions.add(new CertificateNormException(
                    "CRLs to be deployed MUST be v2", EHEALTH_X509_3_2));
        }

        String crlSubject = null;
        if (crl.getIssuerX500Principal() != null) {
            crlSubject = crl.getIssuerX500Principal().toString();
        }

        boolean checked = false;
        for (X509Certificate caCertificate : list) {
            String caSubject = null;
            if (caCertificate.getIssuerX500Principal() != null) {
                caSubject = caCertificate.getIssuerX500Principal().toString();
            }
            if (StringUtils.equals(crlSubject, caSubject)) {
                checked = true;
            }
        }
        if (!checked) {
            exceptions.add(new CertificateNormException(
                    "The DName MUST be identical to the subject DName of the issuer certificate.",
                    EHEALTH_X509_3_2));
        }

        if (crl.getThisUpdate() == null || crl.getNextUpdate() == null) {
            exceptions.add(new CertificateNormException("The CRL’s period of validity (from/to) MUST be stated.",
                    EHEALTH_X509_3_2));
        }

        byte[] declaredCaKeyIdentifier = null;
        byte[] authorityKeyIdentifierBytes = crl.getExtensionValue(getPKiProvider().getAuthorityKeyIdentifierObjectId());
        if (authorityKeyIdentifierBytes != null) {
            declaredCaKeyIdentifier = getPKiProvider().getKeyIdentifier(authorityKeyIdentifierBytes, exceptions);
        }
        if (declaredCaKeyIdentifier == null) {
            exceptions
                    .add(new CertificateNormException("The KeyIdentifier MUST be indicated.", EHEALTH_X509_3_2));
        } else {
            checked = false;

            for (X509Certificate caCertificate : list) {
                byte[] caKeyIdentifier = null;

                caKeyIdentifier = getPKiProvider().getCAKeyIdentifier(caCertificate, exceptions);

                if (caKeyIdentifier == null) {
                    exceptions.add(new CertificateNormException("Failed to load SubjectKeyIdentifier of CA",
                            CA_S_CRLS_VALIDATION));
                } else {
                    if (Arrays.equals(declaredCaKeyIdentifier, caKeyIdentifier)) {
                        checked = true;
                    }
                }
            }

            if (!checked) {
                exceptions.add(new CertificateNormException(
                        "AuthorityKeyIdentifier MUST be equal to the SubjectKeyIdentifier of the issuing CA.",
                        CA_S_CRLS_VALIDATION));
            }
        }

        byte[] crlNumberBytes = crl.getExtensionValue(getPKiProvider().getCRLNumberObjectId());
        if (crlNumberBytes == null) {
            exceptions.add(new CertificateNormException("The consecutive number for the CRL issued MUST be indicated.",
                    EHEALTH_X509_3_2));
        } else {

            try {
                byte[] byteArray = getPKiProvider().getCRLNumbertoByteArray(crlNumberBytes);
                if (byteArray.length > MAX_BYTES_SIZE) {
                    exceptions.add(new CertificateNormException(
                            "It MUST have a unique positive integer value with a maximum length of 20 bytes.",
                            EHEALTH_X509_3_2));
                }

            } catch (Exception e) {
                exceptions.add(new CertificateNormException("Failed to load CRLNumber", CA_S_CRLS_VALIDATION, e));
            }
        }

        getPKiProvider().checkX509CRLExtention(crl, warnings);
    }

    private X509CRL getCRL(String location) throws CertificateException {
        X509CRL result = null;
        try {
            URL url = new URL(location);

            if (url.getProtocol().equals("http") || url.getProtocol().equals("https")) {
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setUseCaches(false);
                conn.setDoInput(true);
                conn.connect();
                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    CertificateFactory cf = CertificateFactory.getInstance("X.509", "BC");
                    result = (X509CRL) cf.generateCRL(conn.getInputStream());
                } else {
                    throw new Exception(conn.getResponseMessage());
                }
            }
        } catch (Exception e) {
            throw new CertificateNormException("Failed to retrieve CRL from " + location, "Validation of the CA CRLs", e);
        }
        return result;
    }

    @Override
    public void validate(List<X509Certificate> chain, List<CertificateException> pkiValidation,
                         List<CertificateException> warnings, boolean revocation) {
        if (chain != null && chain.size() >= 2) {
            for (int i = 0; i < chain.size(); i++) {
                X509Certificate x509Certificate = chain.get(i);
                if (i == 0) {
                    boolean issuingCaSupportOcsp = false;

                    issuingCaSupportOcsp = getPKiProvider().setIssuingCaSupportOcsp(chain);

                    validate_2_2_2(x509Certificate, pkiValidation, warnings, issuingCaSupportOcsp);
                } else {
                    List<X509Certificate> subList = chain.subList(i, chain.size());
                    validateCA(subList, pkiValidation, warnings, revocation);
                }
            }
            validateIssuers(chain, pkiValidation, warnings);
        } else {
            // FIXME
            if (chain != null && chain.size() == 1) {
                validate_2_2_2(chain.get(0), pkiValidation, warnings, false);
                validateCA(chain, pkiValidation, warnings, revocation);
            }
            pkiValidation.add(new CertificateException("Invalid chain of certificates (at last 2 certificates)"));
        }
    }



    //X.509 Certificate Profiles v2.2.2 (eHealth DSI provider)
    private void validate_2_2_2(X509Certificate x509Certificate, List<CertificateException> exceptions,
                                List<CertificateException> warnings, boolean issuingCaSupportOcsp) {

        // Signature alorithm
        // 5.1
        //FIXME missing signature algorithm validation

        // Serial number
        validateSerialNumber(x509Certificate, exceptions);

        // Validity from/to
        validateDate(x509Certificate, exceptions, warnings);

        // IssuerUniqueID
        boolean[] issuerUniqueID = x509Certificate.getIssuerUniqueID();
        if (issuerUniqueID != null && issuerUniqueID.length > 0) {
            // Not sure about max length...
            exceptions.add(new CertificateNormException("The field \"IssuerUniqueID\" MUST NOT be used.",
                    EHEALTH_X509_3_1));
        }

        // SubjectUniqueID
        boolean[] subjectUniqueID = x509Certificate.getSubjectUniqueID();
        if (subjectUniqueID != null && subjectUniqueID.length > 0) {
            // Not sure about max length...
            exceptions.add(new CertificateNormException("The field \"SubjectUniqueID\" MUST NOT be used.",
                    EHEALTH_X509_3_1));
        }

        // Subject
        X500Principal subject = x509Certificate.getSubjectX500Principal();

        validateSubject(subject.getName(), exceptions, warnings);


        Set<String> criticalExtensionOIDs = x509Certificate.getCriticalExtensionOIDs();
        Set<String> nonCriticalExtensionOIDs = x509Certificate.getNonCriticalExtensionOIDs();

        validateNonCritical("AuthorityKeyIdentifier", getPKiProvider().getAuthorityKeyIdentifierObjectId(), criticalExtensionOIDs,
                nonCriticalExtensionOIDs, false, exceptions, warnings);
        // checked in validateIssuers

        validateSubjectKeyIdentifier(x509Certificate, criticalExtensionOIDs, nonCriticalExtensionOIDs, exceptions,
                warnings);
        validateKeyUsage(x509Certificate, criticalExtensionOIDs, nonCriticalExtensionOIDs, exceptions, warnings);

        validateNonCritical("IssuerAlternativeName", getPKiProvider().getIssuerAlternativeNameObjectId(), criticalExtensionOIDs,
                nonCriticalExtensionOIDs, true, exceptions, warnings);
        validateNonCritical("SubjectAlternativeName", getPKiProvider().getSubjectAlternativeNameObjectId(), criticalExtensionOIDs,
                nonCriticalExtensionOIDs, true, exceptions, warnings);

        try {
            Collection<List<?>> subjectAltNames = x509Certificate.getSubjectAlternativeNames();
            boolean flagURI = false;
            boolean flagrfc822Name = false; //email
            if (subjectAltNames != null) {
                for (List subjectAltName : subjectAltNames) {
                    if ((Integer) subjectAltName.get(0) == getPKiProvider().getRFC822Name()) {
                        flagrfc822Name = true;
                    } else if ((Integer) subjectAltName.get(0) == getPKiProvider().getUniformResourceIdentifier()) {
                        if (subjectAltName.get(1) instanceof String) {
                            String uri = ((String) subjectAltName.get(1));
                            if (uri.startsWith("http://") || uri.startsWith("ldap://") || uri.startsWith("ftp://")) {
                                flagURI = true;
                            }
                        }
                    }
                }
                if (!flagURI) {
                    warnings.add(new CertificateNormException("If [SubjectAltNames] is used, a corresponding LDAP-, HTTP- or FTP-URL SHOULD be " +
                            "provided.",
                            EHEALTH_X509_3_1));
                }
                if (!flagrfc822Name) {
                    warnings.add(new CertificateNormException("If [SubjectAltNames] is used, E-Mail addresses (RFC822-name) [RFC 822] MAY also be " +
                            "made available.",
                            EHEALTH_X509_3_1));
                }
            }
        } catch (CertificateParsingException e) {
            e.printStackTrace();
        }

        validateCritical("BasicConstraints", getPKiProvider().getBasicConstraintsObjectId(), criticalExtensionOIDs,
                nonCriticalExtensionOIDs, true, exceptions, warnings);

        // checked in validateIssuers

        validateExtendedKeyUsage(x509Certificate, criticalExtensionOIDs, nonCriticalExtensionOIDs, exceptions, warnings);

        validateNonCritical("CRLDistributionPoints", getPKiProvider().getCRLDistributionPointsObjectId(), criticalExtensionOIDs,
                nonCriticalExtensionOIDs, true, exceptions, warnings);
        validateNonCritical("CertificatePolicies", getPKiProvider().getCertificatePoliciesObjectId(), criticalExtensionOIDs,
                nonCriticalExtensionOIDs, true, exceptions, warnings);

        try {
            boolean aresPresent = false;
            String policyIdentifier = getPKiProvider().getPolicyIdentifier(x509Certificate);

            if (policyIdentifier.startsWith("1.3.130.0.2017.")) {
                aresPresent = true;
            }

            if (!aresPresent) {
                warnings.add(new CertificateNormException("CertificatePolicies SHOULD be included as an extension in the certificate to " +
                             "include the eHealth DSI certificate policy identifier: 1.3.130.0.2017.ARES_number_of_the_present_document",
                             EHEALTH_X509_3_1));
            }
        } catch (CertificateException e) {
            exceptions.add(e);
        }

        validateNonCritical("AuthorityInfoAccess", getPKiProvider().getAuthorityInfoAccessObjectId(), criticalExtensionOIDs,
                nonCriticalExtensionOIDs, true, exceptions, warnings);

        byte[] crlDistributionPoints = x509Certificate.getExtensionValue(getPKiProvider().getCRLDistributionPointsObjectId());

        boolean crlDistributionPointsNull = false;
        if (crlDistributionPoints == null || crlDistributionPoints.length == 0) {
            crlDistributionPointsNull = true;
        }

        try {
            if (!crlDistributionPointsNull) {
                List<String> crlDistributionPointList = new ArrayList<String>();
                getPKiProvider().getCRLDistributionPoints(x509Certificate, crlDistributionPointList);
                boolean crlUriPresent = false;
                if (crlDistributionPointList != null) {
                    for (String crlDistributionPoint : crlDistributionPointList) {
                        if (crlDistributionPoint.startsWith("http://") || crlDistributionPoint.startsWith("ldap://")) {
                            crlUriPresent = true;
                        }
                    }
                }
                if (!crlUriPresent) {
                    exceptions.add(new CertificateNormException(
                            "In the CRL Distribution Points, at least one of the present references MUST use either http (http://) [RFC 7230-7235] " +
                                    "or ldap (ldap://) [RFC 4516] scheme.", EHEALTH_X509_3_1));
                }
            }
        } catch (CertificateException e) {
            e.printStackTrace();
        }

        boolean ocspPresent = getPKiProvider().getOCSPPresent(x509Certificate);
        boolean accessLocationForOcspPresent = getPKiProvider().getAccessLocationForOcspPresent(x509Certificate);

        if (issuingCaSupportOcsp && !accessLocationForOcspPresent) {
            exceptions.add(new CertificateNormException(
                    "When OCSP is supported by the issuing CA, the Authority Information Access extension MUST include an accessMethod OID, " +
                            "id-ad-ocsp, with an accessLocation value specifying at least one access location of an OCSP [RFC 6960] responder " +
                            "authoritative to provide certificate status information for the present certificate.", EHEALTH_X509_3_1));
        }

        if (crlDistributionPointsNull && !ocspPresent) {
            exceptions.add(new CertificateNormException(
                    "If the certificate does not include any access location of an OCSP responder as specified in “AIA extension”, then the " +
                            "certificate MUST include a CRL distribution point extension.", EHEALTH_X509_3_1));
            exceptions.add(new CertificateNormException(
                    "A reference to at least one OCSP responder MUST be present if the certificate does not include any CRL distribution point " +
                            "extension.", EHEALTH_X509_3_1));
        }

        validateQCStatementUsages(x509Certificate, criticalExtensionOIDs, nonCriticalExtensionOIDs, exceptions, warnings);

    }

    private void validateCritical(String string, String objectIdentifierId,
                                  Set<String> criticalExtensionOIDs, Set<String> nonCriticalExtensionOIDs, boolean should,
                                  List<CertificateException> exceptions, List<CertificateException> warnings) {
        if (criticalExtensionOIDs == null || !criticalExtensionOIDs.contains(objectIdentifierId)) {
            if (!should) {
                exceptions.add(new CertificateNormException(string
                        + " MUST be included as a critical extension in the certificate.",
                        EHEALTH_X509_3_1));
            } else {
                warnings.add(new CertificateNormException(string
                        + " SHOULD be included as a critical extension in the certificate.",
                        EHEALTH_X509_3_1));
            }
        }

        if (nonCriticalExtensionOIDs != null && nonCriticalExtensionOIDs.contains(objectIdentifierId)) {
            if (!should) {
                exceptions.add(new CertificateNormException(string
                        + " MUST always be designated as critical in the certificate (fund it as non critical).",
                        EHEALTH_X509_3_1));
            } else {
                warnings.add(new CertificateNormException(string
                        + " SHOULD always be designated as critical in the certificate (fund it as non critical).",
                        EHEALTH_X509_3_1));
            }
        }
    }

    private void validateNonCritical(String string, String objectIdentifierId,
                                     Set<String> criticalExtensionOIDs, Set<String> nonCriticalExtensionOIDs, boolean should,
                                     List<CertificateException> exceptions, List<CertificateException> warnings) {
        if (criticalExtensionOIDs != null && criticalExtensionOIDs.contains(objectIdentifierId)) {
            exceptions.add(new CertificateNormException(string
                    + " MUST always be designated as non-critical in the certificate (fund it as critical).",
                    EHEALTH_X509_3_1));
        }
        if (nonCriticalExtensionOIDs == null || !nonCriticalExtensionOIDs.contains(objectIdentifierId)) {
            if (!should) {
                exceptions.add(new CertificateNormException(string
                        + " MUST be included as a non-critical extension in the certificate.",
                        EHEALTH_X509_3_1));
            } else {
                warnings.add(new CertificateNormException(string
                        + " SHOULD be included as a non-critical extension in the certificate.",
                        EHEALTH_X509_3_1));
            }
        }
    }

    private void validateDate(X509Certificate x509Certificate, List<CertificateException> exceptions,
                              List<CertificateException> warnings) {
        Date notAfter = x509Certificate.getNotAfter();
        Date notBefore = x509Certificate.getNotBefore();
        Calendar cl = Calendar.getInstance();
        cl.setTime(notBefore);
        cl.add(Calendar.YEAR, 2);
        if (notAfter.after(cl.getTime())) {
            warnings.add(new CertificateNormException(
                    "Certificates used by epSOS services SHOULD be valid for a maximum of 2 year.",
                    EHEALTH_X509_3_1));
        }
    }

    protected void validateExtendedKeyUsage(X509Certificate x509Certificate, Set<String> criticalExtensionOIDs,
                                            Set<String> nonCriticalExtensionOIDs, List<CertificateException> exceptions,
                                            List<CertificateException> warnings) {
        validateNonCritical("ExtendedKeyUsage", getPKiProvider().getExtendedKeyUsageObjectId(), criticalExtensionOIDs,
                nonCriticalExtensionOIDs, true, exceptions, warnings);
        List<String> extendedKeyUsages;
        try {
            extendedKeyUsages = x509Certificate.getExtendedKeyUsage();
        } catch (CertificateParsingException e) {
            exceptions.add(e);
            return;
        }
        if (extendedKeyUsages != null) {
            validateExtendedKeyUsages(extendedKeyUsages, exceptions, warnings);
        }
    }

    protected void validateExtendedKeyUsages(List<String> extendedKeyUsages, List<CertificateException> exceptions, List<CertificateException> warnings) {
        // KeyPurposeId.
    }

    protected void validateQCStatementUsages(X509Certificate x509Certificate, Set<String> criticalExtensionOIDs,
                                             Set<String> nonCriticalExtensionOIDs, List<CertificateException> exceptions,
                                             List<CertificateException> warnings) {
        getPKiProvider().validateQCStatementUsages(x509Certificate, exceptions);
    }



  /*  protected final void validateExtendedKeyUsagesOneValue(List<String> extendedKeyUsage, String string,
                                                           CertificateKeyPurposeId keyPurposeId, List<CertificateException> exceptions, String chapter) {
        if (extendedKeyUsage != null && extendedKeyUsage.size() > 0) {
            if (extendedKeyUsage.size() > 1) {
                exceptions.add(new CertificateNormException(
                        "If ExtendedKeyUsages extension is included in the certificate, "
                                + "it MUST only accept the value " + string + "." + showFund(extendedKeyUsage),
                        "epSOS WP 3.4.2 - Chapter " + chapter));
            } else {
                if (!extendedKeyUsage.get(0).equals(keyPurposeId.getKeyPurposeId().getId())) {
                    exceptions.add(new CertificateNormException(
                            "If ExtendedKeyUsages extension is included in the certificate, "
                                    + "it MUST only accept the value " + string + "." + showFund(extendedKeyUsage),
                            "epSOS WP 3.4.2 - Chapter " + chapter));
                }
            }
        }
    } */

    private void validateIssuers(List<X509Certificate> chain, List<CertificateException> pkiValidation,
                                 List<CertificateException> warnings) {
        X509Certificate endCert = chain.get(0);
        X509Certificate caCert = chain.get(1);

        // 5.4.2 AuthorityKeyIdentifier

        byte[] caKeyIdentifier = null;

        byte[] subjectKeyIdentifierBytes = caCert.getExtensionValue(getPKiProvider().getSubjectKeyIdentifierObjectId());
        if (subjectKeyIdentifierBytes != null) {
            try {
                caKeyIdentifier = getPKiProvider().getKeyIdentifier(subjectKeyIdentifierBytes);
            } catch (Exception e) {
                pkiValidation.add(new CertificateNormException("Failed to load SubjectKeyIdentifier",
                        EHEALTH_X509_3_1, e));
            }
        }

        if (caKeyIdentifier == null) {
            pkiValidation.add(new CertificateNormException("Failed to load SubjectKeyIdentifier of CA",
                    EHEALTH_X509_3_1));
        } else {
            byte[] declaredCaKeyIdentifier = null;

            byte[] authorityKeyIdentifierBytes = endCert.getExtensionValue(getPKiProvider().getAuthorityKeyIdentifierObjectId());
            if (authorityKeyIdentifierBytes != null) {
                try {
                    declaredCaKeyIdentifier = getPKiProvider().getAuthorityKeyIdentifierKeyIdentifier(authorityKeyIdentifierBytes);

                    if (getPKiProvider().getAuthorityCertIssuer(authorityKeyIdentifierBytes) != null) {
                        warnings.add(new CertificateNormException(
                                "AuthorityCertIssuer SHOULD NOT be used in AuthorityKeyIdentifier.",
                                EHEALTH_X509_3_1));
                    }
                    if (getPKiProvider().getAuthorityCertSerialNumber(authorityKeyIdentifierBytes) != null) {
                        warnings.add(new CertificateNormException(
                                "AuthorityCertSerialNumber SHOULD NOT be used in AuthorityKeyIdentifier.",
                                EHEALTH_X509_3_1));
                    }

                } catch (Exception e) {
                    pkiValidation.add(new CertificateNormException("Failed to load AuthorityKeyIdentifier",
                            EHEALTH_X509_3_1, e));
                }
            }

            if (declaredCaKeyIdentifier == null) {
                pkiValidation.add(new CertificateNormException("Failed to load AuthorityKeyIdentifier of end cert",
                        EHEALTH_X509_3_1));
            } else {
                if (!Arrays.equals(declaredCaKeyIdentifier, caKeyIdentifier)) {
                    pkiValidation.add(new CertificateNormException(
                            "AuthorityKeyIdentifier MUST be equal to the SubjectKeyIdentifier of the issuing CA.",
                            EHEALTH_X509_3_1));
                }
            }
        }

        List<String> crlDistributionPointsEndCert = new ArrayList<String>();
        List<String> crlDistributionPointsCA = new ArrayList<String>();

        try {
            getPKiProvider().getCRLDistributionPoints(endCert, crlDistributionPointsEndCert);
        } catch (Exception e) {
            pkiValidation.add(new CertificateNormException("Failed to check CRLDistributionPoints of end certificate",
                    EHEALTH_X509_3_1, e));
        }
        try {
            getPKiProvider().getCRLDistributionPoints(caCert, crlDistributionPointsCA);
        } catch (Exception e) {
            pkiValidation.add(new CertificateNormException("Failed to check CRLDistributionPoints of CA certificate",
                    EHEALTH_X509_3_1, e));
        }
        if (crlDistributionPointsEndCert.size() == 0) {
            warnings.add(new CertificateNormException("CRLDistributionPoints SHOULD be provided for end certificate",
                    EHEALTH_X509_3_1));
        }
        if (crlDistributionPointsCA.size() == 0) {
            warnings.add(new CertificateNormException("CRLDistributionPoints SHOULD be provided for CA certificate",
                    EHEALTH_X509_3_1));
        }
        if (crlDistributionPointsEndCert.size() == 1 &&
                crlDistributionPointsCA.size() == 1 &&
                !StringUtils.equals(crlDistributionPointsEndCert.get(0), crlDistributionPointsCA.get(0))) {
            warnings.add(new CertificateNormException("CRLDistributionPoints SHOULD include the HTTP address from "
                    + "which the certificate-issuing authority’s complete revocation list can be retrieved.",
                    EHEALTH_X509_3_1));
        }

        String ocspEndCert0 = null;
        try {
            ocspEndCert0 = getPKiProvider().getOCSPFromAuthorityInfoAccess(endCert);
            if (ocspEndCert0 != null) {
                log.info(ocspEndCert0);
                URL url = new URL(ocspEndCert0);
                String protocol = url.getProtocol().toLowerCase();
                if (!StringUtils.equals(protocol, "http") && !StringUtils.equals(protocol, "https")) {
                    log.info(ocspEndCert0);
                    pkiValidation.add(new CertificateNormException("When the issuing CA offers an OCSP service, "
                            + "its HTTP URI MUST be included in the AuthorityInfoAccess extension. " + ocspEndCert0,
                            EHEALTH_X509_3_1));
                }
            }
        } catch (Exception e) {
            pkiValidation.add(new CertificateNormException("Failed to check AuthorityInfoAccess of end certificate",
                    EHEALTH_X509_3_1, e));
        }

    }

    /**
     *
     * @param bits
     * @param exceptions
     */
    protected void validateKeyUsage(int bits, List<CertificateException> exceptions) {
    }

    /**
     *
     * @param x509Certificate
     * @param criticalExtensionOIDs
     * @param nonCriticalExtensionOIDs
     * @param exceptions
     * @param warnings
     */
    private void validateKeyUsage(X509Certificate x509Certificate, Set<String> criticalExtensionOIDs,
                                  Set<String> nonCriticalExtensionOIDs, List<CertificateException> exceptions,
                                  List<CertificateException> warnings) {
        validateCritical("KeyUsage", getPKiProvider().getKeyUsageObjectId(), criticalExtensionOIDs, nonCriticalExtensionOIDs, false,
                exceptions, warnings);
        byte[] keyUsageBytes = x509Certificate.getExtensionValue(getPKiProvider().getKeyUsageObjectId());
        if (keyUsageBytes == null) {
            exceptions.add(new CertificateNormException("KeyUsage is null", EHEALTH_X509_3_1));
        } else {

            int bits = getPKiProvider().getkeyUsageBytes(keyUsageBytes, exceptions);
            validateKeyUsage(bits, exceptions);
        }
    }

    protected final void validateKeyUsageValue(int bits, int keyUsageBits, String error,
                                               List<CertificateException> exceptions, String chapter) {
        if (bits != keyUsageBits) {
            exceptions.add(new CertificateNormException(error + keyUsageToString(bits), chapter));
        }
    }

    protected final void validateKeyUsageValue(int bits, int[] keyUsageBitsTab, String error,
                                               List<CertificateException> exceptions, String chapter) {
        boolean flag = false;
        for (int keyUsageBits : keyUsageBitsTab) {
            if (bits == keyUsageBits) {
                flag = true;
                break;
            }
        }

        if (flag == false) {
            exceptions.add(new CertificateNormException(error + keyUsageToString(bits), chapter));
        }
    }

    private String keyUsageToString(int bits) {
        List<String> usages = new ArrayList<String>();
        if ((bits & getPKiProvider().getKeyUsageDigitalSignature()) == getPKiProvider().getKeyUsageDigitalSignature()) {
            usages.add("digitalSignature");
        }
        if ((bits & getPKiProvider().getKeyUsageNonRepudiation()) == getPKiProvider().getKeyUsageNonRepudiation()) {
            usages.add("nonRepudiation");
        }
        if ((bits & getPKiProvider().getKeyUsageKeyEncipherment()) == getPKiProvider().getKeyUsageKeyEncipherment()) {
            usages.add("keyEncipherment");
        }
        if ((bits & getPKiProvider().getKeyUsageDataEncipherment()) == getPKiProvider().getKeyUsageDataEncipherment()) {
            usages.add("dataEncipherment");
        }
        if ((bits & getPKiProvider().getKeyUsageKeyAgreement()) == getPKiProvider().getKeyUsageKeyAgreement()) {
            usages.add("keyAgreement");
        }
        if ((bits & getPKiProvider().getKeyUsageKeyCertSign()) == getPKiProvider().getKeyUsageKeyCertSign()) {
            usages.add("keyCertSign");
        }
        if ((bits & getPKiProvider().getKeyUsageCRLSign()) == getPKiProvider().getKeyUsageCRLSign()) {
            usages.add("cRLSign");
        }
        if ((bits & getPKiProvider().getKeyUsageEncipherOnly()) == getPKiProvider().getKeyUsageEncipherOnly()) {
            usages.add("encipherOnly");
        }
        if ((bits & getPKiProvider().getKeyUsageDecipherOnly()) == getPKiProvider().getKeyUsageDecipherOnly()) {
            usages.add("decipherOnly");
        }
        return showFund(usages);
    }

    /**
     *
     * @param name
     * @param exceptions
     * @param warnings
     */
    public void validateSubject(String name, List<CertificateException> exceptions, List<CertificateException> warnings) {
        getPKiProvider().validateSubject(name, exceptions, warnings);
    }

    /**
     *
     * @param x509Certificate
     * @param exceptions
     */
    private void validateSerialNumber(X509Certificate x509Certificate, List<CertificateException> exceptions) {
        byte[] byteArray = x509Certificate.getSerialNumber().toByteArray();
        if (byteArray.length > MAX_BYTES_SIZE) {
            exceptions.add(new CertificateNormException(
                    "The serial number MUST be an unambiguous integer value with a maximum of 20 bytes.",
                    EHEALTH_X509_3_1));
        }
        int compareTo = x509Certificate.getSerialNumber().compareTo(BigInteger.valueOf(0));
        if (compareTo < 0) {
            exceptions.add(new CertificateNormException(
                    "The serial number MUST be an unambiguous positive integer value.",
                    EHEALTH_X509_3_1));
        }
    }

    private void validateSubjectKeyIdentifier(X509Certificate x509Certificate, Set<String> criticalExtensionOIDs,
                                              Set<String> nonCriticalExtensionOIDs, List<CertificateException> exceptions,
                                              List<CertificateException> warnings) {
        byte[] value = x509Certificate.getExtensionValue(getPKiProvider().getSubjectKeyIdentifierObjectId());
        if (value == null || value.length == 0) {
            exceptions.add(new CertificateNormException(
                    "SubjectKeyIdentifier MUST be included as an extension in the certificate.",
                    EHEALTH_X509_3_1));
        }
        validateNonCritical("SubjectKeyIdentifier", getPKiProvider().getSubjectKeyIdentifierObjectId(), criticalExtensionOIDs,
                nonCriticalExtensionOIDs, false, exceptions, warnings);
        // One of the methods described in RFC5280 (ch. 4.2.1.2) SHOULD be used.
        try {


            byte[] goodKeyIdentifier     = getPKiProvider().getSKIKeyIdentifier(x509Certificate);
            byte[] providedKeyIdentifier = getPKiProvider().getKeyIdKeyIdentifier(x509Certificate, exceptions);



            if (providedKeyIdentifier != null && !Arrays.equals(providedKeyIdentifier, goodKeyIdentifier)) {
                exceptions.add(new CertificateNormException(
                        "One of the methods described in RFC5280 (ch. 4.2.1.2) SHOULD be used.",
                        EHEALTH_X509_3_1));
            }
        } catch (NoSuchAlgorithmException e) {
            exceptions.add(new CertificateNormException("Failed to validate SubjectKeyIdentifier",
                    EHEALTH_X509_3_1, e));
        }
    }
}
