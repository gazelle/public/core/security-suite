package net.ihe.gazelle.pki.bouncycastle.extension;

import net.ihe.gazelle.pki.bouncycastle.X509CertificateParametersContainer;
import net.ihe.gazelle.pki.bouncycastle.CertificateKeyUsage;
import net.ihe.gazelle.pki.bouncycastle.CertificateNetscapeCertType;
import net.ihe.gazelle.pki.bouncycastle.CertificateBuilder;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public class CertificateExtenderCA extends CertificateExtenderCRL {

    @Override
    public void addExtension(CertificateBuilder certGen, X509CertificateParametersContainer parameters)
            throws CertificateException {
        super.addExtension(certGen, parameters);

        // KeyUsage 0x6 = '0000 0110'B = keyCertSign ('0000 0100'B) + cRLSign ('0000 0010'B)
        certGen.addKeyUsageExtension(CertificateKeyUsage.KEY_CERT_SIGN | CertificateKeyUsage.CRL_SIGN);

        certGen.addBasicConstraintExtension(true);
        certGen.addSubjectKeyIdentifierExtension(parameters.getPublicKey());

        if (parameters.getCertificateRequest().getCertificateAuthority() != null) {
            certGen.addAuthorityKeyIdentifierExtension(parameters.getCertificateRequest().getCertificateAuthority().getPublicKey().getKey());
        } else {
            certGen.addAuthorityKeyIdentifierExtension(parameters.getPublicKey());
        }

        certGen.addNetscapeCertTypeExtension(CertificateNetscapeCertType.sslCA | CertificateNetscapeCertType.smimeCA | CertificateNetscapeCertType.objectSigningCA);

    }

    @Override
    public void modifyCertificate(X509Certificate cert, X509CertificateParametersContainer parameters)
            throws CertificateException {
        super.modifyCertificate(cert, parameters);
        // PKCS12BagAttributeCarrier bagAttr = (PKCS12BagAttributeCarrier) cert;
        // bagAttr.setBagAttribute(PKCSObjectIdentifiers.pkcs_9_at_friendlyName,
        // new DERBMPString(parameters
        // .getCertificateRequest().getName() + " Authority Certificate"));
    }
}
