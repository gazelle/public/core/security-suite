package net.ihe.gazelle.pki.validator.epsos.v6;

import net.ihe.gazelle.pki.model.PKiProvider;
import net.ihe.gazelle.pki.validator.CertificateNormException;
import net.ihe.gazelle.pki.validator.epsos.v4.EpsosValidator;

import java.security.cert.CertificateException;
import java.util.List;

public class EpsosSealWave6 extends EpsosValidator {
    /**
     * Constructor.
     *
     * @param po_pkiProvider Instance of the PKI Provider.
     */
    public EpsosSealWave6(PKiProvider po_pkiProvider) {
        super(po_pkiProvider);
    }

    @Override
    protected void validateKeyUsage(int bits, List<CertificateException> exceptions) {
        super.validateKeyUsage(bits, exceptions);
        int keyUsageBits = getPKiProvider().getSealKeyUsageBits();
        validateKeyUsageValue(bits, keyUsageBits,
                "The digitalSignature and keyEncipherment bits MUST both be set to true to the exclusion of all other KeyUSage bits that MUST be " +
                        "set to false.", exceptions,
                EHEALTH_X509_3_1);
    }

    @Override
    protected void validateExtendedKeyUsages(List<String> extendedKeyUsages, List<CertificateException> exceptions, List<CertificateException> warnings) {
        super.validateExtendedKeyUsages(extendedKeyUsages, exceptions, warnings);
        if (extendedKeyUsages != null && extendedKeyUsages.size() > 0) {
            if (!(extendedKeyUsages.contains(getPKiProvider().getIdKpClientAuthId())
                    && extendedKeyUsages.contains(getPKiProvider().getIdKpEmailProtection()))) {
                warnings.add(new CertificateNormException(
                        "Both values id_kp_emailProtection [RFC5280] and id‐kp‐clientAuth [RFC5280] SHOULD be present.", EHEALTH_X509_3_1));
            }
        } else {
            exceptions.add(new CertificateNormException(
                    "Both values id_kp_emailProtection [RFC5280] and id‐kp‐clientAuth [RFC5280] SHOULD be present.", EHEALTH_X509_3_1));
        }
    }
}
