package net.ihe.gazelle.pki.core;

public enum PKIKeyFormat {

    PKCS8("PKCS#8", "PKCS8"),
    X509("X.509", "X509"),
    RAW("RAW");

    private PKIKeyFormat(String... providerLabel) {

    }

}
