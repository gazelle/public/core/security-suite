package net.ihe.gazelle.pki.bouncycastle;

import net.ihe.gazelle.pki.model.CertificatePrivateKey;
import net.ihe.gazelle.pki.model.CertificateX509;
import org.apache.poi.util.IOUtils;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.openssl.PEMDecryptorProvider;
import org.bouncycastle.openssl.PEMEncryptedKeyPair;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.bouncycastle.openssl.jcajce.JcePEMDecryptorProviderBuilder;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;

import java.io.*;
import java.security.KeyPair;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

public class CertificatePEMTools {
    private static CertificatePEMTools mo_instance;

    /**
     *
     */
    private CertificatePEMTools() { }

    /**
     *
     * @return
     */
    public static CertificatePEMTools getInstance() {
        if (mo_instance == null)
            mo_instance = new CertificatePEMTools();

        return mo_instance;
    }

    /**
     *
     * @param certificateX509
     * @return
     * @throws CertificateException
     */
    public byte[] getPEMCertificate(CertificateX509 certificateX509) throws CertificateException {
        return writePEMObject(certificateX509.getX509Certificate(new CertificateBC()));
    }

    /**
     *
     * @param certificatePrivateKey
     * @return
     * @throws CertificateException
     */
    public byte[] getPEMKey(CertificatePrivateKey certificatePrivateKey) throws CertificateException {
        return writePEMObject(certificatePrivateKey.getKey());
    }

    /**
     *
     * @param certificateChain
     * @return
     * @throws CertificateException
     */
    public byte[] getPEMCertificateChain(X509Certificate[] certificateChain) throws CertificateException {
        return writeMultiplePEMObject(certificateChain);
    }

    /**
     *
     * @param pkiObject
     * @return
     * @throws CertificateException
     */
    public byte[] writePEMObject(Object pkiObject) throws CertificateException {
        ByteArrayOutputStream baos        = null;
        Writer destination = null;
        try {
            baos = new ByteArrayOutputStream();
            destination = new OutputStreamWriter(baos);

            try(final JcaPEMWriter pemWriter = new JcaPEMWriter(destination)) {
                pemWriter.writeObject(pkiObject);
            } catch (IOException e) {
                throw new CertificateException(e);
            }

            final byte[] bytes = baos.toByteArray();

            return bytes;
        } finally {
            try {
                if (destination != null) {
                    destination.close();
                }
                if (baos != null) {
                    baos.close();
                }
            } catch (IOException e) {
                throw new CertificateException("Unexpected error, unable to close stream", e);
            }
        }
    }

    /**
     *
     * @param pkiObjects
     * @param <T>
     * @return
     * @throws CertificateException
     */
    public <T extends Object> byte[] writeMultiplePEMObject(T[] pkiObjects) throws CertificateException {
        ByteArrayOutputStream baos        = null;
        OutputStreamWriter    destination = null;
        try {
            baos = new ByteArrayOutputStream();
            destination = new OutputStreamWriter(baos);

            try(final JcaPEMWriter pemWriter = new JcaPEMWriter(destination)) {
                for (T pkiObject : pkiObjects) {
                    pemWriter.writeObject(pkiObject);
                }
            } catch (IOException e) {
                throw new CertificateException(e);
            }

            final byte[] bytes = baos.toByteArray();

            return bytes;
        } catch (Exception e) {
            throw new CertificateException(e);
        } finally {
            try {
                if (destination != null) {
                    destination.close();
                }
                if (baos != null) {
                    baos.close();
                }
            } catch (IOException e) {
                throw new CertificateException("Unexpected error, unable to close stream", e);
            }
        }
    }

    /**
     *
     * @param po_reader
     * @return
     * @throws IOException
     * @throws CertificateException
     */
    public List<X509Certificate> getCertificatesFromPemObject(Reader po_reader) throws IOException, CertificateException {
        List<X509Certificate> certificates = new ArrayList<>();
        PemReader pemReader = new PemReader(po_reader);

        try {
            PemObject pemObject = pemReader.readPemObject();
            JcaX509CertificateConverter certificateConverter = new JcaX509CertificateConverter().setProvider("BC");
            while (pemObject != null) {
                X509CertificateHolder fromFile = new X509CertificateHolder(pemObject.getContent());
                certificates.add(certificateConverter.getCertificate(fromFile));
                pemObject = pemReader.readPemObject();
            }
        } finally {
            IOUtils.closeQuietly(pemReader);
        }
        return certificates;
    }

    /**
     *
     * @param po_reader
     * @param ps_password
     * @return
     * @throws IOException
     */
    public KeyPair getKeyPairFromPEM(Reader po_reader, String ps_password) throws IOException {
        PEMParser pemParser = null;

        try {
            pemParser = new PEMParser(po_reader);
            Object readObject = pemParser.readObject();

            PEMKeyPair pemKeyPair;

            if (readObject instanceof PEMEncryptedKeyPair) {
                PEMDecryptorProvider decryptor = new JcePEMDecryptorProviderBuilder().build(ps_password.toCharArray());
                pemKeyPair = ((PEMEncryptedKeyPair) readObject).decryptKeyPair(decryptor);
            } else if (readObject instanceof PEMKeyPair) {
                pemKeyPair = (PEMKeyPair) readObject;
            } else {
                throw new IOException(
                        "The given PEM file is not a supported format [BEGIN RSA PRIVATE KEY, BEGIN DSA PRIVATE KEY, BEGIN EC PRIVATE KEY]");
            }

            JcaPEMKeyConverter converter = new JcaPEMKeyConverter().setProvider("BC");
            return converter.getKeyPair(pemKeyPair);

        } finally {
            IOUtils.closeQuietly(pemParser);
        }
    }

}
