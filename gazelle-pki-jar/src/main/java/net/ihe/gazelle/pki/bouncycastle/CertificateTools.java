package net.ihe.gazelle.pki.bouncycastle;

import net.ihe.gazelle.pki.validator.CertificateNormException;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.asn1.x500.AttributeTypeAndValue;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x509.*;

import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.DEROctetString;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.cert.*;
import java.util.*;

import static net.ihe.gazelle.pki.validator.epsos.v4.EpsosValidator.EHEALTH_X509_3_1;


public class CertificateTools {
    public static final int SUBJECT_ATTRIBUTE_MAX_BYTES_SIZE = 64;

    private List<String> countries = Arrays.asList(Locale.getISOCountries());

    private static CertificateTools mo_instance;

    /**
     *
     */
    private CertificateTools() { }

    /**
     *
     * @return
     */
    public static CertificateTools getInstance() {
        if (mo_instance == null)
            mo_instance = new CertificateTools();

        return mo_instance;
    }

    /**
     *
     * @param po_basicConstraintsBytes
     * @param po_exceptions
     * @throws IOException
     */
    public void checkCABasicConstraint(byte[] po_basicConstraintsBytes, List<CertificateException> po_exceptions) throws IOException {
        DEROctetString oct = (DEROctetString) (new ASN1InputStream(new ByteArrayInputStream(
                po_basicConstraintsBytes)).readObject());
        BasicConstraints basicConstraints = BasicConstraints.getInstance(new ASN1InputStream(
                new ByteArrayInputStream(oct.getOctets())).readObject());

        if (basicConstraints.isCA()) {
            po_exceptions.add(new CertificateNormException(
                    "The BasicConstraints extension MUST assume the value FALSE for ca.",
                    CertificateConstants.EP_SOS_WP_3_4_2_CHAPTER_5_4_2));
        }
    }

    /**
     *
     * @param po_crlNumberBytes
     * @return
     * @throws Exception
     */
    public byte[] getCRLNumbertoByteArray(byte[] po_crlNumberBytes) throws Exception {
        ASN1InputStream aIn = new ASN1InputStream(new ByteArrayInputStream(po_crlNumberBytes));
        ASN1OctetString octs = (ASN1OctetString) aIn.readObject();

        aIn = new ASN1InputStream(new ByteArrayInputStream(octs.getOctets()));
        ASN1Primitive obj = aIn.readObject();

        CRLNumber crlnum = CRLNumber.getInstance(obj);
        BigInteger crlNumberBigInt = crlnum.getCRLNumber();

        return crlNumberBigInt.toByteArray();
    }

    /**
     *
     * @param po_name
     * @param po_identifier
     * @return
     */
    public final Vector<String> getSubjectVector(X500Name po_name, ASN1ObjectIdentifier po_identifier) {
        AttributeTypeAndValue[] atts;
        RDN[] rdns = po_name.getRDNs(po_identifier);

        Vector  v = new Vector();
        for (int i = 0; i != rdns.length; i++) {
            atts = rdns[i].getTypesAndValues();
            v.addElement(atts[0].getValue().toString());
        }
        return v;
    }

    /**
     *
     * @param name
     * @param po_exceptions
     * @return
     */
    public CertificateXName checkX509NameV1(String name, List<CertificateException> po_exceptions) {
        CertificateXName X500Name;
        try {
            X500Name = new CertificateXName(name);
        } catch (IllegalArgumentException e) {
            po_exceptions.add(new CertificateNormException(e, CertificateConstants.EP_SOS_WP_3_4_2_CHAPTER_5_4_1));
            return null;
        }
        return X500Name;
    }

    /**
     *
     * @param name
     * @param po_exceptions
     * @return
     */
    public CertificateXName checkX509Name(String name, List<CertificateException> po_exceptions) {
        CertificateXName X500Name;
        try {
            X500Name = new CertificateXName(name);
        } catch (IllegalArgumentException e) {
            po_exceptions.add(new CertificateNormException(e, EHEALTH_X509_3_1));
            return null;
        }
        return X500Name;
    }

    /**
     *
     * @param name
     * @param po_exceptions
     * @return
     */
    public CertificateXName checkX509NameCertificate(String name, List<CertificateException> po_exceptions, String ps_certificateType) {
        CertificateXName X500Name;
        try {
            X500Name = new CertificateXName(name);
        } catch (IllegalArgumentException e) {
            po_exceptions.add(new CertificateNormException(e, "epSOS WP 3.4.2 - Chapter 5.4.4/5.4.6 - "
                    + ps_certificateType));
            return null;
        }
        return X500Name;
    }

    /**
     *
     * @param country
     * @param organization
     * @param commonName
     * @param title
     * @param givenName
     * @param surname
     * @param organizationalUnit
     * @param eMail
     * @return
     */
    public String computeSubjectUsingAttributes(String country, String organization, String commonName, String title,
                                                String givenName, String surname, String organizationalUnit, String eMail) {

        X500NameBuilder nameBuilder = new X500NameBuilder();

        nameBuilder.addRDN(BCStyle.C, country);
        nameBuilder.addRDN(BCStyle.O, organization);
        nameBuilder.addRDN(BCStyle.CN, commonName);

        if (StringUtils.trimToNull(title) != null) {
            nameBuilder.addRDN(BCStyle.T, title);
        }
        if (StringUtils.trimToNull(givenName) != null) {
            nameBuilder.addRDN(BCStyle.GIVENNAME, givenName);
        }
        if (StringUtils.trimToNull(surname) != null) {
            nameBuilder.addRDN(BCStyle.SURNAME, surname);
        }
        if (StringUtils.trimToNull(organizationalUnit) != null) {
            nameBuilder.addRDN(BCStyle.OU, organizationalUnit);
        }
        if (StringUtils.trimToNull(eMail) != null) {
            nameBuilder.addRDN(BCStyle.E, eMail);
        }

        X500Name principal2= nameBuilder.build();

        return  principal2.toString();
    }

    /**
     *
     * @param name
     * @param exceptions
     * @param warnings
     */
    public void validateSubject(String name, List<CertificateException> exceptions, List<CertificateException> warnings) {
        CertificateXName X509Name;
        X509Name = CertificateTools.getInstance().checkX509Name(name, exceptions);
        if (X509Name != null) {
            validateSubjectCountry(X509Name, exceptions, warnings);
            validateSubjectAttribute(X509Name, CertificateConstants.BCStyleO , SUBJECT_ATTRIBUTE_MAX_BYTES_SIZE, true, false, false, "O", exceptions, warnings);
            validateSubjectAttribute(X509Name, CertificateConstants.BCStyleCN, SUBJECT_ATTRIBUTE_MAX_BYTES_SIZE, true, false, false,"CN", exceptions, warnings);

            validateSubjectAttribute(X509Name, CertificateConstants.BCStyleOU, SUBJECT_ATTRIBUTE_MAX_BYTES_SIZE, true, true, true,"OU", exceptions, warnings);

            Vector<ASN1ObjectIdentifier> oids = new Vector<>();
            oids.add(CertificateConstants.BCStyleDN_QUALIFIER);
            oids.add(CertificateConstants.BCStyleC);
            oids.add(CertificateConstants.BCStyleO);
            oids.add(CertificateConstants.BCStyleCN);
            oids.add(CertificateConstants.BCStyleOU);

            ASN1ObjectIdentifier[] x509NameAttributeTypes = (ASN1ObjectIdentifier[]) X509Name.getAttributeTypes();

            Vector<ASN1ObjectIdentifier> oiDs = new Vector<>();

            for (int i = 0; i != x509NameAttributeTypes.length; i++) {
                oiDs.addElement(x509NameAttributeTypes[i]);
            }

            for (ASN1ObjectIdentifier oid : oiDs) {
                if (!oids.contains(oid)) {
                    String ls_oidName = CertificateConstants.oidToDisplayName(oid);
                    warnings.add(new CertificateNormException(ls_oidName + " SHOULD NOT be provided",
                            EHEALTH_X509_3_1));
                }
            }
            if (!oiDs.contains(CertificateConstants.BCStyleOU)) {
                warnings.add(new CertificateNormException("The DName MAY have OU.", EHEALTH_X509_3_1));
            }
        }
    }

    /**
     *
     * @param name
     * @param identifier
     * @param length
     * @param maxBytes
     * @param should
     * @param identifierName
     * @param exceptions
     * @param warnings
     * @return
     */
    private String validateSubjectAttribute(CertificateXName name, ASN1ObjectIdentifier identifier, int length,
                                            boolean maxBytes, boolean should, boolean canBeRepeated, String identifierName, List<CertificateException> exceptions,
                                            List<CertificateException> warnings) {
        Vector<String> vector = CertificateTools.getInstance().getSubjectVector(name, identifier);

        if (should) {
            if (vector == null || vector.size() == 0) {
                warnings.add(new CertificateNormException("The DName SHOULD have " + identifierName + ".",
                        EHEALTH_X509_3_1));
                return null;
            }
        } else {
            if (vector == null || vector.size() == 0) {
                exceptions.add(new CertificateNormException("The DName MUST have " + identifierName + ".",
                        EHEALTH_X509_3_1));
                return null;
            }
        }

        if (vector.size() != 1 && !canBeRepeated) {
            exceptions.add(new CertificateNormException("The DName MUST have only one " + identifierName + ".",
                    EHEALTH_X509_3_1));
        }
        String value = vector.get(0);

        if (maxBytes) {
            if (value == null || value.length() > length) {
                exceptions.add(new CertificateNormException(
                        identifierName + " MUST be limited by " + length + " bytes", EHEALTH_X509_3_1));
            }
        } else {
            if (value == null || value.length() != length) {
                exceptions.add(new CertificateNormException(
                        identifierName + " MUST be limited by " + length + " bytes", EHEALTH_X509_3_1));
            }
        }
        return value;
    }

    /**
     *
     * @param name
     * @param exceptions
     * @param warnings
     */
    private void validateSubjectCountry(CertificateXName name, List<CertificateException> exceptions,
                                        List<CertificateException> warnings) {
        String country = validateSubjectAttribute(name, CertificateConstants.RFC4519StyleC, 2, false, false, false, "C", exceptions, warnings);
        int indexOfCountry = countries.indexOf(country);
        if (indexOfCountry == -1) {
            exceptions.add(new CertificateNormException("C MUST be a ISO 3166 code", EHEALTH_X509_3_1));
        }
    }
}
