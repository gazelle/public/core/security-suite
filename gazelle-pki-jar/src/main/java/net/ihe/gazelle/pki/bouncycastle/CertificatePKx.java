package net.ihe.gazelle.pki.bouncycastle;

import net.ihe.gazelle.pki.validator.CertificateNormException;
import org.apache.commons.ssl.PEMItem;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.i18n.ErrorBundle;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.jcajce.JcaPKCS10CertificationRequest;
import org.bouncycastle.x509.CertPathReviewerException;
import org.bouncycastle.x509.PKIXCertPathReviewer;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.CertPath;
import java.security.cert.CertificateException;
import java.security.cert.PKIXParameters;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Locale;

public class CertificatePKx {
    private static CertificatePKx mo_instance;

    /**
     *
     */
    private CertificatePKx() { }

    /**
     *
     * @return
     */
    public static CertificatePKx getInstance() {
        if (mo_instance == null)
            mo_instance = new CertificatePKx();

        return mo_instance;
    }

    /**
     *
     * @param po_chain
     * @param po_certPath
     * @param po_pkixParameters
     * @param po_exceptions
     */
    public void validatePKIX(List<X509Certificate> po_chain, CertPath po_certPath, PKIXParameters po_pkixParameters, List<CertificateException> po_exceptions) {
        PKIXCertPathReviewer reviewer;
        try {
            reviewer = new PKIXCertPathReviewer(po_certPath, po_pkixParameters);
            if(!reviewer.isValidCertPath()) {
                for (int i = 0; i < reviewer.getErrors().length-1; i++) {
                    List<ErrorBundle> list = reviewer.getErrors(i);
                    for (int j = 0; j < list.size(); j++) {
                        String subject;
                        X509Certificate cert = po_chain.get(i);
                        subject = "PKIX validation (" + cert.getSubjectDN().getName() + ")";
                        po_exceptions.add(new CertificateNormException(subject + " : " + list.get(j).getDetail(Locale.ENGLISH),
                                "PKIX"));
                    }
                }
            }
        } catch (CertPathReviewerException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @return
     * @throws CertificateException
     */
    public PublicKey getPublicKeyFromPKCS10(PEMItem po_csrPemFormat) throws CertificateException, IOException {
        final JcaPKCS10CertificationRequest pkcs10CertificationRequest = new JcaPKCS10CertificationRequest(
                po_csrPemFormat.getDerBytes());
        try {
            return pkcs10CertificationRequest.getPublicKey();
        } catch (InvalidKeyException e) {
            throw new CertificateException("Loading PublicKey From CSR: Invalid Key", e);
        } catch (NoSuchAlgorithmException e) {
            throw new CertificateException("Loading PublicKey From CSR: No Such Algorithm", e);
        }
    }

    /**
     *
     * @param po_csrPemFormat
     * @return
     */
    public String getPKCS10CertificationSubjectToString(PEMItem po_csrPemFormat) throws IOException {
        final PKCS10CertificationRequest pkcs10CertificationRequest = new PKCS10CertificationRequest(
                po_csrPemFormat.getDerBytes());

        X500Name subject = pkcs10CertificationRequest.getSubject();
        return subject.toString();
    }
}
