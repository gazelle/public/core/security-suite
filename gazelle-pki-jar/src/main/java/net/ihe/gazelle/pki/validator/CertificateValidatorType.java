package net.ihe.gazelle.pki.validator;

import net.ihe.gazelle.common.jsf.Labelable;
import net.ihe.gazelle.pki.bouncycastle.CertificateBC;
import net.ihe.gazelle.pki.enums.ContextType;
import net.ihe.gazelle.pki.model.PKiProvider;
import net.ihe.gazelle.pki.validator.epsos.v4.EpsosSeal;
import net.ihe.gazelle.pki.validator.epsos.v6.EpsosSealWave6;
import net.ihe.gazelle.pki.validator.epsos.v6.EpsosTLSWave6;
import net.ihe.gazelle.pki.validator.tls.CertificateAuthorityValidator;
import net.ihe.gazelle.pki.validator.tls.TlsClientValidator;
import net.ihe.gazelle.pki.validator.tls.TlsServerValidator;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public enum CertificateValidatorType implements
        CertificateValidator, Serializable, Labelable {

    EPSOS_VPN_CLIENT(ContextType.EPSOS, null, "epSOS VPN client v1 (Deprecated)", true),

    EPSOS_VPN_SERVER(ContextType.EPSOS, null, "epSOS VPN server v1 (Deprecated)", true),

    EPSOS_SERVICE_CONSUMER(ContextType.EPSOS, null, "epSOS service consumer v1 (Deprecated)", true),

    EPSOS_SERVICE_PROVIDER(ContextType.EPSOS, null, "epSOS service provider v1 (Deprecated)", true),

    EPSOS_NCP_SIGNATURE(ContextType.EPSOS, null, "epSOS NCP signature v1 (Deprecated)", true),

    EPSOS_OCSP_RESPONDER(ContextType.EPSOS, null, "epSOS OCSP responder v1 (Deprecated)", true),

    TLS_SERVER(ContextType.IHE, TlsServerValidator.class, "TLS server", false),

    TLS_CLIENT(ContextType.IHE, TlsClientValidator.class, "TLS client", false),

    CA(ContextType.IHE, CertificateAuthorityValidator.class, "CA", false),

    EPSOS_VPN_V3(ContextType.EPSOS, null, "epSOS VPN v3 (Deprecated)", true),

    EPSOS_TLS_V3(ContextType.EPSOS, null, "epSOS TLS v3 (Deprecated)", true),

    EPSOS_NCP_V3(ContextType.EPSOS, null, "epSOS Object Signing v3 (Deprecated)", true),

    EPSOS_SEAL_V4(ContextType.EPSOS, EpsosSeal.class, "eHDSI - PKI Seal Certificate - Wave 1, 2, 3, 4, 5", false),

    EPSOS_TLS_V4(ContextType.EPSOS, net.ihe.gazelle.pki.validator.epsos.v4.EpsosTLS.class, "eHDSI - PKI TLS Certificate - Wave 1, 2, 3, 4, 5", false),

    EPSOS_SEAL_V6(ContextType.EPSOS, EpsosSealWave6.class, "eHDSI - PKI Seal Certificate - Wave 6, 7, 8", false),

    EPSOS_TLS_V6(ContextType.EPSOS, EpsosTLSWave6.class, "eHDSI - PKI TLS Certificate - Wave 6, 7, 8", false);

    private String friendlyName;
    private CertificateValidator instance;
    private ContextType context;
    private boolean deprecated;

    private PKiProvider mo_pKiProvider = new CertificateBC();

    /**
     * Getter of attribute mo_pkiProvider.
     * @return the Instance of PKIProvider.
     */
    protected PKiProvider getPKiProvider() {
        return mo_pKiProvider;
    }

    private CertificateValidatorType(ContextType context, Class<? extends CertificateValidator> certificateValidatorClass,
                                     String friendlyName, boolean deprecated) {
        if (certificateValidatorClass != null) {
            try {
                Class[] cArg = new Class[1]; //Our constructor has 3 arguments
                cArg[0] = PKiProvider.class;

              //  instance = certificateValidatorClass.newInstance();

                instance = certificateValidatorClass.getDeclaredConstructor(cArg).newInstance(new CertificateBC());


            } catch (InstantiationException e) {
                instance = null;
                CertificateValidator.log.error("Failed to init " + this, e);
            } catch (IllegalAccessException e) {
                instance = null;
                CertificateValidator.log.error("Failed to init " + this, e);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        this.friendlyName = friendlyName;
        this.context = context;
        this.deprecated = deprecated;
    }

    public CertificateValidatorResult validate(List<X509Certificate> chain, Collection<X509Certificate> trusted,
                                               boolean revocation) {
        return instance.validate(chain, trusted, revocation);
    }

    public CertificateValidatorResult validate(List<X509Certificate> chain, EntityManager em, boolean revocation) {
        return instance.validate(chain, em, revocation);
    }

    public CertificateValidatorResult validate(List<X509Certificate> chain, boolean revocation) {
        return instance.validate(chain, revocation);
    }

    public ContextType getContext() {
        return context;
    }

    public CertificateValidator getCertificateValidator() {
        return instance;
    }

    public String toString() {
        return friendlyName;
    }

    public boolean isDeprecated() {
        return deprecated;
    }

    @Override
    public String getDisplayLabel() {
        return toString();
    }


    /**
     * Get the enumeration values filtered by {@link ContextType}
     *
     * @param contextTypeFilter the context type filter
     * @return the enumeration values filtered by context type
     */
    public static CertificateValidatorType[] getByContext(ContextType contextTypeFilter) {
        CertificateValidatorType[] certificateValidatorTypes = CertificateValidatorType.values();
        if (contextTypeFilter != null) {

            List<CertificateValidatorType> filteredTmpList = new ArrayList<>();
            for (int i = 0; i < certificateValidatorTypes.length; i++) {
                if (certificateValidatorTypes[i].getContext() == contextTypeFilter
                        && !certificateValidatorTypes[i].isDeprecated()) {
                    filteredTmpList.add(certificateValidatorTypes[i]);
                }
            }
            return filteredTmpList.toArray(new CertificateValidatorType[filteredTmpList.size()]);

        } else {
            List<CertificateValidatorType> filteredTmpList = new ArrayList<>();
            for (int i = 0; i < certificateValidatorTypes.length; i++) {
                if (!certificateValidatorTypes[i].isDeprecated()) {
                    filteredTmpList.add(certificateValidatorTypes[i]);
                }
            }
            return filteredTmpList.toArray(new CertificateValidatorType[filteredTmpList.size()]);
        }
    }

    public static CertificateValidatorType getByFriendlyName(String friendlyName) {
        for (CertificateValidatorType validator : CertificateValidatorType.values()) {
            if (validator.toString().equalsIgnoreCase(friendlyName)
                    && !validator.isDeprecated()) {
                return validator;
            }
        }
        return null;
    }
}
