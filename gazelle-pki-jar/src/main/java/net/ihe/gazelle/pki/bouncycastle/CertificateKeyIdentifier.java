package net.ihe.gazelle.pki.bouncycastle;

import net.ihe.gazelle.pki.validator.CertificateNormException;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.x509.AuthorityKeyIdentifier;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.SubjectKeyIdentifier;
import org.bouncycastle.cert.jcajce.JcaX509ExtensionUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;

import static net.ihe.gazelle.pki.validator.epsos.v4.EpsosValidator.CA_S_CRLS_VALIDATION;
import static net.ihe.gazelle.pki.validator.epsos.v4.EpsosValidator.EHEALTH_X509_3_1;

public class CertificateKeyIdentifier {
    private static CertificateKeyIdentifier mo_instance;

    /**
     *
     */
    private CertificateKeyIdentifier() { }

    /**
     *
     * @return
     */
    public static CertificateKeyIdentifier getInstance() {
        if (mo_instance == null)
            mo_instance = new CertificateKeyIdentifier();

        return mo_instance;
    }

    /**
     *
     * @param po_authorityKeyIdentifierBytes
     * @param po_exceptions
     * @return
     */
    public byte[] getKeyIdentifier(byte[] po_authorityKeyIdentifierBytes, List<CertificateException> po_exceptions) {
        try {
            ASN1OctetString oct = (ASN1OctetString) ASN1Primitive.fromByteArray(po_authorityKeyIdentifierBytes);
            AuthorityKeyIdentifier authorityKeyIdentifier = AuthorityKeyIdentifier.getInstance(ASN1Primitive.fromByteArray(oct.getOctets()));

            return authorityKeyIdentifier.getKeyIdentifier();
        } catch (Exception e) {
            po_exceptions.add(new CertificateNormException("Failed to load AuthorityKeyIdentifier",
                    CA_S_CRLS_VALIDATION, e));
        }
        return null;
    }

    /**
     *
     * @param po_caCertificate
     * @param po_exceptions
     * @return
     */
    public byte[] getCAKeyIdentifier(X509Certificate po_caCertificate, List<CertificateException> po_exceptions) {
        byte[] subjectKeyIdentifierBytes = po_caCertificate.getExtensionValue(Extension.subjectKeyIdentifier
                .getId());
        if (subjectKeyIdentifierBytes != null) {
            try {
                DEROctetString oct = (DEROctetString) (new ASN1InputStream(new ByteArrayInputStream(
                        subjectKeyIdentifierBytes)).readObject());
                SubjectKeyIdentifier keyId = SubjectKeyIdentifier.getInstance(new ASN1InputStream(
                        new ByteArrayInputStream(oct.getOctets())).readObject());
                return keyId.getKeyIdentifier();
            } catch (Exception e) {
                po_exceptions.add(new CertificateNormException("Failed to load SubjectKeyIdentifier",
                        CA_S_CRLS_VALIDATION, e));
            }
        }
        return null;
    }

    /**
     *
     * @param po_subjectKeyIdentifierBytes
     * @return
     * @throws Exception
     */
    public byte[] getKeyIdentifier(byte[] po_subjectKeyIdentifierBytes) throws Exception {
        DEROctetString oct = (DEROctetString) (new ASN1InputStream(new ByteArrayInputStream(
                po_subjectKeyIdentifierBytes)).readObject());
        SubjectKeyIdentifier keyId = SubjectKeyIdentifier.getInstance(new ASN1InputStream(
                new ByteArrayInputStream(oct.getOctets())).readObject());

        return  keyId.getKeyIdentifier();
    }

    /**
     *
     * @param po_authorityKeyIdentifierBytes
     * @return
     * @throws IOException
     */
    public AuthorityKeyIdentifier getAuthorityKeyIdentifier(byte[] po_authorityKeyIdentifierBytes) throws IOException {
        ASN1OctetString oct = (ASN1OctetString) ASN1Primitive.fromByteArray(po_authorityKeyIdentifierBytes);

        return AuthorityKeyIdentifier.getInstance(ASN1Primitive.fromByteArray(oct.getOctets()));
    }

    /**
     *
     * @param po_authorityKeyIdentifierBytes
     * @return
     * @throws IOException
     */
    public byte[] getAuthorityKeyIdentifierKeyIdentifier(byte[] po_authorityKeyIdentifierBytes) throws IOException {
        AuthorityKeyIdentifier authorityKeyIdentifier = getAuthorityKeyIdentifier(po_authorityKeyIdentifierBytes);

        return authorityKeyIdentifier.getKeyIdentifier();

    }

    /**
     *
     * @param po_authorityKeyIdentifierBytes
     * @return
     * @throws IOException
     */
    public Object getAuthorityCertIssuer(byte[] po_authorityKeyIdentifierBytes) throws IOException {
        AuthorityKeyIdentifier authorityKeyIdentifier = getAuthorityKeyIdentifier(po_authorityKeyIdentifierBytes);

        return authorityKeyIdentifier.getAuthorityCertIssuer();
    }

    /**
     *
     * @param po_authorityKeyIdentifierBytes
     * @return
     * @throws IOException
     */
    public Object getAuthorityCertSerialNumber(byte[] po_authorityKeyIdentifierBytes) throws IOException {
        AuthorityKeyIdentifier authorityKeyIdentifier = getAuthorityKeyIdentifier(po_authorityKeyIdentifierBytes);

        return authorityKeyIdentifier.getAuthorityCertSerialNumber();
    }

    /**
     *
     * @return
     * @throws NoSuchAlgorithmException
     */
    public byte[] getSKIKeyIdentifier(X509Certificate x509Certificate) throws NoSuchAlgorithmException {
        JcaX509ExtensionUtils x509ExtensionUtils = new JcaX509ExtensionUtils();
        SubjectKeyIdentifier ski = x509ExtensionUtils.createSubjectKeyIdentifier(x509Certificate.getPublicKey());
        return ski.getKeyIdentifier();
    }

    /**
     *
     * @param x509Certificate
     * @return
     */
    public byte[] getKeyIdKeyIdentifier(X509Certificate x509Certificate, List<CertificateException> exceptions)  {
        byte[] subjectKeyIdentifierBytes = x509Certificate.getExtensionValue(Extension.subjectKeyIdentifier
                .getId());
        if (subjectKeyIdentifierBytes != null) {
            try {
                DEROctetString oct = (DEROctetString) (new ASN1InputStream(new ByteArrayInputStream(
                        subjectKeyIdentifierBytes)).readObject());
                SubjectKeyIdentifier keyId = SubjectKeyIdentifier.getInstance(new ASN1InputStream(
                        new ByteArrayInputStream(oct.getOctets())).readObject());


                return keyId.getKeyIdentifier();
            } catch (Exception e) {
                exceptions.add(new CertificateNormException("Failed to validate SubjectKeyIdentifier",
                        EHEALTH_X509_3_1, e));
            }
        }
        return null;
    }
}
