package net.ihe.gazelle.pki.crl;

import java.security.cert.CertificateException;

import net.ihe.gazelle.pki.model.Certificate;

public interface CrlProvider {

    byte[] generateCRL(Certificate certificateAuthority) throws CertificateException;

}
