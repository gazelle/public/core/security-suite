package net.ihe.gazelle.pki.bouncycastle;

import org.bouncycastle.asn1.x509.KeyPurposeId;

public class CertificateKeyPurposeIdTools {
    private static CertificateKeyPurposeIdTools mo_instance;

    /**
     *
     */
    private CertificateKeyPurposeIdTools() { }

    /**
     *
     * @return
     */
    public static CertificateKeyPurposeIdTools getInstance() {
        if (mo_instance == null)
            mo_instance = new CertificateKeyPurposeIdTools();

        return mo_instance;
    }
    /**
     *
     * @return
     */
    public static KeyPurposeId[] getKeyPurposeIds() {
        return CertificateConstants.KEY_PURPOSE_IDS;
    }

    /**
     *
     * @return
     */
    public static String getIdKpClientAuthId() {
        return KeyPurposeId.id_kp_clientAuth.getId();
    }

    /**
     *
     * @return
     */
    public static String getIdKpServerAuthId() {
        return KeyPurposeId.id_kp_serverAuth.getId();
    }

    /**
     *
     * @param ps_value
     * @param pi_index
     * @param po_keyPurposeString
     * @return
     */
    public String getPurposeIdValue(String ps_value, Integer pi_index, String[] po_keyPurposeString) {
        String ls_value = ps_value;
        KeyPurposeId keyPurpose = CertificateConstants.KEY_PURPOSE_IDS[pi_index];
        if (keyPurpose.getId().equals(ps_value)) {
            ls_value = po_keyPurposeString[pi_index];
        }
        return ls_value;
    }

    /**
     *
     * @return
     */
    public static String getIdKpEmailProtection() {
        return KeyPurposeId.id_kp_emailProtection.getId();
    }
}
