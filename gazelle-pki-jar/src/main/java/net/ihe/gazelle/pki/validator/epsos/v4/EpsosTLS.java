package net.ihe.gazelle.pki.validator.epsos.v4;

import net.ihe.gazelle.pki.model.PKiProvider;
import net.ihe.gazelle.pki.validator.CertificateNormException;

import java.security.cert.CertificateException;
import java.util.List;

public class EpsosTLS extends EpsosValidator {

    /**
     * Constructor.
     *
     * @param po_pkiProvider Instance of the PKI Provider.
     */
    public EpsosTLS(PKiProvider po_pkiProvider) {
        super(po_pkiProvider);
    }

    @Override
    protected void validateKeyUsage(int bits, List<CertificateException> exceptions) {
        super.validateKeyUsage(bits, exceptions);
        int keyUsageBits[] = getPKiProvider().getTLSKeyUsageBits();
        validateKeyUsageValue(bits, keyUsageBits,
                "The digitalSignature and keyEncipherment bits MUST both be set to true to the exclusion of all other KeyUSage bits that MUST be " +
                        "set to false, excepted for the dataEncipherment bit that MAY be set to true.", exceptions,
                EHEALTH_X509_3_1);
    }

    @Override
    protected void validateExtendedKeyUsages(List<String> extendedKeyUsages, List<CertificateException> exceptions, List<CertificateException> warnings) {
        super.validateExtendedKeyUsages(extendedKeyUsages, exceptions, warnings);
        if (extendedKeyUsages != null && extendedKeyUsages.size() > 0) {
            if (!(extendedKeyUsages.contains(getPKiProvider().getIdKpClientAuthId())
                    && extendedKeyUsages.contains(getPKiProvider().getIdKpServerAuthId()))) {
                exceptions.add(new CertificateNormException(
                        "Both values id‐kp‐serverAuth [RFC5280] and id‐kp‐clientAuth [RFC5280] MUST be present.", EHEALTH_X509_3_1));
            }
        } else {
            exceptions.add(new CertificateNormException(
                    "Both values id‐kp‐serverAuth [RFC5280] and id‐kp‐clientAuth [RFC5280] MUST be present.", EHEALTH_X509_3_1));
        }
    }
}
