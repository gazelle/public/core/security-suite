package net.ihe.gazelle.pki;

/*
 * Copyright (c) 2003, D�niel D�k�ny
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 *
 * - Neither the name "FMPP" nor the names of the project contributors may
 *   be used to endorse or promote products derived from this software
 *   without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * Collection of string manipulation functions.
 */
public class StringWrapper {

    /**
     * The default line-break string used by the methods in this class.
     */
    public static final String LINE_BREAK = System.getProperty("line.separator");

    public static String wrap(String text) {
        return wrap(text, 160);
    }

    /**
     * Hard-wraps flow-text. This is a convenience method that equivalent with
     * <code>wrap(text, screenWidth, 0, 0, LINE_BREAK, false)</code>.
     *
     * @param text        The flow-text to wrap. The explicit line-breaks of the source
     *                    text will be kept. All types of line-breaks (UN*X, Mac,
     *                    DOS/Win) are understood.
     * @param screenWidth The (minimum) width of the screen. It does not utilize the
     *                    <code>screenWidth</code>-th column of the screen to store
     *                    characters, except line-breaks (because some terminals/editors
     *                    do an automatic line-break when you write visible character
     *                    there, and some doesn't... so it is unpredicalbe if an
     *                    explicit line-break is needed or not.).
     * @return Wrapped text
     * @see #wrap(String, int, int, int, String, boolean)
     */
    public static String wrap(String text, int screenWidth) {
        return wrap(text, screenWidth, 0, 0, LINE_BREAK, false);
    }

    /**
     * Hard-wraps flow-text. This is a convenience method that equivalent with
     * <code>wrap(text, screenWidth, 0, 0, LINE_BREAK, true)</code>.
     *
     * @param text        The flow-text to wrap. The explicit line-breaks of the source
     *                    text will be kept. All types of line-breaks (UN*X, Mac,
     *                    DOS/Win) are understood.
     * @param screenWidth The (minimum) width of the screen. It does not utilize the
     *                    <code>screenWidth</code>-th column of the screen to store
     *                    characters, except line-breaks (because some terminals/editors
     *                    do an automatic line-break when you write visible character
     *                    there, and some doesn't... so it is unpredicalbe if an
     *                    explicit line-break is needed or not.).
     * @return Wrapped text
     * @see #wrap(String, int, int, int, String, boolean)
     */
    public static String wrapTrace(String text, int screenWidth) {
        return wrap(text, screenWidth, 0, 0, LINE_BREAK, true);
    }

    /**
     * Hard-wraps flow-text. This is a convenience method that equivalent with
     * <code>wrap(text, screenWidth, 0, 0, lineBreak, false)</code>.
     *
     * @param text        The flow-text to wrap. The explicit line-breaks of the source
     *                    text will be kept. All types of line-breaks (UN*X, Mac,
     *                    DOS/Win) are understood.
     * @param screenWidth The (minimum) width of the screen. It does not utilize the
     *                    <code>screenWidth</code>-th column of the screen to store
     *                    characters, except line-breaks (because some terminals/editors
     *                    do an automatic line-break when you write visible character
     *                    there, and some doesn't... so it is unpredicalbe if an
     *                    explicit line-break is needed or not.).
     * @param lineBreak   The String used for line-breaks
     * @return Wrapped text
     * @see #wrap(String, int, int, int, String, boolean)
     */
    public static String wrap(String text, int screenWidth, String lineBreak) {
        return wrap(text, screenWidth, 0, 0, lineBreak, false);
    }

    /**
     * Hard-wraps flow-text. This is a convenience method that equivalent with
     * <code>wrap(text, screenWidth, indent, indent, LINE_BREAK, false)</code>.
     *
     * @param text        The flow-text to wrap. The explicit line-breaks of the source
     *                    text will be kept. All types of line-breaks (UN*X, Mac,
     *                    DOS/Win) are understood.
     * @param screenWidth The (minimum) width of the screen. It does not utilize the
     *                    <code>screenWidth</code>-th column of the screen to store
     *                    characters, except line-breaks (because some terminals/editors
     *                    do an automatic line-break when you write visible character
     *                    there, and some doesn't... so it is unpredicalbe if an
     *                    explicit line-break is needed or not.).
     * @param indent      The indentation of all lines but the first line
     * @return Wrapped text
     * @see #wrap(String, int, int, int, String, boolean)
     */
    public static String wrap(String text, int screenWidth, int indent) {
        return wrap(text, screenWidth, indent, indent, LINE_BREAK, false);
    }

    /**
     * Hard-wraps flow-text. This is a convenience method that equivalent with
     * <code>wrap(text, screenWidth, firstIndent, indent, LINE_BREAK,
     * false)</code>.
     *
     * @param text        The flow-text to wrap. The explicit line-breaks of the source
     *                    text will be kept. All types of line-breaks (UN*X, Mac,
     *                    DOS/Win) are understood.
     * @param screenWidth The (minimum) width of the screen. It does not utilize the
     *                    <code>screenWidth</code>-th column of the screen to store
     *                    characters, except line-breaks (because some terminals/editors
     *                    do an automatic line-break when you write visible character
     *                    there, and some doesn't... so it is unpredicalbe if an
     *                    explicit line-break is needed or not.).
     * @param firstIndent The indentation of the first line
     * @param indent      The indentation of all lines but the first line
     * @return Wrapped text
     * @see #wrap(String, int, int, int, String, boolean)
     */
    public static String wrap(String text, int screenWidth, int firstIndent, int indent) {
        return wrap(text, screenWidth, firstIndent, indent, LINE_BREAK, false);
    }

    /**
     * Hard-wraps flow-text. This is a convenience method that equivalent with
     * <code>wrap(text, screenWidth, indent, indent, lineBreak, false)</code>.
     *
     * @param text        The flow-text to wrap. The explicit line-breaks of the source
     *                    text will be kept. All types of line-breaks (UN*X, Mac,
     *                    DOS/Win) are understood.
     * @param screenWidth The (minimum) width of the screen. It does not utilize the
     *                    <code>screenWidth</code>-th column of the screen to store
     *                    characters, except line-breaks (because some terminals/editors
     *                    do an automatic line-break when you write visible character
     *                    there, and some doesn't... so it is unpredicalbe if an
     *                    explicit line-break is needed or not.).
     * @param indent      The indentation of all lines but the first line
     * @param lineBreak   The String used for line-breaks
     * @return Wrapped text
     * @see #wrap(String, int, int, int, String, boolean)
     */
    public static String wrap(String text, int screenWidth, int indent, String lineBreak) {
        return wrap(text, screenWidth, indent, indent, lineBreak, false);
    }

    /**
     * Hard-wraps flow-text. This is a convenience method that equivalent with
     * <code>wrap(text, screenWidth, firstIndent, indent, lineBreak,
     * false)</code>.
     *
     * @param text        The flow-text to wrap. The explicit line-breaks of the source
     *                    text will be kept. All types of line-breaks (UN*X, Mac,
     *                    DOS/Win) are understood.
     * @param screenWidth The (minimum) width of the screen. It does not utilize the
     *                    <code>screenWidth</code>-th column of the screen to store
     *                    characters, except line-breaks (because some terminals/editors
     *                    do an automatic line-break when you write visible character
     *                    there, and some doesn't... so it is unpredicalbe if an
     *                    explicit line-break is needed or not.).
     * @param firstIndent The indentation of the first line
     * @param indent      The indentation of all lines but the first line
     * @param lineBreak   The String used for line-breaks
     * @return Wrapped text
     * @see #wrap(String, int, int, int, String, boolean)
     */
    public static String wrap(String text, int screenWidth, int firstIndent, int indent, String lineBreak) {
        return wrap(text, screenWidth, firstIndent, indent, lineBreak, false);
    }

    /**
     * Hard-wraps flow-text. Uses StringBuffer-s instead of String-s. This is a
     * convenience method that equivalent with
     * <code>wrap(text, screenWidth, firstIndent, indent, LINE_BREAK)</code>.
     *
     * @param text        The flow-text to wrap. The explicit line-breaks of the source
     *                    text will be kept. All types of line-breaks (UN*X, Mac,
     *                    DOS/Win) are understood.
     * @param screenWidth The (minimum) width of the screen. It does not utilize the
     *                    <code>screenWidth</code>-th column of the screen to store
     *                    characters, except line-breaks (because some terminals/editors
     *                    do an automatic line-break when you write visible character
     *                    there, and some doesn't... so it is unpredicalbe if an
     *                    explicit line-break is needed or not.).
     * @param firstIndent The indentation of the first line
     * @param indent      The indentation of all lines but the first line
     * @return Wrapped text
     * @see #wrap(StringBuffer, int, int, int, String, boolean)
     */
    public static StringBuffer wrap(StringBuffer text, int screenWidth, int firstIndent, int indent) {
        return wrap(text, screenWidth, firstIndent, indent, LINE_BREAK, false);
    }

    /**
     * Hard-wraps flow-text.
     *
     * @param text        The flow-text to wrap. The explicit line-breaks of the source
     *                    text will be kept. All types of line-breaks (UN*X, Mac,
     *                    DOS/Win) are understood.
     * @param screenWidth The (minimum) width of the screen. It does not utilize the
     *                    <code>screenWidth</code>-th column of the screen to store
     *                    characters, except line-breaks (because some terminals/editors
     *                    do an automatic line-break when you write visible character
     *                    there, and some doesn't... so it is unpredicalbe if an
     *                    explicit line-break is needed or not.).
     * @param firstIndent The indentation of the first line
     * @param indent      The indentation of all lines but the first line
     * @param lineBreak   The String used for line-breaks
     * @param traceMode   Set this true if the input text is a Java stack trace. In this
     *                    mode, all lines starting with optional indentation +
     *                    <tt>'at'</tt> + space are treated as location lines, and will
     *                    be indented and wrapped in a silghtly special way.
     * @return Wrapped text
     * @throws IllegalArgumentException if the number of columns remaining for the text is less than
     *                                  2.
     */
    public static String wrap(String text, int screenWidth, int firstIndent, int indent, String lineBreak,
                              boolean traceMode) {
        return wrap(new StringBuffer(text), screenWidth, firstIndent, indent, lineBreak, traceMode).toString();
    }

    /**
     * Hard-wraps flow-text. Uses StringBuffer-s instead of String-s. This is
     * the method that is internally used by all other <code>wrap</code>
     * variations, so if you are working with StringBuffers anyway, it gives
     * better performance.
     *
     * @param text        The flow-text to wrap. The explicit line-breaks of the source
     *                    text will be kept. All types of line-breaks (UN*X, Mac,
     *                    DOS/Win) are understood.
     * @param screenWidth The (minimum) width of the screen. It does not utilize the
     *                    <code>screenWidth</code>-th column of the screen to store
     *                    characters, except line-breaks (because some terminals/editors
     *                    do an automatic line-break when you write visible character
     *                    there, and some doesn't... so it is unpredicalbe if an
     *                    explicit line-break is needed or not.).
     * @param firstIndent The indentation of the first line
     * @param indent      The indentation of all lines but the first line
     * @param lineBreak   The String used for line-breaks
     * @param traceMode   Set this true if the input text is a Java stack trace. In this
     *                    mode, all lines starting with optional indentation +
     *                    <tt>'at'</tt> + space are treated as location lines, and will
     *                    be indented and wrapped in a silghtly special way.
     * @return Wrapped text as {@link StringBuffer}
     * @see #wrap(String, int, int, int, String, boolean)
     */
    public static StringBuffer wrap(StringBuffer text, int screenWidth, int firstIndent, int indent, String lineBreak,
                                    boolean traceMode) {
        return wrap(text, screenWidth, firstIndent, indent, lineBreak, traceMode, "-");
    }


    public static StringBuffer wrap(StringBuffer text, int screenWidth, int firstIndent, int indent, String lineBreak,
                                    boolean traceMode, String spacer) {

        if (firstIndent < 0 || indent < 0 || screenWidth < 0) {
            throw new IllegalArgumentException("Negative dimension");
        }

        int allowedCols = screenWidth - 1;

        if ((allowedCols - indent) < 2 || (allowedCols - firstIndent) < 2) {
            throw new IllegalArgumentException("Usable columns < 2");
        }

        int ln = text.length();
        int defaultNextLeft = allowedCols - indent;
        int b = 0;
        int e = 0;

        StringBuffer res = new StringBuffer((int) (ln * 1.2));
        int left = allowedCols - firstIndent;
        for (int i = 0; i < firstIndent; i++) {
            res.append(' ');
        }
        StringBuffer tempb = new StringBuffer(indent + 2);
        tempb.append(lineBreak);
        for (int i = 0; i < indent; i++) {
            tempb.append(' ');
        }
        String defaultBreakAndIndent = tempb.toString();

        boolean firstSectOfSrcLine = true;
        boolean firstWordOfSrcLine = true;
        int traceLineState = 0;
        int nextLeft = defaultNextLeft;
        String breakAndIndent = defaultBreakAndIndent;
        int wln = 0, x;
        char c, c2;
        do {
            word:
            while (e <= ln) {
                if (e != ln) {
                    c = text.charAt(e);
                } else {
                    c = ' ';
                }
                if (traceLineState > 0 && e > b) {
                    if (c == '.' && traceLineState == 1) {
                        c = ' ';
                    } else {
                        c2 = text.charAt(e - 1);
                        if (c2 == ':') {
                            c = ' ';
                        } else if (c2 == '(') {
                            traceLineState = 2;
                            c = ' ';
                        }
                    }
                }
                if (c != ' ' && c != '\n' && c != '\r' && c != '\t') {
                    e++;
                } else {
                    wln = e - b;
                    if (left >= wln) {
                        res.append(text.substring(b, e));
                        left -= wln;
                        b = e;
                    } else {
                        wln = e - b;
                        if (wln > nextLeft || firstWordOfSrcLine) {
                            int ob = b;
                            while (wln > left) {
                                if (left > 2 || (left == 2 && (firstWordOfSrcLine || !(b == ob && nextLeft > 2)))) {
                                    res.append(text.substring(b, b + left - 1));
                                    res.append(spacer);
                                    res.append(breakAndIndent);
                                    wln -= left - 1;
                                    b += left - 1;
                                    left = nextLeft;
                                } else {
                                    x = res.length() - 1;
                                    if (x >= 0 && res.charAt(x) == ' ') {
                                        res.delete(x, x + 1);
                                    }
                                    res.append(breakAndIndent);
                                    left = nextLeft;
                                }
                            }
                            res.append(text.substring(b, b + wln));
                            b += wln;
                            left -= wln;
                        } else {
                            x = res.length() - 1;
                            if (x >= 0 && res.charAt(x) == ' ') {
                                res.delete(x, x + 1);
                            }
                            res.append(breakAndIndent);
                            res.append(text.substring(b, e));
                            left = nextLeft - wln;
                            b = e;
                        }
                    }
                    firstSectOfSrcLine = false;
                    firstWordOfSrcLine = false;
                    break word;
                }
            }
            int extra = 0;
            space:
            while (e < ln) {
                c = text.charAt(e);
                if (c == ' ') {
                    e++;
                } else if (c == '\t') {
                    e++;
                    extra += 7;
                } else if (c == '\n' || c == '\r') {
                    nextLeft = defaultNextLeft;
                    breakAndIndent = defaultBreakAndIndent;
                    res.append(breakAndIndent);
                    e++;
                    if (e < ln) {
                        c2 = text.charAt(e);
                        if ((c2 == '\n' || c2 == '\r') && c != c2) {
                            e++;
                        }
                    }
                    left = nextLeft;
                    b = e;
                    firstSectOfSrcLine = true;
                    firstWordOfSrcLine = true;
                    traceLineState = 0;
                } else {
                    wln = e - b + extra;
                    if (firstSectOfSrcLine) {
                        int y = allowedCols - indent - wln;
                        if (traceMode && ln > e + 2 && text.charAt(e) == 'a' && text.charAt(e + 1) == 't'
                                && text.charAt(e + 2) == ' ') {
                            if (y > 5 + 3) {
                                y -= 3;
                            }
                            traceLineState = 1;
                        }
                        if (y > 5) {
                            y = allowedCols - y;
                            nextLeft = allowedCols - y;
                            tempb = new StringBuffer(indent + 2);
                            tempb.append(lineBreak);
                            for (int i = 0; i < y; i++) {
                                tempb.append(' ');
                            }
                            breakAndIndent = tempb.toString();
                        }
                    }
                    if (wln <= left) {
                        res.append(text.substring(b, e));
                        left -= wln;
                        b = e;
                    } else {
                        res.append(breakAndIndent);
                        left = nextLeft;
                        b = e;
                    }
                    firstSectOfSrcLine = false;
                    break space;
                }
            }
        } while (e < ln);

        return res;
    }

}
