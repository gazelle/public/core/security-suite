package net.ihe.gazelle.pki;

import net.ihe.gazelle.pki.bouncycastle.CertificateBuilder;
import net.ihe.gazelle.pki.bouncycastle.X509CertificateParametersContainer;
import net.ihe.gazelle.pki.crl.CrlUtil;
import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.env.PKITestEnvironment;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.pki.model.CertificateRequestWithGeneratedKeys;
import net.ihe.gazelle.pki.validator.CertificateValidatorResult;
import org.apache.commons.lang.RandomStringUtils;
import org.bouncycastle.x509.X509V3CertificateGenerator;
import org.junit.Test;

import java.math.BigInteger;
import java.security.cert.CertificateException;
import java.util.Calendar;
import java.util.Locale;

import static net.ihe.gazelle.pki.CertificateTestTools.assertContainsError;
import static net.ihe.gazelle.pki.CertificateTestTools.assertNotContainsError;
import static net.ihe.gazelle.pki.CertificateTestTools.createCertificateBadIssuer;
import static net.ihe.gazelle.pki.CertificateTestTools.createCertificateSerialNumber;
import static net.ihe.gazelle.pki.CertificateTestTools.getCertificateAuthorityOCSP;
import static net.ihe.gazelle.pki.CertificateTestTools.getCertificateAuthorityOK;
import static net.ihe.gazelle.pki.CertificateTestTools.getCertificateRequestAutoSignOK;
import static net.ihe.gazelle.pki.CertificateTestTools.getCertificateRequestAutoSignHack;

public abstract class EpsosAbstractV4CertificateTest extends PKITestEnvironment {

    abstract CertificateType getCertificateType();

    @Test
    public void testCreateValidateUpdate() throws CertificateException {
        //testCreateValidateUpdateCertificate(getCertificateType());
    }

    @Test
    public void testEHEALTH_X509_3_1_Signature() throws CertificateException {
        // FIXME
    }

    @Test
    public void testEHEALTH_X509_3_1_SerialNumber() throws CertificateException {

        // Impossible to set with BC
        // testEHEALTH_X509_3_1_SerialNumber(BigInteger.valueOf(-1));

        byte[] bytes = new byte[24];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = 13;
        }

        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignOK(getCertificateType(),
                certificateAuthority);
        Certificate certificate = createCertificateSerialNumber(certificateRequestWithGeneratedKeys, null, new BigInteger(bytes));
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true,
                "The serial number MUST be an unambiguous integer value with a maximum of 20 bytes.");
    }

    @Test
    public void testEHEALTH_X509_3_1_Validity() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignOK(getCertificateType(),
                certificateAuthority);

        Calendar notAfter = Calendar.getInstance();
        notAfter.setTime(certificateRequestWithGeneratedKeys.getNotBefore());
        notAfter.add(Calendar.YEAR, 3);
        certificateRequestWithGeneratedKeys.setNotAfter(notAfter.getTime());
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, false,
                "Certificates used by epSOS services SHOULD be valid for a maximum of 2 year.");
    }

    @Test
    public void testEHEALTH_X509_3_1_IssuerUniqueID() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackIssuerUniqueID(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true, "The field \"IssuerUniqueID\" MUST NOT be used.");
    }

    @Test
    public void testEHEALTH_X509_3_1_SubjectUniqueID() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackSubjectUniqueID(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true, "The field \"SubjectUniqueID\" MUST NOT be used.");
    }

    private void testEHEALTH_X509_3_1_Subject(String subject, boolean error, String errorString) throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignOK(getCertificateType(),
                certificateAuthority);
        certificateRequestWithGeneratedKeys.setSubject(subject);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, error, errorString);
    }

    @Test
    public void testEHEALTH_X509_3_1_Subject_C() throws CertificateException {
        testEHEALTH_X509_3_1_Subject("DN=glandais", true, "The DName MUST have C.");
        testEHEALTH_X509_3_1_Subject("DN=glandais,C=ABC", true, "C MUST be limited by");
        testEHEALTH_X509_3_1_Subject("DN=glandais,C=A", true, "C MUST be limited by");
        testEHEALTH_X509_3_1_Subject("DN=glandais,C=EU", true, "C MUST be a ISO 3166 code");
    }

    @Test
    public void testEHEALTH_X509_3_1_Subject_O() throws CertificateException {
        testEHEALTH_X509_3_1_Subject("DN=glandais", true, "The DName MUST have O.");
        String longValue = RandomStringUtils.randomAlphabetic(70);
        testEHEALTH_X509_3_1_Subject("DN=glandais,O=" + longValue, true, "O MUST be limited by");
    }

    @Test
    public void testEHEALTH_X509_3_1_Subject_CN() throws CertificateException {
        testEHEALTH_X509_3_1_Subject("C=FR", true, "The DName MUST have CN.");
        String longValue = RandomStringUtils.randomAlphabetic(70);
        testEHEALTH_X509_3_1_Subject("DN=glandais,CN=" + longValue, true, "CN MUST be limited by");
    }

    @Test
    public void testEHEALTH_X509_3_1_Subject_OU() throws CertificateException {
        testEHEALTH_X509_3_1_Subject("DN=glandais", false, "The DName MAY have OU.");
        String longValue = RandomStringUtils.randomAlphabetic(70);
        testEHEALTH_X509_3_1_Subject("DN=glandais,OU=" + longValue, true, "OU MUST be limited by");
    }

    @Test
    public void testEHEALTH_X509_3_1_Subject_E() throws CertificateException {
        testEHEALTH_X509_3_1_Subject("DN=glandais,E=test@test.com", false, "E SHOULD NOT be provided");
    }

    @Test
    public void testEHEALTH_X509_3_1_Issuer() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignOK(getCertificateType(),
                certificateAuthority);

        certificateRequestWithGeneratedKeys.setSubjectUsingAttributes(Locale.FRANCE.getCountry(), "IHE", "glandais2", null,
                null, null, "IRISA", null);
        Certificate certificate = createCertificateBadIssuer(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true, "Issuer of certificate is not valid.");
    }

    @Test
    public void testEHEALTH_X509_3_2_AuthorityKeyIdentifier_critical() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackAuthorityKeyIdentifierExtension(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true,
                "AuthorityKeyIdentifier MUST always be designated as non-critical in the certificate");
        assertContainsError(result, true,
                "AuthorityKeyIdentifier MUST be included as a non-critical extension in the certificate.");
    }

    @Test
    public void testEHEALTH_X509_3_2_KeyUsage_critical() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackKeyUsageEncipherment(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true, "KeyUsage MUST always be designated as critical in the certificate");
        assertContainsError(result, true, "KeyUsage MUST be included as a critical extension in the certificate.");
    }

    @Test
    public void testEHEALTH_X509_3_2_KeyUsage_none() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackNone(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true, "KeyUsage MUST be included as a critical extension in the certificate.");
    }

    @Test
    public void testEHEALTH_X509_3_2_AuthorityKeyIdentifier_none() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackNone(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true,
                "AuthorityKeyIdentifier MUST be included as a non-critical extension in the certificate.");
    }

    @Test
    public void testEHEALTH_X509_3_2_AuthorityKeyIdentifier_CASubjectKeyIdentifier() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackCAAuthorityKeyIdentifierExtension(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true,
                "AuthorityKeyIdentifier MUST be equal to the SubjectKeyIdentifier of the issuing CA.");
    }

    @Test
    public void testEHEALTH_X509_3_2_AuthorityKeyIdentifier_AuthorityKeyIdentifierParameters() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackAuthorityKeyIdentifierParameters(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, false, "AuthorityCertIssuer SHOULD NOT be used in AuthorityKeyIdentifier.");
        assertContainsError(result, false, "AuthorityCertSerialNumber SHOULD NOT be used in AuthorityKeyIdentifier.");
    }

    @Test
    public void testEHEALTH_X509_3_2_SubjectKeyIdentifier_critical() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackSubjectKeyIdentifierExtension(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true,
                "SubjectKeyIdentifier MUST always be designated as non-critical in the certificate");
        assertContainsError(result, true,
                "SubjectKeyIdentifier MUST be included as a non-critical extension in the certificate.");
    }

    @Test
    public void testEHEALTH_X509_3_2_SubjectKeyIdentifier_none() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackNone(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true,
                "SubjectKeyIdentifier MUST be included as a non-critical extension in the certificate.");
    }

    @Test
    public void testEHEALTH_X509_3_2_SubjectKeyIdentifier_RFC5280_4_2_1_2() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackSubjectKeyIdentifierExtensionRFC5280(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true, "One of the methods described in RFC5280 (ch. 4.2.1.2) SHOULD be used.");
    }

    @Test
    public void testEHEALTH_X509_3_2_IssuerAltNames_critical() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackIssuerAlternativeNameExtension(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true,
                "IssuerAlternativeName MUST always be designated as non-critical in the certificate");

    }

    @Test
    public void testEHEALTH_X509_3_2_SubjectAltNames_critical() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackSubjectAlternativeNameExtension(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true,
                "SubjectAlternativeName MUST always be designated as non-critical in the certificate");
    }

    @Test
    public void testEHEALTH_X509_3_2_SubjectAltNames_uri() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackSubjectAlternativeNameExtension(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, false,
                "If [SubjectAltNames] is used, a corresponding LDAP-, HTTP- or FTP-URL SHOULD be provided.");
        assertContainsError(result, false,
                "If [SubjectAltNames] is used, E-Mail addresses (RFC822-name) [RFC 822] MAY also be made available.");
    }

    @Test
    public void testEHEALTH_X509_3_2_BasicConstraints_none() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackNone(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, false,
                "BasicConstraints SHOULD be included as a critical extension in the certificate.");
    }

    @Test
    public void testEHEALTH_X509_3_2_BasicConstraints_critical() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackBasicConstraintExtension(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, false,
                "BasicConstraints SHOULD be included as a critical extension in the certificate.");
        assertContainsError(result, false, "BasicConstraints SHOULD always be designated as critical in the certificate");
    }

    @Test
    public void testEHEALTH_X509_3_2_CRLDistributionPoints_none() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackNone(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, false,
                "CRLDistributionPoints SHOULD be included as a non-critical extension in the certificate.");
    }

    @Test
    public void testEHEALTH_X509_3_2_CRLDistributionPoints_critical() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackCRLExtension(getCertificateType(),
                certificateAuthority, "http://server/crl/1/cacrl.crl");
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true,
                "CRLDistributionPoints MUST always be designated as non-critical in the certificate");
        assertContainsError(result, false,
                "CRLDistributionPoints SHOULD be included as a non-critical extension in the certificate.");
    }

    @Test
    public void testEHEALTH_X509_3_2_CRLDistributionPoints_noHTTP() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackCRLExtension(getCertificateType(),
                certificateAuthority, "http://toto/crl/1/cacrl.crl");
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, false, "CRLDistributionPoints SHOULD include the HTTP address from "
                + "which the certificate-issuing authority’s complete revocation list can be retrieved.");
    }

    @Test
    public void testEHEALTH_X509_3_2_CertificatePolicies_none() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackNone(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, false,
                "CertificatePolicies SHOULD be included as a non-critical extension in the certificate.");
    }

    @Test
    public void testEHEALTH_X509_3_2_CertificatePolicies_critical() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackPoliciesExtension(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, false,
                "CertificatePolicies SHOULD be included as a non-critical extension in the certificate.");
        assertContainsError(result, true,
                "CertificatePolicies MUST always be designated as non-critical in the certificate");
    }

    @Test
    public void testEHEALTH_X509_3_2_CertificatePolicies_Policyinformation_OID() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackPoliciesExtension(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, false,
                "CertificatePolicies SHOULD be included as an extension in the certificate to include the eHealth DSI certificate policy " +
                        "identifier: 1.3.130.0.2017.ARES_number_of_the_present_document");
    }

    @Test
    public void testEHEALTH_X509_3_2_AuthorityInfoAccess_none() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackNone(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, false,
                "AuthorityInfoAccess SHOULD be included as a non-critical extension in the certificate.");
    }

    @Test
    public void testEHEALTH_X509_3_2_AuthorityInfoAccess_critical() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackAuthorityInfoAccessExtension(getCertificateType(),
                certificateAuthority, "http://server/pki/1/authorityInfoAccess");
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), false);
        assertContainsError(result, false,
                "AuthorityInfoAccess SHOULD be included as a non-critical extension in the certificate.");
        assertContainsError(result, true,
                "AuthorityInfoAccess MUST always be designated as non-critical in the certificate");
    }

    @Test
    public void testEHEALTH_X509_3_2_AuthorityInfoAccess_OCSP_CA_OK() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackAuthorityInfoAccessExtension(getCertificateType(),
                certificateAuthority, "http://server/pki/1/authorityInfoAccess");
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), false);
        assertNotContainsError(result, true,
                "When the issuing CA offers an OCSP service, its HTTP URI MUST be included in the AuthorityInfoAccess extension.");
    }

    @Test
    public void testEHEALTH_X509_3_2_AuthorityInfoAccess_OCSP_CA_NOK() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackAuthorityInfoAccessExtension(getCertificateType(),
                certificateAuthority, "ftp://server/pki/2/authorityInfoAccess");
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), false);
        assertContainsError(result, true,
                "When the issuing CA offers an OCSP service, its HTTP URI MUST be included in the AuthorityInfoAccess extension.");
    }

    @Test
    public void testEHEALTH_X509_3_2_AuthorityInfoAccess_OR_CRLDistributionPoints_OK_AuthorityInfoAccess()
            throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackAuthorityInfoAccessExtension(getCertificateType(),
                certificateAuthority, "http://server/pki/1/authorityInfoAccess");
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), false);
        assertNotContainsError(result, true,
                "If the certificate does not include any access location of an OCSP responder as specified in “AIA extension”, then the certificate" +
                        " MUST include a CRL distribution point extension.");
        assertNotContainsError(result, true,
                "A reference to at least one OCSP responder MUST be present if the certificate does not include any CRL distribution point " +
                        "extension.");
    }

    @Test
    public void testEHEALTH_X509_3_2_AuthorityInfoAccess_OR_CRLDistributionPoints_OK_CRLDistributionPoints()
            throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackCRLExtension(getCertificateType(),
                certificateAuthority,"http://server/crl/1/cacrl.crl");
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertNotContainsError(result, true,
                "If the certificate does not include any access location of an OCSP responder as specified in “AIA extension”, then the certificate" +
                        " MUST include a CRL distribution point extension.");
        assertNotContainsError(result, true,
                "A reference to at least one OCSP responder MUST be present if the certificate does not include any CRL distribution point " +
                        "extension.");
    }

    @Test
    public void testEHEALTH_X509_3_2_AuthorityInfoAccess_OR_CRLDistributionPoints_NOK() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOCSP("http://server/pki/1/authorityInfoAccess");

        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys =CertificateRequestHacked.getInstance().generateAutoSignHackNone(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), false);
        assertContainsError(result, true,
                "If the certificate does not include any access location of an OCSP responder as specified in “AIA extension”, then the certificate" +
                        " MUST include a CRL distribution point extension.");
        assertContainsError(result, true,
                "A reference to at least one OCSP responder MUST be present if the certificate does not include any CRL distribution point " +
                        "extension.");
    }

}
