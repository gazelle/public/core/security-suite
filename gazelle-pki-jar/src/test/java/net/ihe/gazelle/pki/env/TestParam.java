package net.ihe.gazelle.pki.env;

/**
 * Created by cel on 26/01/16.
 */
public class TestParam {

    public static final boolean ENABLE_REVOCATION_CHECK = false ;
    public static final int CRL_PORT = 10808 ;
    public static final String CRL_URL = "http://127.0.0.1:10808/pki";
    public static final String CACERTS_PASSWORD = "changeit";

}
