package net.ihe.gazelle.pki;

import net.ihe.gazelle.pki.bouncycastle.CertificateBC;
import net.ihe.gazelle.pki.crl.CrlUtil;
import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.enums.CertificateVersion;
import net.ihe.gazelle.pki.enums.KeyAlgorithm;
import net.ihe.gazelle.pki.enums.SignatureAlgorithm;
import net.ihe.gazelle.pki.bouncycastle.extension.CertificateExtenderCA;
import net.ihe.gazelle.pki.model.*;
import net.ihe.gazelle.pki.validator.CertificateValidatorResult;
import net.ihe.gazelle.pki.validator.CertificateValidatorResultEnum;

import javax.persistence.EntityManager;
import javax.security.auth.x500.X500Principal;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Locale;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CertificateTestTools {

    static CertificateRequest getCertificateRequest(CertificateType type, Certificate certificateAuthority)
            throws CertificateException {
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = new CertificateRequestWithGeneratedKeys(KeyAlgorithm.RSA, 1024);
        certificateRequestWithGeneratedKeys.setCertificateAuthority(certificateAuthority);
        certificateRequestWithGeneratedKeys.setCertificateExtension(type);
        certificateRequestWithGeneratedKeys.setNotAfter(CertificateUtil.getNotAfter());
        certificateRequestWithGeneratedKeys.setNotBefore(CertificateUtil.getNotBefore());
        certificateRequestWithGeneratedKeys.setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION);
        certificateRequestWithGeneratedKeys.setSubjectUsingAttributes(Locale.FRANCE.getCountry(), "IHE", "glandais", null, null,
                null, "IRISA", null);

        return certificateRequestWithGeneratedKeys;
    }

    static CertificateRequestAuthority getCertificateRequestAuthority() throws CertificateException {
        CertificateRequestAuthority cra = new CertificateRequestAuthority(KeyAlgorithm.RSA, 1024);
        cra.setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION);
        cra.setCertificateExtension(CertificateType.CA_KEY_USAGE_ALL);
        cra.setCertificateVersion(CertificateVersion.V3);
        cra.setSubjectUsingAttributes(Locale.FRANCE.getCountry(), "IHE", "glandais", null, null, null, "IRISA", null);
        cra.setIssuer(cra.getSubject());

        cra.setNotAfter(CertificateUtil.getNotAfter());
        cra.setNotBefore(CertificateUtil.getNotBefore());
        return cra;
    }

    static void testCreateValidateUpdateCertificate(CertificateType certificateType) throws CertificateException {
        CertificateRequestAuthority certificateRequestAuthority = getCertificateRequestAuthority();
        Certificate certificateAuthority = CertificateManager.createCertificateAuthority(certificateRequestAuthority,
                null);
        CrlUtil.addTestCertificate(certificateAuthority);
        CertificateValidatorResult result = CertificateType.CA_KEY_USAGE_ALL.getValidator().validate(
                certificateAuthority.getChain(), true);

        assertTrue(result.getResult().equals(CertificateValidatorResultEnum.PASSED));

        CertificateRequest certificateRequest = getCertificateRequest(certificateType, certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequest, null);
        CrlUtil.addTestCertificate(certificate);
        result = certificateType.getValidator().validate(certificate.getChain(), true);

        assertTrue(result.getResult().equals(CertificateValidatorResultEnum.PASSED));

        CertificateManager.updateCertificate(certificateRequest, null);
        result = certificateType.getValidator().validate(certificate.getChain(), true);

        assertTrue(result.getResult().equals(CertificateValidatorResultEnum.PASSED));
    }

    static void testEpsos(CertificateType certificateType) throws CertificateException, NoSuchAlgorithmException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        Certificate certificate = getCertificateOK(certificateType, certificateAuthority);
        CertificateValidatorResult result = certificateType.getValidator().validate(certificate.getChain(), true);
        assertTrue(result.getResult().equals(CertificateValidatorResultEnum.PASSED));
    }

    static Certificate getCertificateOK(CertificateType certificateType, Certificate certificateAuthority)
            throws CertificateException, NoSuchAlgorithmException {
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = getCertificateRequestAutoSignOK(certificateType,
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        return certificate;
    }

    static CertificateRequestWithGeneratedKeys getCertificateRequestAutoSignOK(CertificateType certificateType,
                                                                               Certificate certificateAuthority) throws CertificateException {
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = new CertificateRequestWithGeneratedKeys(KeyAlgorithm.RSA, 1024);
        certificateRequestWithGeneratedKeys.setCertificateAuthority(certificateAuthority);
        certificateRequestWithGeneratedKeys.setCertificateExtension(certificateType);
        certificateRequestWithGeneratedKeys.setNotAfter(CertificateUtil.getNotAfter());
        certificateRequestWithGeneratedKeys.setNotBefore(CertificateUtil.getNotBefore());
        certificateRequestWithGeneratedKeys.setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION);
        certificateRequestWithGeneratedKeys.setSubjectUsingAttributes(Locale.FRANCE.getCountry(), "IHE", "glandais", null, null,
                null, "IRISA", null);
        return certificateRequestWithGeneratedKeys;
    }

    static CertificateRequestWithGeneratedKeys getCertificateRequestAutoSignHack(CertificateType certificateType,
                                                                                 Certificate certificateAuthority,
                                                                                 final CertificateExtender hackedCertificateExtender)
            throws CertificateException {
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = new CertificateRequestWithGeneratedKeys(KeyAlgorithm.RSA, 1024) {
            @Override
            public CertificateExtender getCertificateExtender() {
                return hackedCertificateExtender;
            }
        };
        certificateRequestWithGeneratedKeys.setCertificateAuthority(certificateAuthority);
        certificateRequestWithGeneratedKeys.setCertificateExtension(certificateType);
        certificateRequestWithGeneratedKeys.setNotAfter(CertificateUtil.getNotAfter());
        certificateRequestWithGeneratedKeys.setNotBefore(CertificateUtil.getNotBefore());
        certificateRequestWithGeneratedKeys.setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION);
        certificateRequestWithGeneratedKeys.setSubjectUsingAttributes(Locale.FRANCE.getCountry(), "IHE", "glandais", null, null,
                null, "IRISA", null);
        return certificateRequestWithGeneratedKeys;
    }

    static Certificate getCertificateAuthorityOK() throws CertificateException {
        CertificateRequestAuthority cra = new CertificateRequestAuthority(KeyAlgorithm.RSA, 1024);
        cra.setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION);
        cra.setCertificateExtension(CertificateType.CA_KEY_USAGE_ALL);
        cra.setCertificateVersion(CertificateVersion.V3);
        cra.setSubjectUsingAttributes(Locale.FRANCE.getCountry(), "IHE", "unit-test CA", null, null, null, "Kereval", null);
        cra.setIssuerUsingAttributes(Locale.FRANCE.getCountry(), "IHE", "unit-test CA", null, null, null, "Kereval", null);
        cra.setNotAfter(CertificateUtil.getNotAfter());
        cra.setNotBefore(CertificateUtil.getNotBefore());

        Certificate certificateAuthority = CertificateManager.createCertificateAuthority(cra, null);
        CrlUtil.addTestCertificate(certificateAuthority);
        return certificateAuthority;
    }

    static Certificate getCertificateAuthorityOCSP(final String authorityInfoAccess) throws CertificateException {

        CertificateRequestAuthority cra = CertificateRequestHacked.getInstance().getCertificateExtenderCA(authorityInfoAccess);
        cra.setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION);
        cra.setCertificateExtension(CertificateType.CA_KEY_USAGE_ALL);
        cra.setCertificateVersion(CertificateVersion.V3);
        cra.setSubjectUsingAttributes(Locale.FRANCE.getCountry(), "IHE", "glandais", null, null, null, "IRISA", null);
        cra.setIssuerUsingAttributes(Locale.FRANCE.getCountry(), "IHE", "glandais", null, null, null, "IRISA", null);
        cra.setNotAfter(CertificateUtil.getNotAfter());
        cra.setNotBefore(CertificateUtil.getNotBefore());

        Certificate certificateAuthority = CertificateManager.createCertificateAuthority(cra, null);
        CrlUtil.addTestCertificate(certificateAuthority);
        return certificateAuthority;
    }

    static Certificate createCertificateBadIssuer(CertificateRequest certificateRequest,
                                                         EntityManager entityManager) throws CertificateException {
        PublicKey publicKey = certificateRequest.getPublicKey();
        PrivateKey privateKey = certificateRequest.getPrivateKey();
        Certificate certificateAuthority = certificateRequest.getCertificateAuthority();

        if (certificateAuthority == null || certificateAuthority.getCertificateX509() == null
                || certificateAuthority.getCertificateX509().getX509Certificate(new CertificateBC()) == null) {
            throw new IllegalArgumentException("Invalid certificateAuthority");
        }

        Certificate certificate = new Certificate();
        certificate.setRequest(certificateRequest);
        certificateRequest.setCertificate(certificate);
        certificate.setSubject(certificateRequest.getSubject());

        certificate.getPublicKey().setKey(publicKey);
        certificate.getPrivateKey().setKey(privateKey);
        certificate.setCertificateAuthority(certificateAuthority);

        if (entityManager != null) {
            entityManager.persist(certificate);
        } else {
            certificate.setId(CertificateManager.testCertificateId++);
        }

        int serialNumber = certificateAuthority.getNextSerialNumber();
        X509Certificate cert = createX509CertificateBadIssuer(certificateRequest, publicKey, certificateAuthority,
                BigInteger.valueOf(serialNumber), certificate);
        certificate.getCertificateX509().setX509Certificate(cert);

        certificateAuthority.getCertificates().add(certificate);

        if (entityManager != null) {
            entityManager.merge(certificate);
            // cascade all !
            entityManager.merge(certificateAuthority);
            entityManager.merge(certificateRequest);
        }

        return certificate;
    }

    static X509Certificate createX509CertificateBadIssuer(CertificateRequest certificateRequest, PublicKey publicKey,
                                                          Certificate certificateAuthority, BigInteger serialNumber, Certificate certificate)
            throws CertificateException {
        PKiProvider lo_pKiProvider = new CertificateBC();

        X509Certificate caCert = certificateAuthority.getCertificateX509().getX509Certificate(new CertificateBC());

        X500Principal issuerPrincipal;

        issuerPrincipal = caCert.getIssuerX500Principal();

        X500Principal subjectPrincipal = new X500Principal(certificateRequest.getSubject());

        // hacking the issuer principal
        String ipS = issuerPrincipal.toString();
        String modifiedIssuer = issuerPrincipal.toString() + "modified";

        X509Certificate cert = lo_pKiProvider.generateCertificate(certificateAuthority.getPrivateKey().getKey(), certificateRequest,
                modifiedIssuer, publicKey, serialNumber, certificate.getId());

        return cert;
    }

    static void assertContainsError(CertificateValidatorResult result, boolean error, String errorString) {
        if (error) {
            assertTrue("Validation result should contain error '" + errorString + "'", result.containsError(errorString));
        } else {
            assertTrue("Validation result should contain warning '" + errorString + "'", result.containsWarning(errorString));
        }
    }

    static void assertNotContainsError(CertificateValidatorResult result, boolean error, String errorString) {
        if (error) {
            assertFalse("Validation result should NOT contain error '" + errorString + "'", result.containsError(errorString));
        } else {
            assertFalse("Validation result should NOT contain warning '" + errorString + "'", result.containsWarning(errorString));
        }
    }

    static Certificate createCertificateSerialNumber(CertificateRequest certificateRequest,
                                                     EntityManager entityManager, BigInteger serialNumber) throws CertificateException {
        PKiProvider lo_pKiProvider = new CertificateBC();

        PublicKey publicKey = certificateRequest.getPublicKey();
        PrivateKey privateKey = certificateRequest.getPrivateKey();
        Certificate certificateAuthority = certificateRequest.getCertificateAuthority();

        if (certificateAuthority == null || certificateAuthority.getCertificateX509() == null
                || certificateAuthority.getCertificateX509().getX509Certificate(new CertificateBC()) == null) {
            throw new IllegalArgumentException("Invalid certificateAuthority");
        }

        Certificate certificate = new Certificate();
        certificate.setRequest(certificateRequest);
        certificateRequest.setCertificate(certificate);
        certificate.setSubject(certificateRequest.getSubject());

        certificate.getPublicKey().setKey(publicKey);
        certificate.getPrivateKey().setKey(privateKey);
        certificate.setCertificateAuthority(certificateAuthority);

        if (entityManager != null) {
            entityManager.persist(certificate);
        } else {
            certificate.setId(CertificateManager.testCertificateId++);
        }

        //the given serial number is set to the X509 cert, but it cannot be taken into account in the CA serialNumber counter.
        X509Certificate cert = lo_pKiProvider.createX509Certificate(certificateRequest, publicKey,
                certificateAuthority, serialNumber, certificate);
        certificate.getCertificateX509().setX509Certificate(cert);

        certificateAuthority.getCertificates().add(certificate);

        if (entityManager != null) {
            entityManager.merge(certificate);
            // cascade all !
            entityManager.merge(certificateAuthority);
            entityManager.merge(certificateRequest);
        }

        return certificate;
    }

}
