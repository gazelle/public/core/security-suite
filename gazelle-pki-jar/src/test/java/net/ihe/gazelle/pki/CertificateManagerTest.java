package net.ihe.gazelle.pki;

import net.ihe.gazelle.pki.bouncycastle.CertificateBC;
import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.enums.KeyAlgorithm;
import net.ihe.gazelle.pki.enums.SignatureAlgorithm;
import net.ihe.gazelle.pki.env.PKITestEnvironment;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.pki.model.CertificateRequestWithGeneratedKeys;
import net.ihe.gazelle.pki.validator.CertificateValidatorResult;
import net.ihe.gazelle.pki.validator.CertificateValidatorResultEnum;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static net.ihe.gazelle.pki.CertificateTestTools.getCertificateAuthorityOK;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class CertificateManagerTest extends PKITestEnvironment {

    @Test
    public void testCAForTest() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();

        List<X509Certificate> certificates = new ArrayList<>();
        certificates.add(certificateAuthority.getCertificateX509().getX509Certificate(new CertificateBC()));

        CertificateValidatorResult result = CertificateType.CA_KEY_USAGE_ALL.getValidator().validate(certificates, false);
        assertTrue(result.getErrors().isEmpty());
        assertTrue(result.getWarnings().isEmpty());

    }


    @Test
    public void testSubjectAltNameCertificate() throws CertificateException {

        Certificate certificateAuthority = getCertificateAuthorityOK();

        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = new CertificateRequestWithGeneratedKeys(KeyAlgorithm.RSA, 2048);
        certificateRequestWithGeneratedKeys.setCertificateAuthority(certificateAuthority);
        certificateRequestWithGeneratedKeys.setCertificateExtension(CertificateType.CLIENT_AND_SERVER);
        certificateRequestWithGeneratedKeys.setNotAfter(CertificateUtil.getNotAfter());
        certificateRequestWithGeneratedKeys.setNotBefore(CertificateUtil.getNotBefore());
        certificateRequestWithGeneratedKeys.setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION);
        certificateRequestWithGeneratedKeys.setSubjectUsingAttributes(Locale.FRANCE.getCountry(), "Kereval", "ycadoret", null, null,
                null, "IHE Services", null);
        certificateRequestWithGeneratedKeys.setSubjectAlternativeName("example.com; *.example2.com");

        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);

        assertEquals("example.com, *.example2.com", certificate.getSubjectAlternativeName());
        assertEquals("[[2, example.com], [2, *.example2.com]]",
                certificate.getCertificateX509().getX509Certificate(new CertificateBC()).getSubjectAlternativeNames().toString());
    }

    @Test
    public void testSubjectAltNameCertificateEmptyEntries() throws CertificateException {

        Certificate certificateAuthority = getCertificateAuthorityOK();

        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = new CertificateRequestWithGeneratedKeys(KeyAlgorithm.RSA, 2048);
        certificateRequestWithGeneratedKeys.setCertificateAuthority(certificateAuthority);
        certificateRequestWithGeneratedKeys.setCertificateExtension(CertificateType.CLIENT_AND_SERVER);
        certificateRequestWithGeneratedKeys.setNotAfter(CertificateUtil.getNotAfter());
        certificateRequestWithGeneratedKeys.setNotBefore(CertificateUtil.getNotBefore());
        certificateRequestWithGeneratedKeys.setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION);
        certificateRequestWithGeneratedKeys.setSubjectUsingAttributes(Locale.FRANCE.getCountry(), "Kereval", "ycadoret-2", null, null,
                null, "IHE Services", null);
        certificateRequestWithGeneratedKeys.setSubjectAlternativeName("example.com; *.example2.com,,");

        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);

        assertEquals("example.com, *.example2.com,,", certificate.getSubjectAlternativeName());
        assertEquals("[[2, example.com], [2, *.example2.com]]",
                certificate.getCertificateX509().getX509Certificate(new CertificateBC()).getSubjectAlternativeNames().toString());
    }

    @Test
    public void testNoSubjectAltNameCertificate() throws CertificateException {

        Certificate certificateAuthority = getCertificateAuthorityOK();

        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = new CertificateRequestWithGeneratedKeys(KeyAlgorithm.RSA, 2048);
        certificateRequestWithGeneratedKeys.setCertificateAuthority(certificateAuthority);
        certificateRequestWithGeneratedKeys.setCertificateExtension(CertificateType.CLIENT_AND_SERVER);
        certificateRequestWithGeneratedKeys.setNotAfter(CertificateUtil.getNotAfter());
        certificateRequestWithGeneratedKeys.setNotBefore(CertificateUtil.getNotBefore());
        certificateRequestWithGeneratedKeys.setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION);
        certificateRequestWithGeneratedKeys.setSubjectUsingAttributes(Locale.FRANCE.getCountry(), "Kereval", "ycadoret-2", null, null,
                null, "IHE Services", null);
        certificateRequestWithGeneratedKeys.setSubjectAlternativeName("");

        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);

        assertEquals("", certificate.getSubjectAlternativeName());
        assertNull(certificate.getCertificateX509().getX509Certificate(new CertificateBC()).getSubjectAlternativeNames());
    }

    private void validatePEM(String fileName, CertificateType type) throws CertificateException {
        try {
            InputStream resourceAsStream = this.getClass().getResourceAsStream("/" + fileName);
            String pem = IOUtils.toString(resourceAsStream, null);
            List<X509Certificate> certificates = CertificateUtil.loadCertificates(pem);

            CertificateValidatorResult result = type.getValidator().validate(certificates, true);
            assertTrue(result.getResult().equals(CertificateValidatorResultEnum.FAILED));
        } catch (IOException e) {
            throw new CertificateException(e);
        }
    }

}
