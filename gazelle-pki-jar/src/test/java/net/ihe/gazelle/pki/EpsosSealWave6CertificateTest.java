package net.ihe.gazelle.pki;

import net.ihe.gazelle.pki.crl.CrlUtil;
import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.pki.model.CertificateRequestWithGeneratedKeys;
import net.ihe.gazelle.pki.validator.CertificateValidatorResult;
import org.junit.Test;

import java.security.cert.CertificateException;

import static net.ihe.gazelle.pki.CertificateTestTools.*;

public class EpsosSealWave6CertificateTest extends EpsosAbstractV4CertificateTest {

    private static final CertificateType CERTIFICATE_TYPE = CertificateType.EPSOS_SEAL_V6;

    @Override
    CertificateType getCertificateType() {
        return CERTIFICATE_TYPE;
    }

    @Test
    public void testEHEALTH_X509_3_1_ExtendedKeyUsageWithoutEmailProtection() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackExtendedKeyUsageExtension(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertNotContainsError(result, true, "ExtendedKeyUsage MUST NOT be included as an extension in the certificate.");
        assertContainsError(result, false, "Both values id_kp_emailProtection [RFC5280] and id‐kp‐clientAuth [RFC5280] SHOULD be present.");
    }

    @Test
    public void testEHEALTH_X509_3_1_ExtendedKeyUsage() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackExtendedKeyUsageExtensionWithEmailProtection(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertNotContainsError(result, true, "ExtendedKeyUsage MUST NOT be included as an extension in the certificate.");
        assertNotContainsError(result, false, "Both values id_kp_emailProtection [RFC5280] and id‐kp‐clientAuth [RFC5280] SHOULD be present.");
    }
}
