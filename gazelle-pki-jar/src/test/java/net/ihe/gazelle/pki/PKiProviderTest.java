package net.ihe.gazelle.pki;

import net.ihe.gazelle.pki.bouncycastle.CertificateBC;
import net.ihe.gazelle.pki.model.PKiProvider;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class PKiProviderTest {
    @Test
    public void testGetRFC822Name() {
        PKiProvider lo_pkiProvider = new CertificateBC();

        assertTrue(lo_pkiProvider.getRFC822Name() == 1);
    }

    @Test
    public void testGetUniformResourceIdentifier() {
        PKiProvider lo_pkiProvider = new CertificateBC();

        assertTrue(lo_pkiProvider.getUniformResourceIdentifier() == 6);
    }

    @Test
    public void testGetProviderName() {
        PKiProvider lo_pkiProvider = new CertificateBC();

        assertTrue(lo_pkiProvider.getProviderName().equals("BC"));
    }

    @Test
    public void testCreateProvider() {
        PKiProvider lo_pkiProvider = new CertificateBC();

        assertTrue(lo_pkiProvider.createProvider() != null);
    }


}
