package net.ihe.gazelle.pki;

import net.ihe.gazelle.pki.bouncycastle.*;
import net.ihe.gazelle.pki.bouncycastle.CertificatePolicies;
import net.ihe.gazelle.pki.bouncycastle.extension.CertificateExtenderCA;
import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.enums.KeyAlgorithm;
import net.ihe.gazelle.pki.enums.SignatureAlgorithm;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.pki.model.CertificateRequestAuthority;
import net.ihe.gazelle.pki.model.CertificateRequestWithGeneratedKeys;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.misc.MiscObjectIdentifiers;
import org.bouncycastle.asn1.misc.NetscapeRevocationURL;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.cert.CertIOException;
import org.bouncycastle.cert.jcajce.JcaX509ExtensionUtils;

import java.io.IOException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Locale;

public class CertificateRequestHacked {
    private static CertificateRequestHacked mo_instance;

    /**
     * Private Constructor.
     */
    private CertificateRequestHacked() { }

    /**
     * Getter for the instance.
     * @return
     */
    public static CertificateRequestHacked getInstance() {
        if (mo_instance == null)
            mo_instance = new CertificateRequestHacked();
        return mo_instance;
    }

    /**
     *
     * @param certificateType
     * @param certificateAuthority
     * @param hackedCertificateExtender
     * @return
     * @throws CertificateException
     */
    private CertificateRequestWithGeneratedKeys getCertificateRequestAutoSignHack(CertificateType certificateType,
                                                                                 Certificate certificateAuthority,
                                                                                 final CertificateExtender hackedCertificateExtender)
            throws CertificateException {
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = new CertificateRequestWithGeneratedKeys(KeyAlgorithm.RSA, 1024) {
            @Override
            public CertificateExtender getCertificateExtender() {
                return hackedCertificateExtender;
            }
        };
        certificateRequestWithGeneratedKeys.setCertificateAuthority(certificateAuthority);
        certificateRequestWithGeneratedKeys.setCertificateExtension(certificateType);
        certificateRequestWithGeneratedKeys.setNotAfter(CertificateUtil.getNotAfter());
        certificateRequestWithGeneratedKeys.setNotBefore(CertificateUtil.getNotBefore());
        certificateRequestWithGeneratedKeys.setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION);
        certificateRequestWithGeneratedKeys.setSubjectUsingAttributes(Locale.FRANCE.getCountry(), "IHE", "glandais", null, null,
                null, "IRISA", null);
        return certificateRequestWithGeneratedKeys;
    }

    /**
     *
     * @param certificateType
     * @param certificateAuthority
     * @return
     * @throws CertificateException
     */
    public CertificateRequestWithGeneratedKeys generateAutoSignHackIssuerUniqueID(CertificateType certificateType, Certificate certificateAuthority) throws CertificateException {
       return getCertificateRequestAutoSignHack(certificateType,
               certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(CertificateBuilder certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        boolean[] uniqueID = new boolean[1];
                        uniqueID[0] = true;
                        certGen.setIssuerUniqueID(uniqueID);
                    }
                });
    }

    /**
     *
     * @param certificateType
     * @param certificateAuthority
     * @return
     * @throws CertificateException
     */
    public CertificateRequestWithGeneratedKeys generateAutoSignHackSubjectUniqueID(CertificateType certificateType, Certificate certificateAuthority) throws CertificateException {
        return getCertificateRequestAutoSignHack(certificateType,
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(CertificateBuilder certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        boolean[] uniqueID = new boolean[1];
                        uniqueID[0] = true;
                        certGen.setSubjectUniqueID(uniqueID);
                    }
                });
    }

    /**
     *
     * @param certificateType
     * @param certificateAuthority
     * @return
     * @throws CertificateException
     */
    public CertificateRequestWithGeneratedKeys generateAutoSignHackAuthorityKeyIdentifierExtension(CertificateType certificateType, Certificate certificateAuthority) throws CertificateException {
        return getCertificateRequestAutoSignHack(certificateType,
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(CertificateBuilder certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        try {
                            JcaX509ExtensionUtils x509ExtensionUtils = new JcaX509ExtensionUtils();
                            AuthorityKeyIdentifier authorityKeyIdentifier = x509ExtensionUtils
                                    .createAuthorityKeyIdentifier(parameters.getCertificateRequest().getCertificateAuthority().getPublicKey().getKey());
                            certGen.getCertV3Builder().addExtension(Extension.authorityKeyIdentifier, true, authorityKeyIdentifier);
                        } catch (CertIOException| NoSuchAlgorithmException e){
                            throw new CertificateException(e);
                        }
                    }
                });
    }

    /**
     *
     * @param certificateType
     * @param certificateAuthority
     * @return
     * @throws CertificateException
     */
    public CertificateRequestWithGeneratedKeys generateAutoSignHackCAAuthorityKeyIdentifierExtension(CertificateType certificateType, Certificate certificateAuthority) throws CertificateException {
        return getCertificateRequestAutoSignHack(certificateType,
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(CertificateBuilder certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = new CertificateRequestWithGeneratedKeys(
                                KeyAlgorithm.RSA, 1024);

                        certGen.addAuthorityKeyIdentifierExtension(certificateRequestWithGeneratedKeys.getPublicKey());
                    }
                });
    }

    /**
     *
     * @param certificateType
     * @param certificateAuthority
     * @return
     * @throws CertificateException
     */
    public CertificateRequestWithGeneratedKeys generateAutoSignHackAuthorityKeyIdentifierParameters(CertificateType certificateType, Certificate certificateAuthority) throws CertificateException {
        return getCertificateRequestAutoSignHack(certificateType,
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(CertificateBuilder certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        try {
                            JcaX509ExtensionUtils x509ExtensionUtils = new JcaX509ExtensionUtils();
                            AuthorityKeyIdentifier authorityKeyIdentifier = x509ExtensionUtils
                                    .createAuthorityKeyIdentifier(parameters.getCertificateRequest().getCertificateAuthority().getCertificateX509()
                                            .getX509Certificate(new CertificateBC()).getPublicKey(),
                                            CertificateGeneralName.getGeneralNames(CertificateGeneralName.getDirectoryName("CN=toto")), BigInteger.TEN);
                            certGen.getCertV3Builder().addExtension(Extension.authorityKeyIdentifier, false, authorityKeyIdentifier);
                        } catch (CertIOException| NoSuchAlgorithmException e){
                            throw new CertificateException(e);
                        }
//                        certGen.addAuthorityKeyIdentifierExtension(parameters.getCertificateRequest().getCertificateAuthority().getCertificateX509().getX509Certificate(new CertificateBC()).getPublicKey());
                    }
                });
    }

    /**
     *
     * @param certificateType
     * @param certificateAuthority
     * @return
     * @throws CertificateException
     */
    public CertificateRequestWithGeneratedKeys generateAutoSignHackKeyUsageEncipherment(CertificateType certificateType, Certificate certificateAuthority) throws CertificateException {
        return getCertificateRequestAutoSignHack(certificateType,
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(CertificateBuilder certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        try {
                            certGen.getCertV3Builder().addExtension(Extension.keyUsage, false, new KeyUsage(CertificateKeyUsage.getInstance().getKeyUsageEncipherment()));
                        } catch (CertIOException e){
                            throw new CertificateException(e);
                        }
                    }
                });
    }

    /**
     *
     * @param certificateType
     * @param certificateAuthority
     * @return
     * @throws CertificateException
     */
    public CertificateRequestWithGeneratedKeys generateAutoSignHackDigitalSignatureKeyEncipherment(CertificateType certificateType, Certificate certificateAuthority) throws CertificateException {
        return getCertificateRequestAutoSignHack(certificateType,
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(CertificateBuilder certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        certGen.addKeyUsageExtension(CertificateKeyUsage.getInstance().getKeyUsageEncipherment() + CertificateKeyUsage.getInstance().getKeyUsageDigitalSignature());
                    }

                });
    }

    /**
     *
     * @param certificateType
     * @param certificateAuthority
     * @return
     * @throws CertificateException
     */
    public CertificateRequestWithGeneratedKeys generateAutoSignHackNone(CertificateType certificateType, Certificate certificateAuthority) throws CertificateException {
        return getCertificateRequestAutoSignHack(certificateType,
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(CertificateBuilder certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        // NONE !
                    }
                });
    }

    /**
     *
     * @param certificateType
     * @param certificateAuthority
     * @return
     * @throws CertificateException
     */
    public CertificateRequestWithGeneratedKeys generateAutoSignHackSubjectKeyIdentifierExtension(CertificateType certificateType, Certificate certificateAuthority) throws CertificateException {
        return getCertificateRequestAutoSignHack(certificateType,
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(CertificateBuilder certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        try {
                            JcaX509ExtensionUtils x509ExtensionUtils = new JcaX509ExtensionUtils();
                            SubjectKeyIdentifier subjectKeyIdentifier = x509ExtensionUtils
                                    .createSubjectKeyIdentifier(parameters.getPublicKey());
                            certGen.getCertV3Builder().addExtension(Extension.subjectKeyIdentifier, true, subjectKeyIdentifier);
                        } catch (CertIOException| NoSuchAlgorithmException e){
                            throw new CertificateException(e);
                        }
                    }
                });
    }

    /**
     *
     * @param certificateType
     * @param certificateAuthority
     * @return
     * @throws CertificateException
     */
    public CertificateRequestWithGeneratedKeys generateAutoSignHackSubjectKeyIdentifierExtensionRFC5280(CertificateType certificateType, Certificate certificateAuthority) throws CertificateException {
        return getCertificateRequestAutoSignHack(certificateType,
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(CertificateBuilder certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = new CertificateRequestWithGeneratedKeys(
                                KeyAlgorithm.RSA, 1024);
                        certGen.addSubjectKeyIdentifierExtension(certificateRequestWithGeneratedKeys.getPublicKey());
                    }
                });
    }

    /**
     *
     * @param certificateType
     * @param certificateAuthority
     * @return
     * @throws CertificateException
     */
    public CertificateRequestWithGeneratedKeys generateAutoSignHackSubjectAlternativeNameExtension(CertificateType certificateType, Certificate certificateAuthority) throws CertificateException {
        return getCertificateRequestAutoSignHack(certificateType,
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(CertificateBuilder certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        try {
                            certGen.getCertV3Builder().addExtension(Extension.subjectAlternativeName, true,
                                    CertificateGeneralName.getGeneralNames(CertificateGeneralName.getDirectoryName("CN=toto")));
                        } catch (CertIOException e){
                            throw new CertificateException(e);
                        }
                    }
                });
    }

    /**
     *
     * @param certificateType
     * @param certificateAuthority
     * @return
     * @throws CertificateException
     */
    public CertificateRequestWithGeneratedKeys generateAutoSignHackIssuerAlternativeNameExtension(CertificateType certificateType, Certificate certificateAuthority) throws CertificateException {
        return getCertificateRequestAutoSignHack(certificateType,
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(CertificateBuilder certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        try {
                            certGen.getCertV3Builder().addExtension(Extension.issuerAlternativeName, true,
                                    CertificateGeneralName.getGeneralNames(CertificateGeneralName.getDirectoryName("CN=toto")));
                        } catch (CertIOException e){
                            throw new CertificateException(e);
                        }
                    }
                });
    }

    /**
     *
     * @param certificateType
     * @param certificateAuthority
     * @return
     * @throws CertificateException
     */
    public CertificateRequestWithGeneratedKeys generateAutoSignHackBasicConstraintExtension(CertificateType certificateType, Certificate certificateAuthority) throws CertificateException {
        return getCertificateRequestAutoSignHack(certificateType,
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(CertificateBuilder certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        try {
                            certGen.getCertV3Builder().addExtension(Extension.basicConstraints, false, new BasicConstraints(false));
                        } catch (CertIOException e){
                            throw new CertificateException(e);
                        }
                    }
                });
    }

    /**
     *
     * @param certificateType
     * @param certificateAuthority
     * @return
     * @throws CertificateException
     */
    public CertificateRequestWithGeneratedKeys generateAutoSignHackCRLExtension(CertificateType certificateType, Certificate certificateAuthority, final String crlUrl) throws CertificateException {
        return getCertificateRequestAutoSignHack(certificateType,
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(CertificateBuilder certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        DistributionPointName distPointOne = new DistributionPointName(new GeneralNames(
                                new GeneralName(GeneralName.uniformResourceIdentifier, crlUrl)));
                        DistributionPoint[] distPoints = new DistributionPoint[1];
                        distPoints[0] = new DistributionPoint(distPointOne, null, null);
                        try {
                            certGen.getCertV3Builder().addExtension(Extension.cRLDistributionPoints, true, new CRLDistPoint(distPoints));
                        } catch (IOException e) {
                            throw new CertificateException(e);
                        }
                    }
                });
    }

    /**
     *
     * @param certificateType
     * @param certificateAuthority
     * @return
     * @throws CertificateException
     */
    public CertificateRequestWithGeneratedKeys generateAutoSignHackPoliciesExtension(CertificateType certificateType, Certificate certificateAuthority) throws CertificateException {
        return getCertificateRequestAutoSignHack(certificateType,
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(CertificateBuilder certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        certGen.addCertificatePoliciesExtension(CertificatePolicies.getPolicyInformation("www.test.com/policy.dpc", "2.22.22.2.2.2"));
                    }
                });
    }

    /**
     *
     * @param certificateType
     * @param certificateAuthority
     * @return
     * @throws CertificateException
     */
    public CertificateRequestWithGeneratedKeys generateAutoSignHackAuthorityInfoAccessExtension(CertificateType certificateType, Certificate certificateAuthority, final String authorityInfoAccess)  throws CertificateException {
        return  getCertificateRequestAutoSignHack(certificateType,
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(CertificateBuilder certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        try {
                            certGen.getCertV3Builder().addExtension(Extension.authorityInfoAccess, true, CertificateGeneralName.getAuthorityInformationAccessMethod(authorityInfoAccess));
                        } catch (CertIOException e){
                            throw new CertificateException(e);
                        }
                    }
                });
    }

    /**
     *
     * @param certificateType
     * @param certificateAuthority
     * @return
     * @throws CertificateException
     */
    public CertificateRequestWithGeneratedKeys generateAutoSignHackExtendedKeyUsageExtension(CertificateType certificateType, Certificate certificateAuthority) throws CertificateException {
        return getCertificateRequestAutoSignHack(certificateType,
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(CertificateBuilder certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        certGen.addExtendedKeyUsageExtension(CertificateKeyPurposeId.SERVER_AUTHENTICATION, CertificateKeyPurposeId.CLIENT_AUTHENTICATION);
                    }
                });
    }

    /**
     *
     * @param certificateType
     * @param certificateAuthority
     * @return
     * @throws CertificateException
     */
    public CertificateRequestWithGeneratedKeys generateAutoSignHackExtendedKeyUsageExtensionWithEmailProtection(CertificateType certificateType, Certificate certificateAuthority) throws CertificateException {
        return getCertificateRequestAutoSignHack(certificateType,
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(CertificateBuilder certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        certGen.addExtendedKeyUsageExtension(CertificateKeyPurposeId.EMAIL_PROTECTION, CertificateKeyPurposeId.CLIENT_AUTHENTICATION);
                    }
                });
    }

    /**
     *
     * @param certificateType
     * @param certificateAuthority
     * @return
     * @throws CertificateException
     */
    public CertificateRequestWithGeneratedKeys generateAutoSignHackExtendedKeyUsageExtensionServerOnly(CertificateType certificateType, Certificate certificateAuthority) throws CertificateException {
        return getCertificateRequestAutoSignHack(certificateType,
                certificateAuthority, new CertificateExtenderAbstract() {
                    @Override
                    public void addExtension(CertificateBuilder certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        certGen.addExtendedKeyUsageExtension(CertificateKeyPurposeId.SERVER_AUTHENTICATION);
                    }
                });
    }

    /**
     *
     * @param authorityInfoAccess
     * @return
     * @throws CertificateException
     */
    public CertificateRequestAuthority getCertificateExtenderCA(final String authorityInfoAccess) throws CertificateException {
        return new CertificateRequestAuthority(KeyAlgorithm.RSA, 1024) {
            @Override
            public CertificateExtender getCertificateExtender() {
                CertificateExtender certificateExtender = new CertificateExtenderCA() {
                    @Override
                    public void addExtension(CertificateBuilder certGen,
                                             X509CertificateParametersContainer parameters) throws CertificateException {
                        super.addExtension(certGen, parameters);
                        GeneralName gn = new GeneralName(GeneralName.uniformResourceIdentifier, new DERIA5String(
                                authorityInfoAccess));
                        AuthorityInformationAccess authorityInformationAccess = new AuthorityInformationAccess(
                                X509ObjectIdentifiers.ocspAccessMethod, gn);

                        certGen.addAuthorityInfoAccessExtension(authorityInformationAccess);
                    }
                };
                return certificateExtender;
            }
        };
    }


}
