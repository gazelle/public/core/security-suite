package net.ihe.gazelle.pki;

import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.env.PKICRLTestEnvironment;
import org.junit.Test;

import java.security.cert.CertificateException;

import static net.ihe.gazelle.pki.CertificateTestTools.testCreateValidateUpdateCertificate;

public class CertificateManagerWithCRLTest extends PKICRLTestEnvironment {

    @Test
    public void testCreateValidateUpdateCertificateCA() throws CertificateException {
        testCreateValidateUpdateCertificate(CertificateType.CA_KEY_USAGE_ALL);
    }

    @Test
    public void testCreateValidateUpdateCertificateClientServer() throws CertificateException {
        testCreateValidateUpdateCertificate(CertificateType.CLIENT_AND_SERVER);
    }

    @Test
    public void testCreateValidateUpdateCertificateClient() throws CertificateException {
        testCreateValidateUpdateCertificate(CertificateType.HTTPS_CLIENT_AUTH);
    }

    @Test
    public void testCreateValidateUpdateCertificateServer() throws CertificateException {
        testCreateValidateUpdateCertificate(CertificateType.HTTPS_SERVER);
    }

}
