package net.ihe.gazelle.pki;

import net.ihe.gazelle.pki.crl.CrlUtil;
import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.pki.model.CertificateRequestWithGeneratedKeys;
import net.ihe.gazelle.pki.validator.CertificateValidatorResult;
import org.junit.Test;

import java.security.cert.CertificateException;

import static net.ihe.gazelle.pki.CertificateTestTools.assertContainsError;
import static net.ihe.gazelle.pki.CertificateTestTools.assertNotContainsError;
import static net.ihe.gazelle.pki.CertificateTestTools.getCertificateAuthorityOK;

public class EpsosTLSCertificateTest extends EpsosAbstractV4CertificateTest {

    private static final CertificateType CERTIFICATE_TYPE = CertificateType.EPSOS_TLS;

    @Override
    CertificateType getCertificateType() {
        return CERTIFICATE_TYPE;
    }

    @Test
    public void testEHEALTH_X509_3_1_KeyUsage_digitalSignature_and_keyEncipherment_only_OK() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackDigitalSignatureKeyEncipherment(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertNotContainsError(result, true, "The digitalSignature and keyEncipherment bits MUST both be set to true to the exclusion of all other " +
                "KeyUSage bits that MUST be set to false, excepted for the dataEncipherment bit that MAY be set to true.");
    }

    @Test
    public void testEHEALTH_X509_3_1_KeyUsage_digitalSignature_and_keyEncipherment_and_dataEncipherment_only_OK() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackDigitalSignatureKeyEncipherment(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertNotContainsError(result, true, "The digitalSignature and keyEncipherment bits MUST both be set to true to the exclusion of all other " +
                "KeyUSage bits that MUST be set to false, excepted for the dataEncipherment bit that MAY be set to true.");
    }

    @Test
    public void testEHEALTH_X509_3_1_KeyUsage_digitalSignature_and_keyEncipherment_only_NOK() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackKeyUsageEncipherment(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true, "The digitalSignature and keyEncipherment bits MUST both be set to true to the exclusion of all other " +
                "KeyUSage bits that MUST be set to false, excepted for the dataEncipherment bit that MAY be set to true.");
    }

    @Test
    public void testEHEALTH_X509_3_1_ExtendedKeyUsage_id_kp_clientAuth_and_id_kp_serverAuth_OK() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackExtendedKeyUsageExtension(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertNotContainsError(result, true,
                "Both values id‐kp‐serverAuth [RFC5280] and id‐kp‐clientAuth [RFC5280] MUST be present.");
    }

    @Test
    public void testEHEALTH_X509_3_1_ExtendedKeyUsage_id_kp_clientAuth_and_id_kp_serverAuth_KO() throws CertificateException {
        Certificate certificateAuthority = getCertificateAuthorityOK();
        CertificateRequestWithGeneratedKeys certificateRequestWithGeneratedKeys = CertificateRequestHacked.getInstance().generateAutoSignHackExtendedKeyUsageExtensionServerOnly(getCertificateType(),
                certificateAuthority);
        Certificate certificate = CertificateManager.createCertificate(certificateRequestWithGeneratedKeys, null);
        CrlUtil.addTestCertificate(certificate);
        CertificateValidatorResult result = getCertificateType().getValidator().validate(certificate.getChain(), true);
        assertContainsError(result, true, "Both values id‐kp‐serverAuth [RFC5280] and id‐kp‐clientAuth [RFC5280] MUST be present.");
    }
}
