package net.ihe.gazelle.pki.env;

import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;

public class CrlServer {

    private Channel serverChannel;

    private int port;
    private boolean ssl;

    public CrlServer(int port, boolean ssl) {
        super();
        this.port = port;
        this.ssl = ssl;
    }

    public void start() {
        if (serverChannel != null) {
            throw new IllegalStateException("Already started");
        }
        // Configure the server.
        ServerBootstrap bootstrap = new ServerBootstrap(new NioServerSocketChannelFactory(
                Executors.newCachedThreadPool(), Executors.newCachedThreadPool()));

        // Set up the event pipeline factory.
        bootstrap.setPipelineFactory(new CrlServerPipelineFactory(ssl));

        // Bind and start to accept incoming connections.
        serverChannel = bootstrap.bind(new InetSocketAddress(port));
    }

    public void stop() {
        if (serverChannel == null) {
            throw new IllegalStateException("Not started");
        }
        serverChannel.close().awaitUninterruptibly();
        serverChannel = null;
    }

}
