package net.ihe.gazelle.xua.model;

import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by aberge on 23/05/17.
 */

@Entity
@Name("assertion")
@DiscriminatorValue("assertion")
public class Assertion extends SOAPHeaderPart {

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "sts_endpoint")
    private String stsEndpoint;

    public Assertion(){

    }

    @Override
    public String getType() {
        return "Assertion";
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Assertion)) {
            return false;
        }

        Assertion assertion = (Assertion) o;

        if (username != null ? !username.equals(assertion.username) : assertion.username != null) {
            return false;
        }
        return password != null ? password.equals(assertion.password) : assertion.password == null;
    }

    @Override
    public int hashCode() {
        int result = username != null ? username.hashCode() : 0;
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }

    public String getStsEndpoint() {
        return stsEndpoint;
    }

    public void setStsEndpoint(String stsEndpoint) {
        this.stsEndpoint = stsEndpoint;
    }
}
