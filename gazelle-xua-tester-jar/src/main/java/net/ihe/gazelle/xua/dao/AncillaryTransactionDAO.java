package net.ihe.gazelle.xua.dao;

import net.ihe.gazelle.xua.model.AncillaryTransaction;
import net.ihe.gazelle.xua.model.AncillaryTransactionQuery;

import java.util.List;

/**
 * Created by aberge on 02/06/17.
 */
public class AncillaryTransactionDAO {

    public static List<AncillaryTransaction> getAllTransactions(){
        AncillaryTransactionQuery query = new AncillaryTransactionQuery();
        return query.getList();
    }

    public static AncillaryTransaction getTransactionById(Integer id) {
        AncillaryTransactionQuery query = new AncillaryTransactionQuery();
        query.id().eq(id);
        return query.getUniqueResult();
    }
}
