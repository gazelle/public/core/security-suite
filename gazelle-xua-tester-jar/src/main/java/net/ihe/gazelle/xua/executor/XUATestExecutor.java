package net.ihe.gazelle.xua.executor;

import net.ihe.gazelle.util.Pair;
import net.ihe.gazelle.xua.builder.SOAPHeaderBuilder;
import net.ihe.gazelle.xua.dao.ServiceProviderTestInstanceDAO;
import net.ihe.gazelle.xua.model.ExpectedResult;
import net.ihe.gazelle.xua.model.ServiceProviderTestInstance;
import net.ihe.gazelle.xua.model.TestStatus;
import net.ihe.gazelle.xua.wsclient.WebserviceClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Created by aberge on 02/06/17.
 */
public class XUATestExecutor {

    public static final TestStatus ABORTED = TestStatus.ABORTED;
    private static final Logger LOG = LoggerFactory.getLogger(XUATestExecutor.class);

    private ServiceProviderTestInstance testInstance;
    private String xuaFileDirectory;

    public XUATestExecutor(ServiceProviderTestInstance testInstance, String xuaBodyDirectory) {
        this.testInstance = testInstance;
        this.xuaFileDirectory = xuaBodyDirectory;
    }

    public ServiceProviderTestInstance execute() {
        try {
            SOAPMessage soapMessage = createSoapMessage();
            SOAPHeaderBuilder headerBuilder = new SOAPHeaderBuilder(testInstance);
            if (headerBuilder.buildSoapHeader(soapMessage)) {
                Document soapBodyContent = createSOAPBodyContent();
                if (soapBodyContent != null) {
                    soapMessage.getSOAPBody().addDocument(soapBodyContent);
                    Pair<SOAPMessage, String> response = sendSoapMessage(soapMessage);
                    SOAPMessage soapResponse = response.getObject1();
                    String responseHeadersDump = response.getObject2();
                    if (soapResponse != null) {
                        testInstance.setResponse(getBytesFromSOAPMessage(soapResponse));
                        setTestStatus(soapResponse);
                    }
                    if (responseHeadersDump != null) {
                        testInstance.setResponseHeaders(responseHeadersDump.getBytes(StandardCharsets.UTF_8));
                    }
                    testInstance.setRequest(getBytesFromSOAPMessage(soapMessage));
                }
            }
        } catch (Exception e) {
            LOG.error(e.getMessage());
            testInstance.setTestStatus(ABORTED);
        }
        return ServiceProviderTestInstanceDAO.saveTestInstance(testInstance);
    }

    private Pair<SOAPMessage, String> sendSoapMessage(SOAPMessage soapMessage) {
        WebserviceClient client = new WebserviceClient(testInstance.getTestedEndpoint());
        try {
            return client.sendMessage(soapMessage, testInstance.getAncillaryTransaction().getSoapAction());
        } catch (Exception e) {
            testInstance.setTestStatus(ABORTED);
            testInstance.setResponse(("Error while sending SOAP message: " + e.getMessage()).getBytes(StandardCharsets.UTF_8));
            LOG.error(e.getMessage(), e);
            return null;
        }
    }

    private void setTestStatus(SOAPMessage response) throws SOAPException {
        ExpectedResult expectedResult = testInstance.getTestCase().getExpectedResult();
        if (ExpectedResult.UNSPECIFIED.equals(expectedResult)) {
            testInstance.setTestStatus(TestStatus.UNKNOWN);
        } else {
            try {
                boolean soapFault = response.getSOAPBody().hasFault();
                if ((soapFault && ExpectedResult.SOAP_FAULT.equals(expectedResult))
                        || (!soapFault && ExpectedResult.NO_SOAP_FAULT.equals(expectedResult))) {
                    testInstance.setTestStatus(TestStatus.PASSED);
                } else if (ExpectedResult.SOAP_FAULT.equals(expectedResult)) {
                    testInstance.setTestStatus(TestStatus.FAILED);
                    testInstance.setReasonForFailure("The system under test should have sent back a SOAP Fault");
                } else if (ExpectedResult.NO_SOAP_FAULT.equals(expectedResult)) {
                    testInstance.setTestStatus(TestStatus.FAILED);
                    testInstance.setReasonForFailure("The system under test shall not send back a SOAP Fault");
                }
            } catch (SOAPException e) {
                testInstance.setTestStatus(TestStatus.UNKNOWN);
                testInstance.setReasonForFailure("The response does not contain a SOAP body");
            }
        }
    }

    private SOAPMessage createSoapMessage() throws SOAPException {
        MessageFactory mf = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
        return mf.createMessage();
    }

    private Document createSOAPBodyContent() throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            factory.setNamespaceAware(true);
            builder = factory.newDocumentBuilder();
            File soapBodyFile = new File(xuaFileDirectory, testInstance.getAncillaryTransaction().getPathToSoapBody());
            if (soapBodyFile.exists()) {
                return builder.parse(soapBodyFile);
            } else {
                throw new FileNotFoundException(soapBodyFile.getAbsolutePath() + " does not exist");
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            LOG.error("An error occurred during SOAP body creation");
            throw e;
        }
    }

    private byte[] getBytesFromSOAPMessage(SOAPMessage message) {
        if (message != null) {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            try {
                message.writeTo(out);
            } catch (SOAPException | IOException e) {
                LOG.error("cannot read message");
                return null;
            }
            return out.toByteArray();
        } else {
            return null;
        }
    }
}
