package net.ihe.gazelle.xua.dao;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.xua.model.ServiceProviderTestInstance;
import net.ihe.gazelle.xua.model.ServiceProviderTestInstanceQuery;
import org.jboss.seam.security.Identity;

import javax.persistence.EntityManager;

/**
 * Created by aberge on 02/06/17.
 */
public class ServiceProviderTestInstanceDAO {

    public static ServiceProviderTestInstance saveTestInstance(ServiceProviderTestInstance testInstance){
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        ServiceProviderTestInstance savedTestInstance = entityManager.merge(testInstance);
        entityManager.flush();
        return savedTestInstance;
    }

    public static ServiceProviderTestInstance getTestInstanceById(Integer id) {
        ServiceProviderTestInstanceQuery query = new ServiceProviderTestInstanceQuery();
        query.id().eq(id);
        Identity identity = Identity.instance();
        // restrict access to admin, monitor and creator of the test instance
        if (identity.isLoggedIn() && !(identity.hasRole("admin_role") || (identity.hasRole("monitor_role")))){
            query.username().eq(identity.getCredentials().getUsername());
        }
        return query.getUniqueResult();
    }
}
