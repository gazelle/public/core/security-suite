package net.ihe.gazelle.xua.builder;

import net.ihe.gazelle.security.builders.AssertionBuilder;
import net.ihe.gazelle.security.builders.TimestampBuilder;
import net.ihe.gazelle.sequoia.builders.SequoiaSignatureBuilder;
import net.ihe.gazelle.simulator.sts.client.WSSEConstants;
import net.ihe.gazelle.wstrust.utility.TimestampType;
import net.ihe.gazelle.xua.dao.SOAPHeaderPartDAO;
import net.ihe.gazelle.xua.model.Exception;
import net.ihe.gazelle.xua.model.SOAPHeaderElement;
import net.ihe.gazelle.xua.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.namespace.QName;
import javax.xml.soap.*;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

/**
 * Created by aberge on 31/05/17.
 */
public class SOAPHeaderBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(SOAPHeaderBuilder.class);
    private static final String SOAP_NS = "http://www.w3.org/2003/05/soap-envelope";
    private static final String SOAP_PREFIX = "env";
    private static final String MUST_UNDERSTAND_LOCAL_NAME = SOAP_PREFIX + ":mustUnderstand";
    private static final QName SECURITY_NAME = new QName(WSSEConstants.WSSE_NS, WSSEConstants.WSSE_LOCAL);
    public static final String PATTERN = "$$";
    public static final String ACTION_PATTERN = "$$ACTION$$";
    public static final String ENDPOINT_PATTERN = "$$ENDPOINT$$";
    public static final String ANONYMOUS_PATTERN = "$$ANONYMOUS$$";
    public static final String RANDOM_PATTERN = "$$RANDOM$$";
    public static final String ANONYMOUS_URL = "http://www.w3.org/2005/08/addressing/anonymous";
    public static final String TIMESTAMP_LOCAL = "Timestamp";
    public static final String ADDRESSING_NS = "http://www.w3.org/2005/08/addressing";
    public static final String ADDRESS_LOCAL = "Address";

    SOAPFactory factory = null;
    Signature signature = null;
    Timestamp timestamp = null;
    String timestampId = null;

    private ServiceProviderTestInstance testInstance;

    public SOAPHeaderBuilder(ServiceProviderTestInstance inTestInstance) {
        this.testInstance = inTestInstance;
    }

    public boolean buildSoapHeader(SOAPMessage soapMessage) throws java.lang.Exception {
        try {
            SOAPHeader soapHeader = soapMessage.getSOAPHeader();
            soapMessage.getSOAPPart().getEnvelope().setPrefix(SOAP_PREFIX);
            factory = SOAPFactory.newInstance();
            List<SOAPHeaderPart> headerParts = SOAPHeaderPartDAO.getPartsforTestCase(testInstance.getTestCase());
            if (headerParts != null) {
                for (SOAPHeaderPart part : headerParts) {
                    if (part instanceof Signature) {
                        // signature might reference other pieces of the soapHeader: keep it for processing at the very latest
                        signature = (Signature) part;
                    } else if (part instanceof Assertion) {
                        addAssertion(soapHeader, (Assertion) part);
                    } else if (part instanceof Timestamp) {
                        timestamp = (Timestamp) part;
                        addTimestamp(soapHeader, timestamp);
                    } else if (part instanceof SOAPHeaderElement) {
                        addSoapHeaderElement(soapHeader, (SOAPHeaderElement) part);
                    }
                }
                if (signature != null && TIMESTAMP_LOCAL.equals(signature.getSignedElementLocalName())) {
                    sign(soapMessage, soapHeader, signature, timestampId);
                }
                // if we want to modify the timestamp, we do so after it has been signed
                modifyTimestamp(soapHeader, timestamp);
            }
            return true;
        } catch (java.lang.Exception e) {
            LOG.error("An error occurred during SOAP header creation");
            throw e;
        }
    }

    private void addAssertion(SOAPHeader soapHeader, Assertion assertion) throws java.lang.Exception {
        // assertion is to be placed in wsse:Security, create it if it does not exists
        Element assertionNode = AssertionBuilder.getAssertionFromSTS(assertion.getUsername(), assertion.getPassword(), testInstance.getTestedEndpoint(), assertion.getStsEndpoint());
        SOAPElement assertionElement = factory.createElement(assertionNode);
        addToSoapSecurity(soapHeader, factory, assertionElement);
    }

    private void sign(SOAPMessage soapMessage, SOAPHeader soapHeader, Signature signature, String timestampId) throws java.lang.Exception {
        SequoiaSignatureBuilder signatureBuilder = new SequoiaSignatureBuilder(signature.getKeystore(), signature.getAlias(),
                signature.getKeystorePassword(), signature.getPrivateKeyPassword());
        QName idName = new QName(WSSEConstants.WSU_NS, WSSEConstants.ID);
        QName timestampName = new QName(WSSEConstants.WSU_NS, signature.getSignedElementLocalName());
        signatureBuilder.signElement(soapMessage.getSOAPPart(), timestampId, timestampName, idName);
        Node signatureElement = getTimestampSignature(soapHeader);
        if (signature.getExceptions() != null) {
            SoapMessageModifier modifier = new SoapMessageModifier((Element) signatureElement);
            for (Exception exception : signature.getExceptions()) {
                modifier.modify(exception);
            }
        }
    }

    private void modifyTimestamp(SOAPHeader soapHeader, Timestamp timestamp){
        if (timestamp != null && timestamp.getExceptions() != null) {
            Node timestampFromHeader = getTimestamp(soapHeader);
            SoapMessageModifier modifier = new SoapMessageModifier((Element) timestampFromHeader);
            for (Exception exception : timestamp.getExceptions()) {
                modifier.modify(exception);
            }
        }
    }

    private void addTimestamp(SOAPHeader soapHeader, Timestamp timestamp) throws SOAPException {
        TimestampType timestampElement = TimestampBuilder.buildWsuTimestamp(timestamp.getCreatedOffset(), timestamp.getDuration());
        timestampId = timestampElement.getId();
        SOAPElement soapTimestamp = factory.createElement((Element) timestampElement.get_xmlNodePresentation());
        if (soapTimestamp != null) {
            // timestamp is to be placed in the wsse:Security element, create it if missing
            addToSoapSecurity(soapHeader, factory, soapTimestamp);
        }
    }

    private void addSoapHeaderElement(SOAPHeader soapHeader, SOAPHeaderElement elementToAdd) throws SOAPException {
        SOAPElement soapElement = factory.createElement(new QName(elementToAdd.getNamespaceUri(), elementToAdd.getLocalName()));
        if (elementToAdd.isMustUnderstand()) {
            soapElement.setAttributeNS(SOAP_NS, MUST_UNDERSTAND_LOCAL_NAME, "true");
        }
        String nodeValue = elementToAdd.getValue();
        if (nodeValue != null && nodeValue.contains(ACTION_PATTERN)) {
            nodeValue = nodeValue.replace(ACTION_PATTERN, testInstance.getAncillaryTransaction().getSoapAction());
        } else if (nodeValue != null && nodeValue.contains(ENDPOINT_PATTERN)) {
            nodeValue = nodeValue.replace(ENDPOINT_PATTERN, testInstance.getTestedEndpoint());
        } else if (nodeValue != null && nodeValue.contains(RANDOM_PATTERN)) {
            nodeValue = nodeValue.replace(RANDOM_PATTERN, UUID.randomUUID().toString());
        } else if (nodeValue != null && nodeValue.contains(ANONYMOUS_PATTERN)) {
            // replyTo
            SOAPElement address = factory.createElement(new QName(ADDRESSING_NS, ADDRESS_LOCAL));
            address.addTextNode(ANONYMOUS_URL);
            soapElement.addChildElement(address);
            nodeValue = null; // set back nodeValue to null so that no value is appended as text node for this soap element
        } else if (nodeValue != null && nodeValue.startsWith(PATTERN) && nodeValue.endsWith(PATTERN)) {
            String variableName = nodeValue.replace(PATTERN, "");
            nodeValue = MessageVariable.getValueForVariable(variableName, testInstance.getAncillaryTransaction().getVariables());
        }
        if (nodeValue != null) {
            soapElement.addTextNode(nodeValue);
        }
        Node importedSoapHeaderElement = soapHeader.getOwnerDocument().importNode(soapElement, true);
        soapHeader.appendChild(importedSoapHeaderElement);
    }

    private void addToSoapSecurity(SOAPHeader soapHeader, SOAPFactory factory, SOAPElement securityChild) throws SOAPException {
        SOAPElement securityNode = getWsseSecurityElement(soapHeader);
        if (securityNode != null) {
            securityNode.addChildElement(securityChild);
        } else {
            securityNode = factory.createElement(SECURITY_NAME);
            securityNode.addChildElement(securityChild);
            Node importedSecurityNode = soapHeader.getOwnerDocument().importNode(securityNode, true);
            soapHeader.appendChild(importedSecurityNode);
        }
    }

    private SOAPElement getWsseSecurityElement(SOAPHeader soapHeader) {
        Iterator<SOAPElement> elements = soapHeader.getChildElements(SECURITY_NAME);
        if (elements != null && elements.hasNext()) {
            return elements.next();
        } else {
            return null;
        }
    }

    private SOAPElement getTimestampSignature(SOAPHeader soapHeader) {
        SOAPElement securityElement = getWsseSecurityElement(soapHeader);
        if (securityElement != null) {
            Iterator<SOAPElement> elements = securityElement.getChildElements(new QName(WSSEConstants.XMLDSIGN_NS, WSSEConstants.SIGNATURE_LOCAL));
            if (elements != null && elements.hasNext()) {
                return elements.next();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    private SOAPElement getTimestamp(SOAPHeader soapHeader) {
        SOAPElement securityElement = getWsseSecurityElement(soapHeader);
        if (securityElement != null) {
            Iterator<SOAPElement> elements = securityElement.getChildElements(new QName(WSSEConstants.WSU_NS, TIMESTAMP_LOCAL));
            if (elements != null && elements.hasNext()) {
                return elements.next();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
