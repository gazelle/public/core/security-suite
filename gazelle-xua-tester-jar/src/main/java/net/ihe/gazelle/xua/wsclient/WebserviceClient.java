package net.ihe.gazelle.xua.wsclient;

import net.ihe.gazelle.util.Pair;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

/**
 * Created by aberge on 24/05/17.
 */
public class WebserviceClient {

    private String sutUrl;

    public WebserviceClient(String sutUrl) {
        this.sutUrl = sutUrl;
    }

    public Pair<SOAPMessage, String> sendMessage(SOAPMessage soapRequest, String soapAction) throws IOException, SOAPException {
        URL endpoint = new URL(sutUrl);
        StringBuilder contentTypeHeader = new StringBuilder("application/soap+xml;charset=UTF-8;action=\"");
        contentTypeHeader.append(soapAction);
        contentTypeHeader.append('\"');
        URLConnection con = endpoint.openConnection();
        con.setRequestProperty("Content-Type", contentTypeHeader.toString());
        con.setDoOutput(true);
        if (con instanceof HttpURLConnection) {
            ((HttpURLConnection) con).setRequestMethod("POST");
        }
        OutputStream os = con.getOutputStream();
        soapRequest.writeTo(os);
        con.connect();
        StringBuilder responseHeadersDump = new StringBuilder();
        for (Map.Entry<String, List<String>> entries : con.getHeaderFields().entrySet()) {
            StringBuilder values = new StringBuilder();
            for (String value : entries.getValue()) {
                if (values.length() != 0) {
                    values.append(", ");
                }
                values.append(value);
            }
            if (entries.getKey() != null) {
                responseHeadersDump.append(entries.getKey()).append(": ");
            }
            responseHeadersDump.append(values).append("\n");
        }
        InputStream inputStream = null;
        try {
            inputStream = con.getInputStream();
        } catch (IOException e) {
            if (con instanceof HttpURLConnection) {
                inputStream = ((HttpURLConnection) con).getErrorStream();
            }
        }
        SOAPMessage response = MessageFactory.newInstance().createMessage(null, inputStream);

        return new Pair<>(response, responseHeadersDump.toString());
    }
}
