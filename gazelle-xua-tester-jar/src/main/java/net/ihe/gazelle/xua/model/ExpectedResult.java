package net.ihe.gazelle.xua.model;

/**
 * <p>ExpectedResult enum.</p>
 *
 * @author aberge
 * @version 1.0: 24/10/17
 */
public enum ExpectedResult {

    SOAP_FAULT ("Soap Fault message shall be returned by the tested system"),
    NO_SOAP_FAULT("No Soap Fault shall be thrown by the tested system"),
    UNSPECIFIED("Unspecified");

    private String label;

    ExpectedResult(String inLabel){
        this.label = inLabel;
    }

    public String getLabel() {
        return label;
    }
}
