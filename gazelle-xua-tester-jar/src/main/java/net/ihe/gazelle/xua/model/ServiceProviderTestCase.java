package net.ihe.gazelle.xua.model;

import net.ihe.gazelle.hql.FilterLabel;
import org.hibernate.annotations.Type;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by aberge on 23/05/17.
 */
@Entity
@Name("serviceProviderTestCase")
@Table(name = "xua_service_provider_test_case", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "keyword"))
@SequenceGenerator(name = "xua_service_provider_test_case_sequence", sequenceName = "xua_service_provider_test_case_id_seq", allocationSize = 1)
public class ServiceProviderTestCase implements Serializable {

    @Id
    @GeneratedValue(generator = "xua_service_provider_test_case_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "keyword")
    private String keyword;

    @Lob
    @Type(type = "text")
    @Column(name = "description")
    private String description;

    @Column(name = "expected_result_details")
    private String expectedResultDetails;

    @Column(name = "last_modifier")
    private String lastModifier;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_changed")
    private Date lastChanged;

    @Column(name = "available")
    private boolean available;

    @Column(name = "expected_result")
    @Enumerated(EnumType.STRING)
    private ExpectedResult expectedResult;

    @OneToMany(targetEntity = SOAPHeaderPart.class, mappedBy = "testCase")
    private List<SOAPHeaderPart> soapHeaderParts;

    public ServiceProviderTestCase(){
        this.available = false;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @FilterLabel
    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ExpectedResult getExpectedResult() {
        return expectedResult;
    }

    public void setExpectedResult(ExpectedResult expectedResult) {
        this.expectedResult = expectedResult;
    }

    public String getLastModifier() {
        return lastModifier;
    }

    public void setLastModifier(String lastModifier) {
        this.lastModifier = lastModifier;
    }

    public Date getLastChanged() {
        return lastChanged;
    }

    public void setLastChanged(Date lastChanged) {
        this.lastChanged = lastChanged;
    }

    public List<SOAPHeaderPart> getSoapHeaderParts() {
        return soapHeaderParts;
    }

    public void setSoapHeaderParts(List<SOAPHeaderPart> soapHeaderParts) {
        this.soapHeaderParts = soapHeaderParts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ServiceProviderTestCase)) {
            return false;
        }

        ServiceProviderTestCase that = (ServiceProviderTestCase) o;

        if (keyword != null ? !keyword.equals(that.keyword) : that.keyword != null) {
            return false;
        }
        if (description != null ? !description.equals(that.description) : that.description != null) {
            return false;
        }
        if (expectedResult != null ? !expectedResult.equals(that.expectedResult) : that.expectedResult != null) {
            return false;
        }
        if (lastModifier != null ? !lastModifier.equals(that.lastModifier) : that.lastModifier != null) {
            return false;
        }
        return lastChanged != null ? lastChanged.equals(that.lastChanged) : that.lastChanged == null;
    }

    @Override
    public int hashCode() {
        int result = keyword != null ? keyword.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (expectedResult != null ? expectedResult.hashCode() : 0);
        result = 31 * result + (lastModifier != null ? lastModifier.hashCode() : 0);
        result = 31 * result + (lastChanged != null ? lastChanged.hashCode() : 0);
        return result;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public String getExpectedResultDetails() {
        return expectedResultDetails;
    }

    public void setExpectedResultDetails(String expectedResultDetails) {
        this.expectedResultDetails = expectedResultDetails;
    }
}
