package net.ihe.gazelle.xua.model;

import org.hibernate.annotations.Type;
import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by aberge on 23/05/17.
 */
@Entity
@Name("exception")
@Table(name = "xua_exception", schema = "public")
@SequenceGenerator(name = "xua_exception_sequence", sequenceName = "xua_exception_id_seq", allocationSize = 1)
public class Exception implements Serializable {

    @Id
    @GeneratedValue(generator = "xua_exception_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @ManyToOne(targetEntity = SOAPHeaderPart.class)
    @JoinColumn(name = "part_id")
    private SOAPHeaderPart part;

    @Column(name = "xpath")
    @Lob
    @Type(type = "text")
    private String xpath;

    @Column(name = "exception_type")
    private ExceptionType exceptionType;

    @Column(name = "new_value")
    private String newValue;

    public Exception(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SOAPHeaderPart getPart() {
        return part;
    }

    public void setPart(SOAPHeaderPart part) {
        this.part = part;
    }

    public String getXpath() {
        return xpath;
    }

    public void setXpath(String xpath) {
        this.xpath = xpath;
    }

    public ExceptionType getExceptionType() {
        return exceptionType;
    }

    public void setExceptionType(ExceptionType exceptionType) {
        this.exceptionType = exceptionType;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Exception)) {
            return false;
        }

        Exception exception = (Exception) o;

        if (xpath != null ? !xpath.equals(exception.xpath) : exception.xpath != null) {
            return false;
        }
        return newValue != null ? newValue.equals(exception.newValue) : exception.newValue == null;
    }

    @Override
    public int hashCode() {
        int result = xpath != null ? xpath.hashCode() : 0;
        result = 31 * result + (newValue != null ? newValue.hashCode() : 0);
        return result;
    }
}
