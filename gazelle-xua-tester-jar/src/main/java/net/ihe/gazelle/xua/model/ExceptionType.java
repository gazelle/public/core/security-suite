package net.ihe.gazelle.xua.model;

/**
 * Created by aberge on 23/05/17.
 */
public enum ExceptionType {

    REMOVE,
    REPLACE
}
