package net.ihe.gazelle.xua.dao;

import net.ihe.gazelle.xua.model.SOAPHeaderPart;
import net.ihe.gazelle.xua.model.SOAPHeaderPartQuery;
import net.ihe.gazelle.xua.model.ServiceProviderTestCase;

import java.util.List;

/**
 * Created by aberge on 02/06/17.
 */
public class SOAPHeaderPartDAO {


    public static List<SOAPHeaderPart> getPartsforTestCase(ServiceProviderTestCase testCase) {
        SOAPHeaderPartQuery query = new SOAPHeaderPartQuery();
        query.testCase().id().eq(testCase.getId());
        return query.getList();
    }
}
