package net.ihe.gazelle.xua.model;

import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.List;

/**
 * Created by aberge on 23/05/17.
 */
@Entity
@Name("messageVariable")
@Table(name = "xua_message_variable", schema = "public")
@SequenceGenerator(name = "xua_message_variable_sequence", sequenceName = "xua_message_variable_id_seq", allocationSize = 1)
public class MessageVariable implements Serializable{

    @Id
    @GeneratedValue(generator = "xua_message_variable_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "token")
    private String token;

    @Column(name = "default_value")
    private String defaultValue;

    @Transient
    private String value;

    @ManyToOne(targetEntity = AncillaryTransaction.class)
    @JoinColumn(name = "transaction_id")
    private AncillaryTransaction transaction;

    public MessageVariable(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public AncillaryTransaction getTransaction() {
        return transaction;
    }

    public void setTransaction(AncillaryTransaction transaction) {
        this.transaction = transaction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MessageVariable)) {
            return false;
        }

        MessageVariable that = (MessageVariable) o;

        if (token != null ? !token.equals(that.token) : that.token != null) {
            return false;
        }
        return defaultValue != null ? defaultValue.equals(that.defaultValue) : that.defaultValue == null;
    }

    @Override
    public int hashCode() {
        int result = token != null ? token.hashCode() : 0;
        result = 31 * result + (defaultValue != null ? defaultValue.hashCode() : 0);
        return result;
    }

    public static String getValueForVariable(String variableName, List<MessageVariable> variables) {
        if (variableName == null || variables == null){
            return null;
        } else {
            for (MessageVariable variable: variables){
                if (variable.getToken().equals(variableName)){
                    return variable.getValue() != null ? variable.getValue() : variable.getDefaultValue();
                } else {
                    continue;
                }
            }
            return null;
        }
    }
}
