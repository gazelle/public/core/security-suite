package net.ihe.gazelle.xua.model;

import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by aberge on 23/05/17.
 */

@Entity
@Name("signature")
@DiscriminatorValue("signature")
public class Signature extends SOAPHeaderPart {


    @Column(name = "signed_element_namespace_uri")
    private String signedElementNamespaceUri;

    @Column(name = "signed_element_local_name")
    private String signedElementLocalName;

    /**
     * absolute path to keystore
     */
    @Column(name = "keystore")
    private String keystore;

    /**
     * alias to access the private key in the keystore
     */
    @Column(name = "alias")
    private String alias;

    /**
     * password to access the keystore and its private key
     */
    @Column(name = "keystore_password")
    private String keystorePassword;

    @Column(name = "private_key_password")
    private String privateKeyPassword;

    public Signature(){

    }

    @Override
    public String getType() {
        return "Signature";
    }

    public String getSignedElementNamespaceUri() {
        return signedElementNamespaceUri;
    }

    public void setSignedElementNamespaceUri(String signedElementNamespaceUri) {
        this.signedElementNamespaceUri = signedElementNamespaceUri;
    }


    public String getKeystorePassword() {
        return keystorePassword;
    }

    public void setKeystorePassword(String keystorePassword) {
        this.keystorePassword = keystorePassword;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getKeystore() {
        return keystore;
    }

    public void setKeystore(String keystore) {
        this.keystore = keystore;
    }

    public String getPrivateKeyPassword() {
        return privateKeyPassword;
    }

    public void setPrivateKeyPassword(String privateKeyPassword) {
        this.privateKeyPassword = privateKeyPassword;
    }

    public String getSignedElementLocalName() {
        return signedElementLocalName;
    }

    public void setSignedElementLocalName(String signedElementLocalName) {
        this.signedElementLocalName = signedElementLocalName;
    }
}
