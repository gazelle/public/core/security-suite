package net.ihe.gazelle.xua.builder;

import net.ihe.gazelle.security.builders.TimestampBuilder;
import net.ihe.gazelle.xua.model.Exception;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.util.Date;

/**
 * Created by aberge on 05/06/17.
 */
public class SoapMessageModifier {

    private static final Logger LOG = LoggerFactory.getLogger(SoapMessageModifier.class);
    public static final String NOW_PATTERN = "$$NOW$$";

    private Element soapElement;

    public SoapMessageModifier(Element message) {
        this.soapElement = message;
    }

    public void modify(Exception exception) {
        XPath xPath = XPathFactory.newInstance().newXPath();
        NodeList nodes;
        try {
            nodes = (NodeList) xPath.evaluate(exception.getXpath(),
                    soapElement, XPathConstants.NODESET);
            LOG.info("Found " + nodes.getLength() + " elements");
            for (int index = 0; index < nodes.getLength(); ++index) {
                Node currentItem = nodes.item(index);
                if (currentItem instanceof Element) {
                    applyException((Element) currentItem, exception);
                } else if (currentItem instanceof Attr) {
                    applyException((Attr) currentItem, exception);
                }
            }
        } catch (XPathExpressionException e) {
            LOG.error(e.getMessage());
        }
    }

    private void applyException(Element node, Exception exception) {
        switch (exception.getExceptionType()) {
            case REMOVE:
                Node parent = node.getParentNode();
                parent.removeChild(node);
                break;
            case REPLACE:
                node.setTextContent(getNewValue(exception.getNewValue()));
                break;
        }
    }

    private void applyException(Attr attribute, Exception exception) {
        switch (exception.getExceptionType()) {
            case REMOVE:
                Element parent = attribute.getOwnerElement();
                parent.removeAttributeNode(attribute);
                break;
            case REPLACE:
                attribute.setValue(getNewValue(exception.getNewValue()));
                break;
        }
    }

    private String getNewValue(String value) {
        if (NOW_PATTERN.equals(value)) {
            Date now = new Date();
            return TimestampBuilder.buildDate(now.getTime());
        } else {
            return value;
        }
    }
}
