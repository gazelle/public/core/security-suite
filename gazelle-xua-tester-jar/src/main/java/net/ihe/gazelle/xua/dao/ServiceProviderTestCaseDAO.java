package net.ihe.gazelle.xua.dao;

import net.ihe.gazelle.xua.model.ServiceProviderTestCase;
import net.ihe.gazelle.xua.model.ServiceProviderTestCaseQuery;

/**
 * Created by aberge on 02/06/17.
 */
public class ServiceProviderTestCaseDAO {

    public static ServiceProviderTestCase getTestCaseById(Integer id){
        if (id == null){
            return null;
        } else {
            ServiceProviderTestCaseQuery query = new ServiceProviderTestCaseQuery();
            query.id().eq(id);
            return query.getUniqueResult();
        }
    }
}
