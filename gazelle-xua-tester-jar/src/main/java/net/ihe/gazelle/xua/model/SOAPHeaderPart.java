package net.ihe.gazelle.xua.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

/**
 * Created by aberge on 23/05/17.
 */
@Entity
@Inheritance(strategy= InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("part")
@Table(name = "xua_soap_header_part", schema = "public")
@SequenceGenerator(name = "xua_soap_header_part_sequence", sequenceName = "xua_soap_header_part_id_seq", allocationSize = 1)
public abstract class SOAPHeaderPart implements Serializable{


    @Id
    @GeneratedValue(generator = "xua_soap_header_part_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @ManyToOne(targetEntity = ServiceProviderTestCase.class)
    @JoinColumn(name = "test_case_id")
    private ServiceProviderTestCase testCase;

    @OneToMany(targetEntity = Exception.class, mappedBy = "part")
    private List<Exception> exceptions;

    public SOAPHeaderPart(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ServiceProviderTestCase getTestCase() {
        return testCase;
    }

    public void setTestCase(ServiceProviderTestCase testCase) {
        this.testCase = testCase;
    }

    public List<Exception> getExceptions() {
        return exceptions;
    }

    public void setExceptions(List<Exception> exceptions) {
        this.exceptions = exceptions;
    }

    public abstract String getType();
}
