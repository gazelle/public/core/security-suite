package net.ihe.gazelle.xua.ws;

import net.ihe.gazelle.validation.model.ValidatorDescription;

public enum Validators implements ValidatorDescription {

    IHE_XUA_BASIC("IHE - ITI - XUA", "IHE"),
    IHE_XUA_SUBJECT_ROLE("IHE - ITI - XUA - Subject-Role Option", "IHE"),
    IHE_XUA_Authz_CONSENT("IHE - ITI - XUA - Authz-Consent Option", "IHE"),
    IHE_XUA_PURPOSE_OF_USE("IHE - ITI - XUA - PurposeOfUse Option", "IHE"),
    KSA_XUA("KSA - XUA", "KSA"),
    SER_ITI79_REQUEST("IHE - ITI - SeR - ITI-79 - Authorization Decisions Query Request", "IHE"),
    SER_ITI79_RESPONSE("IHE - ITI - SeR - ITI-79 - Authorization Decisions Query Response", "IHE"),
    CH_XUA("CH - IHE - ITI - XUA", "CH"),
    EHDSI_XUA_HCP("eHDSI SAML - HCP Identity Assertion - Wave 8 (V8.0.1)", "EHDSI"),
    EHDSI_XUA_NOK("eHDSI SAML - NoK Identity Assertion - Wave 8 (V8.0.1)", "EHDSI"),
    EHDSI_XUA_TRC("eHDSI SAML - TRC Identity Assertion - Wave 8 (V8.0.1)", "EHDSI"),
    IE_XUA("IE - IHE - ITI - XUA", "IE");

    Validators(String val, String inDiscrimator) {
        this.value = val;
        this.discrimator = inDiscrimator;
    }

    String value;
    String discrimator;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static Validators getValidatorFromValue(String value) {
        for (Validators val : Validators.values()) {
            if (val.value.equals(value)) return val;
        }
        return null;
    }

    @Override
    public String getOid() {
        return null;
    }

    @Override
    public String getName() {
        return value;
    }

    @Override
    public String getDescriminator() {
        return this.discrimator;
    }

    @Override
    public String getRootElement() {
        return null;
    }

    @Override
    public String getNamespaceURI() {
        return null;
    }

    @Override
    public boolean extractPartToValidate() {
        return false;
    }
}
