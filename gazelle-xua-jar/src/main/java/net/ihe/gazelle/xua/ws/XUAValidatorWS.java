package net.ihe.gazelle.xua.ws;

import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.exception.GazelleValidationException;
import net.ihe.gazelle.validation.model.ValidatorDescription;
import net.ihe.gazelle.validation.ws.AbstractModelBasedValidation;
import net.ihe.gazelle.xua.util.DetailedResultTransformer;
import org.jboss.seam.annotations.Name;

import javax.ejb.Stateless;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Stateless
@Name("ModelBasedValidationWS")
@WebService(name = "ModelBasedValidationWS",
      serviceName = "ModelBasedValidationWSService",
      portName = "ModelBasedValidationWSPort",
      targetNamespace = "http://ws.mb.validator.gazelle.ihe.net")
public class XUAValidatorWS extends AbstractModelBasedValidation implements Serializable {

   /**
    *
    */
   private static final long serialVersionUID = 1L;

   @Override
   @WebResult(name = "about")
   public String about() {
      String res = "This webservice is developped by IHE-europe / gazelle team. The aim of this validator is to validate CDA documents using model " +
            "based validation.\n";
      res = res + "For more information please contact the manager of gazelle project eric.poiseau@inria.fr";
      return res;
   }

   private String getDetailedResultAsString(DetailedResult dr) {
      if (dr != null) {
         ByteArrayOutputStream baos = new ByteArrayOutputStream();
         try {
            DetailedResultTransformer.save(baos, dr);
         } catch (JAXBException e) {
            e.printStackTrace();
         }
         String res = baos.toString();
         res = deleteUnicodeZero(res);
         return res;
      }
      return null;
   }

   private String deleteUnicodeZero(String s) {
      return s.replaceAll("[\\u0000]", "");
   }


   @Override
   protected String buildReportOnParsingFailure(GazelleValidationException e, ValidatorDescription selectedValidator) {
      return null;
   }


   @Override
   protected String executeValidation(String document, ValidatorDescription validator, boolean extract) throws GazelleValidationException {
      DetailedResult dr = null;
      Validators xuaValidator = (Validators) validator;
      switch (xuaValidator) {
         case IHE_XUA_BASIC:
            dr = XUADocumentValidation.validateIHE_BASIC_XUA(document);
            break;
         case IHE_XUA_SUBJECT_ROLE:
            dr = XUADocumentValidation.validateIHE_XUA_SUBJECT_ROLE(document);
            break;
         case IHE_XUA_Authz_CONSENT:
            dr = XUADocumentValidation.validateIHE_XUA_Authz_CONSENT(document);
            break;
         case IHE_XUA_PURPOSE_OF_USE:
            dr = XUADocumentValidation.validateIHE_XUA_PURPOSE_OF_USE(document);
            break;
         case KSA_XUA:
            dr = XUADocumentValidation.validateKSA_XUA(document);
            break;
         case SER_ITI79_REQUEST:
            dr = XUADocumentValidation.validateSER_ITI79_REQUEST(document);
            break;
         case SER_ITI79_RESPONSE:
            dr = XUADocumentValidation.validateSER_ITI79_RESPONSE(document);
            break;
         case CH_XUA:
            dr = XUADocumentValidation.validateCH_XUA(document);
            break;
         case EHDSI_XUA_HCP:
            dr = XUADocumentValidation.validateEHDSI_XUA_HCP(document);
            break;
         case EHDSI_XUA_NOK:
            dr = XUADocumentValidation.validateEHDSI_XUA_NOK(document);
            break;
         case EHDSI_XUA_TRC:
            dr = XUADocumentValidation.validateEHDSI_XUA_CONFIRMATION(document);
            break;
         case IE_XUA:
            dr = XUADocumentValidation.validateIE_XUA(document);
            break;
         default:
            // should never occur
            throw new GazelleValidationException("No validator defined");
      }
      String res = this.getDetailedResultAsString(dr);
      if (res.contains("?>")) {
         res = res.substring(res.indexOf("?>") + 2);
      }
      return res;
   }

   @Override
   protected ValidatorDescription getValidatorByOidOrName(String value) {
      return Validators.getValidatorFromValue(value);
   }

   @Override
   protected List<ValidatorDescription> getValidatorsForDescriminator(String descriminator) {
      List<ValidatorDescription> matchingValidators = new ArrayList<ValidatorDescription>();
      for (Validators current : Validators.values()) {
         if (descriminator == null || descriminator.isEmpty() || current.getDescriminator().equalsIgnoreCase(descriminator)) {
            matchingValidators.add(current);
         }
      }
      return matchingValidators;
   }

}


