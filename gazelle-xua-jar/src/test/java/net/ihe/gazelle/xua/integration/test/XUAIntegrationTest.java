package net.ihe.gazelle.xua.integration.test;

import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;

import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;

import net.ihe.gazelle.gen.common.CommonOperations;
import net.ihe.gazelle.gen.common.SVSConsumer;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.xua.util.DetailedResultTransformer;
import net.ihe.gazelle.xua.ws.Validators;
import net.ihe.gazelle.xua.ws.XUADocumentValidation;

import org.junit.Assert;
import org.junit.Test;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@PowerMockIgnore({"javax.*", "com.sun.xml.*", "com.sun.org.*",
        "net.sf.saxon.*", "org.xml.sax.*", "net.ihe.gazelle.hql.providers.*"})
@PrepareForTest({PreferenceService.class})
@RunWith(PowerMockRunner.class)
public class XUAIntegrationTest {

//    static {
//        try {
//            CommonOperations.setValueSetProvider(new SVSConsumer() {
//                @Override
//                protected String getSVSRepositoryUrl() {
//                    return "https://gazelle.ihe.net/SVSSimulator/rest/RetrieveValueSetForSimulator";
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    private static final String pathDoc = "src/test/resources/samples";

    @Before
    public void mockPreferences() {
        PowerMockito.mockStatic(PreferenceService.class);
        Mockito.when(PreferenceService.getString("xua_xsd")).thenReturn("/opt/tls/saml-schema-assertion-2.0.xsd");
        Mockito.when(PreferenceService.getString("ser_resp_xsd")).thenReturn("/opt/tls/xacml-os2/XACML-2.0-OS-NORMATIVE/access_control-xacml-2.0-saml-assertion-schema-os.xsd");
        Mockito.when(PreferenceService.getString("ser_req_xsd")).thenReturn("/opt/tls/xacml-os2/XACML-2.0-OS-NORMATIVE/access_control-xacml-2.0-saml-protocol-schema-os.xsd");
        Mockito.when(PreferenceService.getString("svs_simulator_restful_url")).thenReturn("https://gazelle.ihe.net/SVSSimulator/rest/RetrieveValueSetForSimulator");
    }

    @Test
    public void ValidateXUA_IHE() throws SOAPException {
        testValidatorType(Validators.IHE_XUA_BASIC);
    }

    @Test
    public void ValidateXUA_IHE_AuthzConsent() throws SOAPException {
        testValidatorType(Validators.IHE_XUA_Authz_CONSENT);
    }

    @Test
    public void ValidateXUA_IHE_PurposeOfUse() throws SOAPException {
        testValidatorType(Validators.IHE_XUA_PURPOSE_OF_USE);
    }

    @Test
    public void ValidateXUA_IHE_SubjectRole() throws SOAPException {
        testValidatorType(Validators.IHE_XUA_SUBJECT_ROLE);
    }

    private void testValidatorType(Validators val) throws SOAPException {
        File file = new File(pathDoc);
        if (file.isDirectory()) {
            File dir = null;
            for (File fis : file.listFiles()) {
                if (fis.getName().equals(val.toString())) {
                    dir = fis;
                }
            }
            if (dir == null || !dir.isDirectory()) {
                fail("The standard was not found : " + val.toString());
                return;
            }
            for (File fis : dir.listFiles()) {
                if (fis.getName().equals("1.xml")) {
                    String valRes = validateDocument(readDoc(fis.getAbsolutePath()), val.getValue());
                    Assert.assertTrue(!valRes.contains("<Error"));
                } else if (fis.getName().equals("2.xml")) {
                    String valRes = validateDocument(readDoc(fis.getAbsolutePath()), val.getValue());
                    Assert.assertTrue(valRes.contains("<Error"));
                }
            }
        } else {
            fail("The standards specified has no tests !");
        }
    }

    public String validateDocument(String document, String validator) throws SOAPException {
        Validators val = Validators.getValidatorFromValue(validator);
        if (val == null) {
            throw new SOAPException("Invalide Validator parameter");
        }
        DetailedResult dr = null;
        switch (val) {
            case IHE_XUA_BASIC:
                dr = XUADocumentValidation.validateIHE_BASIC_XUA(document);
                break;
            case IHE_XUA_SUBJECT_ROLE:
                dr = XUADocumentValidation.validateIHE_XUA_SUBJECT_ROLE(document);
                break;
            case IHE_XUA_Authz_CONSENT:
                dr = XUADocumentValidation.validateIHE_XUA_Authz_CONSENT(document);
                break;
            case IHE_XUA_PURPOSE_OF_USE:
                dr = XUADocumentValidation.validateIHE_XUA_PURPOSE_OF_USE(document);
                break;
            case KSA_XUA:
                dr = XUADocumentValidation.validateKSA_XUA(document);
                break;
            default:
                throw new SOAPException("Invalide Validator parameter : " + val);
        }
        String res = this.getDetailedResultAsString(dr);
        if (res.contains("?>")) {
            res = res.substring(res.indexOf("?>") + 2);
        }
        return res;
    }

    private static String readDoc(String name) {
        BufferedReader scanner = null;
        StringBuilder res = new StringBuilder();
        try {
            scanner = new BufferedReader(new FileReader(name));
            String line = scanner.readLine();
            while (line != null) {
                res.append(line + "\n");
                line = scanner.readLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                scanner.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return res.toString();
    }

    private String getDetailedResultAsString(DetailedResult dr) {
        if (dr != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            try {
                DetailedResultTransformer.save(baos, dr);
            } catch (JAXBException e) {
                e.printStackTrace();
            }
            String res = baos.toString();
            res = deleteUnicodeZero(res);
            return res;
        }
        return null;
    }

    private static String deleteUnicodeZero(String s) {
        return s.replaceAll("[\\u0000]", "");
    }

}
